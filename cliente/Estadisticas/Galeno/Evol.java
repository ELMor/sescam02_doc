// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   Evol.java

import java.applet.Applet;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.StringTokenizer;

public class Evol extends Applet
{

    int Puntos;
    float Valores[];
    String Fechas[];
    float Max;
    float Min;
    float lMax;
    float lMin;
    double step;
    int expstep;
    int divs;
    String unidad;

    public void init()
    {
        try
        {
            lMax = Float.valueOf(getParameter("MAX")).floatValue();
        }
        catch(Exception _ex)
        {
            lMax = (0.0F / 0.0F);
        }
        try
        {
            lMin = Float.valueOf(getParameter("MIN")).floatValue();
        }
        catch(Exception _ex)
        {
            lMin = (0.0F / 0.0F);
        }
        unidad = getParameter("UNIDAD");
        if(Float.isNaN(lMax))
            Max = 1.401298E-045F;
        else
            Max = lMax;
        if(Float.isNaN(lMin))
            Min = 3.402823E+038F;
        else
            Min = lMin;
        StringTokenizer stringtokenizer = new StringTokenizer(getParameter("VALORES"), "|");
        StringTokenizer stringtokenizer1 = new StringTokenizer(getParameter("FECHAS"), "|");
        Puntos = Math.max(stringtokenizer.countTokens(), stringtokenizer1.countTokens());
        Valores = new float[Puntos];
        Fechas = new String[Puntos];
        for(int i = 0; i < Puntos; i++)
        {
            try
            {
                Float float1 = Float.valueOf(stringtokenizer.nextToken());
                if(!float1.isNaN())
                {
                    Max = Math.max(float1.floatValue(), Max);
                    Min = Math.min(float1.floatValue(), Min);
                }
                Valores[i] = float1.floatValue();
            }
            catch(Exception _ex)
            {
                Valores[i] = (0.0F / 0.0F);
            }
            try
            {
                Fechas[i] = stringtokenizer1.nextToken();
            }
            catch(Exception _ex)
            {
                Fechas[i] = null;
            }
        }

        expstep = (int)Math.rint(Math.log((Max - Min) / 10F) / Math.log(10D));
        step = Math.pow(10D, expstep);
        divs = (int)((double)Max / step) - (int)((double)Min / step);
        if(divs > 10)
            step *= 2D;
        if(divs > 20)
            step *= 2.5D;
        if(divs > 50)
            step *= 2D;
        Max = (float)(Math.rint((double)Max / step + 1.0D) * step);
        Min = (float)(Math.rint((double)Min / step - 1.0D) * step);
        divs = (int)(0.5D + (double)(Max - Min) / step);
        setBackground(Color.white);
    }

    public final int EscalaY(float f, int i, int j)
    {
        return (int)((float)j - ((float)(j - i) * (f - Min)) / (Max - Min));
    }

    public final String HazNumero(double d)
    {
        String s = String.valueOf((float)d);
        if(expstep > 0)
        {
            int i = s.indexOf('.');
            if(i >= 0)
                s = s.substring(0, i);
        }
        if(s.length() > 6)
            s = "";
        return s;
    }

    public void paint(Graphics g)
    {
        FontMetrics fontmetrics = g.getFontMetrics();
        int k = fontmetrics.stringWidth("999999") + 5;
        int l = size().width - 5;
        int k2 = fontmetrics.getAscent();
        int l2 = size().height - fontmetrics.getHeight() - 5;
        int ai[] = new int[Puntos];
        int ai1[] = new int[Puntos];
        g.setColor(new Color(224, 224, 224));
        if(!Float.isNaN(lMax))
        {
            int i1 = EscalaY(lMax, k2, l2);
            g.fillRect(k, k2, l - k, i1 - k2);
        }
        if(!Float.isNaN(lMin))
        {
            int j1 = EscalaY(lMin, k2, l2);
            g.fillRect(k, j1, l - k, l2 - j1);
        }
        g.setColor(Color.lightGray);
        int k4 = 0;
        for(int i3 = 0; i3 < Puntos; i3++)
        {
            int i = k + ((2 * i3 + 1) * (l - k)) / Puntos / 2;
            g.drawLine(i, k2, i, l2);
            if(!Float.isNaN(Valores[i3]))
            {
                ai[k4] = i;
                ai1[k4] = EscalaY(Valores[i3], k2, l2);
                k4++;
            }
        }

        for(int j3 = 1; j3 < divs; j3++)
        {
            int k1 = EscalaY(Min + (float)j3 * (float)step, k2, l2);
            g.drawLine(k, k1, l, k1);
        }

        g.setColor(Color.pink);
        if(!Float.isNaN(lMax))
        {
            int l1 = EscalaY(lMax, k2, l2);
            g.drawLine(k, l1, l, l1);
        }
        if(!Float.isNaN(lMin))
        {
            int i2 = EscalaY(lMin, k2, l2);
            g.drawLine(k, i2, l, i2);
        }
        g.setColor(Color.black);
        g.drawRect(k, k2, l - k, l2 - k2);
        for(int k3 = 0; k3 < Puntos; k3++)
        {
            int j = k + ((2 * k3 + 1) * (l - k)) / Puntos / 2;
            String s = Fechas[k3];
            if(s != null)
                g.drawString(s, j - fontmetrics.stringWidth(s) / 2, l2 + 5 + fontmetrics.getAscent());
        }

        for(int l3 = 1; l3 < divs; l3++)
        {
            int j2 = EscalaY(Min + (float)l3 * (float)step, k2, l2);
            String s1 = HazNumero(Math.rint((double)Min / step + (double)l3) * step);
            g.drawString(s1, k - fontmetrics.stringWidth(s1) - 5, j2 + fontmetrics.getDescent());
        }

        if(unidad != null)
            g.drawString(unidad, k - fontmetrics.stringWidth(unidad) - 5, k2);
        g.setColor(Color.red);
        for(int i4 = 0; i4 < k4; i4++)
            g.fillOval(ai[i4] - 3, ai1[i4] - 3, 6, 6);

        g.setColor(Color.blue);
        for(int j4 = 1; j4 < k4; j4++)
            g.drawLine(ai[j4 - 1], ai1[j4 - 1], ai[j4], ai1[j4]);

    }

    public String getAppletInfo()
    {
        return "Applet Evol.  Horus Hardware S.A. - 1997";
    }

    public String[][] getParameterInfo()
    {
        String as[][] = {
            {
                "VALORES", "float", "Lista (|) de valores"
            }, {
                "MAX", "float", "Valor l\355mite superior"
            }, {
                "MIN", "float", "Valor l\355mite inferior"
            }, {
                "FECHAS", "string", "Lista (|) de leyendas"
            }, {
                "UNIDAD", "string", "Unidad de los valores"
            }
        };
        return as;
    }

    public Evol()
    {
    }
}
