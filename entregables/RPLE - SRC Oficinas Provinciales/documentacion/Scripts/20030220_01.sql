-- ============================================================
--   Database name:  sescam                                    
--   DBMS name:      ORACLE Version 8                          
--   Created on:     20/02/2003  16:32                         
-- ============================================================

create table ses_estadisticas_sync
(
    esync_periodo        VARCHAR2(30)           not null,
    est_tlista           VARCHAR2(4)            not null,
    est_terminado        INTEGER                null    ,
    constraint PK_SES_ESTADISTICAS_SYNC primary key (esync_periodo, est_tlista)
)
/

create table ses_estadisticas
(
    est_pk               INTEGER                not null,
    CODCENTRO            NUMBER                 null    ,
    codtramo             INTEGER                null    ,
    posicion             INTEGER                null    ,
    est_periodo          VARCHAR2(30)           null    ,
    est_tlista           VARCHAR2(4)            null    ,
    tproc_cen_cod        VARCHAR2(4)            null    ,
    proc_cen_cod         VARCHAR2(32)           null    ,
    serv_cen_cod         VARCHAR2(30)           null    ,
    esp_cen_cod          VARCHAR2(30)           null    ,
    diag_cen_cod         VARCHAR2(32)           null    ,
    est_cnt_pac          INTEGER                null    ,
    est_cnt_dias         INTEGER                null    ,
    constraint PK_SES_ESTADISTICAS primary key (est_pk)
)
/

create index Relation_4536_FK on ses_estadisticas (codtramo asc, posicion asc)
/

alter table ses_estadisticas
    add constraint FK_SES_ESTA_RELATION__SES_TRAM foreign key  (codtramo, posicion)
       references ses_tramos (codtramo, posicion)
/

commit
/

