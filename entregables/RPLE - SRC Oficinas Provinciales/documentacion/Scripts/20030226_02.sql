
drop sequence procgar_pk_seq
/
create sequence procgar_pk_seq start with 1 increment by 1
/

delete  ses_proc_garantia
/

-- Quirúrgicas
insert into ses_proc_garantia (procgar_pk, proc_pk, sesesp_cod, proc_garantia, proc_activo)
select procgar_pk_seq.nextval procgar_pk, p.proc_pk,null sesesp_cod,180 proc_garantia,1 proc_activo   
from ses_procedimientos p, ses_proc_tipos t
where  t.PROC_TIPO=p.PROC_TIPO and
t.PROC_TCOD='Q' and
p.PROC_ACTIVO=1 and
p.PROC_GARANTIA<>0
/

-- Consultas
insert into ses_proc_garantia (procgar_pk, proc_pk, sesesp_cod, proc_garantia, proc_activo)
select procgar_pk_seq.nextval procgar_pk, p.proc_pk,e.SESESP_COD sesesp_cod,60 proc_garantia,1 proc_activo   
from ses_procedimientos p, ses_especialidades e
where  p.PROC_COD='C00.9'

/
-- Primera visita
insert into ses_proc_garantia (procgar_pk, proc_pk, sesesp_cod, proc_garantia, proc_activo)
select procgar_pk_seq.nextval procgar_pk, p.proc_pk,null sesesp_cod,30 proc_garantia,1 proc_activo   
from ses_procedimientos p, ses_proc_tipos t
where  t.PROC_TIPO=p.PROC_TIPO and
t.PROC_TCOD='T' 
/

COMMIT;
