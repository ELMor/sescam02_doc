 -- Inicializa CLAVES DEL SISTEMA
delete from evento_var;
delete from evento_tipo_var;
delete from evento;
delete from evento_tipo;

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (1,'A�adir nuevo procedimiento',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (1,1,'Pk del nuevo procedimiento a�adido',1);
insert into evento_tipo_var (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (2,1,'C�digo del nuevo procedimiento a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (3,1,'Descripci�n corta del nuevo procedimiento a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (4,1,'Marca de activo/inactivo del nuevo procedimiento ',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (5,1,'Dias de garantia del nuevo procedimiento a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (6,1,'Monto del nuevo procedimiento a�adido',1);


insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (2,'Modificar procedimiento existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (7,2,'Pk del procedimiento modificado',1);
insert into evento_tipo_var (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (8,2,'Nuevo C�digo del procedimiento modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (9,2,'Desc. corta del procedimiento modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (10,2,'Marca de activo/inactivo del procedimiento modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (11,2,'Nuevos dias de garantia del procedimiento modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (12,2,'Nuevo Monto del procedimiento modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (3,'A�adir nuevo usuario',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (13,3,'Login del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (14,3,'Apellido Paterno del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (15,3,'Apellido Materno del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (16,3,'Nombre del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (17,3,'Oficina del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (18,3,'Cargo del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (19,3,'Perfil � Rol del nuevo usuario a�adido',1);


insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (4,'Modificar usuario existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (20,4,'Login del usuario modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (21,4,'Apellido Paterno del usuario modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (22,4,'Apellido Materno del usuario modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (23,4,'Marca de alta/baja para el usuario modificado',1);

insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (24,4,'Oficina del usuario modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (25,4,'Cargo del usuario modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (26,4,'Perfil � Rol del usuario modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (5,'A�adir Nuevo Rol',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (27,5,'Pk del nuevo rol a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (28,5,'Nombre del nuevo rol a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (29,5,'Descripci�n del nuevo rol a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (30,5,'Pk de permisos que se le asocian',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (6,'Modificar Rol existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (31,6,'Pk del rol modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (32,6,'Nombre del rol modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (33,6,'Descripci�n del rol modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (34,6,'Pk antiguos permisos que tenia asociados',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (35,6,'Pk nuevos permisos que se le asocian',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (7,'Borrar Rol existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (36,7,'Pk del rol que se va ha borrar',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (8,'A�adir nuevo Cargo',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (37,8,'Pk del nuevo cargo a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (38,8,'Nombre del nuevo cargo a�adido',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (9,'Modificar Cargo existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (39,9,'Pk del cargo modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (40,9,'Nombre del cargo modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (10,'Borrar Cargo existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (41,10,'Pk del cargo eliminado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (11,'A�adir nueva Oficina',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (42,11,'Pk de la nueva oficina a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (43,11,'Nombre de la nueva oficina a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (44,11,'Direcci�n de la nueva oficina a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (45,11,'Telefono de la nueva oficina a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (46,11,'Codigo Postal de la nueva oficina a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (47,11,'Localidad a la que pertenece la nueva oficina',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (48,11,'Usuario responsable de la nueva oficina',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (12,'Modificar una Oficina existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (49,12,'Pk de la oficina modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (50,12,'Nuevo nombre de la oficina modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (51,12,'Nueva direcci�n de la oficina modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (52,12,'Nuevo telefono de la oficina modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (53,12,'Nuevo Codigo Postal de la oficina modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (54,12,'Nueva Localidad a la que pertenece la oficina',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (55,12,'Nuevo Usuario responsable de la oficina',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (13,'Borrar Oficina existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (56,13,'Pk de la oficina eliminada',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (14,'Impresion de Documentos',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (57,14,'Documento a Imprimir',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (58,14,'Usuario responsable de la impresi�n',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (59,14,'Centro � Cega del registro de Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (60,14,'NOrden � NCita del registro de Lista de Espera',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (15,'Emision de Solicitudes de Garantia',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (61,15,'Documento asociado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (62,15,'Usuario responsable de la operacion',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (63,15,'Centro � Cega del registro de Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (64,15,'NOrden � NCita del registro de Lista de Espera',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (16,'Aprobaci�n de una solicitud',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (65,16,'Documento asociado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (66,16,'Usuario responsable de la operacion',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (67,16,'Centro � Cega del registro de Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (68,16,'NOrden � NCita del registro de Lista de Espera',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (17,'Denegacion de una solicitud',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (69,17,'Documento asociado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (70,17,'Usuario responsable de la operacion',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (71,17,'Centro � Cega del registro de Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (72,17,'NOrden � NCita del registro de Lista de Espera',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (18,'Renuncia a una Solicitud',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (73,18,'Documento asociado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (74,18,'Usuario responsable de la operacion',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (75,18,'Centro � Cega del registro de Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (76,18,'NOrden � NCita del registro de Lista de Espera',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (19,'Evento de Inicio de Sesi�n',1);



insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (20,'Consulta de L.E. Quir�rgica �  L.E. de CEX',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (77,20,'Primer Apellido del Paciente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (78,20,'Segundo Apellido del Paciente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (79,20,'Nombre del Paciente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (80,20,'Tarjeta Sanitaria � CIP',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (81,20,'Historia Cl�nica',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (82,20,'Centro � Cega',1);


insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (21,'Acceso a Ficha de Paciente en Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (83,21,'Centro � Cega al que pertenece el paciente en L.E.',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (84,21,'NOrden � Ncita del paciente en Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (85,21,'"C" si la entrada en L.E. de CEX � "Q" si es Quir�rgica',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (22,'Acceso a Ficha de Solicitud de Garantia',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (86,22,'Gar_pk de la Solicitud consultada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (87,22,'Estado de Solicitud(Solicitada,Aprobada,Denegada,Renunciada)',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (23,'A�adir nuevo Servicio',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (88,23,'Codigo del nuevo servicio a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (89,23,'Nombre del nuevo servicio a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (90,23,'Especialidad del nuevo servicio a�adido',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (24,'Modificar Servicio existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (91,24,'Codigo del Servicio modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (92,24,'Nombre del Servicio modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (93,24,'Especialidad del Servicio modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (25,'Borrar Servicio existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (94,25,'Codigo del Servicio eliminado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (26,'A�adir nueva Especialidad',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (95,26,'Codigo de la nueva Especialidad a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (96,26,'Nombre de la nueva Especialidad a�adida',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (27,'Modificar Especialidad existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (97,27,'Codigo de la Especialidad modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (98,27,'Nombre de la Especialidad modificada',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (28,'Borrar Especialidad existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (99,28,'Codigo de la Especialidad eliminada',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (29,'A�adir nuevo Mapeo de Servicios',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (100,29,'Codigo del Servicio Sescam a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (101,29,'Codigo del Servicio Mapeado a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (102,29,'Centro',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (30,'Modificar Mapeo de Servicios',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (103,30,'Codigo del Servicio Sescam modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (104,30,'Codigo del Servicio Mapeado modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (105,30,'Centro',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (31,'Eliminar Mapeo de Servicios',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (107,31,'Codigo del Servicio Mapeado eliminado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (108,31,'Codigo del Centro del Servicio Mapeado eliminado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (32,'A�adir nuevo Mapeo de Diagnosticos',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (109,32,'Pk del Diagnostico Sescam a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (110,32,'Codigo del Diagnostico Mapeado a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (111,32,'Codigo del Centro del Diagnostico a�adido',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (33,'Modificar Mapeo de Diagnosticos',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (112,33,'Codigo del Diagnostico Sescam modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (113,33,'Codigo del Diagnostico Mapeado modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (114,33,'Codigo del Centro del Diagnostico Mapeado modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (34,'Eliminar Mapeo de Diagnosticos',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (115,34,'Codigo del Diagnostico Mapeado eliminado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (116,34,'Codigo del Centro del Diagnostico Mapeado eliminado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (35,'A�adir nueva Clasificaci�n ',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (117,35,'Pk de la nueva clasificaci�n de procedimientos ',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (118,35,'Padre de la nueva clasificaci�n si lo tuviese',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (119,35,'Pk del procedimiento en el caso de que fuera un nodo hoja',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (120,35,'Descripci�n de la nueva clasificicaci�n si no es nodo hoja',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (121,35,'Numero de niveles de dicha clasificaci�n si no es nodo hoja',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (122,35,'Tipo (1-Clasif.1�Nivel,2-Clasif.intermedia,3-Procedimiento)',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (36,'Modificar Clasificaci�n existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (123,36,'Pk de la clasificaci�n a modificar ',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (124,36,'Padre de la clasificaci�n modificada,si lo tuviese',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (125,36,'Pk del procedimiento en el caso de que fuera un nodo hoja ',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (126,36,'Descripci�n de la clasificicaci�n si no es nodo hoja',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (127,36,'Numero de niveles de dicha clasificaci�n si no es nodo hoja',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (128,36,'Tipo (1-Clasif.1�Nivel,2-Clasif.intermedia,3-Procedimiento)',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (37,'Eliminar Clasificaci�n de Procedimientos',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (129,37,'Pk de la clasificaci�n a eliminar ',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (38,'A�adir nuevo Mapeo de Procedimientos',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (130,38,'Pk del Procedimiento Sescam a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (131,38,'Codigo del Procedimiento Mapeado a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (132,38,'Codigo del Centro del Procedimiento a�adido',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (39,'Modificar Mapeo de Procedimientos',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (133,39,'Codigo del Procedimiento Sescam modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (134,39,'Codigo del Procedimiento Mapeado modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (135,39,'Codigo del Centro del Procedimiento Mapeado modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (40,'Eliminar Mapeo de Procedimientos',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (136,40,'Codigo del Procedimiento Mapeado eliminado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (137,40,'Codigo del Centro del Procedimiento Mapeado eliminado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (41,'A�adir nuevo diagnostico',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (138,41,'Pk del nuevo diagnostico',1);
insert into evento_tipo_var (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (139,41,'C�digo del nuevo diagnostico a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (140,41,'Descripci�n corta del nuevo diagnostico a�adido',1);


insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (42,'Modificar diagnostico existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (141,42,'Pk del diagnostico modificado',1);
insert into evento_tipo_var (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (142,42,'Nuevo C�digo del diagnostico modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (143,42,'Desc. corta del diagnostico modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (43,'Eliminar diagnostico existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (144,43,'Pk del diagnostico eliminado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (44,'A�adir nuevo tramo',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (145,44,'Codtramo del nuevo tramo a�adido',1);
insert into evento_tipo_var (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (146,44,'posicion del nuevo tramo a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (147,44,'Inicio del nuevo tramo a�adido',1);
insert into evento_tipo_var (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (148,44,'Fin del nuevo tramo a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (149,44,'Descripci�n del nuevo tramo a�adido',1);


insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (45,'Modificar tramo existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (150,45,'Codtramo del tramo modificado',1);
insert into evento_tipo_var (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (151,45,'Posicion del tramo modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (152,45,'Inicio del tramo modificado',1);
insert into evento_tipo_var (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (153,45,'Fin del tramo modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (154,45,'Descripci�n del tramo modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (46,'Eliminar tramo existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (155,46,'Codtramo del tramo eliminado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (156,46,'Posicion del tramo eliminado',1);

commit work; 
