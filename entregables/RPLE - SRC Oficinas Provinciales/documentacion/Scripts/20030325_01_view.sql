CREATE OR REPLACE FORCE VIEW LEGATE.MAPEO_PROCS
(COD_SESCAM, DESCRIP, COD_MAPEO, DESCRIPMAPEO, CODTIPO, 
 TIPO, CODTIPOMAPEO, CENTRO) AS
SELECT distinct sp.PROC_COD cod_sescam, sp.PROC_DESC_CORTA descrip, smp.PROC_CEN_COD cod_mapeo,prest.DESPREST  descripmapeo,sptipo.PROC_TCOD codtipo, sp.proc_tipo tipo,smp.PROC_CEN_TIPO,smp.CENTRO centro 
FROM ses_proc_map smp,ses_procedimientos sp, ses_proc_tipos sptipo, prestacion prest 
WHERE smp.PROC_PK = sp.PROC_PK and sp.PROC_TIPO = sptipo.PROC_TIPO and
      trim(prest.CODPREST)=trim(smp.PROC_CEN_COD) and prest.CENTRO=smp.CENTRO
UNION ALL
SELECT distinct sp.PROC_COD cod_sescam, sp.PROC_DESC_CORTA descrip, smp.PROC_CEN_COD cod_mapeo,l.DPR1  descripmapeo,sptipo.PROC_TCOD codtipo, sp.proc_tipo tipo,smp.PROC_CEN_TIPO,smp.CENTRO centro 
FROM ses_proc_map smp,ses_procedimientos sp, ses_proc_tipos sptipo, lesp l 
WHERE smp.PROC_PK = sp.PROC_PK and sp.PROC_TIPO = sptipo.PROC_TIPO and
      trim(l.CPR1)=trim(smp.PROC_CEN_COD);      