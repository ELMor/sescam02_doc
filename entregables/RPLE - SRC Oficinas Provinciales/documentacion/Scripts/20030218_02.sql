-- ============================================================
--   Database name:  sescam                                    
--   DBMS name:      ORACLE Version 8                          
--   Created on:     18/02/2003  11:58                         
-- ============================================================

drop index ses_proc_cod
/

alter table ses_proc_map
    drop primary key
/

alter table ses_proc_map
    add constraint PK_SES_PROC_MAP primary key (centro, proc_cen_cod, proc_cen_tipo)
/

create unique index ses_proc_cod on ses_procedimientos (proc_tipo asc, proc_cod asc)
/

commit
/

