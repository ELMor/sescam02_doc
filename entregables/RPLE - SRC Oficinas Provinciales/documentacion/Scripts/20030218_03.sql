-- ============================================================
--   Database name:  sescam                                    
--   DBMS name:      ORACLE Version 8                          
--   Created on:     18/02/2003  13:00                         
-- ============================================================

drop index Relation_4264_FK
/

alter table ses_proc_map
    drop constraint FK_SES_PROC_RELATION__SES_PROC
/

alter table ses_procedimientos
    drop constraint FK_SES_P__SES_P
/

drop table tmp_ses_proc_tipos cascade constraints
/

create table tmp_ses_proc_tipos
(
    proc_tipo            INTEGER                not null,
    proc_tcod            VARCHAR2(10)           null    ,
    proc_desc            CHAR(10)               null    
)
/

insert into tmp_ses_proc_tipos (proc_tipo, proc_tcod, proc_desc)
select proc_tipo, proc_tcod, proc_desc
from ses_proc_tipos
/

drop table ses_proc_tipos cascade constraints
/

create table ses_proc_tipos
(
    proc_tipo            INTEGER                not null,
    proc_tcod            VARCHAR2(10)           null    ,
    proc_tdesc           VARCHAR2(30)           null    ,
    constraint PK_SES_PROC_TIPOS primary key (proc_tipo)
)
/

insert into ses_proc_tipos (proc_tipo, proc_tcod, proc_tdesc)
select proc_tipo, proc_tcod, proc_desc
from tmp_ses_proc_tipos
/

drop table tmp_ses_proc_tipos cascade constraints
/

drop table tmp_ses_proc_map cascade constraints
/

create table tmp_ses_proc_map
(
    proc_pk              INTEGER                not null,
    centro               NUMBER                 not null,
    proc_cen_cod         VARCHAR2(32)           not null,
    proc_tipo            INTEGER                null    ,
    proc_cen_tipo        VARCHAR2(4)            null    
)
/

insert into tmp_ses_proc_map (proc_pk, centro, proc_cen_cod, proc_tipo, proc_cen_tipo)
select proc_pk, centro, proc_cen_cod, proc_tipo, proc_cen_tipo
from ses_proc_map
/

drop table ses_proc_map cascade constraints
/

create table ses_proc_map
(
    proc_pk              INTEGER                not null,
    centro               NUMBER                 not null,
    proc_cen_cod         VARCHAR2(32)           not null,
    proc_cen_tipo        VARCHAR2(4)            null    ,
    constraint PK_SES_PROC_MAP primary key (centro, proc_cen_cod, proc_cen_tipo)
)
/

insert into ses_proc_map (proc_pk, centro, proc_cen_cod, proc_cen_tipo)
select proc_pk, centro, proc_cen_cod, proc_cen_tipo
from tmp_ses_proc_map
/

drop table tmp_ses_proc_map cascade constraints
/

create index Relation_2980_FK on ses_proc_map (proc_pk asc)
/

alter table ses_proc_map
    add constraint FK_SES_PROC_SES_PROC foreign key  (proc_pk)
       references ses_procedimientos (proc_pk)
/

alter table ses_procedimientos
    add constraint FK_SES_P__SES_P foreign key  (proc_tipo)
       references ses_proc_tipos (proc_tipo)
/

commit
/

