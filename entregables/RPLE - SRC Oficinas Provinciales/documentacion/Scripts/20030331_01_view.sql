CREATE OR REPLACE FORCE VIEW LEGATE.MAPEO_SERVS
(COD_SESCAM, DESCRIP, COD_MAPEO, DESCRIPMAPEO, CENTRO)
AS 
SELECT distinct ssm.SESSRV_COD cod_sescam,ss.SESSRV_DESC descrip , ssm.SRV_CEN_COD cod_mapeo, s.NOMSERV descripmapeo, ssm.CENTRO 
FROM ses_srv_map ssm,ses_servicios ss, servicios s
where trim(ssm.SESSRV_COD)=trim(ss.SESSRV_COD) and trim(ssm.SRV_CEN_COD)=trim(s.CODSERV);
