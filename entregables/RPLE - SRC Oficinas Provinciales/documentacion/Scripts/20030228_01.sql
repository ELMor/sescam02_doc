/* Agrega las filas a ses_proc_garantia */

-- Tomar el m�ximo PK de ses_proc_garantia para reemplazarlo en la 
select max(sg.PROCGAR_PK)  from ses_proc_garantia sg
/
-- No importa si el Drop falla.
drop sequence tmp_seq_sg
/
commit
/
-- Reemplazar el maximo tomado en el paso 1, por 999999 en la siguiente sentencia
create sequence tmp_seq_sg start with 999999 increment by 1
/
insert into ses_proc_garantia (PROC_GARANTIA,PROC_PK,PROCGAR_PK) 
SELECT 0 garantia,p.proc_pk proc_pk, tmp_seq_sg.nextval 
FROM ses_procedimientos p
where p.proc_pk not in (select proc_pk from ses_proc_garantia sg where sg.sesesp_cod is null)
/

update clave  set clave.CLAVE_NUMERO_ACTUAL= (select max(sg.PROCGAR_PK)+1  from ses_proc_garantia sg)   
where clave_nombre='isf.persistencia.SesProcGarantia'
/
commit
/



