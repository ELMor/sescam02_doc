package isf.persistence;

public class GcgcCorruptedRegisterException extends Exception
{
    public GcgcCorruptedRegisterException()
    {
        super();
    }

    public GcgcCorruptedRegisterException(String s)
    {
        super(s);
    }
}
