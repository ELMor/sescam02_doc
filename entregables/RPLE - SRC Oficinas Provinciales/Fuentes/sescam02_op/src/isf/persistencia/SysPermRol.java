//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SysPermRol
// Table: SYS_PERM_ROL
// Package: ssf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SYS_PERM_ROL
 * Columns for the table:
 * 	(sys_rol_pk - LONG), 
 * 	(sys_perm_pk - LONG), 
 * Primary columns for the table:
 * 	(sys_rol_pk - LONG) 
 * 	(sys_perm_pk - LONG) 
 */
public class SysPermRol
{
	// Properties
	private long sysRolPk;
	private boolean sysRolPkNull=true;
	private boolean sysRolPkModified=false;
	private long sysPermPk;
	private boolean sysPermPkNull=true;
	private boolean sysPermPkModified=false;
	// Access Method
	/** SysRolPk
	 * Get the value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public long getSysRolPk() {
		return sysRolPk;
	}
	/** SysRolPk
	 * Set the value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public void setSysRolPk(long _sysRolPk) {
		this.sysRolPk=_sysRolPk;
		this.sysRolPkModified=true;
		this.sysRolPkNull=false;
	}
	/** SysRolPk
	 * Set Null the value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public void setNullSysRolPk() {
		this.sysRolPk=0;
		this.sysRolPkModified=true;
		this.sysRolPkNull=true;
	}
	/** SysRolPk
	 * Sumatory of value of the property SysRolPk.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	static public double sumSysRolPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SYS_ROL_PK");
	}
	/** SysRolPk
	 * Indicates if the value of the property SysRolPk is null or not.
	 * The column for the database is 'SYS_ROL_PK'SysRolPk.
	 */
	public boolean isNullSysRolPk() {
		return sysRolPkNull;
	}
	/** SysPermPk
	 * Get the value of the property SysPermPk.
	 * The column for the database is 'SYS_PERM_PK'SysPermPk.
	 */
	public long getSysPermPk() {
		return sysPermPk;
	}
	/** SysPermPk
	 * Set the value of the property SysPermPk.
	 * The column for the database is 'SYS_PERM_PK'SysPermPk.
	 */
	public void setSysPermPk(long _sysPermPk) {
		this.sysPermPk=_sysPermPk;
		this.sysPermPkModified=true;
		this.sysPermPkNull=false;
	}
	/** SysPermPk
	 * Set Null the value of the property SysPermPk.
	 * The column for the database is 'SYS_PERM_PK'SysPermPk.
	 */
	public void setNullSysPermPk() {
		this.sysPermPk=0;
		this.sysPermPkModified=true;
		this.sysPermPkNull=true;
	}
	/** SysPermPk
	 * Sumatory of value of the property SysPermPk.
	 * The column for the database is 'SYS_PERM_PK'SysPermPk.
	 */
	static public double sumSysPermPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SYS_PERM_PK");
	}
	/** SysPermPk
	 * Indicates if the value of the property SysPermPk is null or not.
	 * The column for the database is 'SYS_PERM_PK'SysPermPk.
	 */
	public boolean isNullSysPermPk() {
		return sysPermPkNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!sysRolPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYS_ROL_PK");
			ls_values.append((lb_FirstTime?"":",")+sysRolPk);
			lb_FirstTime = false;
		}
		if (!sysPermPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYS_PERM_PK");
			ls_values.append((lb_FirstTime?"":",")+sysPermPk);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SYS_PERM_ROL ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (sysRolPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYS_ROL_PK");
			if(sysRolPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sysRolPk);
			lb_FirstTime = false;
		}
		if (sysPermPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYS_PERM_PK");
			if(sysPermPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sysPermPk);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("SYS_ROL_PK");
			ls_where.append("="+sysRolPk);
		} else {
			ls_where.append(" AND "+"SYS_ROL_PK");
			ls_where.append("="+sysRolPk);
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) { 
			ls_where.append("SYS_PERM_PK");
			ls_where.append("="+sysPermPk);
		} else {
			ls_where.append(" AND "+"SYS_PERM_PK");
			ls_where.append("="+sysPermPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SYS_PERM_ROL SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("SYS_ROL_PK");
			ls_where.append("="+sysRolPk);
		} else {
			ls_where.append(" AND "+"SYS_ROL_PK");
			ls_where.append("="+sysRolPk);
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) { 
			ls_where.append("SYS_PERM_PK");
			ls_where.append("="+sysPermPk);
		} else {
			ls_where.append(" AND "+"SYS_PERM_PK");
			ls_where.append("="+sysPermPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SYS_PERM_ROL WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _sysRolPk,long _sysPermPk
	 * @return SysPermRol - Retrieved object
	 */
	static public SysPermRol read(Connection _connection,long _sysRolPk,long _sysPermPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SysPermRol ls_SysPermRol=new SysPermRol();
		ls_where="SYS_ROL_PK = " + _sysRolPk + " AND " + "SYS_PERM_PK = " + _sysPermPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SYS_PERM_ROL WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SysPermRol.loadResultSet(ls_rs);
		} else {
			ls_SysPermRol=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SysPermRol;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		sysRolPk=_rs.getLong("SYS_ROL_PK");
		sysRolPkNull=_rs.wasNull();
		sysPermPk=_rs.getLong("SYS_PERM_PK");
		sysPermPkNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SYS_PERM_ROL ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SysPermRol mySysPermRol;
		Vector mySysPermRoles = new Vector();
		while (ls_rs.next()) {
			mySysPermRol = new SysPermRol();
			mySysPermRol.loadResultSet (ls_rs);
			mySysPermRoles.addElement(mySysPermRol);
		}
		ls_rs.close();
		myStatement.close();
		return mySysPermRoles;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SYS_PERM_ROL ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SYS_PERM_ROL ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SYS_PERM_ROL ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setSysRolPk(myPool.getSequence(this.getClass().getName(),profile));
		setSysPermPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
