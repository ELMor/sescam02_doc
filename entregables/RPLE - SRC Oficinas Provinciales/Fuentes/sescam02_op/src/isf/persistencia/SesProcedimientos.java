//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesProcedimientos
// Table: SES_PROCEDIMIENTOS
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_PROCEDIMIENTOS
 * Columns for the table:
 * 	(proc_pk - LONG), 
 * 	(proc_cod - STRING), 
 * 	(proc_desc_corta - STRING), 
 * 	(proc_desc - STRING), 
 * 	(proc_activo - LONG), 
 * 	(proc_garantia - LONG), 
 * 	(proc_monto_max_est - DECIMAL), 
 * 	(proc_tipo - LONG), 
 * Primary columns for the table:
 * 	(proc_pk - LONG) 
 */
public class SesProcedimientos
{
	// Properties
	private long procPk;
	private boolean procPkNull=true;
	private boolean procPkModified=false;
	private String procCod;
	private boolean procCodNull=true;
	private boolean procCodModified=false;
	private String procDescCorta;
	private boolean procDescCortaNull=true;
	private boolean procDescCortaModified=false;
	private String procDesc;
	private boolean procDescNull=true;
	private boolean procDescModified=false;
	private long procActivo;
	private boolean procActivoNull=true;
	private boolean procActivoModified=false;
	private long procGarantia;
	private boolean procGarantiaNull=true;
	private boolean procGarantiaModified=false;
	private double procMontoMaxEst;
	private boolean procMontoMaxEstNull=true;
	private boolean procMontoMaxEstModified=false;
	private long procTipo;
	private boolean procTipoNull=true;
	private boolean procTipoModified=false;
	// Access Method
	/** ProcPk
	 * Get the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public long getProcPk() {
		return procPk;
	}
	/** ProcPk
	 * Set the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public void setProcPk(long _procPk) {
		this.procPk=_procPk;
		this.procPkModified=true;
		this.procPkNull=false;
	}
	/** ProcPk
	 * Set Null the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public void setNullProcPk() {
		this.procPk=0;
		this.procPkModified=true;
		this.procPkNull=true;
	}
	/** ProcPk
	 * Sumatory of value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	static public double sumProcPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_PK");
	}
	/** ProcPk
	 * Indicates if the value of the property ProcPk is null or not.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public boolean isNullProcPk() {
		return procPkNull;
	}
	/** ProcCod
	 * Get the value of the property ProcCod.
	 * The column for the database is 'PROC_COD'ProcCod.
	 */
	public String getProcCod() {
		if (procCod==null) return "";
		if (procCod.compareTo("null")==0) return "";
		return procCod;
	}
	/** ProcCod
	 * Set the value of the property ProcCod.
	 * The column for the database is 'PROC_COD'ProcCod.
	 */
	public void setProcCod(String _procCod) {
		this.procCod=_procCod;
		this.procCodModified=true;
		this.procCodNull=false;
	}
	/** ProcCod
	 * Set Null the value of the property ProcCod.
	 * The column for the database is 'PROC_COD'ProcCod.
	 */
	public void setNullProcCod() {
		this.procCod=null;
		this.procCodModified=true;
		this.procCodNull=true;
	}
	/** ProcCod
	 * Indicates if the value of the property ProcCod is null or not.
	 * The column for the database is 'PROC_COD'ProcCod.
	 */
	public boolean isNullProcCod() {
		return procCodNull;
	}
	/** ProcDescCorta
	 * Get the value of the property ProcDescCorta.
	 * The column for the database is 'PROC_DESC_CORTA'ProcDescCorta.
	 */
	public String getProcDescCorta() {
		if (procDescCorta==null) return "";
		if (procDescCorta.compareTo("null")==0) return "";
		return procDescCorta;
	}
	/** ProcDescCorta
	 * Set the value of the property ProcDescCorta.
	 * The column for the database is 'PROC_DESC_CORTA'ProcDescCorta.
	 */
	public void setProcDescCorta(String _procDescCorta) {
		this.procDescCorta=_procDescCorta;
		this.procDescCortaModified=true;
		this.procDescCortaNull=false;
	}
	/** ProcDescCorta
	 * Set Null the value of the property ProcDescCorta.
	 * The column for the database is 'PROC_DESC_CORTA'ProcDescCorta.
	 */
	public void setNullProcDescCorta() {
		this.procDescCorta=null;
		this.procDescCortaModified=true;
		this.procDescCortaNull=true;
	}
	/** ProcDescCorta
	 * Indicates if the value of the property ProcDescCorta is null or not.
	 * The column for the database is 'PROC_DESC_CORTA'ProcDescCorta.
	 */
	public boolean isNullProcDescCorta() {
		return procDescCortaNull;
	}
	/** ProcDesc
	 * Get the value of the property ProcDesc.
	 * The column for the database is 'PROC_DESC'ProcDesc.
	 */
	public String getProcDesc() {
		if (procDesc==null) return "";
		if (procDesc.compareTo("null")==0) return "";
		return procDesc;
	}
	/** ProcDesc
	 * Set the value of the property ProcDesc.
	 * The column for the database is 'PROC_DESC'ProcDesc.
	 */
	public void setProcDesc(String _procDesc) {
		this.procDesc=_procDesc;
		this.procDescModified=true;
		this.procDescNull=false;
	}
	/** ProcDesc
	 * Set Null the value of the property ProcDesc.
	 * The column for the database is 'PROC_DESC'ProcDesc.
	 */
	public void setNullProcDesc() {
		this.procDesc=null;
		this.procDescModified=true;
		this.procDescNull=true;
	}
	/** ProcDesc
	 * Indicates if the value of the property ProcDesc is null or not.
	 * The column for the database is 'PROC_DESC'ProcDesc.
	 */
	public boolean isNullProcDesc() {
		return procDescNull;
	}
	/** ProcActivo
	 * Get the value of the property ProcActivo.
	 * The column for the database is 'PROC_ACTIVO'ProcActivo.
	 */
	public long getProcActivo() {
		return procActivo;
	}
	/** ProcActivo
	 * Set the value of the property ProcActivo.
	 * The column for the database is 'PROC_ACTIVO'ProcActivo.
	 */
	public void setProcActivo(long _procActivo) {
		this.procActivo=_procActivo;
		this.procActivoModified=true;
		this.procActivoNull=false;
	}
	/** ProcActivo
	 * Set Null the value of the property ProcActivo.
	 * The column for the database is 'PROC_ACTIVO'ProcActivo.
	 */
	public void setNullProcActivo() {
		this.procActivo=0;
		this.procActivoModified=true;
		this.procActivoNull=true;
	}
	/** ProcActivo
	 * Sumatory of value of the property ProcActivo.
	 * The column for the database is 'PROC_ACTIVO'ProcActivo.
	 */
	static public double sumProcActivo(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_ACTIVO");
	}
	/** ProcActivo
	 * Indicates if the value of the property ProcActivo is null or not.
	 * The column for the database is 'PROC_ACTIVO'ProcActivo.
	 */
	public boolean isNullProcActivo() {
		return procActivoNull;
	}
	/** ProcGarantia
	 * Get the value of the property ProcGarantia.
	 * The column for the database is 'PROC_GARANTIA'ProcGarantia.
	 */
	public long getProcGarantia() {
		return procGarantia;
	}
	/** ProcGarantia
	 * Set the value of the property ProcGarantia.
	 * The column for the database is 'PROC_GARANTIA'ProcGarantia.
	 */
	public void setProcGarantia(long _procGarantia) {
		this.procGarantia=_procGarantia;
		this.procGarantiaModified=true;
		this.procGarantiaNull=false;
	}
	/** ProcGarantia
	 * Set Null the value of the property ProcGarantia.
	 * The column for the database is 'PROC_GARANTIA'ProcGarantia.
	 */
	public void setNullProcGarantia() {
		this.procGarantia=0;
		this.procGarantiaModified=true;
		this.procGarantiaNull=true;
	}
	/** ProcGarantia
	 * Sumatory of value of the property ProcGarantia.
	 * The column for the database is 'PROC_GARANTIA'ProcGarantia.
	 */
	static public double sumProcGarantia(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_GARANTIA");
	}
	/** ProcGarantia
	 * Indicates if the value of the property ProcGarantia is null or not.
	 * The column for the database is 'PROC_GARANTIA'ProcGarantia.
	 */
	public boolean isNullProcGarantia() {
		return procGarantiaNull;
	}
	/** ProcMontoMaxEst
	 * Get the value of the property ProcMontoMaxEst.
	 * The column for the database is 'PROC_MONTO_MAX_EST'ProcMontoMaxEst.
	 */
	public double getProcMontoMaxEst() {
		return procMontoMaxEst;
	}
	/** ProcMontoMaxEst
	 * Set the value of the property ProcMontoMaxEst.
	 * The column for the database is 'PROC_MONTO_MAX_EST'ProcMontoMaxEst.
	 */
	public void setProcMontoMaxEst(double _procMontoMaxEst) {
		this.procMontoMaxEst=_procMontoMaxEst;
		this.procMontoMaxEstModified=true;
		this.procMontoMaxEstNull=false;
	}
	/** ProcMontoMaxEst
	 * Set Null the value of the property ProcMontoMaxEst.
	 * The column for the database is 'PROC_MONTO_MAX_EST'ProcMontoMaxEst.
	 */
	public void setNullProcMontoMaxEst() {
		this.procMontoMaxEst=0;
		this.procMontoMaxEstModified=true;
		this.procMontoMaxEstNull=true;
	}
	/** ProcMontoMaxEst
	 * Sumatory of value of the property ProcMontoMaxEst.
	 * The column for the database is 'PROC_MONTO_MAX_EST'ProcMontoMaxEst.
	 */
	static public double sumProcMontoMaxEst(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_MONTO_MAX_EST");
	}
	/** ProcMontoMaxEst
	 * Indicates if the value of the property ProcMontoMaxEst is null or not.
	 * The column for the database is 'PROC_MONTO_MAX_EST'ProcMontoMaxEst.
	 */
	public boolean isNullProcMontoMaxEst() {
		return procMontoMaxEstNull;
	}
	/** ProcTipo
	 * Get the value of the property ProcTipo.
	 * The column for the database is 'PROC_TIPO'ProcTipo.
	 */
	public long getProcTipo() {
		return procTipo;
	}
	/** ProcTipo
	 * Set the value of the property ProcTipo.
	 * The column for the database is 'PROC_TIPO'ProcTipo.
	 */
	public void setProcTipo(long _procTipo) {
		this.procTipo=_procTipo;
		this.procTipoModified=true;
		this.procTipoNull=false;
	}
	/** ProcTipo
	 * Set Null the value of the property ProcTipo.
	 * The column for the database is 'PROC_TIPO'ProcTipo.
	 */
	public void setNullProcTipo() {
		this.procTipo=0;
		this.procTipoModified=true;
		this.procTipoNull=true;
	}
	/** ProcTipo
	 * Sumatory of value of the property ProcTipo.
	 * The column for the database is 'PROC_TIPO'ProcTipo.
	 */
	static public double sumProcTipo(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_TIPO");
	}
	/** ProcTipo
	 * Indicates if the value of the property ProcTipo is null or not.
	 * The column for the database is 'PROC_TIPO'ProcTipo.
	 */
	public boolean isNullProcTipo() {
		return procTipoNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!procPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_PK");
			ls_values.append((lb_FirstTime?"":",")+procPk);
			lb_FirstTime = false;
		}
		if (!procCodNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_COD");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(procCod)+"'");
			lb_FirstTime = false;
		}
		if (!procDescCortaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_DESC_CORTA");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(procDescCorta)+"'");
			lb_FirstTime = false;
		}
		if (!procDescNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_DESC");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(procDesc)+"'");
			lb_FirstTime = false;
		}
		if (!procActivoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_ACTIVO");
			ls_values.append((lb_FirstTime?"":",")+procActivo);
			lb_FirstTime = false;
		}
		if (!procGarantiaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_GARANTIA");
			ls_values.append((lb_FirstTime?"":",")+procGarantia);
			lb_FirstTime = false;
		}
		if (!procMontoMaxEstNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_MONTO_MAX_EST");
			ls_values.append((lb_FirstTime?"":",")+procMontoMaxEst);
			lb_FirstTime = false;
		}
		if (!procTipoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_TIPO");
			ls_values.append((lb_FirstTime?"":",")+procTipo);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_PROCEDIMIENTOS ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (procPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_PK");
			if(procPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+procPk);
			lb_FirstTime = false;
		}
		if (procCodModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_COD");
			if(procCodNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(procCod)+"'");
			lb_FirstTime = false;
		}
		if (procDescCortaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_DESC_CORTA");
			if(procDescCortaNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(procDescCorta)+"'");
			lb_FirstTime = false;
		}
		if (procDescModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_DESC");
			if(procDescNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(procDesc)+"'");
			lb_FirstTime = false;
		}
		if (procActivoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_ACTIVO");
			if(procActivoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+procActivo);
			lb_FirstTime = false;
		}
		if (procGarantiaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_GARANTIA");
			if(procGarantiaNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+procGarantia);
			lb_FirstTime = false;
		}
		if (procMontoMaxEstModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_MONTO_MAX_EST");
			if(procMontoMaxEstNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+procMontoMaxEst);
			lb_FirstTime = false;
		}
		if (procTipoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_TIPO");
			if(procTipoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+procTipo);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("PROC_PK");
			ls_where.append("="+procPk);
		} else {
			ls_where.append(" AND "+"PROC_PK");
			ls_where.append("="+procPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_PROCEDIMIENTOS SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("PROC_PK");
			ls_where.append("="+procPk);
		} else {
			ls_where.append(" AND "+"PROC_PK");
			ls_where.append("="+procPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_PROCEDIMIENTOS WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _procPk
	 * @return SesProcedimientos - Retrieved object
	 */
	static public SesProcedimientos read(Connection _connection,long _procPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesProcedimientos ls_SesProcedimientos=new SesProcedimientos();
		ls_where="PROC_PK = " + _procPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_PROCEDIMIENTOS WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesProcedimientos.loadResultSet(ls_rs);
		} else {
			ls_SesProcedimientos=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesProcedimientos;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		procPk=_rs.getLong("PROC_PK");
		procPkNull=_rs.wasNull();
		procCod=_rs.getString("PROC_COD");
		procCodNull=_rs.wasNull();
		procDescCorta=_rs.getString("PROC_DESC_CORTA");
		procDescCortaNull=_rs.wasNull();
		procDesc=_rs.getString("PROC_DESC");
		procDescNull=_rs.wasNull();
		procActivo=_rs.getLong("PROC_ACTIVO");
		procActivoNull=_rs.wasNull();
		procGarantia=_rs.getLong("PROC_GARANTIA");
		procGarantiaNull=_rs.wasNull();
		procMontoMaxEst=_rs.getDouble("PROC_MONTO_MAX_EST");
		procMontoMaxEstNull=_rs.wasNull();
		procTipo=_rs.getLong("PROC_TIPO");
		procTipoNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_PROCEDIMIENTOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesProcedimientos mySesProcedimientos;
		Vector mySesProcedimientoses = new Vector();
		while (ls_rs.next()) {
			mySesProcedimientos = new SesProcedimientos();
			mySesProcedimientos.loadResultSet (ls_rs);
			mySesProcedimientoses.addElement(mySesProcedimientos);
		}
		ls_rs.close();
		myStatement.close();
		return mySesProcedimientoses;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_PROCEDIMIENTOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_PROCEDIMIENTOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_PROCEDIMIENTOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setProcPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
