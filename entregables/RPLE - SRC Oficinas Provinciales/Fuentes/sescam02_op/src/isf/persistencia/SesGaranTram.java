//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesGaranTram
// Table: SES_GARAN_TRAM
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_GARAN_TRAM
 * Columns for the table:
 * 	(gar_operacion - LONG), 
 * 	(gar_tram_pk - LONG), 
 * 	(syslogin - STRING), 
 * 	(oficina_pk - LONG), 
 * 	(gar_fechor - DATE), 
 * 	(gar_repleg_dni - STRING), 
 * 	(gar_repleg_apenom - STRING), 
 * 	(gar_pk - LONG), 
 * Primary columns for the table:
 * 	(gar_tram_pk - LONG) 
 */
public class SesGaranTram
{
	// Properties
	private long garOperacion;
	private boolean garOperacionNull=true;
	private boolean garOperacionModified=false;
	private long garTramPk;
	private boolean garTramPkNull=true;
	private boolean garTramPkModified=false;
	private String syslogin;
	private boolean sysloginNull=true;
	private boolean sysloginModified=false;
	private long oficinaPk;
	private boolean oficinaPkNull=true;
	private boolean oficinaPkModified=false;
	private java.sql.Date garFechor;
	private boolean garFechorNull=true;
	private boolean garFechorModified=false;
	private String garReplegDni;
	private boolean garReplegDniNull=true;
	private boolean garReplegDniModified=false;
	private String garReplegApenom;
	private boolean garReplegApenomNull=true;
	private boolean garReplegApenomModified=false;
	private long garPk;
	private boolean garPkNull=true;
	private boolean garPkModified=false;
	// Access Method
	/** GarOperacion
	 * Get the value of the property GarOperacion.
	 * The column for the database is 'GAR_OPERACION'GarOperacion.
	 */
	public long getGarOperacion() {
		return garOperacion;
	}
	/** GarOperacion
	 * Set the value of the property GarOperacion.
	 * The column for the database is 'GAR_OPERACION'GarOperacion.
	 */
	public void setGarOperacion(long _garOperacion) {
		this.garOperacion=_garOperacion;
		this.garOperacionModified=true;
		this.garOperacionNull=false;
	}
	/** GarOperacion
	 * Set Null the value of the property GarOperacion.
	 * The column for the database is 'GAR_OPERACION'GarOperacion.
	 */
	public void setNullGarOperacion() {
		this.garOperacion=0;
		this.garOperacionModified=true;
		this.garOperacionNull=true;
	}
	/** GarOperacion
	 * Sumatory of value of the property GarOperacion.
	 * The column for the database is 'GAR_OPERACION'GarOperacion.
	 */
	static public double sumGarOperacion(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"GAR_OPERACION");
	}
	/** GarOperacion
	 * Indicates if the value of the property GarOperacion is null or not.
	 * The column for the database is 'GAR_OPERACION'GarOperacion.
	 */
	public boolean isNullGarOperacion() {
		return garOperacionNull;
	}
	/** GarTramPk
	 * Get the value of the property GarTramPk.
	 * The column for the database is 'GAR_TRAM_PK'GarTramPk.
	 */
	public long getGarTramPk() {
		return garTramPk;
	}
	/** GarTramPk
	 * Set the value of the property GarTramPk.
	 * The column for the database is 'GAR_TRAM_PK'GarTramPk.
	 */
	public void setGarTramPk(long _garTramPk) {
		this.garTramPk=_garTramPk;
		this.garTramPkModified=true;
		this.garTramPkNull=false;
	}
	/** GarTramPk
	 * Set Null the value of the property GarTramPk.
	 * The column for the database is 'GAR_TRAM_PK'GarTramPk.
	 */
	public void setNullGarTramPk() {
		this.garTramPk=0;
		this.garTramPkModified=true;
		this.garTramPkNull=true;
	}
	/** GarTramPk
	 * Sumatory of value of the property GarTramPk.
	 * The column for the database is 'GAR_TRAM_PK'GarTramPk.
	 */
	static public double sumGarTramPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"GAR_TRAM_PK");
	}
	/** GarTramPk
	 * Indicates if the value of the property GarTramPk is null or not.
	 * The column for the database is 'GAR_TRAM_PK'GarTramPk.
	 */
	public boolean isNullGarTramPk() {
		return garTramPkNull;
	}
	/** Syslogin
	 * Get the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public String getSyslogin() {
		if (syslogin==null) return "";
		if (syslogin.compareTo("null")==0) return "";
		return syslogin;
	}
	/** Syslogin
	 * Set the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setSyslogin(String _syslogin) {
		this.syslogin=_syslogin;
		this.sysloginModified=true;
		this.sysloginNull=false;
	}
	/** Syslogin
	 * Set Null the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setNullSyslogin() {
		this.syslogin=null;
		this.sysloginModified=true;
		this.sysloginNull=true;
	}
	/** Syslogin
	 * Indicates if the value of the property Syslogin is null or not.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public boolean isNullSyslogin() {
		return sysloginNull;
	}
	/** OficinaPk
	 * Get the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public long getOficinaPk() {
		return oficinaPk;
	}
	/** OficinaPk
	 * Set the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public void setOficinaPk(long _oficinaPk) {
		this.oficinaPk=_oficinaPk;
		this.oficinaPkModified=true;
		this.oficinaPkNull=false;
	}
	/** OficinaPk
	 * Set Null the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public void setNullOficinaPk() {
		this.oficinaPk=0;
		this.oficinaPkModified=true;
		this.oficinaPkNull=true;
	}
	/** OficinaPk
	 * Sumatory of value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	static public double sumOficinaPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"OFICINA_PK");
	}
	/** OficinaPk
	 * Indicates if the value of the property OficinaPk is null or not.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public boolean isNullOficinaPk() {
		return oficinaPkNull;
	}
	/** GarFechor
	 * Get the value of the property GarFechor.
	 * The column for the database is 'GAR_FECHOR'GarFechor.
	 */
	public java.sql.Date getGarFechor() {
		return garFechor;
	}
	/** GarFechor
	 * Set the value of the property GarFechor.
	 * The column for the database is 'GAR_FECHOR'GarFechor.
	 */
	public void setGarFechor(java.sql.Date _garFechor) {
		this.garFechor=_garFechor;
		this.garFechorModified=true;
		this.garFechorNull=false;
	}
	/** GarFechor
	 * Set Null the value of the property GarFechor.
	 * The column for the database is 'GAR_FECHOR'GarFechor.
	 */
	public void setNullGarFechor() {
		this.garFechor=null;
		this.garFechorModified=true;
		this.garFechorNull=true;
	}
	/** GarFechor
	 * Indicates if the value of the property GarFechor is null or not.
	 * The column for the database is 'GAR_FECHOR'GarFechor.
	 */
	public boolean isNullGarFechor() {
		return garFechorNull;
	}
	/** GarReplegDni
	 * Get the value of the property GarReplegDni.
	 * The column for the database is 'GAR_REPLEG_DNI'GarReplegDni.
	 */
	public String getGarReplegDni() {
		if (garReplegDni==null) return "";
		if (garReplegDni.compareTo("null")==0) return "";
		return garReplegDni;
	}
	/** GarReplegDni
	 * Set the value of the property GarReplegDni.
	 * The column for the database is 'GAR_REPLEG_DNI'GarReplegDni.
	 */
	public void setGarReplegDni(String _garReplegDni) {
		this.garReplegDni=_garReplegDni;
		this.garReplegDniModified=true;
		this.garReplegDniNull=false;
	}
	/** GarReplegDni
	 * Set Null the value of the property GarReplegDni.
	 * The column for the database is 'GAR_REPLEG_DNI'GarReplegDni.
	 */
	public void setNullGarReplegDni() {
		this.garReplegDni=null;
		this.garReplegDniModified=true;
		this.garReplegDniNull=true;
	}
	/** GarReplegDni
	 * Indicates if the value of the property GarReplegDni is null or not.
	 * The column for the database is 'GAR_REPLEG_DNI'GarReplegDni.
	 */
	public boolean isNullGarReplegDni() {
		return garReplegDniNull;
	}
	/** GarReplegApenom
	 * Get the value of the property GarReplegApenom.
	 * The column for the database is 'GAR_REPLEG_APENOM'GarReplegApenom.
	 */
	public String getGarReplegApenom() {
		if (garReplegApenom==null) return "";
		if (garReplegApenom.compareTo("null")==0) return "";
		return garReplegApenom;
	}
	/** GarReplegApenom
	 * Set the value of the property GarReplegApenom.
	 * The column for the database is 'GAR_REPLEG_APENOM'GarReplegApenom.
	 */
	public void setGarReplegApenom(String _garReplegApenom) {
		this.garReplegApenom=_garReplegApenom;
		this.garReplegApenomModified=true;
		this.garReplegApenomNull=false;
	}
	/** GarReplegApenom
	 * Set Null the value of the property GarReplegApenom.
	 * The column for the database is 'GAR_REPLEG_APENOM'GarReplegApenom.
	 */
	public void setNullGarReplegApenom() {
		this.garReplegApenom=null;
		this.garReplegApenomModified=true;
		this.garReplegApenomNull=true;
	}
	/** GarReplegApenom
	 * Indicates if the value of the property GarReplegApenom is null or not.
	 * The column for the database is 'GAR_REPLEG_APENOM'GarReplegApenom.
	 */
	public boolean isNullGarReplegApenom() {
		return garReplegApenomNull;
	}
	/** GarPk
	 * Get the value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public long getGarPk() {
		return garPk;
	}
	/** GarPk
	 * Set the value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public void setGarPk(long _garPk) {
		this.garPk=_garPk;
		this.garPkModified=true;
		this.garPkNull=false;
	}
	/** GarPk
	 * Set Null the value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public void setNullGarPk() {
		this.garPk=0;
		this.garPkModified=true;
		this.garPkNull=true;
	}
	/** GarPk
	 * Sumatory of value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	static public double sumGarPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"GAR_PK");
	}
	/** GarPk
	 * Indicates if the value of the property GarPk is null or not.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public boolean isNullGarPk() {
		return garPkNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!garOperacionNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_OPERACION");
			ls_values.append((lb_FirstTime?"":",")+garOperacion);
			lb_FirstTime = false;
		}
		if (!garTramPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_TRAM_PK");
			ls_values.append((lb_FirstTime?"":",")+garTramPk);
			lb_FirstTime = false;
		}
		if (!sysloginNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYSLOGIN");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(syslogin)+"'");
			lb_FirstTime = false;
		}
		if (!oficinaPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"OFICINA_PK");
			ls_values.append((lb_FirstTime?"":",")+oficinaPk);
			lb_FirstTime = false;
		}
		if (!garFechorNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_FECHOR");
			ls_values.append((lb_FirstTime?"":",")+"'"+garFechor.toString() + "'"); 
			lb_FirstTime = false;
		}
		if (!garReplegDniNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_REPLEG_DNI");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(garReplegDni)+"'");
			lb_FirstTime = false;
		}
		if (!garReplegApenomNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_REPLEG_APENOM");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(garReplegApenom)+"'");
			lb_FirstTime = false;
		}
		if (!garPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_PK");
			ls_values.append((lb_FirstTime?"":",")+garPk);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_GARAN_TRAM ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (garOperacionModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_OPERACION");
			if(garOperacionNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+garOperacion);
			lb_FirstTime = false;
		}
		if (garTramPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_TRAM_PK");
			if(garTramPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+garTramPk);
			lb_FirstTime = false;
		}
		if (sysloginModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYSLOGIN");
			if(sysloginNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(syslogin)+"'");
			lb_FirstTime = false;
		}
		if (oficinaPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("OFICINA_PK");
			if(oficinaPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+oficinaPk);
			lb_FirstTime = false;
		}
		if (garFechorModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_FECHOR");
			if(garFechorNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+ garFechor.toString() + "'"); 
			lb_FirstTime = false;
		}
		if (garReplegDniModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_REPLEG_DNI");
			if(garReplegDniNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(garReplegDni)+"'");
			lb_FirstTime = false;
		}
		if (garReplegApenomModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_REPLEG_APENOM");
			if(garReplegApenomNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(garReplegApenom)+"'");
			lb_FirstTime = false;
		}
		if (garPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_PK");
			if(garPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+garPk);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("GAR_TRAM_PK");
			ls_where.append("="+garTramPk);
		} else {
			ls_where.append(" AND "+"GAR_TRAM_PK");
			ls_where.append("="+garTramPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_GARAN_TRAM SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("GAR_TRAM_PK");
			ls_where.append("="+garTramPk);
		} else {
			ls_where.append(" AND "+"GAR_TRAM_PK");
			ls_where.append("="+garTramPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_GARAN_TRAM WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _garTramPk
	 * @return SesGaranTram - Retrieved object
	 */
	static public SesGaranTram read(Connection _connection,long _garTramPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesGaranTram ls_SesGaranTram=new SesGaranTram();
		ls_where="GAR_TRAM_PK = " + _garTramPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_GARAN_TRAM WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesGaranTram.loadResultSet(ls_rs);
		} else {
			ls_SesGaranTram=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesGaranTram;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		garOperacion=_rs.getLong("GAR_OPERACION");
		garOperacionNull=_rs.wasNull();
		garTramPk=_rs.getLong("GAR_TRAM_PK");
		garTramPkNull=_rs.wasNull();
		syslogin=_rs.getString("SYSLOGIN");
		sysloginNull=_rs.wasNull();
		oficinaPk=_rs.getLong("OFICINA_PK");
		oficinaPkNull=_rs.wasNull();
		garFechor=_rs.getDate("GAR_FECHOR");
		garFechorNull=_rs.wasNull();
		garReplegDni=_rs.getString("GAR_REPLEG_DNI");
		garReplegDniNull=_rs.wasNull();
		garReplegApenom=_rs.getString("GAR_REPLEG_APENOM");
		garReplegApenomNull=_rs.wasNull();
		garPk=_rs.getLong("GAR_PK");
		garPkNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_GARAN_TRAM ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesGaranTram mySesGaranTram;
		Vector mySesGaranTrames = new Vector();
		while (ls_rs.next()) {
			mySesGaranTram = new SesGaranTram();
			mySesGaranTram.loadResultSet (ls_rs);
			mySesGaranTrames.addElement(mySesGaranTram);
		}
		ls_rs.close();
		myStatement.close();
		return mySesGaranTrames;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_GARAN_TRAM ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_GARAN_TRAM ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_GARAN_TRAM ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setGarTramPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
