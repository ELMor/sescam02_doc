//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesOficina
// Table: SES_OFICINA
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_OFICINA
 * Columns for the table:
 * 	(syslogin - STRING), 
 * 	(oficina_pk - LONG), 
 * 	(oficina_nombre - STRING), 
 * 	(oficina_direccion - STRING), 
 * 	(oficina_telefono - LONG), 
 * 	(pk_localidad - LONG), 
 * 	(oficina_codpostal - LONG), 
 * Primary columns for the table:
 * 	(oficina_pk - LONG) 
 */
public class SesOficina
{
	// Properties
	private String syslogin;
	private boolean sysloginNull=true;
	private boolean sysloginModified=false;
	private long oficinaPk;
	private boolean oficinaPkNull=true;
	private boolean oficinaPkModified=false;
	private String oficinaNombre;
	private boolean oficinaNombreNull=true;
	private boolean oficinaNombreModified=false;
	private String oficinaDireccion;
	private boolean oficinaDireccionNull=true;
	private boolean oficinaDireccionModified=false;
	private long oficinaTelefono;
	private boolean oficinaTelefonoNull=true;
	private boolean oficinaTelefonoModified=false;
	private long pkLocalidad;
	private boolean pkLocalidadNull=true;
	private boolean pkLocalidadModified=false;
	private long oficinaCodpostal;
	private boolean oficinaCodpostalNull=true;
	private boolean oficinaCodpostalModified=false;
	// Access Method
	/** Syslogin
	 * Get the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public String getSyslogin() {
		if (syslogin==null) return "";
		if (syslogin.compareTo("null")==0) return "";
		return syslogin;
	}
	/** Syslogin
	 * Set the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setSyslogin(String _syslogin) {
		this.syslogin=_syslogin;
		this.sysloginModified=true;
		this.sysloginNull=false;
	}
	/** Syslogin
	 * Set Null the value of the property Syslogin.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public void setNullSyslogin() {
		this.syslogin=null;
		this.sysloginModified=true;
		this.sysloginNull=true;
	}
	/** Syslogin
	 * Indicates if the value of the property Syslogin is null or not.
	 * The column for the database is 'SYSLOGIN'Syslogin.
	 */
	public boolean isNullSyslogin() {
		return sysloginNull;
	}
	/** OficinaPk
	 * Get the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public long getOficinaPk() {
		return oficinaPk;
	}
	/** OficinaPk
	 * Set the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public void setOficinaPk(long _oficinaPk) {
		this.oficinaPk=_oficinaPk;
		this.oficinaPkModified=true;
		this.oficinaPkNull=false;
	}
	/** OficinaPk
	 * Set Null the value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public void setNullOficinaPk() {
		this.oficinaPk=0;
		this.oficinaPkModified=true;
		this.oficinaPkNull=true;
	}
	/** OficinaPk
	 * Sumatory of value of the property OficinaPk.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	static public double sumOficinaPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"OFICINA_PK");
	}
	/** OficinaPk
	 * Indicates if the value of the property OficinaPk is null or not.
	 * The column for the database is 'OFICINA_PK'OficinaPk.
	 */
	public boolean isNullOficinaPk() {
		return oficinaPkNull;
	}
	/** OficinaNombre
	 * Get the value of the property OficinaNombre.
	 * The column for the database is 'OFICINA_NOMBRE'OficinaNombre.
	 */
	public String getOficinaNombre() {
		if (oficinaNombre==null) return "";
		if (oficinaNombre.compareTo("null")==0) return "";
		return oficinaNombre;
	}
	/** OficinaNombre
	 * Set the value of the property OficinaNombre.
	 * The column for the database is 'OFICINA_NOMBRE'OficinaNombre.
	 */
	public void setOficinaNombre(String _oficinaNombre) {
		this.oficinaNombre=_oficinaNombre;
		this.oficinaNombreModified=true;
		this.oficinaNombreNull=false;
	}
	/** OficinaNombre
	 * Set Null the value of the property OficinaNombre.
	 * The column for the database is 'OFICINA_NOMBRE'OficinaNombre.
	 */
	public void setNullOficinaNombre() {
		this.oficinaNombre=null;
		this.oficinaNombreModified=true;
		this.oficinaNombreNull=true;
	}
	/** OficinaNombre
	 * Indicates if the value of the property OficinaNombre is null or not.
	 * The column for the database is 'OFICINA_NOMBRE'OficinaNombre.
	 */
	public boolean isNullOficinaNombre() {
		return oficinaNombreNull;
	}
	/** OficinaDireccion
	 * Get the value of the property OficinaDireccion.
	 * The column for the database is 'OFICINA_DIRECCION'OficinaDireccion.
	 */
	public String getOficinaDireccion() {
		if (oficinaDireccion==null) return "";
		if (oficinaDireccion.compareTo("null")==0) return "";
		return oficinaDireccion;
	}
	/** OficinaDireccion
	 * Set the value of the property OficinaDireccion.
	 * The column for the database is 'OFICINA_DIRECCION'OficinaDireccion.
	 */
	public void setOficinaDireccion(String _oficinaDireccion) {
		this.oficinaDireccion=_oficinaDireccion;
		this.oficinaDireccionModified=true;
		this.oficinaDireccionNull=false;
	}
	/** OficinaDireccion
	 * Set Null the value of the property OficinaDireccion.
	 * The column for the database is 'OFICINA_DIRECCION'OficinaDireccion.
	 */
	public void setNullOficinaDireccion() {
		this.oficinaDireccion=null;
		this.oficinaDireccionModified=true;
		this.oficinaDireccionNull=true;
	}
	/** OficinaDireccion
	 * Indicates if the value of the property OficinaDireccion is null or not.
	 * The column for the database is 'OFICINA_DIRECCION'OficinaDireccion.
	 */
	public boolean isNullOficinaDireccion() {
		return oficinaDireccionNull;
	}
	/** OficinaTelefono
	 * Get the value of the property OficinaTelefono.
	 * The column for the database is 'OFICINA_TELEFONO'OficinaTelefono.
	 */
	public long getOficinaTelefono() {
		return oficinaTelefono;
	}
	/** OficinaTelefono
	 * Set the value of the property OficinaTelefono.
	 * The column for the database is 'OFICINA_TELEFONO'OficinaTelefono.
	 */
	public void setOficinaTelefono(long _oficinaTelefono) {
		this.oficinaTelefono=_oficinaTelefono;
		this.oficinaTelefonoModified=true;
		this.oficinaTelefonoNull=false;
	}
	/** OficinaTelefono
	 * Set Null the value of the property OficinaTelefono.
	 * The column for the database is 'OFICINA_TELEFONO'OficinaTelefono.
	 */
	public void setNullOficinaTelefono() {
		this.oficinaTelefono=0;
		this.oficinaTelefonoModified=true;
		this.oficinaTelefonoNull=true;
	}
	/** OficinaTelefono
	 * Sumatory of value of the property OficinaTelefono.
	 * The column for the database is 'OFICINA_TELEFONO'OficinaTelefono.
	 */
	static public double sumOficinaTelefono(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"OFICINA_TELEFONO");
	}
	/** OficinaTelefono
	 * Indicates if the value of the property OficinaTelefono is null or not.
	 * The column for the database is 'OFICINA_TELEFONO'OficinaTelefono.
	 */
	public boolean isNullOficinaTelefono() {
		return oficinaTelefonoNull;
	}
	/** PkLocalidad
	 * Get the value of the property PkLocalidad.
	 * The column for the database is 'PK_LOCALIDAD'PkLocalidad.
	 */
	public long getPkLocalidad() {
		return pkLocalidad;
	}
	/** PkLocalidad
	 * Set the value of the property PkLocalidad.
	 * The column for the database is 'PK_LOCALIDAD'PkLocalidad.
	 */
	public void setPkLocalidad(long _pkLocalidad) {
		this.pkLocalidad=_pkLocalidad;
		this.pkLocalidadModified=true;
		this.pkLocalidadNull=false;
	}
	/** PkLocalidad
	 * Set Null the value of the property PkLocalidad.
	 * The column for the database is 'PK_LOCALIDAD'PkLocalidad.
	 */
	public void setNullPkLocalidad() {
		this.pkLocalidad=0;
		this.pkLocalidadModified=true;
		this.pkLocalidadNull=true;
	}
	/** PkLocalidad
	 * Sumatory of value of the property PkLocalidad.
	 * The column for the database is 'PK_LOCALIDAD'PkLocalidad.
	 */
	static public double sumPkLocalidad(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PK_LOCALIDAD");
	}
	/** PkLocalidad
	 * Indicates if the value of the property PkLocalidad is null or not.
	 * The column for the database is 'PK_LOCALIDAD'PkLocalidad.
	 */
	public boolean isNullPkLocalidad() {
		return pkLocalidadNull;
	}
	/** OficinaCodpostal
	 * Get the value of the property OficinaCodpostal.
	 * The column for the database is 'OFICINA_CODPOSTAL'OficinaCodpostal.
	 */
	public long getOficinaCodpostal() {
		return oficinaCodpostal;
	}
	/** OficinaCodpostal
	 * Set the value of the property OficinaCodpostal.
	 * The column for the database is 'OFICINA_CODPOSTAL'OficinaCodpostal.
	 */
	public void setOficinaCodpostal(long _oficinaCodpostal) {
		this.oficinaCodpostal=_oficinaCodpostal;
		this.oficinaCodpostalModified=true;
		this.oficinaCodpostalNull=false;
	}
	/** OficinaCodpostal
	 * Set Null the value of the property OficinaCodpostal.
	 * The column for the database is 'OFICINA_CODPOSTAL'OficinaCodpostal.
	 */
	public void setNullOficinaCodpostal() {
		this.oficinaCodpostal=0;
		this.oficinaCodpostalModified=true;
		this.oficinaCodpostalNull=true;
	}
	/** OficinaCodpostal
	 * Sumatory of value of the property OficinaCodpostal.
	 * The column for the database is 'OFICINA_CODPOSTAL'OficinaCodpostal.
	 */
	static public double sumOficinaCodpostal(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"OFICINA_CODPOSTAL");
	}
	/** OficinaCodpostal
	 * Indicates if the value of the property OficinaCodpostal is null or not.
	 * The column for the database is 'OFICINA_CODPOSTAL'OficinaCodpostal.
	 */
	public boolean isNullOficinaCodpostal() {
		return oficinaCodpostalNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!sysloginNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYSLOGIN");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(syslogin)+"'");
			lb_FirstTime = false;
		}
		if (!oficinaPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"OFICINA_PK");
			ls_values.append((lb_FirstTime?"":",")+oficinaPk);
			lb_FirstTime = false;
		}
		if (!oficinaNombreNull) {
			ls_columns.append((lb_FirstTime?"":",")+"OFICINA_NOMBRE");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(oficinaNombre)+"'");
			lb_FirstTime = false;
		}
		if (!oficinaDireccionNull) {
			ls_columns.append((lb_FirstTime?"":",")+"OFICINA_DIRECCION");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(oficinaDireccion)+"'");
			lb_FirstTime = false;
		}
		if (!oficinaTelefonoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"OFICINA_TELEFONO");
			ls_values.append((lb_FirstTime?"":",")+oficinaTelefono);
			lb_FirstTime = false;
		}
		if (!pkLocalidadNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PK_LOCALIDAD");
			ls_values.append((lb_FirstTime?"":",")+pkLocalidad);
			lb_FirstTime = false;
		}
		if (!oficinaCodpostalNull) {
			ls_columns.append((lb_FirstTime?"":",")+"OFICINA_CODPOSTAL");
			ls_values.append((lb_FirstTime?"":",")+oficinaCodpostal);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_OFICINA ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (sysloginModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYSLOGIN");
			if(sysloginNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(syslogin)+"'");
			lb_FirstTime = false;
		}
		if (oficinaPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("OFICINA_PK");
			if(oficinaPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+oficinaPk);
			lb_FirstTime = false;
		}
		if (oficinaNombreModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("OFICINA_NOMBRE");
			if(oficinaNombreNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(oficinaNombre)+"'");
			lb_FirstTime = false;
		}
		if (oficinaDireccionModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("OFICINA_DIRECCION");
			if(oficinaDireccionNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(oficinaDireccion)+"'");
			lb_FirstTime = false;
		}
		if (oficinaTelefonoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("OFICINA_TELEFONO");
			if(oficinaTelefonoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+oficinaTelefono);
			lb_FirstTime = false;
		}
		if (pkLocalidadModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PK_LOCALIDAD");
			if(pkLocalidadNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+pkLocalidad);
			lb_FirstTime = false;
		}
		if (oficinaCodpostalModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("OFICINA_CODPOSTAL");
			if(oficinaCodpostalNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+oficinaCodpostal);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("OFICINA_PK");
			ls_where.append("="+oficinaPk);
		} else {
			ls_where.append(" AND "+"OFICINA_PK");
			ls_where.append("="+oficinaPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_OFICINA SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("OFICINA_PK");
			ls_where.append("="+oficinaPk);
		} else {
			ls_where.append(" AND "+"OFICINA_PK");
			ls_where.append("="+oficinaPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_OFICINA WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _oficinaPk
	 * @return SesOficina - Retrieved object
	 */
	static public SesOficina read(Connection _connection,long _oficinaPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesOficina ls_SesOficina=new SesOficina();
		ls_where="OFICINA_PK = " + _oficinaPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_OFICINA WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesOficina.loadResultSet(ls_rs);
		} else {
			ls_SesOficina=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesOficina;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		syslogin=_rs.getString("SYSLOGIN");
		sysloginNull=_rs.wasNull();
		oficinaPk=_rs.getLong("OFICINA_PK");
		oficinaPkNull=_rs.wasNull();
		oficinaNombre=_rs.getString("OFICINA_NOMBRE");
		oficinaNombreNull=_rs.wasNull();
		oficinaDireccion=_rs.getString("OFICINA_DIRECCION");
		oficinaDireccionNull=_rs.wasNull();
		oficinaTelefono=_rs.getLong("OFICINA_TELEFONO");
		oficinaTelefonoNull=_rs.wasNull();
		pkLocalidad=_rs.getLong("PK_LOCALIDAD");
		pkLocalidadNull=_rs.wasNull();
		oficinaCodpostal=_rs.getLong("OFICINA_CODPOSTAL");
		oficinaCodpostalNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_OFICINA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesOficina mySesOficina;
		Vector mySesOficinaes = new Vector();
		while (ls_rs.next()) {
			mySesOficina = new SesOficina();
			mySesOficina.loadResultSet (ls_rs);
			mySesOficinaes.addElement(mySesOficina);
		}
		ls_rs.close();
		myStatement.close();
		return mySesOficinaes;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_OFICINA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_OFICINA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_OFICINA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setOficinaPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
