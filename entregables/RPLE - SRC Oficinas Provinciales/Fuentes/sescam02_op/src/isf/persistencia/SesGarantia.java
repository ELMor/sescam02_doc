//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesGarantia
// Table: SES_GARANTIA
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_GARANTIA
 * Columns for the table:
 * 	(gar_pk - LONG), 
 * 	(norden - DECIMAL), 
 * 	(centro - DECIMAL), 
 * 	(gar_tipo - LONG), 
 * 	(gar_estado - LONG), 
 * 	(gar_monto - DECIMAL), 
 * 	(gar_motivo_rech - STRING), 
 * 	(cega - DECIMAL), 
 * 	(ncita - DECIMAL), 
 * 	(gar_cq - STRING), 
 * 	(pac_nss - STRING), 
 * Primary columns for the table:
 * 	(gar_pk - LONG) 
 */
public class SesGarantia
{
	// Properties
	private long garPk;
	private boolean garPkNull=true;
	private boolean garPkModified=false;
	private double norden;
	private boolean nordenNull=true;
	private boolean nordenModified=false;
	private double centro;
	private boolean centroNull=true;
	private boolean centroModified=false;
	private long garTipo;
	private boolean garTipoNull=true;
	private boolean garTipoModified=false;
	private long garEstado;
	private boolean garEstadoNull=true;
	private boolean garEstadoModified=false;
	private double garMonto;
	private boolean garMontoNull=true;
	private boolean garMontoModified=false;
	private String garMotivoRech;
	private boolean garMotivoRechNull=true;
	private boolean garMotivoRechModified=false;
	private double cega;
	private boolean cegaNull=true;
	private boolean cegaModified=false;
	private double ncita;
	private boolean ncitaNull=true;
	private boolean ncitaModified=false;
	private String garCq;
	private boolean garCqNull=true;
	private boolean garCqModified=false;
	private String pacNss;
	private boolean pacNssNull=true;
	private boolean pacNssModified=false;
	// Access Method
	/** GarPk
	 * Get the value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public long getGarPk() {
		return garPk;
	}
	/** GarPk
	 * Set the value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public void setGarPk(long _garPk) {
		this.garPk=_garPk;
		this.garPkModified=true;
		this.garPkNull=false;
	}
	/** GarPk
	 * Set Null the value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public void setNullGarPk() {
		this.garPk=0;
		this.garPkModified=true;
		this.garPkNull=true;
	}
	/** GarPk
	 * Sumatory of value of the property GarPk.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	static public double sumGarPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"GAR_PK");
	}
	/** GarPk
	 * Indicates if the value of the property GarPk is null or not.
	 * The column for the database is 'GAR_PK'GarPk.
	 */
	public boolean isNullGarPk() {
		return garPkNull;
	}
	/** Norden
	 * Get the value of the property Norden.
	 * The column for the database is 'NORDEN'Norden.
	 */
	public double getNorden() {
		return norden;
	}
	/** Norden
	 * Set the value of the property Norden.
	 * The column for the database is 'NORDEN'Norden.
	 */
	public void setNorden(double _norden) {
		this.norden=_norden;
		this.nordenModified=true;
		this.nordenNull=false;
	}
	/** Norden
	 * Set Null the value of the property Norden.
	 * The column for the database is 'NORDEN'Norden.
	 */
	public void setNullNorden() {
		this.norden=0;
		this.nordenModified=true;
		this.nordenNull=true;
	}
	/** Norden
	 * Sumatory of value of the property Norden.
	 * The column for the database is 'NORDEN'Norden.
	 */
	static public double sumNorden(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"NORDEN");
	}
	/** Norden
	 * Indicates if the value of the property Norden is null or not.
	 * The column for the database is 'NORDEN'Norden.
	 */
	public boolean isNullNorden() {
		return nordenNull;
	}
	/** Centro
	 * Get the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public double getCentro() {
		return centro;
	}
	/** Centro
	 * Set the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public void setCentro(double _centro) {
		this.centro=_centro;
		this.centroModified=true;
		this.centroNull=false;
	}
	/** Centro
	 * Set Null the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public void setNullCentro() {
		this.centro=0;
		this.centroModified=true;
		this.centroNull=true;
	}
	/** Centro
	 * Sumatory of value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	static public double sumCentro(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CENTRO");
	}
	/** Centro
	 * Indicates if the value of the property Centro is null or not.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public boolean isNullCentro() {
		return centroNull;
	}
	/** GarTipo
	 * Get the value of the property GarTipo.
	 * The column for the database is 'GAR_TIPO'GarTipo.
	 */
	public long getGarTipo() {
		return garTipo;
	}
	/** GarTipo
	 * Set the value of the property GarTipo.
	 * The column for the database is 'GAR_TIPO'GarTipo.
	 */
	public void setGarTipo(long _garTipo) {
		this.garTipo=_garTipo;
		this.garTipoModified=true;
		this.garTipoNull=false;
	}
	/** GarTipo
	 * Set Null the value of the property GarTipo.
	 * The column for the database is 'GAR_TIPO'GarTipo.
	 */
	public void setNullGarTipo() {
		this.garTipo=0;
		this.garTipoModified=true;
		this.garTipoNull=true;
	}
	/** GarTipo
	 * Sumatory of value of the property GarTipo.
	 * The column for the database is 'GAR_TIPO'GarTipo.
	 */
	static public double sumGarTipo(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"GAR_TIPO");
	}
	/** GarTipo
	 * Indicates if the value of the property GarTipo is null or not.
	 * The column for the database is 'GAR_TIPO'GarTipo.
	 */
	public boolean isNullGarTipo() {
		return garTipoNull;
	}
	/** GarEstado
	 * Get the value of the property GarEstado.
	 * The column for the database is 'GAR_ESTADO'GarEstado.
	 */
	public long getGarEstado() {
		return garEstado;
	}
	/** GarEstado
	 * Set the value of the property GarEstado.
	 * The column for the database is 'GAR_ESTADO'GarEstado.
	 */
	public void setGarEstado(long _garEstado) {
		this.garEstado=_garEstado;
		this.garEstadoModified=true;
		this.garEstadoNull=false;
	}
	/** GarEstado
	 * Set Null the value of the property GarEstado.
	 * The column for the database is 'GAR_ESTADO'GarEstado.
	 */
	public void setNullGarEstado() {
		this.garEstado=0;
		this.garEstadoModified=true;
		this.garEstadoNull=true;
	}
	/** GarEstado
	 * Sumatory of value of the property GarEstado.
	 * The column for the database is 'GAR_ESTADO'GarEstado.
	 */
	static public double sumGarEstado(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"GAR_ESTADO");
	}
	/** GarEstado
	 * Indicates if the value of the property GarEstado is null or not.
	 * The column for the database is 'GAR_ESTADO'GarEstado.
	 */
	public boolean isNullGarEstado() {
		return garEstadoNull;
	}
	/** GarMonto
	 * Get the value of the property GarMonto.
	 * The column for the database is 'GAR_MONTO'GarMonto.
	 */
	public double getGarMonto() {
		return garMonto;
	}
	/** GarMonto
	 * Set the value of the property GarMonto.
	 * The column for the database is 'GAR_MONTO'GarMonto.
	 */
	public void setGarMonto(double _garMonto) {
		this.garMonto=_garMonto;
		this.garMontoModified=true;
		this.garMontoNull=false;
	}
	/** GarMonto
	 * Set Null the value of the property GarMonto.
	 * The column for the database is 'GAR_MONTO'GarMonto.
	 */
	public void setNullGarMonto() {
		this.garMonto=0;
		this.garMontoModified=true;
		this.garMontoNull=true;
	}
	/** GarMonto
	 * Sumatory of value of the property GarMonto.
	 * The column for the database is 'GAR_MONTO'GarMonto.
	 */
	static public double sumGarMonto(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"GAR_MONTO");
	}
	/** GarMonto
	 * Indicates if the value of the property GarMonto is null or not.
	 * The column for the database is 'GAR_MONTO'GarMonto.
	 */
	public boolean isNullGarMonto() {
		return garMontoNull;
	}
	/** GarMotivoRech
	 * Get the value of the property GarMotivoRech.
	 * The column for the database is 'GAR_MOTIVO_RECH'GarMotivoRech.
	 */
	public String getGarMotivoRech() {
		if (garMotivoRech==null) return "";
		if (garMotivoRech.compareTo("null")==0) return "";
		return garMotivoRech;
	}
	/** GarMotivoRech
	 * Set the value of the property GarMotivoRech.
	 * The column for the database is 'GAR_MOTIVO_RECH'GarMotivoRech.
	 */
	public void setGarMotivoRech(String _garMotivoRech) {
		this.garMotivoRech=_garMotivoRech;
		this.garMotivoRechModified=true;
		this.garMotivoRechNull=false;
	}
	/** GarMotivoRech
	 * Set Null the value of the property GarMotivoRech.
	 * The column for the database is 'GAR_MOTIVO_RECH'GarMotivoRech.
	 */
	public void setNullGarMotivoRech() {
		this.garMotivoRech=null;
		this.garMotivoRechModified=true;
		this.garMotivoRechNull=true;
	}
	/** GarMotivoRech
	 * Indicates if the value of the property GarMotivoRech is null or not.
	 * The column for the database is 'GAR_MOTIVO_RECH'GarMotivoRech.
	 */
	public boolean isNullGarMotivoRech() {
		return garMotivoRechNull;
	}
	/** Cega
	 * Get the value of the property Cega.
	 * The column for the database is 'CEGA'Cega.
	 */
	public double getCega() {
		return cega;
	}
	/** Cega
	 * Set the value of the property Cega.
	 * The column for the database is 'CEGA'Cega.
	 */
	public void setCega(double _cega) {
		this.cega=_cega;
		this.cegaModified=true;
		this.cegaNull=false;
	}
	/** Cega
	 * Set Null the value of the property Cega.
	 * The column for the database is 'CEGA'Cega.
	 */
	public void setNullCega() {
		this.cega=0;
		this.cegaModified=true;
		this.cegaNull=true;
	}
	/** Cega
	 * Sumatory of value of the property Cega.
	 * The column for the database is 'CEGA'Cega.
	 */
	static public double sumCega(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CEGA");
	}
	/** Cega
	 * Indicates if the value of the property Cega is null or not.
	 * The column for the database is 'CEGA'Cega.
	 */
	public boolean isNullCega() {
		return cegaNull;
	}
	/** Ncita
	 * Get the value of the property Ncita.
	 * The column for the database is 'NCITA'Ncita.
	 */
	public double getNcita() {
		return ncita;
	}
	/** Ncita
	 * Set the value of the property Ncita.
	 * The column for the database is 'NCITA'Ncita.
	 */
	public void setNcita(double _ncita) {
		this.ncita=_ncita;
		this.ncitaModified=true;
		this.ncitaNull=false;
	}
	/** Ncita
	 * Set Null the value of the property Ncita.
	 * The column for the database is 'NCITA'Ncita.
	 */
	public void setNullNcita() {
		this.ncita=0;
		this.ncitaModified=true;
		this.ncitaNull=true;
	}
	/** Ncita
	 * Sumatory of value of the property Ncita.
	 * The column for the database is 'NCITA'Ncita.
	 */
	static public double sumNcita(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"NCITA");
	}
	/** Ncita
	 * Indicates if the value of the property Ncita is null or not.
	 * The column for the database is 'NCITA'Ncita.
	 */
	public boolean isNullNcita() {
		return ncitaNull;
	}
	/** GarCq
	 * Get the value of the property GarCq.
	 * The column for the database is 'GAR_CQ'GarCq.
	 */
	public String getGarCq() {
		if (garCq==null) return "";
		if (garCq.compareTo("null")==0) return "";
		return garCq;
	}
	/** GarCq
	 * Set the value of the property GarCq.
	 * The column for the database is 'GAR_CQ'GarCq.
	 */
	public void setGarCq(String _garCq) {
		this.garCq=_garCq;
		this.garCqModified=true;
		this.garCqNull=false;
	}
	/** GarCq
	 * Set Null the value of the property GarCq.
	 * The column for the database is 'GAR_CQ'GarCq.
	 */
	public void setNullGarCq() {
		this.garCq=null;
		this.garCqModified=true;
		this.garCqNull=true;
	}
	/** GarCq
	 * Indicates if the value of the property GarCq is null or not.
	 * The column for the database is 'GAR_CQ'GarCq.
	 */
	public boolean isNullGarCq() {
		return garCqNull;
	}
	/** PacNss
	 * Get the value of the property PacNss.
	 * The column for the database is 'PAC_NSS'PacNss.
	 */
	public String getPacNss() {
		if (pacNss==null) return "";
		if (pacNss.compareTo("null")==0) return "";
		return pacNss;
	}
	/** PacNss
	 * Set the value of the property PacNss.
	 * The column for the database is 'PAC_NSS'PacNss.
	 */
	public void setPacNss(String _pacNss) {
		this.pacNss=_pacNss;
		this.pacNssModified=true;
		this.pacNssNull=false;
	}
	/** PacNss
	 * Set Null the value of the property PacNss.
	 * The column for the database is 'PAC_NSS'PacNss.
	 */
	public void setNullPacNss() {
		this.pacNss=null;
		this.pacNssModified=true;
		this.pacNssNull=true;
	}
	/** PacNss
	 * Indicates if the value of the property PacNss is null or not.
	 * The column for the database is 'PAC_NSS'PacNss.
	 */
	public boolean isNullPacNss() {
		return pacNssNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!garPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_PK");
			ls_values.append((lb_FirstTime?"":",")+garPk);
			lb_FirstTime = false;
		}
		if (!nordenNull) {
			ls_columns.append((lb_FirstTime?"":",")+"NORDEN");
			ls_values.append((lb_FirstTime?"":",")+norden);
			lb_FirstTime = false;
		}
		if (!centroNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CENTRO");
			ls_values.append((lb_FirstTime?"":",")+centro);
			lb_FirstTime = false;
		}
		if (!garTipoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_TIPO");
			ls_values.append((lb_FirstTime?"":",")+garTipo);
			lb_FirstTime = false;
		}
		if (!garEstadoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_ESTADO");
			ls_values.append((lb_FirstTime?"":",")+garEstado);
			lb_FirstTime = false;
		}
		if (!garMontoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_MONTO");
			ls_values.append((lb_FirstTime?"":",")+garMonto);
			lb_FirstTime = false;
		}
		if (!garMotivoRechNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_MOTIVO_RECH");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(garMotivoRech)+"'");
			lb_FirstTime = false;
		}
		if (!cegaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CEGA");
			ls_values.append((lb_FirstTime?"":",")+cega);
			lb_FirstTime = false;
		}
		if (!ncitaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"NCITA");
			ls_values.append((lb_FirstTime?"":",")+ncita);
			lb_FirstTime = false;
		}
		if (!garCqNull) {
			ls_columns.append((lb_FirstTime?"":",")+"GAR_CQ");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(garCq)+"'");
			lb_FirstTime = false;
		}
		if (!pacNssNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PAC_NSS");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(pacNss)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_GARANTIA ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (garPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_PK");
			if(garPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+garPk);
			lb_FirstTime = false;
		}
		if (nordenModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("NORDEN");
			if(nordenNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+norden);
			lb_FirstTime = false;
		}
		if (centroModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CENTRO");
			if(centroNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+centro);
			lb_FirstTime = false;
		}
		if (garTipoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_TIPO");
			if(garTipoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+garTipo);
			lb_FirstTime = false;
		}
		if (garEstadoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_ESTADO");
			if(garEstadoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+garEstado);
			lb_FirstTime = false;
		}
		if (garMontoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_MONTO");
			if(garMontoNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+garMonto);
			lb_FirstTime = false;
		}
		if (garMotivoRechModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_MOTIVO_RECH");
			if(garMotivoRechNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(garMotivoRech)+"'");
			lb_FirstTime = false;
		}
		if (cegaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CEGA");
			if(cegaNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+cega);
			lb_FirstTime = false;
		}
		if (ncitaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("NCITA");
			if(ncitaNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+ncita);
			lb_FirstTime = false;
		}
		if (garCqModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("GAR_CQ");
			if(garCqNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(garCq)+"'");
			lb_FirstTime = false;
		}
		if (pacNssModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PAC_NSS");
			if(pacNssNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(pacNss)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("GAR_PK");
			ls_where.append("="+garPk);
		} else {
			ls_where.append(" AND "+"GAR_PK");
			ls_where.append("="+garPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_GARANTIA SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("GAR_PK");
			ls_where.append("="+garPk);
		} else {
			ls_where.append(" AND "+"GAR_PK");
			ls_where.append("="+garPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_GARANTIA WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _garPk
	 * @return SesGarantia - Retrieved object
	 */
	static public SesGarantia read(Connection _connection,long _garPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesGarantia ls_SesGarantia=new SesGarantia();
		ls_where="GAR_PK = " + _garPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_GARANTIA WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesGarantia.loadResultSet(ls_rs);
		} else {
			ls_SesGarantia=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesGarantia;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		garPk=_rs.getLong("GAR_PK");
		garPkNull=_rs.wasNull();
		norden=_rs.getDouble("NORDEN");
		nordenNull=_rs.wasNull();
		centro=_rs.getDouble("CENTRO");
		centroNull=_rs.wasNull();
		garTipo=_rs.getLong("GAR_TIPO");
		garTipoNull=_rs.wasNull();
		garEstado=_rs.getLong("GAR_ESTADO");
		garEstadoNull=_rs.wasNull();
		garMonto=_rs.getDouble("GAR_MONTO");
		garMontoNull=_rs.wasNull();
		garMotivoRech=_rs.getString("GAR_MOTIVO_RECH");
		garMotivoRechNull=_rs.wasNull();
		cega=_rs.getDouble("CEGA");
		cegaNull=_rs.wasNull();
		ncita=_rs.getDouble("NCITA");
		ncitaNull=_rs.wasNull();
		garCq=_rs.getString("GAR_CQ");
		garCqNull=_rs.wasNull();
		pacNss=_rs.getString("PAC_NSS");
		pacNssNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_GARANTIA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesGarantia mySesGarantia;
		Vector mySesGarantiaes = new Vector();
		while (ls_rs.next()) {
			mySesGarantia = new SesGarantia();
			mySesGarantia.loadResultSet (ls_rs);
			mySesGarantiaes.addElement(mySesGarantia);
		}
		ls_rs.close();
		myStatement.close();
		return mySesGarantiaes;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_GARANTIA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_GARANTIA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_GARANTIA ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setGarPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
