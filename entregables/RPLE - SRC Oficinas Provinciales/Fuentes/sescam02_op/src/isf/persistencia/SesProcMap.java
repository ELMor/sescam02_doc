//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesProcMap
// Table: SES_PROC_MAP
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_PROC_MAP
 * Columns for the table:
 * 	(proc_pk - LONG), 
 * 	(centro - DECIMAL), 
 * 	(proc_cen_cod - STRING), 
 * 	(proc_cen_tipo - STRING), 
 * Primary columns for the table:
 * 	(centro - DECIMAL) 
 * 	(proc_cen_cod - STRING) 
 * 	(proc_cen_tipo - STRING) 
 */
public class SesProcMap
{
	// Properties
	private long procPk;
	private boolean procPkNull=true;
	private boolean procPkModified=false;
	private double centro;
	private boolean centroNull=true;
	private boolean centroModified=false;
	private String procCenCod;
	private boolean procCenCodNull=true;
	private boolean procCenCodModified=false;
	private String procCenTipo;
	private boolean procCenTipoNull=true;
	private boolean procCenTipoModified=false;
	// Access Method
	/** ProcPk
	 * Get the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public long getProcPk() {
		return procPk;
	}
	/** ProcPk
	 * Set the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public void setProcPk(long _procPk) {
		this.procPk=_procPk;
		this.procPkModified=true;
		this.procPkNull=false;
	}
	/** ProcPk
	 * Set Null the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public void setNullProcPk() {
		this.procPk=0;
		this.procPkModified=true;
		this.procPkNull=true;
	}
	/** ProcPk
	 * Sumatory of value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	static public double sumProcPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_PK");
	}
	/** ProcPk
	 * Indicates if the value of the property ProcPk is null or not.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public boolean isNullProcPk() {
		return procPkNull;
	}
	/** Centro
	 * Get the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public double getCentro() {
		return centro;
	}
	/** Centro
	 * Set the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public void setCentro(double _centro) {
		this.centro=_centro;
		this.centroModified=true;
		this.centroNull=false;
	}
	/** Centro
	 * Set Null the value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public void setNullCentro() {
		this.centro=0;
		this.centroModified=true;
		this.centroNull=true;
	}
	/** Centro
	 * Sumatory of value of the property Centro.
	 * The column for the database is 'CENTRO'Centro.
	 */
	static public double sumCentro(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CENTRO");
	}
	/** Centro
	 * Indicates if the value of the property Centro is null or not.
	 * The column for the database is 'CENTRO'Centro.
	 */
	public boolean isNullCentro() {
		return centroNull;
	}
	/** ProcCenCod
	 * Get the value of the property ProcCenCod.
	 * The column for the database is 'PROC_CEN_COD'ProcCenCod.
	 */
	public String getProcCenCod() {
		if (procCenCod==null) return "";
		if (procCenCod.compareTo("null")==0) return "";
		return procCenCod;
	}
	/** ProcCenCod
	 * Set the value of the property ProcCenCod.
	 * The column for the database is 'PROC_CEN_COD'ProcCenCod.
	 */
	public void setProcCenCod(String _procCenCod) {
		this.procCenCod=_procCenCod;
		this.procCenCodModified=true;
		this.procCenCodNull=false;
	}
	/** ProcCenCod
	 * Set Null the value of the property ProcCenCod.
	 * The column for the database is 'PROC_CEN_COD'ProcCenCod.
	 */
	public void setNullProcCenCod() {
		this.procCenCod=null;
		this.procCenCodModified=true;
		this.procCenCodNull=true;
	}
	/** ProcCenCod
	 * Indicates if the value of the property ProcCenCod is null or not.
	 * The column for the database is 'PROC_CEN_COD'ProcCenCod.
	 */
	public boolean isNullProcCenCod() {
		return procCenCodNull;
	}
	/** ProcCenTipo
	 * Get the value of the property ProcCenTipo.
	 * The column for the database is 'PROC_CEN_TIPO'ProcCenTipo.
	 */
	public String getProcCenTipo() {
		if (procCenTipo==null) return "";
		if (procCenTipo.compareTo("null")==0) return "";
		return procCenTipo;
	}
	/** ProcCenTipo
	 * Set the value of the property ProcCenTipo.
	 * The column for the database is 'PROC_CEN_TIPO'ProcCenTipo.
	 */
	public void setProcCenTipo(String _procCenTipo) {
		this.procCenTipo=_procCenTipo;
		this.procCenTipoModified=true;
		this.procCenTipoNull=false;
	}
	/** ProcCenTipo
	 * Set Null the value of the property ProcCenTipo.
	 * The column for the database is 'PROC_CEN_TIPO'ProcCenTipo.
	 */
	public void setNullProcCenTipo() {
		this.procCenTipo=null;
		this.procCenTipoModified=true;
		this.procCenTipoNull=true;
	}
	/** ProcCenTipo
	 * Indicates if the value of the property ProcCenTipo is null or not.
	 * The column for the database is 'PROC_CEN_TIPO'ProcCenTipo.
	 */
	public boolean isNullProcCenTipo() {
		return procCenTipoNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!procPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_PK");
			ls_values.append((lb_FirstTime?"":",")+procPk);
			lb_FirstTime = false;
		}
		if (!centroNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CENTRO");
			ls_values.append((lb_FirstTime?"":",")+centro);
			lb_FirstTime = false;
		}
		if (!procCenCodNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_CEN_COD");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(procCenCod)+"'");
			lb_FirstTime = false;
		}
		if (!procCenTipoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_CEN_TIPO");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(procCenTipo)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_PROC_MAP ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (procPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_PK");
			if(procPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+procPk);
			lb_FirstTime = false;
		}
		if (centroModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CENTRO");
			if(centroNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+centro);
			lb_FirstTime = false;
		}
		if (procCenCodModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_CEN_COD");
			if(procCenCodNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(procCenCod)+"'");
			lb_FirstTime = false;
		}
		if (procCenTipoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_CEN_TIPO");
			if(procCenTipoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(procCenTipo)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("CENTRO");
			ls_where.append("="+centro);
		} else {
			ls_where.append(" AND "+"CENTRO");
			ls_where.append("="+centro);
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) { 
			ls_where.append("PROC_CEN_COD");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(procCenCod)+"'");
		} else {
			ls_where.append(" AND "+"PROC_CEN_COD");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(procCenCod)+"'");
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) { 
			ls_where.append("PROC_CEN_TIPO");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(procCenTipo)+"'");
		} else {
			ls_where.append(" AND "+"PROC_CEN_TIPO");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(procCenTipo)+"'");
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_PROC_MAP SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("CENTRO");
			ls_where.append("="+centro);
		} else {
			ls_where.append(" AND "+"CENTRO");
			ls_where.append("="+centro);
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) { 
			ls_where.append("PROC_CEN_COD");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(procCenCod)+"'");
		} else {
			ls_where.append(" AND "+"PROC_CEN_COD");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(procCenCod)+"'");
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) { 
			ls_where.append("PROC_CEN_TIPO");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(procCenTipo)+"'");
		} else {
			ls_where.append(" AND "+"PROC_CEN_TIPO");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(procCenTipo)+"'");
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_PROC_MAP WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,double _centro,String _procCenCod,String _procCenTipo
	 * @return SesProcMap - Retrieved object
	 */
	static public SesProcMap read(Connection _connection,double _centro,String _procCenCod,String _procCenTipo) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesProcMap ls_SesProcMap=new SesProcMap();
		ls_where="CENTRO = " + _centro + " AND " + "PROC_CEN_COD  = '"+ SQLLanguageHelper.escapeQuote(  _procCenCod) + "'" + " AND " + "PROC_CEN_TIPO  = '"+ SQLLanguageHelper.escapeQuote(  _procCenTipo) + "'";
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_PROC_MAP WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesProcMap.loadResultSet(ls_rs);
		} else {
			ls_SesProcMap=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesProcMap;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		procPk=_rs.getLong("PROC_PK");
		procPkNull=_rs.wasNull();
		centro=_rs.getDouble("CENTRO");
		centroNull=_rs.wasNull();
		procCenCod=_rs.getString("PROC_CEN_COD");
		procCenCodNull=_rs.wasNull();
		procCenTipo=_rs.getString("PROC_CEN_TIPO");
		procCenTipoNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_PROC_MAP ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesProcMap mySesProcMap;
		Vector mySesProcMapes = new Vector();
		while (ls_rs.next()) {
			mySesProcMap = new SesProcMap();
			mySesProcMap.loadResultSet (ls_rs);
			mySesProcMapes.addElement(mySesProcMap);
		}
		ls_rs.close();
		myStatement.close();
		return mySesProcMapes;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_PROC_MAP ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_PROC_MAP ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_PROC_MAP ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
	}
}
