//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesEstadisticasSync
// Table: SES_ESTADISTICAS_SYNC
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import conf.Constantes;
import isf.db.*;
import isf.negocio.*;
/**
 * Persistent class for table:SES_ESTADISTICAS_SYNC
 * Columns for the table:
 * 	(esync_periodo - STRING),
 * 	(est_tlista - STRING),
 * 	(est_terminado - LONG),
 * 	(esync_tramos - STRING),
 * 	(esync_cntramos - LONG),
 * Primary columns for the table:
 * 	(esync_periodo - STRING)
 * 	(est_tlista - STRING)
 */
public class SesEstadisticasSync
{
	// Properties
	private String esyncPeriodo;
	private boolean esyncPeriodoNull=true;
	private boolean esyncPeriodoModified=false;
	private String estTlista;
	private boolean estTlistaNull=true;
	private boolean estTlistaModified=false;
	private long estTerminado;
	private boolean estTerminadoNull=true;
	private boolean estTerminadoModified=false;
	private String esyncTramos;
	private boolean esyncTramosNull=true;
	private boolean esyncTramosModified=false;
	private long esyncCntramos;
	private boolean esyncCntramosNull=true;
	private boolean esyncCntramosModified=false;





        // Access Method
	/** EsyncPeriodo
	 * Get the value of the property EsyncPeriodo.
	 * The column for the database is 'ESYNC_PERIODO'EsyncPeriodo.
	 */



        public String getEsyncPeriodo() {
		if (esyncPeriodo==null) return "";
		if (esyncPeriodo.compareTo("null")==0) return "";
		return esyncPeriodo;
	}
	/** EsyncPeriodo
	 * Set the value of the property EsyncPeriodo.
	 * The column for the database is 'ESYNC_PERIODO'EsyncPeriodo.
	 */
	public void setEsyncPeriodo(String _esyncPeriodo) {
		this.esyncPeriodo=_esyncPeriodo;
		this.esyncPeriodoModified=true;
		this.esyncPeriodoNull=false;
	}
	/** EsyncPeriodo
	 * Set Null the value of the property EsyncPeriodo.
	 * The column for the database is 'ESYNC_PERIODO'EsyncPeriodo.
	 */
	public void setNullEsyncPeriodo() {
		this.esyncPeriodo=null;
		this.esyncPeriodoModified=true;
		this.esyncPeriodoNull=true;
	}
	/** EsyncPeriodo
	 * Indicates if the value of the property EsyncPeriodo is null or not.
	 * The column for the database is 'ESYNC_PERIODO'EsyncPeriodo.
	 */
	public boolean isNullEsyncPeriodo() {
		return esyncPeriodoNull;
	}
	/** EstTlista
	 * Get the value of the property EstTlista.
	 * The column for the database is 'EST_TLISTA'EstTlista.
	 */
	public String getEstTlista() {
		if (estTlista==null) return "";
		if (estTlista.compareTo("null")==0) return "";
		return estTlista;
	}
	/** EstTlista
	 * Set the value of the property EstTlista.
	 * The column for the database is 'EST_TLISTA'EstTlista.
	 */
	public void setEstTlista(String _estTlista) {
		this.estTlista=_estTlista;
		this.estTlistaModified=true;
		this.estTlistaNull=false;
	}
	/** EstTlista
	 * Set Null the value of the property EstTlista.
	 * The column for the database is 'EST_TLISTA'EstTlista.
	 */
	public void setNullEstTlista() {
		this.estTlista=null;
		this.estTlistaModified=true;
		this.estTlistaNull=true;
	}
	/** EstTlista
	 * Indicates if the value of the property EstTlista is null or not.
	 * The column for the database is 'EST_TLISTA'EstTlista.
	 */
	public boolean isNullEstTlista() {
		return estTlistaNull;
	}
	/** EstTerminado
	 * Get the value of the property EstTerminado.
	 * The column for the database is 'EST_TERMINADO'EstTerminado.
	 */
	public long getEstTerminado() {
		return estTerminado;
	}
	/** EstTerminado
	 * Set the value of the property EstTerminado.
	 * The column for the database is 'EST_TERMINADO'EstTerminado.
	 */
	public void setEstTerminado(long _estTerminado) {
		this.estTerminado=_estTerminado;
		this.estTerminadoModified=true;
		this.estTerminadoNull=false;
	}
	/** EstTerminado
	 * Set Null the value of the property EstTerminado.
	 * The column for the database is 'EST_TERMINADO'EstTerminado.
	 */
	public void setNullEstTerminado() {
		this.estTerminado=0;
		this.estTerminadoModified=true;
		this.estTerminadoNull=true;
	}
	/** EstTerminado
	 * Sumatory of value of the property EstTerminado.
	 * The column for the database is 'EST_TERMINADO'EstTerminado.
	 */
	static public double sumEstTerminado(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"EST_TERMINADO");
	}
	/** EstTerminado
	 * Indicates if the value of the property EstTerminado is null or not.
	 * The column for the database is 'EST_TERMINADO'EstTerminado.
	 */
	public boolean isNullEstTerminado() {
		return estTerminadoNull;

        }
        public String GetEsyncTramos(Writer out,Connection _connection,String _where) {
          try {
            CampoCLOBOracle clob = (CampoCLOBOracle)CampoCLOB.getInstance(Constantes.FICHERO_CONFIGURACION);
            clob.obtenerCampoCLOB(out,_connection,"SES_ESTADISTICAS_SYNC","ESYNC_TRAMOS",_where);
            this.esyncTramos = out.toString();
            this.esyncTramosNull = false;
            return out.toString();
          }catch (java.io.IOException e) {
              System.out.println("Error al recoger el CLOB"+ e);
               this.esyncTramos = "";
               this.esyncTramosNull = true;
               return "";
          }catch (java.lang.Exception e) {
              System.out.println("Error al recoger el CLOB"+ e);
               this.esyncTramos = "";
               this.esyncTramosNull = true;
               return "";
          }

	}

	/** EsyncTramos
	 * Set the value of the property EsyncTramos.
	 * The column for the database is 'ESYNC_TRAMOS'EsyncTramos.
	 */
	public void setEsyncTramos(String _esyncTramos) {
		this.esyncTramos=_esyncTramos;
		this.esyncTramosModified=true;
		this.esyncTramosNull=false;
	}
	/** EsyncTramos
	 * Set Null the value of the property EsyncTramos.
	 * The column for the database is 'ESYNC_TRAMOS'EsyncTramos.
	 */
	public void setNullEsyncTramos() {
		this.esyncTramos=null;
		this.esyncTramosModified=true;
		this.esyncTramosNull=true;
	}
	/** EsyncTramos
	 * Indicates if the value of the property EsyncTramos is null or not.
	 * The column for the database is 'ESYNC_TRAMOS'EsyncTramos.
	 */
	public boolean isNullEsyncTramos() {
		return esyncTramosNull;
	}
	/** EsyncCntramos
	 * Get the value of the property EsyncCntramos.
	 * The column for the database is 'ESYNC_CNTRAMOS'EsyncCntramos.
	 */
	public long getEsyncCntramos() {
		return esyncCntramos;
	}
	/** EsyncCntramos
	 * Set the value of the property EsyncCntramos.
	 * The column for the database is 'ESYNC_CNTRAMOS'EsyncCntramos.
	 */
	public void setEsyncCntramos(long _esyncCntramos) {
		this.esyncCntramos=_esyncCntramos;
		this.esyncCntramosModified=true;
		this.esyncCntramosNull=false;
	}
	/** EsyncCntramos
	 * Set Null the value of the property EsyncCntramos.
	 * The column for the database is 'ESYNC_CNTRAMOS'EsyncCntramos.
	 */
	public void setNullEsyncCntramos() {
		this.esyncCntramos=0;
		this.esyncCntramosModified=true;
		this.esyncCntramosNull=true;
	}
	/** EsyncCntramos
	 * Sumatory of value of the property EsyncCntramos.
	 * The column for the database is 'ESYNC_CNTRAMOS'EsyncCntramos.
	 */
	static public double sumEsyncCntramos(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"ESYNC_CNTRAMOS");
	}
	/** EsyncCntramos
	 * Indicates if the value of the property EsyncCntramos is null or not.
	 * The column for the database is 'ESYNC_CNTRAMOS'EsyncCntramos.
	 */
	public boolean isNullEsyncCntramos() {
		return esyncCntramosNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!esyncPeriodoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"ESYNC_PERIODO");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(esyncPeriodo)+"'");
			lb_FirstTime = false;
		}
		if (!estTlistaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"EST_TLISTA");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(estTlista)+"'");
			lb_FirstTime = false;
		}
		if (!estTerminadoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"EST_TERMINADO");
			ls_values.append((lb_FirstTime?"":",")+estTerminado);
			lb_FirstTime = false;
		}

		if (!esyncCntramosNull) {
			ls_columns.append((lb_FirstTime?"":",")+"ESYNC_CNTRAMOS");
			ls_values.append((lb_FirstTime?"":",")+esyncCntramos);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_ESTADISTICAS_SYNC ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
                StringBuffer ls_setValues = new StringBuffer();
                StringBuffer ls_where  = new StringBuffer();
                ByteArrayOutputStream ls_motivo = new ByteArrayOutputStream();

		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;

		if (esyncPeriodoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("ESYNC_PERIODO");
			if(esyncPeriodoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(esyncPeriodo)+"'");
			lb_FirstTime = false;
		}
		if (estTlistaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("EST_TLISTA");
			if(estTlistaNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(estTlista)+"'");
			lb_FirstTime = false;
		}
		if (estTerminadoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("EST_TERMINADO");
			if(estTerminadoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+estTerminado);
			lb_FirstTime = false;
		}

		if (esyncCntramosModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("ESYNC_CNTRAMOS");
			if(esyncCntramosNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+esyncCntramos);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) {
			ls_where.append("ESYNC_PERIODO");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(esyncPeriodo)+"'");
		} else {
			ls_where.append(" AND "+"ESYNC_PERIODO");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(esyncPeriodo)+"'");
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) {
			ls_where.append("EST_TLISTA");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(estTlista)+"'");
		} else {
			ls_where.append(" AND "+"EST_TLISTA");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(estTlista)+"'");
		}
		lb_FirstTime = false;

		try {
                     myStatement = _connection.createStatement();
                     ll_filas = myStatement.executeUpdate("UPDATE SES_ESTADISTICAS_SYNC SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
                     myStatement.close();
                     try{
                       this.ActualizarCLOB(_connection,ls_where.toString());
                     }catch (Exception e) {}
                }
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
                ByteArrayOutputStream ls_motivo = new ByteArrayOutputStream();

		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;

		if (lb_FirstTime ) {
			ls_where.append("ESYNC_PERIODO");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(esyncPeriodo)+"'");
		} else {
			ls_where.append(" AND "+"ESYNC_PERIODO");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(esyncPeriodo)+"'");
		}
		lb_FirstTime = false;
		if (lb_FirstTime ) {
			ls_where.append("EST_TLISTA");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(estTlista)+"'");
		} else {
			ls_where.append(" AND "+"EST_TLISTA");
			ls_where.append("='"+SQLLanguageHelper.escapeQuote(estTlista)+"'");
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_ESTADISTICAS_SYNC WHERE "+ls_where.toString());
			myStatement.close();
                        try{
                         this.ActualizarCLOB(_connection,ls_where.toString());
                        }catch (Exception e) {}
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,String _esyncPeriodo,String _estTlista
	 * @return SesEstadisticasSync - Retrieved object
	 */
	static public SesEstadisticasSync read(Connection _connection,String _esyncPeriodo,String _estTlista) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesEstadisticasSync ls_SesEstadisticasSync=new SesEstadisticasSync();
		ls_where="ESYNC_PERIODO  = '"+ SQLLanguageHelper.escapeQuote(  _esyncPeriodo) + "'" + " AND " + "EST_TLISTA  = '"+ SQLLanguageHelper.escapeQuote(  _estTlista) + "'";
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_ESTADISTICAS_SYNC WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesEstadisticasSync.loadResultSet(ls_rs);
		} else {
			ls_SesEstadisticasSync=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesEstadisticasSync;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		esyncPeriodo=_rs.getString("ESYNC_PERIODO");
		esyncPeriodoNull=_rs.wasNull();
		estTlista=_rs.getString("EST_TLISTA");
		estTlistaNull=_rs.wasNull();
		estTerminado=_rs.getLong("EST_TERMINADO");
		estTerminadoNull=_rs.wasNull();
		esyncCntramos=_rs.getLong("ESYNC_CNTRAMOS");
		esyncCntramosNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_ESTADISTICAS_SYNC ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesEstadisticasSync mySesEstadisticasSync;
                OutputStream out = null;
		Vector mySesEstadisticasSynces = new Vector();
		while (ls_rs.next()) {
			mySesEstadisticasSync = new SesEstadisticasSync();
			mySesEstadisticasSync.loadResultSet (ls_rs);
                        String cadena_where = "WHERE "+_where;
                  //      mySesEstadisticasSync.GetEsyncTramos(out,_connection,cadena_where);
			mySesEstadisticasSynces.addElement(mySesEstadisticasSync);
		}
		ls_rs.close();
		myStatement.close();
		return mySesEstadisticasSynces;
	}

        public void ActualizarCLOB(Connection _connection,String s_where) throws Exception {
          try {
            if (s_where.equals("")) {
              s_where = " ESYNC_PERIODO= '"+this.esyncPeriodo +"'" + " AND EST_TLISTA='" +this.estTlista+ "' ";
            }
            CampoCLOBOracle clob = (CampoCLOBOracle)CampoCLOB.getInstance(Constantes.FICHERO_CONFIGURACION);
            clob.escribirCampoCLOB(this.esyncTramos,_connection,"SES_ESTADISTICAS_SYNC","ESYNC_TRAMOS",s_where);
          //  System.out.println(this.esyncTramos);

            //  _connection.commit();
          }catch (SQLException e){
            System.out.println("Error al guardar el Clob"+ e);
            _connection.rollback();
          }
          catch (java.io.IOException e) {
            System.out.println("Error al cargar el Clob"+ e);
          }
          catch (java.lang.Exception e) {
            System.out.println("Error al cargar el Clob"+ e);
          }
        }
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_ESTADISTICAS_SYNC ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_ESTADISTICAS_SYNC ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_ESTADISTICAS_SYNC ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
	}

        public static void main(String args[]){
          SesEstadisticasSync cpb=new SesEstadisticasSync();
          Vector vp=null;
          Tramos tram = new Tramos();
          try{
            Connection conexion = null;
            Pool pool=null;

            StringWriter outbuffer = new  StringWriter();
            pool = Pool.getInstance();
            conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
            conexion.setAutoCommit(false);
            String clob = tram.CadenaTramo(2);
            System.out.println(clob);
            cpb.setEsyncPeriodo("200303040204");
            cpb.setEstTlista("Q");
            cpb.setEstTerminado(0);
            cpb.setEsyncTramos(clob);
            cpb.setEsyncCntramos(tram.getNTramos());
            System.out.println(tram.getNTramos());
            cpb.update(conexion);
//            cpb.ActualizarCLOB(conexion,"");
            conexion.commit();
            System.out.println("Ole Ole que esto guarda");
          }catch(Exception e){
             e.printStackTrace();
          }
      }
}
