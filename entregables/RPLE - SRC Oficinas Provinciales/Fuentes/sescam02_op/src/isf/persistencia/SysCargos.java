//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SysCargos
// Table: SYS_CARGOS
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SYS_CARGOS
 * Columns for the table:
 * 	(sys_cargos_pk - LONG), 
 * 	(sys_cargos_nombre - STRING), 
 * Primary columns for the table:
 * 	(sys_cargos_pk - LONG) 
 */
public class SysCargos
{
	// Properties
	private long sysCargosPk;
	private boolean sysCargosPkNull=true;
	private boolean sysCargosPkModified=false;
	private String sysCargosNombre;
	private boolean sysCargosNombreNull=true;
	private boolean sysCargosNombreModified=false;
	// Access Method
	/** SysCargosPk
	 * Get the value of the property SysCargosPk.
	 * The column for the database is 'SYS_CARGOS_PK'SysCargosPk.
	 */
	public long getSysCargosPk() {
		return sysCargosPk;
	}
	/** SysCargosPk
	 * Set the value of the property SysCargosPk.
	 * The column for the database is 'SYS_CARGOS_PK'SysCargosPk.
	 */
	public void setSysCargosPk(long _sysCargosPk) {
		this.sysCargosPk=_sysCargosPk;
		this.sysCargosPkModified=true;
		this.sysCargosPkNull=false;
	}
	/** SysCargosPk
	 * Set Null the value of the property SysCargosPk.
	 * The column for the database is 'SYS_CARGOS_PK'SysCargosPk.
	 */
	public void setNullSysCargosPk() {
		this.sysCargosPk=0;
		this.sysCargosPkModified=true;
		this.sysCargosPkNull=true;
	}
	/** SysCargosPk
	 * Sumatory of value of the property SysCargosPk.
	 * The column for the database is 'SYS_CARGOS_PK'SysCargosPk.
	 */
	static public double sumSysCargosPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SYS_CARGOS_PK");
	}
	/** SysCargosPk
	 * Indicates if the value of the property SysCargosPk is null or not.
	 * The column for the database is 'SYS_CARGOS_PK'SysCargosPk.
	 */
	public boolean isNullSysCargosPk() {
		return sysCargosPkNull;
	}
	/** SysCargosNombre
	 * Get the value of the property SysCargosNombre.
	 * The column for the database is 'SYS_CARGOS_NOMBRE'SysCargosNombre.
	 */
	public String getSysCargosNombre() {
		if (sysCargosNombre==null) return "";
		if (sysCargosNombre.compareTo("null")==0) return "";
		return sysCargosNombre;
	}
	/** SysCargosNombre
	 * Set the value of the property SysCargosNombre.
	 * The column for the database is 'SYS_CARGOS_NOMBRE'SysCargosNombre.
	 */
	public void setSysCargosNombre(String _sysCargosNombre) {
		this.sysCargosNombre=_sysCargosNombre;
		this.sysCargosNombreModified=true;
		this.sysCargosNombreNull=false;
	}
	/** SysCargosNombre
	 * Set Null the value of the property SysCargosNombre.
	 * The column for the database is 'SYS_CARGOS_NOMBRE'SysCargosNombre.
	 */
	public void setNullSysCargosNombre() {
		this.sysCargosNombre=null;
		this.sysCargosNombreModified=true;
		this.sysCargosNombreNull=true;
	}
	/** SysCargosNombre
	 * Indicates if the value of the property SysCargosNombre is null or not.
	 * The column for the database is 'SYS_CARGOS_NOMBRE'SysCargosNombre.
	 */
	public boolean isNullSysCargosNombre() {
		return sysCargosNombreNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!sysCargosPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYS_CARGOS_PK");
			ls_values.append((lb_FirstTime?"":",")+sysCargosPk);
			lb_FirstTime = false;
		}
		if (!sysCargosNombreNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SYS_CARGOS_NOMBRE");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(sysCargosNombre)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SYS_CARGOS ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (sysCargosPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYS_CARGOS_PK");
			if(sysCargosPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sysCargosPk);
			lb_FirstTime = false;
		}
		if (sysCargosNombreModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SYS_CARGOS_NOMBRE");
			if(sysCargosNombreNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(sysCargosNombre)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("SYS_CARGOS_PK");
			ls_where.append("="+sysCargosPk);
		} else {
			ls_where.append(" AND "+"SYS_CARGOS_PK");
			ls_where.append("="+sysCargosPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SYS_CARGOS SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("SYS_CARGOS_PK");
			ls_where.append("="+sysCargosPk);
		} else {
			ls_where.append(" AND "+"SYS_CARGOS_PK");
			ls_where.append("="+sysCargosPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SYS_CARGOS WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _sysCargosPk
	 * @return SysCargos - Retrieved object
	 */
	static public SysCargos read(Connection _connection,long _sysCargosPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SysCargos ls_SysCargos=new SysCargos();
		ls_where="SYS_CARGOS_PK = " + _sysCargosPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SYS_CARGOS WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SysCargos.loadResultSet(ls_rs);
		} else {
			ls_SysCargos=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SysCargos;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		sysCargosPk=_rs.getLong("SYS_CARGOS_PK");
		sysCargosPkNull=_rs.wasNull();
		sysCargosNombre=_rs.getString("SYS_CARGOS_NOMBRE");
		sysCargosNombreNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SYS_CARGOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SysCargos mySysCargos;
		Vector mySysCargoses = new Vector();
		while (ls_rs.next()) {
			mySysCargos = new SysCargos();
			mySysCargos.loadResultSet (ls_rs);
			mySysCargoses.addElement(mySysCargos);
		}
		ls_rs.close();
		myStatement.close();
		return mySysCargoses;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SYS_CARGOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SYS_CARGOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SYS_CARGOS ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setSysCargosPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
