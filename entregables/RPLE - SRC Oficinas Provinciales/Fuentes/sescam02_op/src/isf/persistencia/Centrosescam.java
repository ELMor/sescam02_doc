//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: Centrosescam
// Table: CENTROSESCAM
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:CENTROSESCAM
 * Columns for the table:
 * 	(descentro - STRING), 
 * 	(descentrolarga - STRING), 
 * 	(codcentro - DECIMAL), 
 * Primary columns for the table:
 * 	(codcentro - DECIMAL) 
 */
public class Centrosescam
{
	// Properties
	private String descentro;
	private boolean descentroNull=true;
	private boolean descentroModified=false;
	private String descentrolarga;
	private boolean descentrolargaNull=true;
	private boolean descentrolargaModified=false;
	private double codcentro;
	private boolean codcentroNull=true;
	private boolean codcentroModified=false;
	// Access Method
	/** Descentro
	 * Get the value of the property Descentro.
	 * The column for the database is 'DESCENTRO'Descentro.
	 */
	public String getDescentro() {
		if (descentro==null) return "";
		if (descentro.compareTo("null")==0) return "";
		return descentro;
	}
	/** Descentro
	 * Set the value of the property Descentro.
	 * The column for the database is 'DESCENTRO'Descentro.
	 */
	public void setDescentro(String _descentro) {
		this.descentro=_descentro;
		this.descentroModified=true;
		this.descentroNull=false;
	}
	/** Descentro
	 * Set Null the value of the property Descentro.
	 * The column for the database is 'DESCENTRO'Descentro.
	 */
	public void setNullDescentro() {
		this.descentro=null;
		this.descentroModified=true;
		this.descentroNull=true;
	}
	/** Descentro
	 * Indicates if the value of the property Descentro is null or not.
	 * The column for the database is 'DESCENTRO'Descentro.
	 */
	public boolean isNullDescentro() {
		return descentroNull;
	}
	/** Descentrolarga
	 * Get the value of the property Descentrolarga.
	 * The column for the database is 'DESCENTROLARGA'Descentrolarga.
	 */
	public String getDescentrolarga() {
		if (descentrolarga==null) return "";
		if (descentrolarga.compareTo("null")==0) return "";
		return descentrolarga;
	}
	/** Descentrolarga
	 * Set the value of the property Descentrolarga.
	 * The column for the database is 'DESCENTROLARGA'Descentrolarga.
	 */
	public void setDescentrolarga(String _descentrolarga) {
		this.descentrolarga=_descentrolarga;
		this.descentrolargaModified=true;
		this.descentrolargaNull=false;
	}
	/** Descentrolarga
	 * Set Null the value of the property Descentrolarga.
	 * The column for the database is 'DESCENTROLARGA'Descentrolarga.
	 */
	public void setNullDescentrolarga() {
		this.descentrolarga=null;
		this.descentrolargaModified=true;
		this.descentrolargaNull=true;
	}
	/** Descentrolarga
	 * Indicates if the value of the property Descentrolarga is null or not.
	 * The column for the database is 'DESCENTROLARGA'Descentrolarga.
	 */
	public boolean isNullDescentrolarga() {
		return descentrolargaNull;
	}
	/** Codcentro
	 * Get the value of the property Codcentro.
	 * The column for the database is 'CODCENTRO'Codcentro.
	 */
	public double getCodcentro() {
		return codcentro;
	}
	/** Codcentro
	 * Set the value of the property Codcentro.
	 * The column for the database is 'CODCENTRO'Codcentro.
	 */
	public void setCodcentro(double _codcentro) {
		this.codcentro=_codcentro;
		this.codcentroModified=true;
		this.codcentroNull=false;
	}
	/** Codcentro
	 * Set Null the value of the property Codcentro.
	 * The column for the database is 'CODCENTRO'Codcentro.
	 */
	public void setNullCodcentro() {
		this.codcentro=0;
		this.codcentroModified=true;
		this.codcentroNull=true;
	}
	/** Codcentro
	 * Sumatory of value of the property Codcentro.
	 * The column for the database is 'CODCENTRO'Codcentro.
	 */
	static public double sumCodcentro(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"CODCENTRO");
	}
	/** Codcentro
	 * Indicates if the value of the property Codcentro is null or not.
	 * The column for the database is 'CODCENTRO'Codcentro.
	 */
	public boolean isNullCodcentro() {
		return codcentroNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!descentroNull) {
			ls_columns.append((lb_FirstTime?"":",")+"DESCENTRO");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(descentro)+"'");
			lb_FirstTime = false;
		}
		if (!descentrolargaNull) {
			ls_columns.append((lb_FirstTime?"":",")+"DESCENTROLARGA");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(descentrolarga)+"'");
			lb_FirstTime = false;
		}
		if (!codcentroNull) {
			ls_columns.append((lb_FirstTime?"":",")+"CODCENTRO");
			ls_values.append((lb_FirstTime?"":",")+codcentro);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO CENTROSESCAM ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (descentroModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("DESCENTRO");
			if(descentroNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(descentro)+"'");
			lb_FirstTime = false;
		}
		if (descentrolargaModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("DESCENTROLARGA");
			if(descentrolargaNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(descentrolarga)+"'");
			lb_FirstTime = false;
		}
		if (codcentroModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("CODCENTRO");
			if(codcentroNull) {
					ls_setValues.append("=null");
			} else ls_setValues.append("="+codcentro);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("CODCENTRO");
			ls_where.append("="+codcentro);
		} else {
			ls_where.append(" AND "+"CODCENTRO");
			ls_where.append("="+codcentro);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE CENTROSESCAM SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("CODCENTRO");
			ls_where.append("="+codcentro);
		} else {
			ls_where.append(" AND "+"CODCENTRO");
			ls_where.append("="+codcentro);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE CENTROSESCAM WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,double _codcentro
	 * @return Centrosescam - Retrieved object
	 */
	static public Centrosescam read(Connection _connection,double _codcentro) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		Centrosescam ls_Centrosescam=new Centrosescam();
		ls_where="CODCENTRO = " + _codcentro;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM CENTROSESCAM WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_Centrosescam.loadResultSet(ls_rs);
		} else {
			ls_Centrosescam=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_Centrosescam;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		descentro=_rs.getString("DESCENTRO");
		descentroNull=_rs.wasNull();
		descentrolarga=_rs.getString("DESCENTROLARGA");
		descentrolargaNull=_rs.wasNull();
		codcentro=_rs.getDouble("CODCENTRO");
		codcentroNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM CENTROSESCAM ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		Centrosescam myCentrosescam;
		Vector myCentrosescames = new Vector();
		while (ls_rs.next()) {
			myCentrosescam = new Centrosescam();
			myCentrosescam.loadResultSet (ls_rs);
			myCentrosescames.addElement(myCentrosescam);
		}
		ls_rs.close();
		myStatement.close();
		return myCentrosescames;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM CENTROSESCAM ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM CENTROSESCAM ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM CENTROSESCAM ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
	}
}
