//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesClasificaciones
// Table: SES_CLASIFICACIONES
// Package: isf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_CLASIFICACIONES
 * Columns for the table:
 * 	(sesclas_pk - LONG), 
 * 	(proc_pk - LONG), 
 * 	(sesclas_pkpadre - LONG), 
 * 	(sesclas_niveles - LONG), 
 * 	(sesclas_desc - STRING), 
 * 	(sesclas_tipo - LONG), 
 * Primary columns for the table:
 * 	(sesclas_pk - LONG) 
 */
public class SesClasificaciones
{
	// Properties
	private long sesclasPk;
	private boolean sesclasPkNull=true;
	private boolean sesclasPkModified=false;
	private long procPk;
	private boolean procPkNull=true;
	private boolean procPkModified=false;
	private long sesclasPkpadre;
	private boolean sesclasPkpadreNull=true;
	private boolean sesclasPkpadreModified=false;
	private long sesclasNiveles;
	private boolean sesclasNivelesNull=true;
	private boolean sesclasNivelesModified=false;
	private String sesclasDesc;
	private boolean sesclasDescNull=true;
	private boolean sesclasDescModified=false;
	private long sesclasTipo;
	private boolean sesclasTipoNull=true;
	private boolean sesclasTipoModified=false;
	// Access Method
	/** SesclasPk
	 * Get the value of the property SesclasPk.
	 * The column for the database is 'SESCLAS_PK'SesclasPk.
	 */
	public long getSesclasPk() {
		return sesclasPk;
	}
	/** SesclasPk
	 * Set the value of the property SesclasPk.
	 * The column for the database is 'SESCLAS_PK'SesclasPk.
	 */
	public void setSesclasPk(long _sesclasPk) {
		this.sesclasPk=_sesclasPk;
		this.sesclasPkModified=true;
		this.sesclasPkNull=false;
	}
	/** SesclasPk
	 * Set Null the value of the property SesclasPk.
	 * The column for the database is 'SESCLAS_PK'SesclasPk.
	 */
	public void setNullSesclasPk() {
		this.sesclasPk=0;
		this.sesclasPkModified=true;
		this.sesclasPkNull=true;
	}
	/** SesclasPk
	 * Sumatory of value of the property SesclasPk.
	 * The column for the database is 'SESCLAS_PK'SesclasPk.
	 */
	static public double sumSesclasPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SESCLAS_PK");
	}
	/** SesclasPk
	 * Indicates if the value of the property SesclasPk is null or not.
	 * The column for the database is 'SESCLAS_PK'SesclasPk.
	 */
	public boolean isNullSesclasPk() {
		return sesclasPkNull;
	}
	/** ProcPk
	 * Get the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public long getProcPk() {
		return procPk;
	}
	/** ProcPk
	 * Set the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public void setProcPk(long _procPk) {
		this.procPk=_procPk;
		this.procPkModified=true;
		this.procPkNull=false;
	}
	/** ProcPk
	 * Set Null the value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public void setNullProcPk() {
		this.procPk=0;
		this.procPkModified=true;
		this.procPkNull=true;
	}
	/** ProcPk
	 * Sumatory of value of the property ProcPk.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	static public double sumProcPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROC_PK");
	}
	/** ProcPk
	 * Indicates if the value of the property ProcPk is null or not.
	 * The column for the database is 'PROC_PK'ProcPk.
	 */
	public boolean isNullProcPk() {
		return procPkNull;
	}
	/** SesclasPkpadre
	 * Get the value of the property SesclasPkpadre.
	 * The column for the database is 'SESCLAS_PKPADRE'SesclasPkpadre.
	 */
	public long getSesclasPkpadre() {
		return sesclasPkpadre;
	}
	/** SesclasPkpadre
	 * Set the value of the property SesclasPkpadre.
	 * The column for the database is 'SESCLAS_PKPADRE'SesclasPkpadre.
	 */
	public void setSesclasPkpadre(long _sesclasPkpadre) {
		this.sesclasPkpadre=_sesclasPkpadre;
		this.sesclasPkpadreModified=true;
		this.sesclasPkpadreNull=false;
	}
	/** SesclasPkpadre
	 * Set Null the value of the property SesclasPkpadre.
	 * The column for the database is 'SESCLAS_PKPADRE'SesclasPkpadre.
	 */
	public void setNullSesclasPkpadre() {
		this.sesclasPkpadre=0;
		this.sesclasPkpadreModified=true;
		this.sesclasPkpadreNull=true;
	}
	/** SesclasPkpadre
	 * Sumatory of value of the property SesclasPkpadre.
	 * The column for the database is 'SESCLAS_PKPADRE'SesclasPkpadre.
	 */
	static public double sumSesclasPkpadre(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SESCLAS_PKPADRE");
	}
	/** SesclasPkpadre
	 * Indicates if the value of the property SesclasPkpadre is null or not.
	 * The column for the database is 'SESCLAS_PKPADRE'SesclasPkpadre.
	 */
	public boolean isNullSesclasPkpadre() {
		return sesclasPkpadreNull;
	}
	/** SesclasNiveles
	 * Get the value of the property SesclasNiveles.
	 * The column for the database is 'SESCLAS_NIVELES'SesclasNiveles.
	 */
	public long getSesclasNiveles() {
		return sesclasNiveles;
	}
	/** SesclasNiveles
	 * Set the value of the property SesclasNiveles.
	 * The column for the database is 'SESCLAS_NIVELES'SesclasNiveles.
	 */
	public void setSesclasNiveles(long _sesclasNiveles) {
		this.sesclasNiveles=_sesclasNiveles;
		this.sesclasNivelesModified=true;
		this.sesclasNivelesNull=false;
	}
	/** SesclasNiveles
	 * Set Null the value of the property SesclasNiveles.
	 * The column for the database is 'SESCLAS_NIVELES'SesclasNiveles.
	 */
	public void setNullSesclasNiveles() {
		this.sesclasNiveles=0;
		this.sesclasNivelesModified=true;
		this.sesclasNivelesNull=true;
	}
	/** SesclasNiveles
	 * Sumatory of value of the property SesclasNiveles.
	 * The column for the database is 'SESCLAS_NIVELES'SesclasNiveles.
	 */
	static public double sumSesclasNiveles(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SESCLAS_NIVELES");
	}
	/** SesclasNiveles
	 * Indicates if the value of the property SesclasNiveles is null or not.
	 * The column for the database is 'SESCLAS_NIVELES'SesclasNiveles.
	 */
	public boolean isNullSesclasNiveles() {
		return sesclasNivelesNull;
	}
	/** SesclasDesc
	 * Get the value of the property SesclasDesc.
	 * The column for the database is 'SESCLAS_DESC'SesclasDesc.
	 */
	public String getSesclasDesc() {
		if (sesclasDesc==null) return "";
		if (sesclasDesc.compareTo("null")==0) return "";
		return sesclasDesc;
	}
	/** SesclasDesc
	 * Set the value of the property SesclasDesc.
	 * The column for the database is 'SESCLAS_DESC'SesclasDesc.
	 */
	public void setSesclasDesc(String _sesclasDesc) {
		this.sesclasDesc=_sesclasDesc;
		this.sesclasDescModified=true;
		this.sesclasDescNull=false;
	}
	/** SesclasDesc
	 * Set Null the value of the property SesclasDesc.
	 * The column for the database is 'SESCLAS_DESC'SesclasDesc.
	 */
	public void setNullSesclasDesc() {
		this.sesclasDesc=null;
		this.sesclasDescModified=true;
		this.sesclasDescNull=true;
	}
	/** SesclasDesc
	 * Indicates if the value of the property SesclasDesc is null or not.
	 * The column for the database is 'SESCLAS_DESC'SesclasDesc.
	 */
	public boolean isNullSesclasDesc() {
		return sesclasDescNull;
	}
	/** SesclasTipo
	 * Get the value of the property SesclasTipo.
	 * The column for the database is 'SESCLAS_TIPO'SesclasTipo.
	 */
	public long getSesclasTipo() {
		return sesclasTipo;
	}
	/** SesclasTipo
	 * Set the value of the property SesclasTipo.
	 * The column for the database is 'SESCLAS_TIPO'SesclasTipo.
	 */
	public void setSesclasTipo(long _sesclasTipo) {
		this.sesclasTipo=_sesclasTipo;
		this.sesclasTipoModified=true;
		this.sesclasTipoNull=false;
	}
	/** SesclasTipo
	 * Set Null the value of the property SesclasTipo.
	 * The column for the database is 'SESCLAS_TIPO'SesclasTipo.
	 */
	public void setNullSesclasTipo() {
		this.sesclasTipo=0;
		this.sesclasTipoModified=true;
		this.sesclasTipoNull=true;
	}
	/** SesclasTipo
	 * Sumatory of value of the property SesclasTipo.
	 * The column for the database is 'SESCLAS_TIPO'SesclasTipo.
	 */
	static public double sumSesclasTipo(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"SESCLAS_TIPO");
	}
	/** SesclasTipo
	 * Indicates if the value of the property SesclasTipo is null or not.
	 * The column for the database is 'SESCLAS_TIPO'SesclasTipo.
	 */
	public boolean isNullSesclasTipo() {
		return sesclasTipoNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!sesclasPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SESCLAS_PK");
			ls_values.append((lb_FirstTime?"":",")+sesclasPk);
			lb_FirstTime = false;
		}
		if (!procPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROC_PK");
			ls_values.append((lb_FirstTime?"":",")+procPk);
			lb_FirstTime = false;
		}
		if (!sesclasPkpadreNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SESCLAS_PKPADRE");
			ls_values.append((lb_FirstTime?"":",")+sesclasPkpadre);
			lb_FirstTime = false;
		}
		if (!sesclasNivelesNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SESCLAS_NIVELES");
			ls_values.append((lb_FirstTime?"":",")+sesclasNiveles);
			lb_FirstTime = false;
		}
		if (!sesclasDescNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SESCLAS_DESC");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(sesclasDesc)+"'");
			lb_FirstTime = false;
		}
		if (!sesclasTipoNull) {
			ls_columns.append((lb_FirstTime?"":",")+"SESCLAS_TIPO");
			ls_values.append((lb_FirstTime?"":",")+sesclasTipo);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_CLASIFICACIONES ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (sesclasPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SESCLAS_PK");
			if(sesclasPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sesclasPk);
			lb_FirstTime = false;
		}
		if (procPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROC_PK");
			if(procPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+procPk);
			lb_FirstTime = false;
		}
		if (sesclasPkpadreModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SESCLAS_PKPADRE");
			if(sesclasPkpadreNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sesclasPkpadre);
			lb_FirstTime = false;
		}
		if (sesclasNivelesModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SESCLAS_NIVELES");
			if(sesclasNivelesNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sesclasNiveles);
			lb_FirstTime = false;
		}
		if (sesclasDescModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SESCLAS_DESC");
			if(sesclasDescNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(sesclasDesc)+"'");
			lb_FirstTime = false;
		}
		if (sesclasTipoModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("SESCLAS_TIPO");
			if(sesclasTipoNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+sesclasTipo);
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) { 
			ls_where.append("SESCLAS_PK");
			ls_where.append("="+sesclasPk);
		} else {
			ls_where.append(" AND "+"SESCLAS_PK");
			ls_where.append("="+sesclasPk);
		}
		lb_FirstTime = false;
		
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_CLASIFICACIONES SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;
		
		if (lb_FirstTime ) { 
			ls_where.append("SESCLAS_PK");
			ls_where.append("="+sesclasPk);
		} else {
			ls_where.append(" AND "+"SESCLAS_PK");
			ls_where.append("="+sesclasPk);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_CLASIFICACIONES WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier 
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _sesclasPk
	 * @return SesClasificaciones - Retrieved object
	 */
	static public SesClasificaciones read(Connection _connection,long _sesclasPk) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesClasificaciones ls_SesClasificaciones=new SesClasificaciones();
		ls_where="SESCLAS_PK = " + _sesclasPk;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_CLASIFICACIONES WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesClasificaciones.loadResultSet(ls_rs);
		} else {
			ls_SesClasificaciones=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesClasificaciones;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		sesclasPk=_rs.getLong("SESCLAS_PK");
		sesclasPkNull=_rs.wasNull();
		procPk=_rs.getLong("PROC_PK");
		procPkNull=_rs.wasNull();
		sesclasPkpadre=_rs.getLong("SESCLAS_PKPADRE");
		sesclasPkpadreNull=_rs.wasNull();
		sesclasNiveles=_rs.getLong("SESCLAS_NIVELES");
		sesclasNivelesNull=_rs.wasNull();
		sesclasDesc=_rs.getString("SESCLAS_DESC");
		sesclasDescNull=_rs.wasNull();
		sesclasTipo=_rs.getLong("SESCLAS_TIPO");
		sesclasTipoNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_CLASIFICACIONES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesClasificaciones mySesClasificaciones;
		Vector mySesClasificacioneses = new Vector();
		while (ls_rs.next()) {
			mySesClasificaciones = new SesClasificaciones();
			mySesClasificaciones.loadResultSet (ls_rs);
			mySesClasificacioneses.addElement(mySesClasificaciones);
		}
		ls_rs.close();
		myStatement.close();
		return mySesClasificacioneses;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_CLASIFICACIONES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_CLASIFICACIONES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_CLASIFICACIONES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setSesclasPk(myPool.getSequence(this.getClass().getName(),profile));
	}
}
