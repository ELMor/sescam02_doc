//////////////////////////////////////////////////////////
// Generation for Java 1.0
// Group Common Objects
// Class: SesLocalidades
// Table: SES_LOCALIDADES
// Package: ssf.persistencia
//////////////////////////////////////////////////////////

// Package declaration
package isf.persistencia;
// Imports
import java.sql.*;
import java.io.*;
import isf.persistence.SQLLanguageHelper;
import java.util.Vector;
import isf.db.Pool;

/**
 * Persistent class for table:SES_LOCALIDADES
 * Columns for the table:
 * 	(pk_localidad - LONG),
 * 	(provincia_pk - LONG),
 * 	(localidad - STRING),
 * Primary columns for the table:
 * 	(pk_localidad - LONG)
 */
public class SesLocalidades
{
	// Properties
	private long pkLocalidad;
	private boolean pkLocalidadNull=true;
	private boolean pkLocalidadModified=false;
	private long provinciaPk;
	private boolean provinciaPkNull=true;
	private boolean provinciaPkModified=false;
	private String localidad;
	private boolean localidadNull=true;
	private boolean localidadModified=false;
	// Access Method
	/** PkLocalidad
	 * Get the value of the property PkLocalidad.
	 * The column for the database is 'PK_LOCALIDAD'PkLocalidad.
	 */
	public long getPkLocalidad() {
		return pkLocalidad;
	}
	/** PkLocalidad
	 * Set the value of the property PkLocalidad.
	 * The column for the database is 'PK_LOCALIDAD'PkLocalidad.
	 */
	public void setPkLocalidad(long _pkLocalidad) {
		this.pkLocalidad=_pkLocalidad;
		this.pkLocalidadModified=true;
		this.pkLocalidadNull=false;
	}
	/** PkLocalidad
	 * Set Null the value of the property PkLocalidad.
	 * The column for the database is 'PK_LOCALIDAD'PkLocalidad.
	 */
	public void setNullPkLocalidad() {
		this.pkLocalidad=0;
		this.pkLocalidadModified=true;
		this.pkLocalidadNull=true;
	}
	/** PkLocalidad
	 * Sumatory of value of the property PkLocalidad.
	 * The column for the database is 'PK_LOCALIDAD'PkLocalidad.
	 */
	static public double sumPkLocalidad(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PK_LOCALIDAD");
	}
	/** PkLocalidad
	 * Indicates if the value of the property PkLocalidad is null or not.
	 * The column for the database is 'PK_LOCALIDAD'PkLocalidad.
	 */
	public boolean isNullPkLocalidad() {
		return pkLocalidadNull;
	}
	/** ProvinciaPk
	 * Get the value of the property ProvinciaPk.
	 * The column for the database is 'PROVINCIA_PK'ProvinciaPk.
	 */
	public long getProvinciaPk() {
		return provinciaPk;
	}
	/** ProvinciaPk
	 * Set the value of the property ProvinciaPk.
	 * The column for the database is 'PROVINCIA_PK'ProvinciaPk.
	 */
	public void setProvinciaPk(long _provinciaPk) {
		this.provinciaPk=_provinciaPk;
		this.provinciaPkModified=true;
		this.provinciaPkNull=false;
	}
	/** ProvinciaPk
	 * Set Null the value of the property ProvinciaPk.
	 * The column for the database is 'PROVINCIA_PK'ProvinciaPk.
	 */
	public void setNullProvinciaPk() {
		this.provinciaPk=0;
		this.provinciaPkModified=true;
		this.provinciaPkNull=true;
	}
	/** ProvinciaPk
	 * Sumatory of value of the property ProvinciaPk.
	 * The column for the database is 'PROVINCIA_PK'ProvinciaPk.
	 */
	static public double sumProvinciaPk(Connection _connection,String _where) throws SQLException {
		return sumByCriteria(_connection, _where,"PROVINCIA_PK");
	}
	/** ProvinciaPk
	 * Indicates if the value of the property ProvinciaPk is null or not.
	 * The column for the database is 'PROVINCIA_PK'ProvinciaPk.
	 */
	public boolean isNullProvinciaPk() {
		return provinciaPkNull;
	}
	/** Localidad
	 * Get the value of the property Localidad.
	 * The column for the database is 'LOCALIDAD'Localidad.
	 */
	public String getLocalidad() {
		if (localidad==null) return "";
		if (localidad.compareTo("null")==0) return "";
		return localidad;
	}
	/** Localidad
	 * Set the value of the property Localidad.
	 * The column for the database is 'LOCALIDAD'Localidad.
	 */
	public void setLocalidad(String _localidad) {
		this.localidad=_localidad;
		this.localidadModified=true;
		this.localidadNull=false;
	}
	/** Localidad
	 * Set Null the value of the property Localidad.
	 * The column for the database is 'LOCALIDAD'Localidad.
	 */
	public void setNullLocalidad() {
		this.localidad=null;
		this.localidadModified=true;
		this.localidadNull=true;
	}
	/** Localidad
	 * Indicates if the value of the property Localidad is null or not.
	 * The column for the database is 'LOCALIDAD'Localidad.
	 */
	public boolean isNullLocalidad() {
		return localidadNull;
	}


	// Persistent Method
	/**
	 * Insert the current object in the database.
	 * @param connection to use in insert action
	 * @return true - OK, false - not necessary insert because not modified values
	 */
	public boolean insert(Connection _connection) throws SQLException {
		StringBuffer ls_columns = new StringBuffer();
		StringBuffer ls_values  = new StringBuffer();
		boolean lb_FirstTime = true;
		if (!pkLocalidadNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PK_LOCALIDAD");
			ls_values.append((lb_FirstTime?"":",")+pkLocalidad);
			lb_FirstTime = false;
		}
		if (!provinciaPkNull) {
			ls_columns.append((lb_FirstTime?"":",")+"PROVINCIA_PK");
			ls_values.append((lb_FirstTime?"":",")+provinciaPk);
			lb_FirstTime = false;
		}
		if (!localidadNull) {
			ls_columns.append((lb_FirstTime?"":",")+"LOCALIDAD");
			ls_values.append((lb_FirstTime?"":",") + "'" + SQLLanguageHelper.escapeQuote(localidad)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		Statement myStatement = null;
		try {
			myStatement = _connection.createStatement();
			myStatement.executeUpdate("INSERT INTO SES_LOCALIDADES ("+ls_columns.toString()+") VALUES ("+ls_values.toString()+")");
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Update the current object in the database.
	 * @param connection to use in update action
	 * @return true - OK, false - not necessary update because not modified values
	 */
	public boolean update(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_setValues = new StringBuffer();
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas = 0;
		Statement myStatement = null;
		long ll_recordCount = 0;

		if (pkLocalidadModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PK_LOCALIDAD");
			if(pkLocalidadNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+pkLocalidad);
			lb_FirstTime = false;
		}
		if (provinciaPkModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("PROVINCIA_PK");
			if(provinciaPkNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("="+provinciaPk);
			lb_FirstTime = false;
		}
		if (localidadModified) {
			if (!lb_FirstTime) ls_setValues.append(",");
			ls_setValues.append("LOCALIDAD");
			if(localidadNull) {
				ls_setValues.append("=null");
			} else ls_setValues.append("='"+SQLLanguageHelper.escapeQuote(localidad)+"'");
			lb_FirstTime = false;
		}
		if (lb_FirstTime) {
			return false;
		}
		lb_FirstTime=true;
		if (lb_FirstTime ) {
			ls_where.append("PK_LOCALIDAD");
			ls_where.append("="+pkLocalidad);
		} else {
			ls_where.append(" AND "+"PK_LOCALIDAD");
			ls_where.append("="+pkLocalidad);
		}
		lb_FirstTime = false;

		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("UPDATE SES_LOCALIDADES SET "+ls_setValues.toString()+" WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Delete the current object in the database.
	 * @param connection to use in delete action
	 * @return true OK, false Error
	 */
	public boolean delete(Connection _connection) throws SQLException, isf.persistence.GcgcCorruptedRegisterException {
		StringBuffer ls_where  = new StringBuffer();
		boolean lb_FirstTime = true;
		long ll_filas=0;
		Statement myStatement = null;
		long ll_recordCount = 0;

		if (lb_FirstTime ) {
			ls_where.append("PK_LOCALIDAD");
			ls_where.append("="+pkLocalidad);
		} else {
			ls_where.append(" AND "+"PK_LOCALIDAD");
			ls_where.append("="+pkLocalidad);
		}
		lb_FirstTime = false;
		try {
			myStatement = _connection.createStatement();
			ll_filas = myStatement.executeUpdate("DELETE SES_LOCALIDADES WHERE "+ls_where.toString());
			myStatement.close();
		}
		catch (SQLException e) {
			if (myStatement != null)
				myStatement.close();
			throw e;
		}
		return true;
	}
	/**
	 * Retrieve an object in the database for the param identifier
	 * @param connection - the conection for retrieve object
	 * @param Identifiers - ,long _pkLocalidad
	 * @return SesLocalidades - Retrieved object
	 */
	static public SesLocalidades read(Connection _connection,long _pkLocalidad) throws SQLException {
		String ls_where  = new String();
		ResultSet ls_rs;
		SesLocalidades ls_SesLocalidades=new SesLocalidades();
		ls_where="PK_LOCALIDAD = " + _pkLocalidad;
		Statement myStatement = _connection.createStatement();
		ls_rs = myStatement.executeQuery("SELECT * FROM SES_LOCALIDADES WHERE " + ls_where );
		if (ls_rs.next()) {
			ls_SesLocalidades.loadResultSet(ls_rs);
		} else {
			ls_SesLocalidades=null;
		}
		ls_rs.close();
		myStatement.close();
		return ls_SesLocalidades;
	}
	public void loadResultSet(ResultSet _rs) throws SQLException{
		pkLocalidad=_rs.getLong("PK_LOCALIDAD");
		pkLocalidadNull=_rs.wasNull();
		provinciaPk=_rs.getLong("PROVINCIA_PK");
		provinciaPkNull=_rs.wasNull();
		localidad=_rs.getString("LOCALIDAD");
		localidadNull=_rs.wasNull();
	}
	/**
	 * Retrieve an objects list in the database for the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @param sort - condition for sort retrieve objects list
	 * @return vector - objects list retrieved
	 */
	static public Vector search(Connection _connection,String _where,String _sort) throws SQLException {
		//Construimos la query.
		String query = "SELECT * FROM SES_LOCALIDADES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		if ((_sort != null) && (_sort.trim().compareTo("")!=0)) {
			query +=" ORDER BY " + _sort + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		// Procesamos el ResultSet.
		SesLocalidades mySesLocalidades;
		Vector mySesLocalidadeses = new Vector();
		while (ls_rs.next()) {
			mySesLocalidades = new SesLocalidades();
			mySesLocalidades.loadResultSet (ls_rs);
			mySesLocalidadeses.addElement(mySesLocalidades);
		}
		ls_rs.close();
		myStatement.close();
		return mySesLocalidadeses;
	}
	/**
	 * Retrieve the number of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return long - number of objects found. 0 no one
	 */
	static public long countByCriteria(Connection _connection,String _where) throws SQLException {
		//Construimos la query.
		String query = "SELECT COUNT(*) AS RECORD_COUNT FROM SES_LOCALIDADES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		long ll_recordCount;
		// Procesamos el ResultSet.
		ls_rs.next();
		ll_recordCount = ls_rs.getLong("RECORD_COUNT");
		ls_rs.close();
		myStatement.close();
		return ll_recordCount;
	}
	/**
	 * Retrieve the sumatory for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return double - Sumatory result
	 */
	static private double sumByCriteria(Connection _connection,String _where,String _columnName) throws SQLException {
		//Construimos la query.
		String query = "SELECT SUM(" + _columnName + ") AS SUMATORY FROM SES_LOCALIDADES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		double ld_sumatory;
		// Procesamos el ResultSet.
		ls_rs.next();
		ld_sumatory = ls_rs.getDouble("SUMATORY");
		ls_rs.close();
		myStatement.close();
		return ld_sumatory;
	}
	/**
	 * Retrieve the maximum or minimum value for an attribute of objects that accomplishe the condition specified
	 * @param connection - the conection for retrieve objects list
	 * @param where - condition for retrieve objects list
	 * @return Object - Maximum or minimum result.
	 * this method returns datatypes according to this list:<ul>
		 <li>for any numeric type: java.lang.Double</li>
		 <li>for date and/or times: java.sql.Date</li>
		 <li>for any text type: java.lang.String</li>
		 </ul>
	*/
	static private Object functionByCriteria(Connection _connection, String _where, String _columnName, String _function) throws SQLException {
		//Construimos la query.
		String query = "SELECT "+_function+"(" + _columnName + ") AS CALCULATION FROM SES_LOCALIDADES ";
		if ((_where != null) && (_where.trim().compareTo("")!=0)) {
			query +="WHERE " + _where + " ";
		}
		// Creamos el Statement y el ResultSet.
		Statement myStatement = _connection.createStatement();
		ResultSet ls_rs = myStatement.executeQuery (query);
		Object obj_result = null;
		// Procesamos el ResultSet.
		if (ls_rs.next())
		{
			 obj_result = ls_rs.getObject("CALCULATION");
			 // Traduccion de clases usadas por el driver jdbc de Oracle a las usadas en GCCOM
			 if (obj_result instanceof java.math.BigDecimal)
			 {
				  obj_result = new Double(((java.math.BigDecimal)obj_result).doubleValue());
			 }
			 else if (obj_result instanceof java.sql.Timestamp)
			 {
				  obj_result = new java.sql.Date(((java.sql.Timestamp)obj_result).getTime());
			 }
		}
		ls_rs.close();
		myStatement.close();
		return obj_result;
	}
	static public Object maxByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MAX");
	}
	static public Object minByCriteria(Connection _connection, String _where, String _columnName) throws SQLException {
		return functionByCriteria(_connection, _where, _columnName, "MIN");
	}
	/**
	 * Create the unique identifier for the object.
	 */
	public void newId(String profile) throws SQLException {
		Pool myPool=Pool.getInstance();
		setPkLocalidad(myPool.getSequence(this.getClass().getName(),profile));
	}
}
