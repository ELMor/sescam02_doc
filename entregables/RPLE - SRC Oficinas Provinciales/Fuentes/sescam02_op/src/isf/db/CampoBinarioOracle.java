/*

*   Copyright ( 1999 Uni�n Fenosa International Software Factory,

*   S.A.. All Rights Reserved.

*

*   This software is the confidential and proprietary information of

*   Uni�n Fenosa International Software Factory, S.A.. You shall not

*   disclose such confidential information and shall not accordance

*   with the terms of the license agreement you entered into with

*   I.S.F..

*

*/
package isf.db;

import oracle.jdbc.driver.*;

import oracle.sql.*;

import java.io.*;

import java.sql.*;


/**
   Prop�sito: Tratamiento de los campos binarios en bd Oracle.
   @author PSN, JDZ
   @version 2.0
*/
public class CampoBinarioOracle extends CampoBinario {
    /**
            Recupera el contenido de un campo binario
            @param                        OutputStream out         Stream por el que sale el binario
            @param                        Connection conexion conexi�n de BD a utilizar
            @param                        String Str_tabla                 Tabla de la que se que quiere leer
            @param                        String Str_campo                 nombre del campo que tiene el binario
            @param                        String Str_condicion         condicion que debe cumplir la fila que se quiere modificar
            @return                        void
            @exception                SQLException, Exception
            @since                        29-11-2000

    */
    public void obtenerCampoBinario(OutputStream out, Connection conexion,
        String Str_tabla, String Str_campo, String Str_condicion)
        throws SQLException, Exception {
        Statement sentencia = null;
        ResultSet resultado = null;
        byte[] buffer;
        int int_i = 0;
        int int_bytesLeidos = 0;
        BLOB blob;
        InputStream blobInputStream = null;
        String Str_sentencia = null;
        int int_numeroFilasProcesadas = 0;

        try {
            sentencia = conexion.createStatement();

            Str_sentencia = "SELECT " + Str_campo + " FROM " + Str_tabla +
                " WHERE " + Str_condicion;
            resultado = sentencia.executeQuery(Str_sentencia);

            if (resultado.next()) {
                blob = ((OracleResultSet) resultado).getBLOB(Str_campo);
                blobInputStream = blob.getBinaryStream();
                buffer = new byte[10 * 1024];

                while ((int_bytesLeidos = blobInputStream.read(buffer)) != -1) {
                    out.write(buffer, 0, int_bytesLeidos);
                }
            }
        } catch (SQLException eSql) {
            eSql.printStackTrace();
            throw eSql;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            sentencia.close();

            if (resultado != null) {
                resultado.close();
            }

            if (blobInputStream != null) {
                blobInputStream.close();
            }
        }
    }
     // obtenerCampoBinario

    /**
      Recupera el contenido de un campo BLOB y lo inserta en otro
      @param                Connection conexion conexi�n de BD a utilizar
      @param                String Str_tablaOrigen                 tabla origen
      @param                String Str_campoOrigen                  nombre del campo origen que tiene el binario
      @param                String Str_condicionOrigen         condicion de la fila de origen
      @param                String Str_tablaDestino                 tabla origen
      @param                String Str_campoDestino                  nombre del campo destino que tiene el binario
      @param                String Str_condicionDestino         condicion de la fila de origen
      @return                void
      @exception        SQLException, Exception
      @since                29-11-2000
    */
    public void copiarCampoBinario(Connection conexion, String Str_tablaOrigen,
        String Str_campoOrigen, String Str_condicionOrigen,
        String Str_tablaDestino, String Str_campoDestino,
        String Str_condicionDestino) throws SQLException, Exception {
        byte[] buffer = new byte[10 * 1024];
        byte[] bufferIntermedio;
        int int_i = 0;
        int int_bytesLeidos = 0;
        BLOB blobOrigen;
        BLOB blobDestino;
        InputStream blobInputStream = null;
        ResultSet resOrigen = null;
        ResultSet resDestino = null;
        Statement sentencia = null;
        OracleCallableStatement mandatoLocal = null;
        String Str_sentenciaOrigen = null;
        String Str_sentenciaDestino = null;
        String Str_sentencia = null;
        int int_numeroFilasProcesadas = 0;

        Str_sentenciaOrigen = "SELECT " + Str_campoOrigen + " FROM " +
            Str_tablaOrigen + " WHERE " + Str_condicionOrigen;

        Str_sentenciaDestino = "SELECT " + Str_campoDestino + " FROM " +
            Str_tablaDestino + " WHERE " + Str_condicionDestino +
            " FOR UPDATE";

        try {
            sentencia = conexion.createStatement();

            Str_sentencia = "UPDATE " + Str_tablaDestino + " SET " +
                Str_campoDestino + " = empty_blob()" + " WHERE " +
                Str_condicionDestino;

            int_numeroFilasProcesadas = sentencia.executeUpdate(Str_sentencia);

            // Si ya existe el registro -> int_numeroFilasProcesadas > 0
            if (int_numeroFilasProcesadas > 0) {
                resOrigen = sentencia.executeQuery(Str_sentenciaOrigen);

                if (resOrigen.next()) {
                    blobOrigen = ((OracleResultSet) resOrigen).getBLOB(Str_campoOrigen);
                    blobInputStream = blobOrigen.getBinaryStream();
                    resDestino = sentencia.executeQuery(Str_sentenciaDestino);

                    if (resDestino.next()) {
                        blobDestino = ((OracleResultSet) resDestino).getBLOB(Str_campoDestino);
                        mandatoLocal = (OracleCallableStatement) conexion.prepareCall(
                                "begin dbms_lob.write(:1, :2, :3, :4); end;");
                        mandatoLocal.registerOutParameter(1, OracleTypes.BLOB);

                        int offset = 1;

                        while ((int_bytesLeidos = blobInputStream.read(buffer)) != -1) {
                            mandatoLocal.setBLOB(1, blobDestino);
                            mandatoLocal.setInt(2, int_bytesLeidos);
                            mandatoLocal.setInt(3, offset);
                            mandatoLocal.setBytes(4, buffer);
                            mandatoLocal.executeUpdate();
                            blobDestino = mandatoLocal.getBLOB(1);
                            offset = offset + int_bytesLeidos;
                        }
                    }
                }
            }
        } catch (SQLException eSql) {
            eSql.printStackTrace();
            throw eSql;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            sentencia.close();

            if (mandatoLocal != null) {
                mandatoLocal.close();
            }

            if (blobInputStream != null) {
                blobInputStream.close();
            }

            if (resDestino != null) {
                resDestino.close();
            }

            if (resOrigen != null) {
                resOrigen.close();
            }
        }
    }
     // copiarCampoBinario

    /**
      Actualiza el contenido de un campo BLOB
    @param                String Str_fichero         Fichero a insertar
      @param                Connection conexion conexi�n de BD a utilizar
      @param                String Str_tabla        tabla donde est� el campo binario
      @param                String Str_campo          nombre del campo que tiene el binario
      @param                String Str_condicion condicion de la fila a modificar
      @return                void
      @exception        SQLException, Exception
      @since                16-11-2000

    */
    public void escribirCampoBinario(String Str_fichero, Connection conexion,
        String Str_tabla, String Str_campo, String Str_condicion)
        throws SQLException, Exception {
        byte[] buffer;
        int int_i = 0;
        int int_bytesLeidos = 0;
        BLOB blob;
        File fichero;
        InputStream inputStream = null;
        int offset = 1;
        int int_tamanoOptimo = 3000; // tama�o �ptimo 3000 bytes
        Statement sentencia = null;
        OracleCallableStatement mandatoLocal = null;
        ResultSet resultado = null;
        String Str_sentencia = null;
        int int_numeroFilasProcesadas = 0;

        try {
            sentencia = conexion.createStatement();
            Str_sentencia = "UPDATE " + Str_tabla + " SET " + Str_campo +
                " = empty_blob()" + " WHERE " + Str_condicion;

            int_numeroFilasProcesadas = sentencia.executeUpdate(Str_sentencia);

            // Si ya existe el registro -> int_numeroFilasProcesadas > 0
            if (int_numeroFilasProcesadas > 0) {
                fichero = new File(Str_fichero);
                inputStream = new FileInputStream(fichero);

                // Inicializar el buffer
                buffer = new byte[int_tamanoOptimo];

                Str_sentencia = "SELECT " + Str_campo + " FROM " + Str_tabla +
                    " WHERE " + Str_condicion + " FOR UPDATE";

                // Ejecutar la sentencia
                resultado = sentencia.executeQuery(Str_sentencia);

                if (resultado.next()) {
                    // Obtener el BLOB
                    blob = ((OracleResultSet) resultado).getBLOB(Str_campo);
                    mandatoLocal = (OracleCallableStatement) conexion.prepareCall(
                            "begin dbms_lob.write(:1, :2, :3, :4); end;");
                    mandatoLocal.registerOutParameter(1, OracleTypes.BLOB);

                    while ((int_bytesLeidos = inputStream.read(buffer)) != -1) {
                        mandatoLocal.setBLOB(1, blob);
                        mandatoLocal.setInt(2, int_bytesLeidos);
                        mandatoLocal.setInt(3, offset);
                        mandatoLocal.setBytes(4, buffer);
                        mandatoLocal.executeUpdate();
                        blob = mandatoLocal.getBLOB(1);
                        offset = offset + int_bytesLeidos;
                    }
                }
            }
        } catch (SQLException eSql) {
            eSql.printStackTrace();
            throw eSql;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            sentencia.close();

            if (mandatoLocal != null) {
                mandatoLocal.close();
            }

            if (resultado != null) {
                resultado.close();
            }

            if (inputStream != null) {
                inputStream.close();
            }
        }
    }
     // escribirCampoBinario

    /**
      Actualiza el contenido de un campo BLOB
    @param                ByteArrayOutputStream, outputStream de un multiparte
      @param                Connection conexion conexi�n de BD a utilizar
      @param                String Str_tabla        tabla donde est� el campo binario
      @param                String Str_campo          nombre del campo que tiene el binario
      @param                String Str_condicion condicion de la fila a modificar
      @return                void
      @exception        SQLException, Exception
      @since                16-11-2000

    */
    public void escribirCampoBinario(ByteArrayOutputStream out,
        Connection conexion, String Str_tabla, String Str_campo,
        String Str_condicion) throws SQLException, Exception {
        BLOB blobDestino;
        OutputStream blobOutputStream = null;
        ResultSet resDestino = null;
        Statement sentencia = null;
        String Str_sentencia = null;
        int int_numeroFilasProcesadas = 0;

        try {
            sentencia = conexion.createStatement();

            Str_sentencia = "UPDATE " + Str_tabla + " SET " + Str_campo +
                " = empty_blob()" + " WHERE " + Str_condicion;

            int_numeroFilasProcesadas = sentencia.executeUpdate(Str_sentencia);

            // Si ya existe el registro -> int_numeroFilasProcesadas > 0
            if (int_numeroFilasProcesadas > 0) {
                Str_sentencia = "SELECT " + Str_campo + " FROM " + Str_tabla +
                    " WHERE " + Str_condicion + " FOR UPDATE";

                resDestino = sentencia.executeQuery(Str_sentencia);

                if (resDestino.next()) {
                    blobDestino = ((OracleResultSet) resDestino).getBLOB(Str_campo);
                    blobOutputStream = blobDestino.getBinaryOutputStream();
                    out.writeTo(blobOutputStream);
                }
            }
        } catch (SQLException eSql) {
            eSql.printStackTrace();
            throw eSql;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            sentencia.close();

            if (resDestino != null) {
                resDestino.close();
            }

            if (blobOutputStream != null) {
                blobOutputStream.close();
            }
        }
    }
     // escribirCampoBinario
}
 // CampoBinarioOracle
