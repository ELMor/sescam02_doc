package isf.db;

import java.io.*;

/**
* Carga el fichero de propiedades por defecto o el indicado como parametro.
*/
public class PoolLoader {

	public InputStream loadPoolProperties() {
		InputStream is = getClass().getResourceAsStream ("/db.properties");
		return is;
	}
	
	public InputStream loadPoolProperties(String _dbProfileName)
	{
	    InputStream is = getClass().getResourceAsStream ("/"+_dbProfileName + ".properties");
	    return is;
	}
}
