package isf.db;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 * Esta clase interna representa una conexi�n simple. A la base de datos
 * @version 1.0
 * @author IEC 02-10-2002
 */
public class SingleConn{
        static private 	PrintWriter log;
        /**
         * Crea una nueva conexi�n a una BD con un user y password especificado en el fichero
         * de propiedades especificado.
         */
        public Connection newConnection()
        {
            Connection conn = null;
            Properties profileObject = new Properties();
            try {
            InputStream is = getClass().getResourceAsStream("/db.properties");
            profileObject.load(is);
            }
            catch (Exception e)
            {
                System.out.println ("No se puede leer el fichero de propiedades");
                return null;
            }
            try
            {
                Class.forName(profileObject.getProperty ("driver"));
            // Se escribe en el fichero de log correspondiente.
                log = new PrintWriter (new FileWriter (profileObject.getProperty ("log"), true), true);
                   log.println (": nueva conexi�n creada.");
                  log.println ("\t\t\tDriver  : " + profileObject.getProperty ("driver"));
                   log.println ("\t\t\tURL     : " + profileObject.getProperty ("url"));
                   log.println ("\t\t\tuser    : " + profileObject.getProperty ("user"));
                conn = DriverManager.getConnection(profileObject.getProperty ("url"), profileObject.getProperty ("user"), profileObject.getProperty ("password"));
            }
            catch (SQLException e)
            {
                System.out.println(e);
            }
            catch (Exception e)
            {
                System.out.println(e);
            }
            return conn;
        }

}