package isf.util;

import java.io.*;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Locale;

/**
 * Clase para cargar ficheros de propiedades.
 * @author MGO
 */
public class PropertiesLoader {

    protected static final String _claseBundle = "isf.util.PropertiesLoaderBundle";
    protected static final String RESOURCE_FILE_NOT_FOUND = "-2";

    protected static PropertiesLoader _instance = null;

    private PropertiesLoader () {

    }


   /**
    * Metodo para acceder al al objeto que implementa la funcionalidad.
    *
    *
    */
    public synchronized static  PropertiesLoader getInstance()
    {
        if ( null == _instance)
        {
            _instance = new PropertiesLoader();
        }
        return _instance;

    }

   /**
    * Metodo que devuelve un objeto Properties con el contenido
    * del fichero especificado como parametro. Hay que pasarle el path absoluto
    *
    * @param strPath Path al fichero de propiedades
    * @exception ResourceFileNotFound Si no se ha encontrado el fichero o no se puede leer
    */
    public synchronized  Properties getProperties(String strPath) throws ResourceFileNotFound
    {

        Properties properties = new Properties();
        InputStream is = null;

		// Intentamos obtener un flujo de entrada para el
		// el fichero especificado como parametro
        try
		{
			is = getInputStream(strPath);
			// Cargamos las propiedades a partir de ese flujo de entrada
			properties.load(is);
       	}
       	// No encontro el fichero de recursos?
 		catch (FileNotFoundException fnfe)
		{
			// Inicializamos el objeto Bundle con el elegido por defecto
			ResourceBundle resourceBundle = ResourceBundle.getBundle(_claseBundle,
			                                                     Locale.getDefault());

			// Dejamos una traza indicando el problema
			//Log.getInstance().trace(fnfe.toString());

			// Lanzamos una excepcion indicando que no se ha encontrado el
			// fichero especificado
			throw new ResourceFileNotFound
			 	(resourceBundle.getString(RESOURCE_FILE_NOT_FOUND) + strPath);
		}
		catch (Exception ioe)
        {
			// Inicializamos el objeto Bundle con el elegido por defecto
			ResourceBundle resourceBundle = ResourceBundle.getBundle(_claseBundle,
			                                                     Locale.getDefault());

			// Dejamos una traza indicando el problema
			//Log.getInstance().trace(ioe.toString());

			// Lanzamos una excepcion indicando que no se ha encontrado el
			// fichero especificado
			throw new ResourceFileNotFound(resourceBundle.getString(RESOURCE_FILE_NOT_FOUND) + strPath);
        }
        return properties;
    }


   /**
    * Metodo que devuelve un flujo de entrada para el fichero especificado
    *
    * @param strPath Path al fichero de propiedades
    * @exception FileNotFoundException Si no se ha encontrado el fichero especificado
    */

    protected synchronized InputStream getInputStream(String strPath) throws FileNotFoundException
    {

      String str = null;
      int i = 0;

      // El path puede vener con o sin .properties

      // Tiene especificado .properties?
      if ( -1 !=( i = strPath.indexOf(".properties")) )
      {
         // Se lo quitamos
      	 str = strPath.substring(0, i);
      }
      else
      {
      	str = strPath;
      }

     InputStream is = null;

     // Intentamos crear un flujo de entrada basado en ficheros
     // con el path especificado
     try {
       is = new FileInputStream ( str + ".properties") ;
     }
     // No se encontro el fichero?
     catch (FileNotFoundException fnfe)
     {
     	throw  fnfe;
     }

     return is;
    }

	/**
	* Carga el fichero de propiedades por defecto o el indicado como parametro.
	* Carga ficheros que se encuentren en el classpath sin tener que pasarle el pata absoluto
	*/

	public Properties loadProperties(String strPath) throws ResourceFileNotFound
	{
	    InputStream is =  null;
		Properties properties = new Properties();
		// Intentamos obtener un flujo de entrada para el
		// el fichero especificado como parametro
        try
		{
			is = getClass().getResourceAsStream ("/"+strPath + ".properties");
			// Cargamos las propiedades a partir de ese flujo de entrada
			properties.load(is);
       	}
       	// No encontro el fichero de recursos?
 		catch (FileNotFoundException fnfe)
		{
			// Inicializamos el objeto Bundle con el elegido por defecto
			ResourceBundle resourceBundle = ResourceBundle.getBundle(_claseBundle,
			                                                     Locale.getDefault());

			// Dejamos una traza indicando el problema
			//Log.getInstance().trace(fnfe.toString());

			// Lanzamos una excepcion indicando que no se ha encontrado el
			// fichero especificado
			throw new ResourceFileNotFound
			 	(resourceBundle.getString(RESOURCE_FILE_NOT_FOUND) + strPath);
		}
		catch (Exception ioe)
        {
			// Inicializamos el objeto Bundle con el elegido por defecto
			ResourceBundle resourceBundle = ResourceBundle.getBundle(_claseBundle,
			                                                     Locale.getDefault());

			// Dejamos una traza indicando el problema
			//Log.getInstance().trace(ioe.toString());

			// Lanzamos una excepcion indicando que no se ha encontrado el
			// fichero especificado
			throw new ResourceFileNotFound(resourceBundle.getString(RESOURCE_FILE_NOT_FOUND) + strPath);
        }
        return properties;
	}

		// main de prueba
	public static void main(String argv[])
	{
		try
		{
			//Log ref_log = null;
			//ref_log = Log.getInstance();
			//ref_log.info("Comienza el proceso");
			PropertiesLoader p = PropertiesLoader.getInstance();
			p.loadProperties("db55");
			//ref_log.info("Finaliza el proceso");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
