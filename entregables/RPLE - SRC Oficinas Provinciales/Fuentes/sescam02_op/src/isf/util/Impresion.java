package isf.util;

import java.io.*;
import java.util.*;
import javax.servlet.ServletOutputStream;

//A ver si comentamos un poquito mas el codigo

public class Impresion {
  final int MAX_FIELD_LENGTH=30;

  private void cadenaANull(byte[] pbCadena,int iCCadena){
   for(int i=0;i<iCCadena;i++){
     pbCadena[i]=0;
   }
  }
  private String cadenaAString(byte[] pbCadena,int iCCadena){
   char cCadena[]=new char[MAX_FIELD_LENGTH];
   for(int i=0;i<iCCadena;i++){
     cCadena[i]=(char)pbCadena[i];
   }
   return String.copyValueOf(cCadena,0,iCCadena);
  }

  public void imprimir(String docName,Hashtable translate, ServletOutputStream outStream) throws IOException {
    byte fieldSeparator='^';
    byte bLec[]=new byte[1];
    byte bCadena[]=new byte[MAX_FIELD_LENGTH];
    int iCCadena=0;
    String fieldTrad, fileAbsoluteName, strDebug;
    Properties docProps = new Properties();
    InputStream is = getClass().getResourceAsStream ("/Impresion.properties");
    docProps.load(is);
    fileAbsoluteName=docProps.getProperty("FORM_DIR");
    fileAbsoluteName+="/";
    fileAbsoluteName+=docProps.getProperty(docName+"_DOC");
    strDebug=docProps.getProperty("DEBUG");
    try{
      //if (strDebug.equals("1"))
      //System.out.println("Nombre del fichero a imprimir:"+fileAbsoluteName);
      //File fichero=new File(fileAbsoluteName);
      //FileInputStream infile= new FileInputStream(fichero);
      InputStream infile=this.getClass().getResourceAsStream(fileAbsoluteName);
      int iRet;
      boolean bFieldFound=false;
      iRet=infile.read(bLec);
      while(iRet!=-1){
        if (bFieldFound){
          if ((bLec[0]==fieldSeparator)){
            fieldTrad=(String)translate.get(cadenaAString(bCadena,iCCadena));
            if (fieldTrad!=null){
              if (strDebug.equals("1"))
                //System.out.println("Encontre campo:"+cadenaAString(bCadena,iCCadena)+" Traducido a:"+fieldTrad);
              outStream.print(fieldTrad);
            }else{
              outStream.write(fieldSeparator);
              outStream.write(bCadena,0,iCCadena);
              outStream.write(fieldSeparator);
            }
            bFieldFound=false;
            this.cadenaANull(bCadena,iCCadena);
            iCCadena=0;
          }
          else{
            if (iCCadena==MAX_FIELD_LENGTH){
              outStream.write(fieldSeparator);
              outStream.write(bCadena,0,iCCadena);
              outStream.write(bLec);
              this.cadenaANull(bCadena,iCCadena);
              iCCadena=0;
              bFieldFound=false;
            }
            else{
              bCadena[iCCadena++]=bLec[0];
            }
          }
        }else{
          if ((bLec[0]==fieldSeparator)){ bFieldFound=true;}
          else{outStream.write(bLec);}
        }
        iRet=infile.read(bLec);
      }
      infile.close();
    }
    catch(IOException e){
      throw e;
    }
  }
}