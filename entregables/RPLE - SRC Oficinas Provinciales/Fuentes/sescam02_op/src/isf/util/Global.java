package isf.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Sistema de recopilacion de Informix HP-HIS de listas de espera</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class Global {

  /**
   * Devuelve un String con la fecha y hora actual
   * @return Fecha y hora actual formateada para realizar logging
   */
  public static String now(){
    SimpleDateFormat df=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    String now=df.format(new Date(System.currentTimeMillis())).toString();
    return "["+now+"]";
  }
  /**
   * Comprueba si rs(field), que es un campo tipo fecha es anterior a ahora
   * @param rs ResultSet que contiene el campo fecha
   * @param field Nombre del campo fecha dentro de rs
   * @return true si rs(field) es anterior a ahora
   */
  public static boolean isBeforeNow(ResultSet rs, String field){
    boolean ret=true;
    try{
      Timestamp ts=rs.getTimestamp(field);
      if(ts==null)
        return true;
      if(ts.getTime()<System.currentTimeMillis())
        return true;
    }catch(Exception e){
    }
    return false;
  }

  /**
   * Reemplaza en un String una cadena por otra
   * @param main Donde se quiere reemplazar
   * @param search cadena de busqueda
   * @param replace cadena de reemplazo
   * @return cadena modificada.
   */
  public static String replaceString(String main,String search, String replace){
    for(int pos=main.indexOf(search);
        pos>=0;
        pos=main.indexOf(search,pos+replace.length()))
      main=main.substring(0,pos)+replace+main.substring(pos+search.length());
    return main;

  }

}