package isf.util;


import java.util.*;                                

/**
 * Clase contenedora de los mensajes internacionalizables del catalogo de reports para el idioma Ingles.
 * 
 * @author RRO
 * @author MGO
 */
public class PropertiesLoaderBundle_es extends ListResourceBundle  {
    
  public Object [][] getContents() {                
    return contents;
  }                                                 
                                                          
  static final Object[][] contents = {                                                 
     {"-2", " No se ha encontrado el fichero de recursos para : " },
  };                                                
 
}