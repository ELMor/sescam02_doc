/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import conf.*;

/**
 * <p>Clase: Usuarios_clave </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SysUsuClaves </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 08/12/2002
 * @author Felix Alberto Bravo Hermoso
 */

public class Usuarios_clave {

        public static final String ID_TABLA="SYS_USU_CLAVES";

        /**
          * Devuelve un vector DE OBJETOS SysUsuClaves(la clase de persistencia) con las claves que cumplan la clausula del where.
          * @param String _where Clausula where para la consulta de objetos de clase SysUsuClaves
          * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SysUsuClaves (la clase de persistencia) con las claves que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

        public Vector busquedaUsuClaves(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{

                          v = SysUsuClaves.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar Claves de Usuarios:"+ e) ;
                }

                finally{
                         pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }


        /**
          * Realiza la insercion de una Clave.
          * @param SysUsuClaves _sysUsuClaves Objeto de clase SysUsuClaves (clase de persistencia asociada a tabla Sys_Usu_Claves)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Sys_Usu
         */
        public void insertarUsuClaves(SysUsuClaves _sysUsuClaves) throws Exception{
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                SysUsu sysUsu = new SysUsu();
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{
                _sysUsuClaves.newId(Constantes.FICHERO_CONFIGURACION);
                _sysUsuClaves.insert(conexion);
                conexion.commit();
                }catch(SQLException e){
                        System.out.println("Error al Insertar la clave de Usuario:"+ e) ;
                        conexion.rollback();
                        throw e;
                }
                catch(Exception e){
                        System.out.println("Error al Insertar la clave de Usuario:"+ e) ;
                        conexion.rollback();
                        throw e;
                }
                finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }

        }

        /**
         * Realiza la eliminacion de una clave
         * @param _sysUsuClaves Objeto de clase SysUsuClaves (clase de persistencia asociada a tabla Sys_Usu_Claves)
         * @throws Exception
         */
        public void eliminarUsuClaves(SysUsuClaves _sysUsuClaves) throws Exception {
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{
                   _sysUsuClaves.delete(conexion);
                   conexion.commit();
                }catch(SQLException e){
                   String mensaje = new String(e.getMessage());
                   conexion.rollback();
                   throw e;
                }
                finally{
                   pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
        }


/*        public static void main(String args[]) {
                Vector v = new Vector();
                Usuarios_clave user_clave = new Usuarios_clave();
                SysUsuClaves sUC = new SysUsuClaves();
                TrataClave t = new TrataClave();
                try
                 {
                  //antes de usar este metodo main hay que hacer publico el metodo Encriptar de la clase TrataClave.
                  String clave=t.Encriptar("admin");
                  sUC.setSysclvClave(clave);
                  sUC.setSyslogin("admin");
                  user_clave.insertarUsuClaves(sUC);
                 }catch (Exception e){
                        System.out.println("Error Excepcion Main:"+ e) ;
                 }

        }
*/

} //fin de la clase

