/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: EventosFicha </p>
 * <p>Descripcion: Clase de negocio que contiene la informaci�n de una ficha de eventos</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 30/10/2002
 * @author Iv�n Enrique Caneri
 * @version 1.0
 */
public class EventosFicha{
   public String 	fsyslogin="SD";
   public long		fOficina_pk=0;
   public String 	fOficina_nombre="SD";
   public String 	fSess_fecha_inicio="SD";
   public String 	fSess_srv_rmt="SD";
   public long		fTpoevtoO_pk=0;
   public String 	fTpoevto_desc="SD";
   public String 	fEvto_var_valor="SD";
   public String 	fTpoevto_var_desc="SD";
   public String        fSess_cod_acc = "SD";
   }
