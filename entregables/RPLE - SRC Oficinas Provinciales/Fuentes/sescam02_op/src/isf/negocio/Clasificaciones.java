/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;
import conf.Constantes;

/**
 * <p>Clase: Clasificaciones </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesClasificaciones </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 02/02/2003
 * @author Jose Vicente Nieto-Marquez Fdez-Medina
 */

public class Clasificaciones {

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio Clasificaciones
            */
        public static final String ID_TABLA="SES_CLASIFICACIONES";
        private NegEvento negevent;


        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }


        /**
          * Devuelve un vector DE OBJETOS SesClasificaciones(la clase de persistencia) con las Clasificaciones que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase SesClasificaciones
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesClasificaciones (la clase de persistencia) con las Especialidades que cumplan la clausula del where.
          * @autor Jos� Vicente Nieto-Marquez-Fdez-Medina
          */

        public Vector busquedaClasificaciones(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{

                          v = SesClasificaciones.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar la clasificacion:"+ e) ;
                }

                finally{
                         pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }


        /**
          * Realiza la insercion de la Clasificacion.
          * @param _sesClas Objeto de clase SesClasificaciones (clase de persistencia asociada a tabla Ses_Clasificaciones)
          * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Clasificaciones
          * @throws Exception
         */

        public void insertarClasificacion(SesClasificaciones _sesClas) throws ExceptionSESCAM,Exception {
                String dato = "";
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesClas.newId(Constantes.FICHERO_CONFIGURACION);
                   _sesClas.insert(conexion);
                   try {
                     if (negevent != null) {
                      dato =  new Long(_sesClas.getSesclasPk()).toString();
                      negevent.addVar(117,dato);
                      dato =  new Long(_sesClas.getSesclasPkpadre()).toString();
                      negevent.addVar(118,dato);
                      dato =  new Long(_sesClas.getProcPk()).toString();
                      negevent.addVar(119,dato);
                      negevent.addVar(120,_sesClas.getSesclasDesc());
                      dato =  new Long(_sesClas.getSesclasNiveles()).toString();
                      negevent.addVar(121,dato);
                      dato =  new Long(_sesClas.getSesclasTipo()).toString();
                      negevent.addVar(122,dato);
                      negevent.guardar(conexion);
                    }
                   }catch (Exception e) {
                         fichero_log = Log.getInstance();
                         fichero_log.warning(" No se ha insertado el evento de inserci�n de la clasificacion con pk = " + _sesClas.getSesclasPk()+ " Error -> " + e.toString());
                   }
                   conexion.commit();
                }catch(SQLException e){
//			System.out.println("Error al Insertar el Especialidad:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la insercion de la clasificacion con pk = " + _sesClas.getSesclasPk() + " Error -> " + e.toString());
                        conexion.rollback();
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
                   }finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                   }
                }

        /**
          * Realiza la modificacion de la Clasificaci�n.
          * @param _sesClas Objeto de clase SesClasificaciones (clase de persistencia asociada a tabla Ses_Clasificaciones)
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Clasificaciones
         */
        public void modificarClasificacion(SesClasificaciones _sesClas) throws SQLException,ExceptionSESCAM{
                String dato = "";
                Log fichero_log;
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesClas.update(conexion);
                   try {
                    if (negevent != null) {
                      dato =  new Long(_sesClas.getSesclasPk()).toString();
                      negevent.addVar(123,dato);
                      dato =  new Long(_sesClas.getSesclasPkpadre()).toString();
                      negevent.addVar(124,dato);
                      dato =  new Long(_sesClas.getProcPk()).toString();
                      negevent.addVar(125,dato);
                      negevent.addVar(126,_sesClas.getSesclasDesc());
                      dato =  new Long(_sesClas.getSesclasNiveles()).toString();
                      negevent.addVar(127,dato);
                      dato =  new Long(_sesClas.getSesclasTipo()).toString();
                      negevent.addVar(128,dato);
                      negevent.guardar(conexion);                    }
                   }catch (Exception e) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha insertado el evento de modificacion de la Clasificacion  con Pk = " + _sesClas.getSesclasPk() + " Error -> " + e.toString());
                   }
                   conexion.commit();
                }catch(Exception e){
                        conexion.rollback();
//                        System.out.println("Error al Modificar el Usuario:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion de la Clasificacion con Pk = " + _sesClas.getSesclasPk() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                 }

                 finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                 }

        }

        /**
         * Realiza la eliminacion de la Clasificacion.
         * @param _sesEsp Objeto de clase SesEspecialidades (clase de persistencia asociada a tabla Ses_Especialidades)
         * @throws Exception
         */

        public void eliminarClasificacion(SesClasificaciones _sesClas) throws Exception,ExceptionSESCAM {
          String dato = "";

          Pool pool=null;
          Connection conexion = null;
          Log fichero_log = null;
          Vector v = new Vector();
          pool = Pool.getInstance();
          conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
          conexion.setAutoCommit(false);
          try{
            _sesClas.delete(conexion);
            try{
              if (negevent != null) {
                dato =  new Long(_sesClas.getSesclasPk()).toString();
                negevent.addVar(129,dato);
                negevent.guardar(conexion);
              }
            }catch(Exception e){
              fichero_log = Log.getInstance();
              fichero_log.warning(" No se ha insertado el evento de eliminacion de la Clasificacion  con Pk = " + _sesClas.getSesclasPk() + " Error -> " + e.toString());
            }
            conexion.commit();
          }catch(SQLException e){
            conexion.rollback();
            fichero_log = Log.getInstance();
            fichero_log.error(" No se ha realizado la eliminaci�n de la Clasificacion = " + _sesClas.getSesclasPk() + " Error -> " + e.toString());
            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
            throw es;
          }
          finally{
             pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
          }
        }

} //fin de la clase

