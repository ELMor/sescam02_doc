/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;


/**
 * <p>Clase: VarTipoEvento </p>
 * <p>Descripcion: Clase de negocio de la tabla Evento_Tipo_Var </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 04/12/2002
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */
public class VarTipoEvento {

  /**
   * Nombre de la clase de persistencia asociada a la clase de Negocio VarTipoEvento
   *    */
        public static final String ID_TABLA="EVENTO_TIPO_VAR";


        private static final String DB = "db"; //Nombre de la conexi�n a la base de datos


        // Devuelve un vector DE OBJETOS EventoTipoVar(la clase de persistencia)
        // con las variable de eventos que cumplan la clausula del where.
        /**
         * Devuelve un vector DE OBJETOS EventoTipoVar(la clase de persistencia)con las variable de eventos que cumplan la clausula del where.
         * @param String _where Clausula where para la consulta de objetos de clase EventoTipoVar
         * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
         * @return Devuelve un vector DE OBJETOS EventoTipoVar(la clase de persistencia) con las variable de eventos que cumplan la clausula del where.
         * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
         */
        public Vector busquedaVarTipoEvento(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);

                  v = EventoTipoVar.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar la variables de eventos:"+ e) ;
                }

                finally{
                        pool.freeConnection(DB,conexion);
                }
                return v;
        }


        //Realiza la modificacion de una variable Tipo Evento.
        /**
         * Realiza la modificacion de una variable Tipo Evento
         * @param EventoTipoVar _eventipovar Objeto de clase EventoTipoVar (clase de persistencia asociada a tabla Evento_Tipo_Var)
         * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
         * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Evento_Tipo_Var
         */

        public void modificarVarTipoEvento(EventoTipoVar _eventipovar) throws SQLException,ExceptionSESCAM  {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                try{
                //System.out.println("Entra en modificarPerm");
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
                _eventipovar.update(conexion);
                conexion.commit();
                }catch(SQLException e){
//                        System.out.println("Error al Modificar la variable de un evento :"+ e);
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha podido realizar modificaci�n de la variable de evento " + _eventipovar.getTpoevtoVarDesc() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;

                }catch(Exception ex){
                        conexion.rollback();
                 //       System.out.println("Error al Modificar la variable de un evento :"+ ex);
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha podido realizar modificaci�n de la variable de evento " + _eventipovar.getTpoevtoVarDesc() + " Error -> " + ex.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;

                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }

} //fin de la clase

