/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;
import conf.Constantes;

/**
 * <p>Clase: Diagnosticos </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesDiagnosticos </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 03/02/2003
 * @author Felix Alberto Bravo Hermoso
 */

public class Diagnosticos {

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio Diagnosticos
            */
        public static final String ID_TABLA="SES_DIAGNOSTICOS";
        private NegEvento negevent;


        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }


        /**
          * Devuelve un vector DE OBJETOS SesDiagnosticos(la clase de persistencia) con los Diagnosticos que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase SesDiagnosticos
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesDiagnosticos (la clase de persistencia) con los Diagnosticos que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

        public Vector busquedaDiag(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{

                          v = SesDiagnosticos.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar Diagnostico:"+ e) ;
                }

                finally{
                         pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }


        /**
          * Realiza la insercion del Diagnostico.
          * @param _sesDiag Objeto de clase SesDiagnosticos (clase de persistencia asociada a tabla Ses_Diagnosticos)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Diagnosticos
          * @throws Exception
         */

        public void insertarDiag(SesDiagnosticos _sesDiag) throws ExceptionSESCAM,Exception{
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesDiag.newId(Constantes.FICHERO_CONFIGURACION);
                   _sesDiag.insert(conexion);
                   try {
                     if (negevent != null) {
                      String diagpk = new Long(_sesDiag.getDiagPk()).toString();
                      negevent.addVar(138,diagpk);
                      negevent.addVar(139,_sesDiag.getDiagCod());
                      negevent.addVar(140,_sesDiag.getDiagDesc());
                      negevent.guardar(conexion);
                    }
                   }catch (Exception e) {
                         fichero_log = Log.getInstance();
                         fichero_log.warning(" No se ha insertado el evento de inserci�n del Diagnostico con C�digo = " + _sesDiag.getDiagCod() + " Error -> " + e.toString());
                   }
                   conexion.commit();
                }catch(SQLException e){
//			System.out.println("Error al Insertar el Usuario:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la insercion del Diagnostico con Codigo = " + _sesDiag.getDiagCod() + " Error -> " + e.toString());
                        conexion.rollback();
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
                   }finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                   }
                }

        /**
          * Realiza la modificacion del Diagnostico.
          * @param _sesDiag Objeto de clase SesDiagnosticos (clase de persistencia asociada a tabla Ses_Diagnosticos)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Diagnosticos
         */
        public void modificarDiag(SesDiagnosticos _sesDiag) throws SQLException,ExceptionSESCAM{
                Log fichero_log;
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesDiag.update(conexion);
                  try {
                    if (negevent != null) {
                      String diagpk = new Long(_sesDiag.getDiagPk()).toString();
                      negevent.addVar(141,diagpk);
                      negevent.addVar(142,_sesDiag.getDiagCod());
                      negevent.addVar(143,_sesDiag.getDiagDesc());
                      negevent.guardar(conexion);
                    }
                  }catch (Exception e) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha insertado el evento de modificacion del Diagnostico con C�digo = " + _sesDiag.getDiagCod() + " Error -> " + e.toString());
                  }
                 conexion.commit();
                    }catch(Exception e){
                        conexion.rollback();
//                        System.out.println("Error al Modificar el Usuario:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion del Diagnostico con Codigo = " + _sesDiag.getDiagCod() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                 }

                 finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                 }

        }

        /**
         * Realiza la eliminacion del Diagnostico.
         * @param _sesDiag Objeto de clase SesDiagnosticos (clase de persistencia asociada a tabla Ses_Diagnosticos)
         * @throws Exception
         */

        public void eliminarDiag(SesDiagnosticos _sesDiag) throws Exception {
          Pool pool=null;
          Connection conexion = null;
          Log fichero_log = null;
          Vector v = new Vector();
          pool = Pool.getInstance();
          conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
          conexion.setAutoCommit(false);
          try{
            _sesDiag.delete(conexion);
            try{
              if (negevent != null) {
                      String diagpk = new Long(_sesDiag.getDiagPk()).toString();
                      negevent.addVar(144,diagpk);
                      negevent.guardar(conexion);
              }
            }catch(Exception e){
              fichero_log = Log.getInstance();
              fichero_log.warning(" No se ha insertado el evento de eliminacion del Diagnostico con C�digo = " + _sesDiag.getDiagCod() + " Error -> " + e.toString());
            }
            conexion.commit();
          }catch(SQLException e){
            conexion.rollback();
            //System.out.println(" Error al borrar Diagnostico: "+e);
            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
            throw es;
           }
           finally{
             pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
           }
        }

} //fin de la clase

