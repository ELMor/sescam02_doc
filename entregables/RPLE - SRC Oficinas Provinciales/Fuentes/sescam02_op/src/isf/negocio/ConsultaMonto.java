/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

import  java.sql.*;
import java.util.Vector;
import isf.db.*;
import conf.*;
import isf.persistencia.*;

/**
 * <p>Clase: ConsultaMonto </p>
 * <p>Descripcion: Clase de negocio para obtener Monto por defecto para una entrada en Lista Espera </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 04/11/2002
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */

public class ConsultaMonto {

  private static String codigo_procedimiento;

  /**
  * Constructor de la clase, inicializa atributos
  */

  public ConsultaMonto() {
    codigo_procedimiento = "";
  }

  /**
   * Devuelve monto definido para el c�digo de procedimiento pasado como par�metro
   * @param String codigo_proc Codigo de Procedimiento para el cual se necesita su monto asociado en tabla SesProcedimientos, si lo tuviese
   * @return Devuelve monto definido para el c�digo de procedimiento pasado como par�metro
   */
  public double CalcularMonto (String codigo_proc){
    Procedimientos proc = new Procedimientos();
    Vector v;

    v = proc.busquedaProced("upper(PROC_COD) = '" +codigo_proc+ "'","");

    if (v.size() > 0 ) {
      SesProcedimientos ses_proc = (SesProcedimientos)v.elementAt(0);
      return ses_proc.getProcMontoMaxEst();
    }else {
      return 0;
    }
  }

  /**
   * Devuelve el codigo de procedimiento de la entrada en Lista de Espera asociado al CENTRO,ORDEN O NCITA y gar_cq
   * @param long centro Centro asociado a LESP � LESPCEX  para obtener la entrada en Lista de Espera asociada
   * @param long orden Orden (en el caso de trabajar con LESP) o NCita (en el caso de LESPCEX) para obtener la entrada en Lista de Espera asociada
   * @param String gar_cq 'C' identifica que estamos trabajando con LESPCEX (Lista de Espera de Consultas Externas) u 'Q' para identificar que trabajamos con LESP (Lista de Espera Quir�rgica)
   * @return Devuelve el codigo de procedimiento de la entrada en Lista de Espera asociado al CENTRO,ORDEN O NCITA y gar_cq
   */
  public String ConstruyeConsulta(long centro,long orden,String gar_cq) {
    Pool pool=null;
    Connection conexion = null;
    String query = new String();
    String campo = new String();
    try{
      pool = Pool.getInstance();
      conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
      if (gar_cq.equals("Q") ) {
        query = "SELECT * FROM LESP WHERE NORDEN = " +orden+ " AND CENTRO = " +centro;
        campo = "CPR1";
      } else {
        query = "SELECT * FROM LESPCEX WHERE NCITA = " +orden+ " AND CEGA = " +centro;
        campo = "PRESTACI";
      }
      // Creamos el Statement y el ResultSet.
      Statement myStatement = conexion.createStatement();
      ResultSet ls_rs = myStatement.executeQuery (query);
      String cod_proc;
      if (ls_rs.next()) {
        cod_proc = ls_rs.getString(campo);
      } else {
        cod_proc = "";
      }
   // Procesamos el ResultSet.
      myStatement.close();
      return cod_proc;
    }
    catch(Exception e){
        System.out.println("Error al Buscar monto procedimiento:"+ e);
        return "";
    }
    finally{
        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
    }
 }
}
