/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.*;
import isf.util.log.Log;

/**
 * <p>Clase: Usuarios </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SysUsu </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 24/10/2002
 * @author Felix Alberto Bravo Hermoso
 */

public class Usuarios {

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio Usuarios
            */
	public static final String ID_TABLA="SYS_USU";

	private static final String DB = "db"; //Nombre de la conexi�n a la base de datos

        private NegEvento negevent;



        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }

	// Devuelve un vector DE OBJETOS SysUsu(la clase de persistencia)
	// con los usuarios que cumplan la clausula del where.

        /**
          * Devuelve un vector DE OBJETOS SysUsu(la clase de persistencia) con los usuarios que cumplan la clausula del where.
          * @param String _where Clausula where para la consulta de objetos de clase SysUsu
          * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SysUsu (la clase de persistencia) con los usuarios que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

	public Vector busquedaUsu(String _where,String _sort) {
		Vector v = new Vector();
		Pool pool=null;
		Connection conexion = null;
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		try{

	  		v = SysUsu.search(conexion,_where,_sort);
		}
		catch(Exception e){
			System.out.println("Error al Buscar Usuarios:"+ e) ;
		}

		finally{
 			pool.freeConnection(DB,conexion);
		}
		return v;
	}


        /**
          * Realiza la insercion del Usuario.
          * @param SysUsu _sysUsu Objeto de clase SysUsu (clase de persistencia asociada a tabla Sys_Usu)
          * @param clave, String, clave a insertar con el usuario.
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Sys_Usu
          * @throws Exception
         */

	public void insertarUsu(SysUsu _sysUsu, String clave) throws ExceptionSESCAM,Exception{
                Log fichero_log = null;
		Pool pool=null;
		Connection conexion = null;
		Vector v = new Vector();
                TrataClave tC = new TrataClave();
                SysUsuClaves _sysUsuClaves = new SysUsuClaves();
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		v = _sysUsu.search(conexion,"SYSLOGIN='"+_sysUsu.getSyslogin()+"'","");
		if (v.size()>0){
			ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.LOGIN_DUPLICADO);
			throw es;
		}else{
                    conexion.setAutoCommit(false);
		    try{
                      _sysUsu.insert(conexion);
                      try {
                        _sysUsuClaves.newId(DB);
                        _sysUsuClaves.setSyslogin(_sysUsu.getSyslogin());
                        _sysUsuClaves.setSysclvClave(tC.Encriptar(clave));
                        _sysUsuClaves.insert(conexion);
                      } catch (Exception e) {
                         fichero_log = Log.getInstance();
                         fichero_log.error(" No se ha realizado la insercion de la nueva clave del usuario con Login = " + _sysUsu.getSyslogin() + " Error -> " + e.toString());
                         ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                         throw es;
                      }
                      try {
                       if (negevent != null) {
                        negevent.addVar(13,_sysUsu.getSyslogin());
                        negevent.addVar(14,_sysUsu.getApellido1());
                        negevent.addVar(15,_sysUsu.getApellido2());
                        negevent.addVar(16,_sysUsu.getNombre());
                        Long dato = new Long (_sysUsu.getOficinaPk());
                        negevent.addVar(17,dato.toString());
                        dato = new Long (_sysUsu.getSysCargosPk());
                        negevent.addVar(18,dato.toString());
                        dato = new Long (_sysUsu.getSysRolPk());
                        negevent.addVar(19,dato.toString());
                        negevent.guardar(conexion);
                       }
                      }
                      catch (Exception e) {
                         fichero_log = Log.getInstance();
                         fichero_log.warning(" No se ha insertado el evento de inserci�n del usuario con Login = " + _sysUsu.getSyslogin() + " Error -> " + e.toString());
                      }
                      conexion.commit();
		   }catch(SQLException e){
//			System.out.println("Error al Insertar el Usuario:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion del usuario con Login = " + _sysUsu.getSyslogin() + " Error -> " + e.toString());
			conexion.rollback();
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
		   }finally{
			pool.freeConnection(DB,conexion);
		   }
		}
	}

	//Realiza la modificacion del Usuario.
        /**
          * Realiza la modificacion del Usuario, y si la clave ha sido modificada realiza la modificacion de la misma.
          * @param SysUsu _sysUsu Objeto de clase SysUsu (clase de persistencia asociada a tabla Sys_Usu)
          * @param clave String Clave a modificada asignada al usuario.
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Sys_Usu
         */
	public void modificarUsu(SysUsu _sysUsu,String clave) throws SQLException,ExceptionSESCAM{
                Log fichero_log;
		Pool pool=null;
		Connection conexion = null;
                TrataClave tC = new TrataClave();
                SysUsuClaves _sysUsuClaves = new SysUsuClaves();
                pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
                conexion.setAutoCommit(false);
		try{
	  	_sysUsu.update(conexion);
                 try {
                  if ((clave!=null)&&(!clave.equals(""))){
                   _sysUsuClaves.newId(DB);
                   _sysUsuClaves.setSyslogin(_sysUsu.getSyslogin());
                   _sysUsuClaves.setSysclvClave(tC.Encriptar(clave));
                   _sysUsuClaves.insert(conexion);
                  }
                 }catch (Exception e) {
                    fichero_log = Log.getInstance();
                    fichero_log.error(" No se ha realizado la insercion de la nueva clave del usuario con Login = " + _sysUsu.getSyslogin() + " Error -> " + e.toString());
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                    throw es;
                  }
                  try {
                    if (negevent != null) {
                      negevent.addVar(20,_sysUsu.getSyslogin());
                      negevent.addVar(21,_sysUsu.getApellido1());
                      negevent.addVar(22,_sysUsu.getApellido2());
                      negevent.addVar(23,_sysUsu.getNombre());
                      Long dato = new Long (_sysUsu.getOficinaPk());
                      negevent.addVar(24,dato.toString());
                      dato = new Long (_sysUsu.getSysCargosPk());
                      negevent.addVar(25,dato.toString());
                      dato = new Long (_sysUsu.getSysRolPk());
                      negevent.addVar(26,dato.toString());
                      negevent.guardar(conexion);
                    }
                  }catch (Exception e) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha insertado el evento de modificacion del usuario con Login = " + _sysUsu.getSyslogin() + " Error -> " + e.toString());
                  }
		  conexion.commit();
   		  }catch(Exception e){
			conexion.rollback();
//                        System.out.println("Error al Modificar el Usuario:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion del usuario con Login = " + _sysUsu.getSyslogin() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
			throw es;
                 }

		 finally{
			pool.freeConnection(DB,conexion);
		 }

	}

        /**
         * Metodo para debloquear un usuario bloqueado por la reiteracion de claves fallidas.
         * @param login, usuario a desbloquear.
         * @return verdadero si se ha desbloqueado y falso si ha habido algun error.
         * @throws SQLException
         */
	public boolean desbloquearUsu(String login) throws SQLException,ExceptionSESCAM {
          boolean bret=false;
          Pool pool=null;
          Connection conexion = null;
          pool = Pool.getInstance();
          conexion = pool.getConnection(DB);
          Vector v = new Vector();
          try{
          v = busquedaUsu("SYSLOGIN='"+login+"'","");
          SysUsu sU = (SysUsu)v.elementAt(0);
          if (v.size()>0){
              sU.setNullFechaBloqueo();
              sU.update(conexion);
              bret=true;
              conexion.commit();
          }
          }catch(Exception e){
                conexion.rollback();
                System.out.println("Error al Desbloquear el Usuario.");
                ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                throw es;

          }finally{
                        pool.freeConnection(DB,conexion);
          }
	  return bret;
        }

        public void eliminarUsuario(SysUsu _sysUsu) throws Exception {
          Pool pool=null;
          Connection conexion = null;
          Vector v = new Vector();
          pool = Pool.getInstance();
          conexion = pool.getConnection(DB);
          conexion.setAutoCommit(false);
          try{
            _sysUsu.delete(conexion);
            conexion.commit();
          }catch(SQLException e){
            conexion.rollback();
            System.out.println(" Error al borrar usuario: "+e);
            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
            throw es;
           }
           finally{
             pool.freeConnection(DB,conexion);
           }
	}
/*		public static void main(String args[]) {
	    Vector v = new Vector();
		Usuarios user = new Usuarios();
		try
 		{
		v = user.busquedaUsu("ACTIVO_SN=1 AND SYSLOGIN='adm'","APELLIDO1 ASC");
		System.out.println("4") ;
		for(int int_pos = 0;int_pos < v.size(); int_pos++){
						SysUsu x=(SysUsu)v.elementAt(int_pos);
						System.out.print(x.getApellido1() +"-");
						System.out.print(x.getApellido2() +"-");
						System.out.println(x.getNombre() +"");
			}
 		}catch (Exception e){
			System.out.println("Error Excepcion Main:"+ e) ;
 		}

  		}		*/


} //fin de la clase

