/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;

/**
 * <p>Clase: Perfiles </p>
 * <p>Descripcion: Clase de negocio que maneja la clase de persistencia SysRoles </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 28/10/2002
 * @author Felix Alberto Bravo Hermoso
 */

public class Perfiles {

  /**
      * Nombre de la clase de persistencia asociada a la clase de Negocio Oficina
      */

	public static final String ID_TABLA="SYS_ROL";

	private static final String DB = "db"; //Nombre de la conexi�n a la base de datos

        private NegEvento negevent;
	// Devuelve un vector DE OBJETOS SysRoles(la clase de persistencia)
	// con los perfiles que cumplan la clausula del where.

        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }

        /**
         * Devuelve un vector DE OBJETOS SysRoles(la clase de persistencia) con los perfiles que cumplan la clausula del where.
         * @param String _where Clausula where para la consulta de objetos de clase SysRoles
         * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
         * @return Devuelve un vector DE OBJETOS SysRoles(la clase de persistencia) con los perfiles que cumplan la clausula del where.
         * @autor Felix Alberto Bravo Hermoso
        */

	public Vector busquedaPerfil(String _where,String _sort) {
		Vector v = new Vector();
		Pool pool=null;
		Connection conexion = null;
		try{
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		v = SysRoles.search(conexion,_where,_sort);
		}
		catch(Exception e){
			System.out.println("Error al Buscar Perfiles:"+ e) ;
		}

		finally{
			pool.freeConnection(DB,conexion);
		}
		return v;
	}


        /**
         * Realiza la insercion del perfil y de todas las asociaciones entre el nuevo perfil y los permisos que se le desean asociar (Entradas en tabla sys_perm_rol) , a partir de objeto de clase sysRoles y los permisos  asociados
         * @param SysRoles _sysRol Objeto de clase SysRoles (clase de persistencia asociada a la tabla Sys_Roles)
         * @param String sfinal Vector de permisos que quieren asociarse al nuevo perfil
         * @throws SQLException Exception producida por la operacion INSERT sobre tabla Sys_Roles y cada uno de los INSERT sobre Sys_Perm_Rol
         */
	public void insertarPerfil(SysRoles _sysRol,String[] sfinal) throws SQLException, ExceptionSESCAM{
                Log fichero_log = null;
		Pool pool=null;
		Connection conexion = null;
		long permpk;
                String sperm = "";
		SysPermRol spr = new SysPermRol();
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		conexion.setAutoCommit(false);
		try{
		  _sysRol.newId(DB);
                  _sysRol.insert(conexion);
                  try {
                         if (sfinal!=null){
                           for (int i=0;i<sfinal.length;i++){
                                   permpk=new Long(sfinal[i]).longValue();
                                   if (i == (sfinal.length -1)) {
                                     sperm = sperm + permpk;
                                   }
                                   else {
                                     sperm = sperm + permpk + " , ";
                                   }
                                   spr.setSysPermPk(permpk);
                                   spr.setSysRolPk(_sysRol.getSysRolPk());
                                   spr.insert(conexion);
                           } //fin for
                    } //fin if
                  } catch (Exception e) {
                      conexion.rollback();
              //      System.out.println("Error al Insertar el Procedimiento:"+ e);

                      fichero_log = Log.getInstance();
                      fichero_log.error(" No se ha podido realizar la insercion de los permisos asociados al perfil " + _sysRol.getSysRolNombre() + " Error -> " + e.toString());
                      ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                      throw es;
                  }
                  try {
                    if (negevent != null) {
                      Long dato = new Long(_sysRol.getSysRolPk());
                      negevent.addVar(27, dato.toString());
                      negevent.addVar(28, _sysRol.getSysRolNombre());
                      negevent.addVar(29, _sysRol.getSysRolDesc());
                      negevent.addVar(30, sperm);
                      negevent.guardar(conexion);
                    }
                  }
                  catch (SQLException ex) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha realizado el evento de inserci�n del nuevo perfil de nombre  " + _sysRol.getSysRolNombre() + " Error -> " + ex.toString());
                  }
     		  conexion.commit();
		} //fin try
		catch(Exception e){
			conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado el evento de inserci�n del nuevo perfil de nombre  " + _sysRol.getSysRolNombre() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
		//	System.out.println("Error al Insertar el Perfil:"+ e) ;
		}
		finally{
			pool.freeConnection(DB,conexion);
		}

	}

        /**
         * Realiza la modificacion del perfil e inserta los nuevos permisos (en Sys_Perm_Rol) que se le quieran a�adir y borra los que ya no se desean tener (de Sys_Perm_Rol)
         * @param SysRoles _sysRol Objeto de clase SysRoles (clase de persistencia asociada a la tabla Sys_Roles)
         * @param Vector vorigen Vector con los Objetos SysPermRol que tenia asociados el perfil a modificar
         * @param String sfinal Vector de permisos que se desean tener asociados a partir de ahora
         * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Sys_Roles y cada uno de los INSERT y DELETE sobre Sys_Perm_Rol
         */
	public void modificarPerfil(SysRoles _sysRol,Vector vorigen,String[] sfinal) throws SQLException,ExceptionSESCAM{
                Log fichero_log = null;
		Pool pool=null;
		Connection conexion = null;
		long rolpk=_sysRol.getSysRolPk();
		long permpk;
		SysPermRol spr = new SysPermRol();
		boolean encontrado;
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		conexion.setAutoCommit(false);
		try{
		//System.out.println("Entra en modificarPerfil");
		_sysRol.update(conexion);
		//System.out.println("Hecho el update del perfil.");
                String sperm_origen = null; String sperm_final = null;
                try {
                  sperm_origen = "";
                  sperm_final = "";
                  for (int i = 0; i < vorigen.size(); i++) {
                    encontrado = false;
                    SysPermRol sysPermRol = (SysPermRol) vorigen.elementAt(i);
                    permpk = sysPermRol.getSysPermPk();
                    if (i == (vorigen.size() - 1)) {
                      sperm_origen = sperm_origen + permpk;
                    }
                    else {
                      sperm_origen = sperm_origen + permpk + " , ";
                    }
                    //System.out.println("Tomada la pk del permiso:"+permpk);
                    if (sfinal != null) {
                      for (int j = 0; j < sfinal.length; j++) {
                        //System.out.println("Permiso origen"+permpk+",Permiso final:"+new Long(sfinal[j]).longValue());
                        if (permpk == new Long(sfinal[j]).longValue()) {
                          encontrado = true;
                          break;
                        }
                        else {
                          encontrado = false;
                        }
                      }
                    }
                    if (!encontrado) {
                      //System.out.println("Hay registros para borrar:"+permpk+" "+rolpk);
                      spr.setSysPermPk(permpk);
                      spr.setSysRolPk(rolpk);
                      spr.delete(conexion);
                    }
                  }
                }
                catch (SQLException ex) {
                  conexion.rollback();
                  //      System.out.println("Error al Insertar el Procedimiento:"+ e);

                  fichero_log = Log.getInstance();
                  fichero_log.error(" No se ha podido borrar los permisos que dejan de estar asociados al perfil " + _sysRol.getSysRolNombre() + " Error -> " + ex.toString());
                  ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
                  throw es;
                }
                try {
                  if (sfinal != null) {
                    for (int i = 0; i < sfinal.length; i++) {
                      encontrado = false;
                      permpk = new Long(sfinal[i]).longValue();
                      if (i == (sfinal.length - 1)) {
                        sperm_final = sperm_final + permpk;
                      }
                      else {
                        sperm_final = sperm_final + permpk + " , ";
                      }

                      //System.out.println("Tomada la pk del permiso final:"+permpk);
                      for (int j = 0; j < vorigen.size(); j++) {
                        SysPermRol sysPermRol = (SysPermRol) vorigen.elementAt(
                            j);
                        //System.out.println("Permiso final"+permpk+",Permiso origen:"+sysPermRol.getSysPermPk());
                        if (permpk == sysPermRol.getSysPermPk()) {
                          encontrado = true;
                          //System.out.println(encontrado);
                          break;
                        }
                        else {
                          encontrado = false;
                        }
                      }
                      if (!encontrado) {
                        //System.out.println("Hay registros para insertar:"+permpk+" "+rolpk);
                        spr.setSysPermPk(permpk);
                        spr.setSysRolPk(rolpk);
                        spr.insert(conexion);
                      }
                    } //fin for
                  } //fin if
                }
                catch (SQLException ex1) {
                  conexion.rollback();
                  //      System.out.println("Error al Insertar el Procedimiento:"+ e);
                  fichero_log = Log.getInstance();
                  fichero_log.error(" No se ha podido insertar los nuevos permisos que se han asociado al perfil " + _sysRol.getSysRolNombre() + " Error -> " + ex1.toString());
                  ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                  throw es;
                }
                try {
                  if (negevent != null) {
                    Long dato = new Long(_sysRol.getSysRolPk());
                    negevent.addVar(31, dato.toString());
                    negevent.addVar(32, _sysRol.getSysRolNombre());
                    negevent.addVar(33, _sysRol.getSysRolDesc());
                    negevent.addVar(34, sperm_origen);
                    negevent.addVar(35, sperm_final);
                    negevent.guardar(conexion);
                  }
                }
                catch (SQLException ex2) {
                  fichero_log = Log.getInstance();
                  fichero_log.warning(" No se ha realizado el evento de modificaci�n del nuevo perfil de nombre  " + _sysRol.getSysRolNombre() + " Error -> " + ex2.toString());
                }
		conexion.commit();
		}catch(Exception e){
                   conexion.rollback();
                   fichero_log = Log.getInstance();
                   fichero_log.error(" No se ha podido modificar el perfil " + _sysRol.getSysRolNombre() + " Error -> " + e.toString());
                   ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                   throw es;
//                   System.out.println("Error al Modificar el Perfil:"+ e) ;
		}

		finally{
			pool.freeConnection(DB,conexion);
		}

	}

        //Realiza la eliminacion del Perfil.
        /**
         * Realiza la eliminacion del Perfil.
         * @param SysRoles _sysRol Objeto de clase SysRoles (clase de persistencia asociada a tabla Sys_Roles)
         * @autor Felix Alberto Bravo Hermoso
         * @throws ExceptionSESCAM Exception producida al incumplirse las restricciones de integridad con alguna de las tablas asociadas
         */

	public void eliminarPerfil(SysRoles _sysRol) throws ExceptionSESCAM,Exception {
                Log fichero_log = null;
		Pool pool=null;
		Connection conexion = null;
		Vector v = new Vector();
		SysPermRol spr = new SysPermRol();
		pool = Pool.getInstance();
		conexion = pool.getConnection(DB);
		conexion.setAutoCommit(false);
                try{
                  _sysRol.delete(conexion);
                  try {
                    if (negevent != null) {
                      Long dato = new Long(_sysRol.getSysRolPk());
                      negevent.addVar(36, dato.toString());
                      negevent.guardar(conexion);
                    }
                  }
                  catch (SQLException ex) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha realizado el evento de borrado del perfil de nombre  " + _sysRol.getSysRolNombre() + " Error -> " + ex.toString());
                  }
                  conexion.commit();
                }catch(SQLException e){
                  String mensaje = new String(e.getMessage());
                  if (mensaje.indexOf("FK_SYS_PERM_RELATION") > 0) {
                    fichero_log = Log.getInstance();
                    fichero_log.error(" No se ha podido eliminar el perfil " + _sysRol.getSysRolNombre() + " Error -> " + e.toString());
                    conexion.rollback();
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.PERFIL_CON_PERMISOS);
                    throw es;
                  }
                  else {
                    if (mensaje.indexOf("FK_SYS_USU_RELATION") > 0) {
                     conexion.rollback();
                     fichero_log = Log.getInstance();
                     fichero_log.error(" No se ha podido eliminar el perfil " + _sysRol.getSysRolNombre() + " Error -> " + e.toString());
                     ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.PERFIL_TIENE_USUARIOS);
                     throw es;
                    }
                    else {
                      conexion.rollback();
                      fichero_log = Log.getInstance();
                      fichero_log.error(" No se ha podido eliminar el perfil " + _sysRol.getSysRolNombre() + " Error -> " + e.toString());
                      ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
                      throw es;

//                      System.out.println(" Error al borrar perfil: "+e);
                    }
                  }
                }
		finally{
			pool.freeConnection(DB,conexion);
		}
	}

}//fin de la clase