/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: ServMapeados </p>
 * <p>Descripcion: Clase que guarda informaci�n de los Servicios que pueden ser mapeados automaticamente</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 28/02/2003
 * @author Felix Alberto Bravo Hermoso.
 */

public class ServMapeados {

    /**
      * Codigo del Servicio SESCAM obtenido en la consulta
      */
    public String 	CodServSESCAM="SD";

    /**
      * Descripci�n del Servicio SESCAM obtenido en la consulta
      */
    public String 	DescServSESCAM="SD";

    /**
      *  Descripcion del codigo de servicio a mapear
      */
    public String        CodServMap="SD";

    /**
     *  Descripcion del servicio a mapear
     */
    public String        DescServMap="SD";

    /**
      *  Valor que indica si el mapeo del servicio ha sido modificado     */
    public String          CodEspMapeo="SD";

    /**
      *  Valor que indica si el mapeo del servicio ha sido modificado     */
    public long         CodCentroMapeo=0;

    /**
      *  Valor que indica si el mapeo del servicio ha sido modificado     */
    public long          Modificado=0;

}