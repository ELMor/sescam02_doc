/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: EntradaLE </p>
 * <p>Descripcion: Clase de negocio que contiene cierta informaci�n de una entrada en Lista Espera</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 30/10/2002
 * @author Iv�n Enrique Caneri
 * @version 1.0
 */
public class EntradaLE{
   /**
   * Numero de Orden o NCita
   */
   public int 		fNorden=0;

   /**
   * Centro
   */

   public int 		fCentro=0;
   public int		fAntiguedad=0;
   public int		fHicl=0;
   public String 	fCip="SD";
   public String 	fCProc="SD";
   public String 	fDProc="SD";
   public String 	fSalida;
   public int		fMotivo;
   public int		fLink;
   public int		fMaxEspera=0;
   public int		fSituacion=0;
   public int		fActivo=0;
   public String    fServicio="SD";
   
   /**
   * Distingue si se trata de una entrada en LESP (Q) O en LESPCEX (C)
   */

   public String 	fTipoCQ="SD";

   /**
    * Comprueba si la entrada en LESP o en LESPCEX se ha pasado a Hist�ricos
    * @return Devuelve TRUE si la entrada en LESP o en LESPCEX se ha pasado a Hist�ricos y FALSE en caso contrario
    */
   public boolean isHistory(){
     if (fActivo==0)
       return true;
     else
       return false;
   }
   /**
   * Comprueba si la entrada en LESP o en LESPCEX tiene garantia asociada
   * @return Devuelve TRUE si la entrada en LESP o en LESPCEX tiene garantia asociada y FALSE en caso contrario
   */

   public boolean isGaranted(){
     if (fMaxEspera!=0)
       return true;
     else
       return false;
   }
   /**
   * Comprueba si la entrada en LESP o en LESPCEX esta fuera de fecha
   * @return Devuelve TRUE si la entrada en LESP o en LESPCEX esta fuera de fecha y FALSE en caso contrario
   */

   public boolean isOutOfDate(){
     if ((fMaxEspera!=0)&&(fMaxEspera<fAntiguedad)&&
         (fActivo==1))
       return true;
     else
       return false;
   }
   }
