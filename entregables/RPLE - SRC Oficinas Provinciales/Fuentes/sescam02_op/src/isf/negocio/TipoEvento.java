/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;


/**
 * <p>Clase: TipoEvento </p>
 * <p>Descripcion: Clase de negocio de la tabla EventoTipo </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 04/12/2002
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */
public class TipoEvento {

  /**
   * Nombre de la clase de persistencia asociada a la clase de Negocio EventoTipo
   */
        public static final String ID_TABLA="EVENTO_TIPO";


        private static final String DB = "db"; //Nombre de la conexi�n a la base de datos


        // Devuelve un vector DE OBJETOS EventoTipo(la clase de persistencia)
        // con los Tipos de Eventos que cumplan la clausula del where.
        /**
         * Devuelve un vector DE OBJETOS SysCargos(la clase de persistencia)con los Cargos que cumplan la clausula del where.
         * @param String _where Clausula where para la consulta de objetos de clase EventoTipo
         * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
         * @return Devuelve un vector DE OBJETOS EventoTipo(la clase de persistencia) con los tipos de eventos que cumplan la clausula del where.
         * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
         */
        public Vector busquedaTiposEventos(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);

                  v = EventoTipo.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar Tipo Evento:"+ e) ;
                }

                finally{
                        pool.freeConnection(DB,conexion);
                }
                return v;
        }

        //Realiza la modificacion del Tipo de Evento.
        /**
         * Realiza la modificacion del Tipo de Evento
         * @param EventoTipo _eventip Objeto de clase EventoTipo (clase de persistencia asociada a tabla Evento_Tipo)
         * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
         * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Evento_Tipo
         */

        public void modificarTipoEvento(EventoTipo _eventip) throws SQLException,ExceptionSESCAM  {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                try{
                //System.out.println("Entra en modificarPerm");
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
                _eventip.update(conexion);
                conexion.commit();
                }catch(SQLException e){
//                        System.out.println("Error al Modificar el Tipo de Evento:"+ e);
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha podido realizar modificaci�n del tipo de evento  " + _eventip.getTpoevtoDesc() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                }catch(Exception ex){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha podido realizar modificaci�n del tipo de evento  " + _eventip.getTpoevtoDesc() + " Error -> " + ex.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
//                        System.out.println("Error al Modificar el Tipo de Evento:"+ ex);
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }
} //fin de la clase

