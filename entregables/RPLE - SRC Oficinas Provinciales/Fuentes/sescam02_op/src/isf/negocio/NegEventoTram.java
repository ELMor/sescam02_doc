/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;

import java.util.Hashtable;
import java.util.Enumeration;
import isf.negocio.NegEventoVar;
import isf.persistencia.Evento;
import isf.persistencia.EventoVar;
import java.sql.*;
import conf.Constantes;


/**
 * <p>Clase: NegEventoTram </p>
 * <p>Descripcion: Clase que desciende de NegEvento para instancias eventos realizados con el tramite de garantia</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 17/12/2002
 * @author Jos� Vicente Nieto-M�rquez Fdez-Medina
 */

public class NegEventoTram extends NegEvento {
  /**
   gar_pk : PK de la garantia a la que esta referida el evento.
   */
  private long gar_pk;
  /**
   garOperacion : Operacion que se esta realizando sobre la garantia en cuestion.
   */

  private long garOperacion;

  /**
     * Contructor espec�fico de la clase, llama al constructor de la clase NegEvento de la que desciende.
     * @param pidEv : int, Identificador del evento que se ha producido
     * @param phte : Hashtable, Hashtable con informacion relativa a los eventos
     * @param plSessPK : long, Sess_pk de la tabla Evento_Session
     * @since 16/12/2002
     * @author JVN
   */
  public NegEventoTram(int pidEv,Hashtable phte, long plSessPK) {
    super(pidEv,phte,plSessPK);
  }

  /**
  * Guarda las variables relacionadas con un evento de tipo Impresion Tr�mite, Solicitud, Aprobaci�n, Denegaci�n, Renuncia.
  * @return Boolean, True si no se han producido errores y False si se han producido los errores
  * @since 17/12/2002
  * @author JVN
  */

  public boolean guardar(Connection conn) throws SQLException {
    if (super.hte.get(super.idEv+"/")!=null){
    Evento ev=new Evento();
    ev.setTpoevtoPk(super.idEv);
    ev.setSessPk(super.lSessPK);
    ev.setGarPk(this.gar_pk);
    ev.setGarOperacion(this.garOperacion);
    ev.newId(Constantes.FICHERO_CONFIGURACION);
    ev.insert(conn);
    // Asigno el Pk de evento a la variable local
    long lEvtoPk;
    lEvtoPk=ev.getEvtoPk();
    // Almaceno los valores de las variables
    Enumeration enumv= vVar.elements();
    EventoVar evv=new EventoVar();
    NegEventoVar nevn;
    while(enumv.hasMoreElements()){
      nevn=(NegEventoVar)enumv.nextElement();
      if( hte.get(super.idEv+"/"+nevn.id)==null) continue;
      evv.setEvtoPk(lEvtoPk);
      evv.setTpoevtoVarPk(nevn.id);
      evv.setEvtoVarValor(nevn.valor);
      evv.newId(Constantes.FICHERO_CONFIGURACION);
      evv.insert(conn);
      }
    }
    return true;
  }
  /**
  * Obtiene el valor del atributo gar_pk de la clase.
  * @return Long, gar_pk
  * @since 16/12/2002
  * @author JVN
  */
  public long getGarPk() {
   return gar_pk;
}

/**
 * Asigna el parametro al atributo gar_pk de la clase.
 * @param garpk :long, Gar_pk a guardar
 * @since 16/12/2002
 * @author JVN
 */
public void setGarPk(long garpk) {
this.gar_pk = garpk;
}
/**
* Obtiene el valor del atributo garOperacion de la clase.
* @return Long, gar_operacion
* @since 16/12/2002
* @author JVN
*/
public long getGarOperacion() {
 return garOperacion;
}

/**
* Asigna el parametro al atributo garOperacion de la clase.
* @param garop :long, GarOperacion a guardar
* @since 16/12/2002
* @author JVN
*/
public void setGarOperacion(long garop) {
this.garOperacion = garop;
}

}
