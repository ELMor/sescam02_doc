/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;
import conf.Constantes;

/**
 * <p>Clase: ProcGarantia </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesProcGarantia </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 27/02/2003
 * @author Jos� Vicente Nieto-Marquez
 */

public class ProcGarantias {

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio SesProcGarantia
            */
        public static final String ID_TABLA="SES_PROC_GARANTIA";



        /**
          * Devuelve un vector DE OBJETOS SesProcGarantia(la clase de persistencia) con las garantias que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase SesProcGarantia
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesProcGarantia (la clase de persistencia) con las Garantias que cumplan la clausula del where.
          * @author Jos� Vicente Nieto-Marquez
          */

        public Vector busquedaProcGarantia(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{

                          v = SesProcGarantia.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar Tipo de Procedimiento:"+ e) ;
                }

                finally{
                         pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }


        /**
          * Realiza la insercion de la Garantia
          * @param _sesPGar Objeto de clase SesProcGarantia (clase de persistencia asociada a tabla Ses_Proc_Garantia)
          * @author Jos� Vicente Nieto-Marquez
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Proc_Garantia
          * @throws Exception
         */

        public void insertarProcGar(SesProcGarantia _sesPGar) throws ExceptionSESCAM,Exception{
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesPGar.newId(Constantes.FICHERO_CONFIGURACION);
                   _sesPGar.insert(conexion);
                   conexion.commit();
                }catch(SQLException e){
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la insercion de la garantia para la especialidad con codigo " + _sesPGar.getSesespCod()+ " Error -> " + e.toString());
                        conexion.rollback();
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
                   }finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                   }
                }

        /**
          * Realiza la modificacion de la Garantia.
          * @param _sesPGar Objeto de clase SesProcGarantia (clase de persistencia asociada a tabla Ses_Proc_Garantia)
          * @author Jos� Vicente Nieto-Marquez
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Proc_Garantia
         */
        public void modificarProcGar(SesProcGarantia _sesPGar) throws SQLException,ExceptionSESCAM{
                Log fichero_log;
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesPGar.update(conexion);
                   conexion.commit();
                }catch(Exception e){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion de la garantia para la especialidad con codigo " + _sesPGar.getSesespCod()+ " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                 }

                 finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                 }

        }

        /**
         * Realiza la eliminacion de la Garantia.
         * @param _sesPGar Objeto de clase SesProcGarantia (clase de persistencia asociada a tabla Ses_Proc_Garantia)
         * @throws Exception
         * @author Jos� Vicente Nieto-Marquez
         */

        public void eliminarProcGar(SesProcGarantia _sesPGar) throws Exception,ExceptionSESCAM {
          Log log_fichero = null;
          Pool pool=null;
          Connection conexion = null;
          Log fichero_log = null;
          Vector v = new Vector();
          pool = Pool.getInstance();
          conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
          conexion.setAutoCommit(false);
          try{
            _sesPGar.delete(conexion);
            conexion.commit();
          }catch(SQLException e){
            conexion.rollback();
            fichero_log = Log.getInstance();
            fichero_log.error(" No se ha realizado la modificacion de la garantia para la especialidad con codigo " + _sesPGar.getSesespCod()+ " Error -> " + e.toString());
            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
            throw es;
          }
          finally{
            pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
          }
        }

} //fin de la clase

