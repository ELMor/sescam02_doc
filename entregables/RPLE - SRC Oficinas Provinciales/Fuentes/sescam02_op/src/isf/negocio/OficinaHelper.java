package isf.negocio;

/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

import java.util.Vector;
import isf.persistencia.*;



/**
 * <p>Clase: OficinaHelper</p>
 * <p>Descripcion: Clase Helper para la clase Oficina</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 11/11/2002
 * @author F�lix Alberto Bravo Hermoso
 */
 public class OficinaHelper {

   /**
    * Devuelve el nombre de la Oficina asociado al usuario pasado como parametro
    * @param String login Login del usuario de la tabla Sys_Usu
    * @return Devuelve el nombre de la Oficina asociado al usuario pasado como parametro
    */

  public static String DameOficina(String login){
    Usuarios user = new Usuarios();
    Vector vu = new Vector();
    Vector vo = new Vector();
    vu = user.busquedaUsu("SYSLOGIN='"+login+"'","");
    SysUsu sU=(SysUsu)vu.elementAt(0);
    long ofi_pk=sU.getOficinaPk();
    Oficina ofi= new Oficina();
    vo = ofi.busquedaOfi("OFICINA_PK="+ofi_pk,"");
    SesOficina sO=(SesOficina)vo.elementAt(0);
    return sO.getOficinaNombre();
  }

  /**
   * Devuelve el nombre de la Oficina a partir del pk pasado por parametro
   * @param long pk Pk de tabla Ses_Oficina
   * @return Devuelve el nombre de la Oficina asociada al pk de la tabla Ses_Oficina pasada como parametro.
   */

  public static String DameOficina(long pk){
    Oficina ofi = new Oficina();
    Vector vo = new Vector();
    vo = ofi.busquedaOfi("OFICINA_PK="+pk,"");
    SesOficina sO=(SesOficina)vo.elementAt(0);
    //System.out.println(sO.getOficinaNombre());
    return sO.getOficinaNombre();
  }

  /**
   * Devuelve el pk  de la Oficina (Tabla Ses_Oficina) asociado al usuario pasado como parametro
   * @param String login Login del usuario de la tabla Sys_Usu
   * @return Devuelve el pk de la Oficina (Tabla Ses_Oficina) asociado al usuario pasado como parametro
   */

  public static long DameOficinaPk(String login){
    Usuarios user = new Usuarios();
    Vector vu = new Vector();
    vu = user.busquedaUsu("SYSLOGIN='"+login+"'","");
    SysUsu sU=(SysUsu)vu.elementAt(0);
    return sU.getOficinaPk();
  }

}