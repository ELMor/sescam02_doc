package isf.negocio;

/**
 * <p>Title: ContinuosDet </p>
 * <p>Description: Clase que almacena registro del bean ConsultaContinuos</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author F�lix Alberto Bravo Hermoso
 * @version 1.0
 */

public class ContinuosDet {

   private int clave=0;
   private String fecha_inicio="S/D";
   private String fecha_fin="S/D";
   private String situacion="S/D";
   private int dias_acumulados=0;

  public int getDias_acumulados() {
    return dias_acumulados;
  }
  public void setDias_acumulados(int dias_acumulados) {
    this.dias_acumulados = dias_acumulados;
  }
  public void setFecha_fin(String fecha_fin) {
    this.fecha_fin = fecha_fin;
  }
  public String getFecha_fin() {
    return fecha_fin;
  }
  public String getFecha_inicio() {
    return fecha_inicio;
  }
  public void setFecha_inicio(String fecha_inicio) {
    this.fecha_inicio = fecha_inicio;
  }
  public void setSituacion(String situacion) {
    this.situacion = situacion;
  }
  public String getSituacion() {
    return situacion;
  }
  public int getClave() {
    return clave;
  }
  public void setClave(int clave) {
    this.clave = clave;
  }



}