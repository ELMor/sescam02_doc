package isf.negocio;

import  isf.persistencia.EventoSession;
import isf.db.*;
import java.sql.*;
import conf.Constantes;
import isf.util.log.Log;

public class NegEventoSession {

  private String strSyslogin;
  private int iOficina_pk;
  private String strSess_id;
  private String strSess_SrvRmt;
  private long iSess_pk;
  private EventoSession es;
  private long iRet;





  public NegEventoSession() {
    es=new EventoSession();
  }
  public void setStrSyslogin(String strSyslogin) {
    this.strSyslogin = strSyslogin;
    es.setSyslogin(strSyslogin);
  }
  public void setIOficina_pk(int iOficina_pk) {
    this.iOficina_pk = iOficina_pk;
    if (iOficina_pk!=0)
      es.setOficinaPk(iOficina_pk);
  }
  public void setStrSess_id(String strSess_id) {
    this.strSess_id = strSess_id;
    es.setSessId(strSess_id);
  }
  public void setStrSess_SrvRmt(String strSess_SrvRmt) {
    this.strSess_SrvRmt = strSess_SrvRmt;
    es.setSessSrvRmt(strSess_SrvRmt);
  }
  public long getISess_pk() {
    return iSess_pk;
  }
  public boolean guardar(){
    Connection conn = null;
    Pool ref_pool=null;
    ref_pool = Pool.getInstance();
    Log fichero_log;

    conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);
    try
    {
      conn.setAutoCommit(false);
      es.newId(Constantes.FICHERO_CONFIGURACION);
      es.insert(conn);

      conn.commit();
    }catch(Exception e){
        fichero_log = Log.getInstance();
        fichero_log.warning(" No se ha insertado el evento session: " + e.getMessage());

        try{conn.rollback();} catch(Exception esql){e.printStackTrace();}
        e.printStackTrace();
    }
    finally
    {
        ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
    }
    iSess_pk=es.getSessPk();
    return true;
  }

  public void setIRet(long iRet) {
    this.iRet = iRet;
    es.setSessAccCod(iRet);
  }
}