/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import conf.*;
import isf.util.log.Log;
/**
 * <p>Clase:  Garantia</p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesGarantia</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 30/10/2002
 * @author Felix Alberto Bravo Hermoso
 */
public class Garantia {
       /**
        * Nombre de la clase de persistencia asociada a la clase de Negocio Cargos
        */
        public static final String ID_TABLA="SES_GARANTIA";
        public String str_documento = "";

        private int ivar_evento1;
        private long icentro,inorden;

        private NegEventoTram negevent;


        /**
           * Instancia el objeto de clase NegEvento
           * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
           * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
           */

        public void SetNegEvento (NegEventoTram evento,int numero,String doc,long centro,long norden) {
          str_documento = doc;
          icentro = centro;
          inorden = norden;
          ivar_evento1 = numero;
          negevent = evento;
        }

        // Devuelve un vector DE OBJETOS SesGarantia(la clase de persistencia)
        // con las garantias que cumplan la clausula del where.

        /**
         * Devuelve un vector DE OBJETOS SesGarantia(la clase de persistencia)con las garantias que cumplan la clausula del where.
         * @param String _where Clausula where para la consulta de objetos de clase SesGarantia
         * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
         * @return Devuelve un vector DE OBJETOS SesGarantia(la clase de persistencia) con las garantias que cumplan la clausula del where.
         * @autor Felix Alberto Bravo Hermoso (30/11/2002)
        */
        public Vector busquedaGaran(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);

                v = SesGarantia.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar garantia:"+ e) ;
                }

                finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }

        //Realiza la insercion de la garantia y del tramo de garantia asociado.
        /**
         * Realiza la insercion de la garantia y del tramo de garantia asociado.
         * @param SesGarantia _sesGar Objeto de clase SesGarantia (clase de persistencia asociada a tabla Ses_Garantia)
         * @param long operacion Operacion que se va a efectuar 1-Solicitud, 2-Aprobacion Solicitud, 3-Denegacion Solicitud, 4-Renuncia
         * @param String login Login del usuario que desea realizar la operacion
         * @param String nombre Nombre del responsable de la operaci�n a realizar
         * @param String dni Dni del responsable de la operacion a realizar correctamente formateado
         * @autor Felix Alberto Bravo Hermoso (30/11/2002)
         * @throws SQLException Exception producida por la operacion INSERT de tabla Ses_Garantia o del INSERT sobre tabla Ses_Garan_Tram
         */
        public void insertarGaran(SesGarantia _sesGar,long operacion,String login,String nombre,String dni) throws SQLException,ExceptionSESCAM {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{
                //System.out.println("Entra en insertarGaran");
                //Insertamos la garantia.
                conexion.setAutoCommit(false);
                _sesGar.newId(Constantes.FICHERO_CONFIGURACION);
                _sesGar.insert(conexion);
                try {
                  //Inseramos el tramo correspondiente.
                  SesGaranTram sGT = new SesGaranTram();
                  sGT.setGarPk(_sesGar.getGarPk());
                  sGT.setSyslogin(login);
                  sGT.setOficinaPk(OficinaHelper.DameOficinaPk(login));
                  sGT.setGarReplegDni(dni);
                  sGT.setGarReplegApenom(nombre);
                  sGT.setGarOperacion(operacion);
                  //System.out.println("fin sets ");
                  /*inserto el objeto*/
                  sGT.newId(Constantes.FICHERO_CONFIGURACION);
                  sGT.insert(conexion);
                } catch (Exception e) {
                  fichero_log = Log.getInstance();
                  fichero_log.error(" Error al insertar el tramo de garantia asociado a la garantia de PK  " + _sesGar.getGarPk() + " Error -> " + e.toString());
                  ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                  throw es;
                }
                try {
                  if (negevent != null) {

                    negevent.setGarPk(_sesGar.getGarPk());
                    negevent.setGarOperacion(operacion);
                    negevent.addVar(ivar_evento1,str_documento);
                    negevent.addVar(ivar_evento1 + 1,login);
                    Long dato = new Long (icentro);
                    negevent.addVar(ivar_evento1 + 2,dato.toString());
                    dato = new Long (inorden);
                    negevent.addVar(ivar_evento1 + 3,dato.toString());

                    negevent.guardar(conexion);
                  }
                }
                catch (SQLException ex) {
                  fichero_log = Log.getInstance();
                  fichero_log.warning(" No se ha realizado el evento " + ivar_evento1 + " Error -> " + ex.toString());
                }
                conexion.commit();
                }catch(SQLException e){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" Error al insertar la garantia de PK  " + _sesGar.getGarPk() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
//                        System.out.println("Error al Insertar garantia:"+ e);
                }catch(Exception ex){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" Error al insertar la garantia de PK  " + _sesGar.getGarPk() + " Error -> " + ex.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
//                        System.out.println("Error al Insertar garantia:"+ ex);
                }
                finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
        }

        //Realiza la insercion del tramo de garantia.
        /**
         * Realiza la modificacion de la garantia y la insercion del tramo de garantia.
         * @param SesGarantia _sesGar Objeto de clase SesGarantia (clase de persistencia asociada a tabla Ses_Garantia)
         * @param long operacion Operacion que se va a efectuar 1-Solicitud, 2-Aprobacion Solicitud, 3-Denegacion Solicitud, 4-Renuncia
         * @param String login Login del usuario que desea realizar la operacion
         * @param String nombre Nombre del responsable de la operaci�n a realizar
         * @param String dni Dni del responsable de la operacion a realizar correctamente formateado
         * @autor Felix Alberto Bravo Hermoso (30/11/2002)
         * @throws SQLException Exception producida por la operacion UPDATE de tabla Ses_Garantia o del INSERT sobre tabla Ses_Garan_Tram
         */

        public void modificarGaran(SesGarantia _sesGar,long operacion,String login,String nombre,String dni) throws SQLException,ExceptionSESCAM {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{
                //System.out.println("Entra en insertarGaran");
                //Modificamos la garantia.
                conexion.setAutoCommit(false);
                _sesGar.update(conexion);
                try {
//                 System.out.println("Update hecho");
                //Insertamos el nuevo tramo correspondiente.
                  SesGaranTram sGT = new SesGaranTram();
                  sGT.setGarPk(_sesGar.getGarPk());
                  sGT.setSyslogin(login);
                  sGT.setOficinaPk(OficinaHelper.DameOficinaPk(login));
                  sGT.setGarReplegDni(dni);
                  sGT.setGarReplegApenom(nombre);
                  sGT.setGarOperacion(operacion);
                  //System.out.println("fin sets ");
                  /*inserto el objeto*/
                  sGT.newId(Constantes.FICHERO_CONFIGURACION);

                  sGT.insert(conexion);
  //                System.out.println("Update hecho");
                }catch (Exception e) {
                  fichero_log = Log.getInstance();
                  fichero_log.error(" Error al insertar el tramo de garantia asociado a la garantia de PK  " + _sesGar.getGarPk() + " Error -> " + e.toString());
                  ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                  throw es;
                }
                try {
                  if (negevent != null) {
                    negevent.setGarPk(_sesGar.getGarPk());
                    negevent.setGarOperacion(operacion);
                    negevent.addVar(ivar_evento1, str_documento);
                    negevent.addVar(ivar_evento1 + 1,login);
                    Long dato = new Long (icentro);
                    negevent.addVar(ivar_evento1 + 2,dato.toString());
                    dato = new Long (inorden);
                    negevent.addVar(ivar_evento1 + 3,dato.toString());

                    negevent.guardar(conexion);
                  }
                }
                catch (SQLException ex) {
                  fichero_log = Log.getInstance();
                  fichero_log.warning(" No se ha realizado el evento " + ivar_evento1 + " Error -> " + ex.toString());
                }
              // System.out.println("Insert hecho");
                conexion.commit();
                }catch(SQLException e){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" Error al modificar la garantia de PK  " + _sesGar.getGarPk() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
//                        System.out.println("Error SQL al Modificar garantia:"+ e);
                }catch(Exception ex){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" Error al modificar la garantia de PK  " + _sesGar.getGarPk() + " Error -> " + ex.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
//                        System.out.println("Error al Modificar garantia:"+ ex);
                }
                finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
        }

        //Realiza la eliminacion de la garantia.

       /**
        * Realiza la eliminacion de la garantia.
        * @param SesGarantia _sesGar Objeto de clase SesGarantia (clase de persistencia asociada a tabla Ses_Garantia)
        * @autor Felix Alberto Bravo Hermoso (30/11/2002)
        * @throws ExceptionSESCAM Excepcion producida porque no se cumpla alguna restriccion de integridad con las tablas asociadas
        */

        public void eliminarGaran(SesGarantia _sesGar) throws ExceptionSESCAM,Exception {
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                SysPermRol spr = new SysPermRol();
                try{
                //System.out.println("Entra en metodo eliminarGaran");
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                _sesGar.delete(conexion);
                conexion.commit();
                //System.out.println("Delete hecho");
                }catch(SQLException e){

                        System.out.println("Error al Modificar garantia:"+ e);
                        conexion.rollback();
                }
                finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
        }



} //fin de la clase