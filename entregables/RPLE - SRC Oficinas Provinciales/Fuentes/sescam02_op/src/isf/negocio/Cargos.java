/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;

import isf.db.*;

import isf.exceptions.*;

import isf.persistencia.*;

import isf.util.log.Log;

// Imports
import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: Cargos </p>
 * <p>Descripcion: Clase de negocio de la tabla Cargos </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 26/10/2002
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */
public class Cargos {
    /**
     * Nombre de la clase de persistencia asociada a la clase de Negocio Cargos
     */
    public static final String ID_TABLA = "SYS_CARGOS";
    private static final String DB = "db"; //Nombre de la conexi�n a la base de datos
    private NegEvento negevent;

    /**
    * Instancia el objeto de clase NegEvento
    * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
    * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
    */
    public void SetNegEvento(NegEvento evento) {
        negevent = evento;
    }

    // Devuelve un vector DE OBJETOS SysCargos(la clase de persistencia)
    // con los Cargos que cumplan la clausula del where.

    /**
     * Devuelve un vector DE OBJETOS SysCargos(la clase de persistencia)con los Cargos que cumplan la clausula del where.
     * @param String _where Clausula where para la consulta de objetos de clase SysCargos
     * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
     * @return Devuelve un vector DE OBJETOS SysCargos(la clase de persistencia) con los Cargos que cumplan la clausula del where.
     * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
     */
    public Vector busquedaCargo(String _where, String _sort) {
        Vector v = new Vector();
        Pool pool = null;
        Connection conexion = null;

        try {
            pool = Pool.getInstance();
            conexion = pool.getConnection(DB);

            v = SysCargos.search(conexion, _where, _sort);
        } catch (Exception e) {
            System.out.println("Error al Buscar Cargo:" + e);
        }
         finally {
            pool.freeConnection(DB, conexion);
        }

        return v;
    }

    //Realiza la insercion del Cargo.

    /**
     * Realiza la insercion del Cargo
     * @param SysCargos _syscar Objeto de clase SysCargos (clase de persistencia asociada a tabla SysCargos)
     * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
     * @throws SQLException Exception producida por la operacion INSERT sobre tabla Sys_Cargos
     */
    public void insertarCargo(SysCargos _syscar)
        throws SQLException, ExceptionSESCAM {
        Log fichero_log = null;
        Pool pool = null;
        Connection conexion = null;

        try {
            pool = Pool.getInstance();
            conexion = pool.getConnection(DB);

            //System.out.println("Entra en insertarPerm");
            _syscar.newId(DB);
            _syscar.insert(conexion);

            try {
                if (negevent != null) {
                    Long dato = new Long(_syscar.getSysCargosPk());
                    negevent.addVar(37, dato.toString());
                    negevent.addVar(38, _syscar.getSysCargosNombre());
                    negevent.guardar(conexion);
                }
            } catch (SQLException ex) {
                fichero_log = Log.getInstance();
                fichero_log.warning(
                    " No se ha realizado el evento de inserci�n del cargo con nombre  = " +
                    _syscar.getSysCargosNombre() + " Error -> " +
                    ex.toString());
            }

            conexion.commit();
        } catch (SQLException e) {
            conexion.rollback();

            //                        System.out.println("Error al Insertar el Cargo:"+ e);
            fichero_log = Log.getInstance();
            fichero_log.error(" No se ha realizado la inserci�n del cargo " +
                _syscar.getSysCargosNombre() + " Error -> " + e.toString());
            conexion.rollback();

            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
            throw es;
        } catch (Exception ex) {
            conexion.rollback();

            //System.out.println("Error al Insertar el Cargo:"+ ex);
            fichero_log = Log.getInstance();
            fichero_log.error(" No se ha realizado la inserci�n del cargo " +
                _syscar.getSysCargosNombre() + " Error -> " + ex.toString());
            conexion.rollback();

            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
            throw es;
        } finally {
            pool.freeConnection(DB, conexion);
        }
    }

    //Realiza la modificacion del Permiso.

    /**
     * Realiza la modificacion del Cargo
     * @param SysCargos _syscar Objeto de clase SysCargos (clase de persistencia asociada a tabla SysCargos)
     * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
     * @throws SQLException Exception producida por la operacion UPDATE sobre tabla SysCargos
     */
    public void modificarCargo(SysCargos _syscar)
        throws SQLException, ExceptionSESCAM {
        Log fichero_log = null;
        Pool pool = null;
        Connection conexion = null;

        try {
            //System.out.println("Entra en modificarPerm");
            pool = Pool.getInstance();
            conexion = pool.getConnection(DB);
            _syscar.update(conexion);

            try {
                if (negevent != null) {
                    Long dato = new Long(_syscar.getSysCargosPk());
                    negevent.addVar(39, dato.toString());
                    negevent.addVar(40, _syscar.getSysCargosNombre());
                    negevent.guardar(conexion);
                }
            } catch (SQLException ex) {
                fichero_log = Log.getInstance();
                fichero_log.warning(
                    " No se ha realizado el evento de modificaci�n del cargo con nombre  = " +
                    _syscar.getSysCargosNombre() + " Error -> " +
                    ex.toString());
            }

            conexion.commit();
        } catch (SQLException e) {
            //                        System.out.println("Error al Modificar el Cargo:"+ e);
            fichero_log = Log.getInstance();
            fichero_log.error(" No se ha realizado la modificaci�n del cargo " +
                _syscar.getSysCargosNombre() + " Error -> " + e.toString());
            conexion.rollback();

            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
            throw es;
        } catch (Exception ex) {
            fichero_log = Log.getInstance();
            fichero_log.error(" No se ha realizado la modificaci�n del cargo " +
                _syscar.getSysCargosNombre() + " Error -> " + ex.toString());
            conexion.rollback();

            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
            throw es;

            //System.out.println("Error al Modificar el Cargo:"+ ex);
        } finally {
            pool.freeConnection(DB, conexion);
        }
    }

    //Realiza la eliminacion del Cargo.

    /**
     * Realiza la eliminacion del Cargo.
     * @param SysCargos _syscar Objeto de clase SysCargos (clase de persistencia asociada a tabla SysCargos)
     * @autor Jos� Vicente Nieto-Marquez Fdez-Medina
     * @throws ExceptionSESCAM Excepcion producida porque no se cumpla alguna restriccion de integridad con las tablas asociadas
     */
    public void eliminarCargo(SysCargos _syscar)
        throws ExceptionSESCAM, Exception {
        Log fichero_log = null;
        Pool pool = null;
        Connection conexion = null;
        Vector v = new Vector();

        if (_syscar.getSysCargosPk() == 1) {
            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.CARG0_RESPONSABLE);
            throw es;
        } else {
            try {
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);

                //System.out.println("Tama�o v:"+v.size());
                _syscar.delete(conexion);

                try {
                    if (negevent != null) {
                        Long dato = new Long(_syscar.getSysCargosPk());
                        negevent.addVar(41, dato.toString());
                        negevent.guardar(conexion);
                    }
                } catch (SQLException ex) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(
                        " No se ha realizado el evento de modificaci�n del cargo con nombre  = " +
                        _syscar.getSysCargosNombre() + " Error -> " +
                        ex.toString());
                }

                conexion.commit();
            } catch (SQLException e) {
                String mensaje = new String(e.getMessage());

                if ((mensaje.indexOf("FK_SYS_USU_RELATION__SYS_CARG")) > 0) {
                    fichero_log = Log.getInstance();
                    fichero_log.error(
                        " No se ha realizado la eliminaci�n del cargo " +
                        _syscar.getSysCargosNombre() + " Error -> " +
                        e.toString());
                    conexion.rollback();

                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
                    throw es;
                } else {
                    conexion.rollback();
                    fichero_log = Log.getInstance();
                    fichero_log.error(
                        " No se ha realizado la eliminaci�n del cargo " +
                        _syscar.getSysCargosNombre() + " Error -> " +
                        e.toString());

                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
                    throw es;

                    //                           System.out.println(" Error al borrar un cargo: "+e);
                }
            } finally {
                pool.freeConnection(DB, conexion);
            }
        }
    }

    /*                public static void main(String args[]) {
                Vector v = new Vector();
                    Usuarios user = new Usuarios();
                    try
                     {
                    v = user.busquedaUsu("ACTIVO_SN=1 AND SYSLOGIN='adm'","APELLIDO1 ASC");
                    System.out.println("4") ;
                    for(int int_pos = 0;int_pos < v.size(); int_pos++){
                                                    SysUsu x=(SysUsu)v.elementAt(int_pos);
                                                    System.out.print(x.getApellido1() +"-");
                                                    System.out.print(x.getApellido2() +"-");
                                                    System.out.println(x.getNombre() +"");
                            }
                     }catch (Exception e){
                            System.out.println("Error Excepcion Main:"+ e) ;
                     }

                      }                */
}
 //fin de la clase
