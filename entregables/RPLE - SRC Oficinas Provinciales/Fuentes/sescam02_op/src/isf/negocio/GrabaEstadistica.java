/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.*;
import isf.db.*;
import isf.persistencia.*;

/**
 * <p>Clase: ConsultaMonto </p>
 * <p>Descripcion: Clase de negocio para obtener Monto por defecto para una entrada en Lista Espera </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 04/11/2002
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */

public class GrabaEstadistica{

  /**
  * Constructor de la clase, inicializa atributos
  */
  public GrabaEstadistica() {
  }
  private String periodo;
  private String tlista;

  public void setPeriodo(String periodo) {
    this.periodo = periodo;
  }
  public String getPeriodo(){
    return(this.periodo);
  }

  public boolean graba_sync(Connection conni,long codtramo) throws Exception{
    SesEstadisticasSync sye=new SesEstadisticasSync();
    Tramos tram = new Tramos();
    if (sye.countByCriteria(conni," esync_periodo='"+this.getPeriodo()+"' and est_tlista='"+this.getTlista()+"'")>0){
      return false;
      }
    else{
      String clob = tram.CadenaTramo(codtramo);
      sye.setEstTlista(this.getTlista());
      sye.setEsyncPeriodo(this.getPeriodo());
      sye.setEstTerminado(0);
      sye.setEsyncTramos(clob);
      sye.setEsyncCntramos(tram.getNTramos());
      sye.insert(conni);
      sye.ActualizarCLOB(conni,"");
      return true;
    }
  }
  public boolean grabar() throws Exception{
    // Validaci�n de parametros
    boolean ftoday=false;
    if ((periodo==null)||(periodo.equals(""))){
      // Setear el periodo con la fecha actual
      ftoday=true;
      Date dge=new Date();
      SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMddhhmm");
      this.setPeriodo(formatter.format(dge));
      System.out.println("El periodo es :"+this.getPeriodo());
    }
    if ((tlista==null)||(tlista.equals(""))){
      return false;
    }
    Connection conn = null;
    Connection conni = null;
    Pool ref_pool=null;
    SesEstadisticas se=new SesEstadisticas();
    PreparedStatement st;
    ResultSet rs;
    String sentBusqueda;
    if (this.tlista.equals("Q")){
      if(ftoday){
        sentBusqueda=" select centro, espe, serv, proc,tproc,diag,catalog,t.codtramo,t.POSICION, sum(pacientes) cnt_pacientes,sum(espera*pacientes) cnt_dias "+
                     " from ses_tramos t, "+
                     " (select le.CENTRO centro,le.SECC espe,le.SERV serv, le.CPR1 proc,'Q' tproc, le.CDI1 diag, "+
                     "  ce.acu+decode(sign(signo),1,trunc(sysdate)-ce.fi,0) espera, "+
                     "  le.catalog catalog, "+
                     "  count(*) pacientes "+
                     "  from lesp le , continuos ce "+
                     "  where le.CENTRO=ce.CENTRO and ce.TIPO=1 and le.NORDEN=ce.CLAVEINICIAL "+
                     "  and (le.LINK IS NULL OR le.LINK = -le.norden) "+
                     "  and decode(sign(signo),1,1,0)=1 "+
                     "  and trunc(sysdate) between ce.fi and nvl(ce.ff,sysdate) "+
                     "  group by le.CENTRO ,le.SECC ,le.SERV , le.CPR1 ,'Q' , le.CDI1,le.catalog, ce.acu+decode(sign(signo),1,trunc(sysdate)-ce.fi,0) "+
                     " )c "+
                     " where (t.inicio<=c.espera or t.inicio is null) "+
                     " and (t.fin>=c.espera  or t.fin is null) "+
                     " and t.CODTRAMO=1 "+
                     " group by centro, espe, serv, proc,tproc,diag,catalog,t.codtramo,t.POSICION ";
      }else{
        sentBusqueda=" select centro, espe, serv, proc,tproc,diag,catalog,t.codtramo,t.POSICION, sum(pacientes) cnt_pacientes,sum(espera*pacientes) cnt_dias "+
                     " from ses_tramos t, "+
                     " (select le.CENTRO centro,le.SECC espe,le.SERV serv, le.CPR1 proc,'Q' tproc, le.CDI1 diag, "+
                     "  ce.acu+decode(sign(signo),1,trunc(to_date('"+this.getPeriodo()+"','YYYYMMDDHH24MI'))-ce.fi,0) espera, "+
                     "  le.catalog catalog, "+
                     "  count(*) pacientes "+
                     "  from lesp le , continuos ce "+
                     "  where le.CENTRO=ce.CENTRO and ce.TIPO=1 and le.NORDEN=ce.CLAVEINICIAL "+
                     "  and (le.LINK IS NULL OR le.LINK = -le.norden) "+
                         "  and decode(sign(signo),1,1,0)=1 "+
                     "  and trunc(to_date('"+this.getPeriodo()+"','YYYYMMDDHH24MI')) between ce.fi and nvl(ce.ff,to_date('"+this.getPeriodo()+"','YYYYMMDDHH24MI')) "+
                     "  group by le.CENTRO ,le.SECC ,le.SERV , le.CPR1 ,'Q' , le.CDI1,le.catalog, ce.acu+decode(sign(signo),1,trunc(to_date('"+this.getPeriodo()+"','YYYYMMDDHH24MI'))-ce.fi,0) "+
                     " )c "+
                     " where (t.inicio<=c.espera or t.inicio is null) "+
                     " and (t.fin>=c.espera  or t.fin is null) "+
                     " and t.CODTRAMO=1 "+
                     " group by centro, espe, serv, proc,tproc,diag,catalog,t.codtramo,t.POSICION ";
      }
    }else{
      return false;
    }
    ref_pool = Pool.getInstance();
    conni= ref_pool.getConnection(conf.Constantes.FICHERO_CONFIGURACION);
  if (!this.graba_sync(conni,1)){
      ref_pool.freeConnection(conf.Constantes.FICHERO_CONFIGURACION, conni);
      System.out.println("Error: Periodo ya almacenado.");
      return false;
    }
    conn = ref_pool.getConnection(conf.Constantes.FICHERO_CONFIGURACION);
    try{
      conn.setAutoCommit(false);
      st=conn.prepareStatement(sentBusqueda);
      rs=st.executeQuery();
      while(rs.next()) {
        se.newId(conf.Constantes.FICHERO_CONFIGURACION);
        se.setEstTlista(this.tlista);
        se.setEstPeriodo(this.periodo);
        se.setCodcentro(rs.getInt("centro"));
        if (rs.getString("espe")!=null) se.setEspCenCod(rs.getString("espe"));
        else se.setNullEspCenCod();
        if (rs.getString("serv")!=null) se.setServCenCod(rs.getString("serv"));
        else se.setNullServCenCod();
        if (rs.getString("proc")!=null) se.setProcCenCod(rs.getString("proc"));
        else se.setNullProcCenCod();
        if (rs.getString("tproc")!=null) se.setTprocCenCod(rs.getString("tproc"));
        else se.setNullTprocCenCod();
        if (rs.getString("catalog")!=null) se.setEstCatalog(rs.getString("catalog"));
        else se.setNullEstCatalog();
        if (rs.getString("diag")!=null) se.setDiagCenCod(rs.getString("diag"));
        else se.setNullDiagCenCod();
        se.setCodtramo(rs.getInt("codtramo"));
        se.setPosicion(rs.getInt("posicion"));
        se.setEstCntPac(rs.getLong("cnt_pacientes"));
        se.setEstCntDias(rs.getLong("cnt_dias"));
        se.insert(conni);
      }
      conni.commit();
      conn.commit();
       try {
          rs.close();
          st.close();
        }
       catch (Exception ex){throw ex;}
      }catch(Exception e){
        e.printStackTrace();
        try{
          conn.rollback();
          conni.rollback();
        }
        catch(Exception esql){
        esql.printStackTrace();
        throw esql;
        }
        throw e;
      }
      finally{
        ref_pool.freeConnection(conf.Constantes.FICHERO_CONFIGURACION, conn);
        ref_pool.freeConnection(conf.Constantes.FICHERO_CONFIGURACION, conni);
      }
      return true;
  }

  static public void main(String[] args){
    GrabaEstadistica ge=new GrabaEstadistica();
    try {
      if(args.length==0){
        System.out.println("Usage: GrabaEstadistica <tipoLista> <periodo> ");
        System.out.println("       GrabaEstadistica <tipoLista> ");
        System.out.println("tipoLista: Tipo de informaci�n que debe almacenar. ");
        System.out.println("           Q -Quir�rgica ");
//        System.out.println("           C -Consultas  ");
//        System.out.println("           T -T�cnicas   ");
        System.out.println("periodo: Fecha en la cual se debe calcular la estadistica. Si no se pasa el par�metro se asume la fecha actual");
        System.out.println("         El formato en el que se debe escribir este parametro es 'YYYYMMDDHHMM'.");
        return;
      }
      if(args.length==1){
        if (args[0].equals("Q")) ge.setTlista("Q");
        else{
          System.out.println(" Error: Parametro Tipo de lista incorrecto.");
          return;
        }
      }
      if(args.length==2){
        if (args[0].equals("Q")) ge.setTlista("Q");
        else{
          System.out.println(" Error: Parametro Tipo de lista incorrcto.");
          return;
        }
        // Setear el periodo con la fecha actual
        ge.setPeriodo(args[1]);
      }
      if (ge.grabar()) System.out.println("Grab� correctamente, Tipo de lista:"+ge.getTlista()+" Periodo:"+ge.getPeriodo());
      else System.out.println("No se han grabado datos.");
    }
    catch (Exception ex) {
      System.out.println("Error: No se ha grabado la estadistica.");
      ex.printStackTrace();
    }
  }
  public void setTlista(String tlista) {
    this.tlista = tlista;
  }
  public String getTlista() {
    return tlista;
  }
}
