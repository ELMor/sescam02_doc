/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 * <p>Clase: ProcMapeados </p>
 * <p>Descripcion: Clase que guarda informaci�n de los Procedimientos que pueden ser mapeados automaticamente</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 10/02/2003
 * @author Jos� Vicente Nieto-Marquez Fdez-Medina
 */

public class ProcMapeados {
    /**
     * pk del procedimiento  obtenido en la consulta
     */
    public long 	ProcPk=0;

    /**
      * Codigo del Procedimiento SESCAM obtenido en la consulta
      */
    public String 	CodProcSESCAM="SD";

    /**
     * Codigo del Tipo de Procedimiento SESCAM  obtenido en la consulta
     */
    public String 	CodProcTipoSESCAM="SD";

    /**
      * Descripci�n del procedimiento obtenido en la consulta
      */
    public String 	ProcDesc="SD";

    /**
     * Descripcion del procedimiento a mapear
     */
    public String       ProcDescMapeo="SD";
    /**
      *  Descripcion del tipo de procedimiento
      */
    public String        CodProcTipo="SD";

    /**
      *  Descripcion del codigo de procedimiento mapeado
      */
    public String        CodProcMap="SD";

    /**
      *  Descripcion del codigo de centro del mapeo
      */
    public long          CodCentroMap=0;

    /**
      *  Descripcion del codigo de centro mapeado
      */
    public String        DesCentroMap="SD";

    /**
      *  Valor que indica si el mapeo del procedimiento ha sido modificado     */
    public long          Modificado=0;





}