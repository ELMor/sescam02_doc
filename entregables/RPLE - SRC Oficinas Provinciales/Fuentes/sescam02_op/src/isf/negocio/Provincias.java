/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;


// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import conf.*;

/**
 * <p>Clase: Provincias </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesProvincias</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 30/10/2002
 * @author Felix Alberto Bravo Hermoso
 */

public class Provincias {
        /**
          * Nombre de la clase de persistencia asociada a la clase de Negocio Provincias
          */

        public static final String ID_TABLA="SES_PROVINCIAS";

        private static final String DB = "db"; //Nombre de la conexi�n a la base de datos


        // Devuelve un vector DE OBJETOS SeS_Provincias la clase de persistencia)
        // con las provincias que cumplan la clausula del where.

        /**
          * Devuelve un vector DE OBJETOS Ses_Provincias(la clase de persistencia) con las provincias que cumplan la clausula del where.
          * @param String _where Clausula where para la consulta de objetos de clase SesProvincias
          * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesProvincias (la clase de persistencia) con las provincias que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

        public Vector busquedaProv(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);

                  v = SesProvincias.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar la Provincia:"+ e) ;
                }

                finally{
                        pool.freeConnection(DB,conexion);
                }
                return v;
        }

        //Realiza la insercion de la Provincia.
        /**
         * Realiza la insercion de la Provincia.
         * @param SesProvincias _sesProv Objeto de clase SesProvincias (clase de persistencia asociada a tabla Ses_Provincias)
         * @autor Felix Alberto Bravo Hermoso
         * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Provincias
         */
        public void insertarProv(SesProvincias _sesProv) throws SQLException {
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
                //System.out.println("Entra en insertarPerm");
                _sesProv.newId(DB);
                _sesProv.insert(conexion);
                conexion.commit();
                }catch(SQLException e){
                        conexion.rollback();
                        System.out.println("Error al Insertar la Provincia:"+ e);
                }catch(Exception ex){
                        conexion.rollback();
                        System.out.println("Error al Insertar la Provincia:"+ ex);
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }

        //Realiza la modificacion de la Provincia.
        /**
         * Realiza la modificacion de la Provincia.
         * @param SesProvincias _sesProv Objeto de clase SesProvincias (clase de persistencia asociada a tabla Ses_Provincias)
         * @autor Felix Alberto Bravo Hermoso
         * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Provincias
         */

        public void modificarProv(SesProvincias _sesProv) throws SQLException  {
                Pool pool=null;
                Connection conexion = null;
                try{
                //System.out.println("Entra en modificarPerm");
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
                _sesProv.update(conexion);
                conexion.commit();
                }catch(SQLException e){
                        System.out.println("Error al Modificar la Provincia:"+ e);
                        conexion.rollback();
                }catch(Exception ex){
                        conexion.rollback();
                        System.out.println("Error al Modificar la Provincia:"+ ex);
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }

        //Realiza la eliminacion de la Provincia.
        /**
         * Realiza la eliminacion de la Provincia.
         * @param SesProvincias _sesProv Objeto de clase SesProvincias (clase de persistencia asociada a tabla Ses_Provincias)
         * @autor Felix Alberto Bravo Hermoso
         * @throws ExceptionSESCAM Exception producida al incumplirse las restricciones de integridad con alguna de las tablas asociadas
         */

        public void eliminarProv(SesProvincias _sesProv) throws ExceptionSESCAM,Exception {
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                SesLocalidades sloc = new SesLocalidades();
                try{
                //System.out.println("Entra en metodo eliminarPerm");
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
                v = sloc.search(conexion,"PROVINCIA_PK="+_sesProv.getProvinciaPk(),"");
                //System.out.println("Tama�o v:"+v.size());
                if (v.size()>0){
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.PROVINCIA_USADA);
                        throw es;
                }else{
                 _sesProv.delete(conexion);
                 conexion.commit();
                 //System.out.println("Delete hecho");
                }

                }catch(SQLException e){
                        System.out.println("Error al Eliminar la Provincia:"+ e);
                        conexion.rollback();
                }
                finally{
                        pool.freeConnection(DB,conexion);
                }
        }


} //fin de la clase

