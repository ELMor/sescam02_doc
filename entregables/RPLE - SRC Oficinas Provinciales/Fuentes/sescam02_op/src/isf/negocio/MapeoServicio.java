/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.*;
import isf.util.log.Log;
import conf.Constantes;

/**
 * <p>Clase: MapeoServicio </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesSrvMap</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 30/01/2003
 * @author Felix Alberto Bravo Hermoso
 */

public class MapeoServicio {

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio MapeoServicio
            */
        public static final String ID_TABLA="SES_SERV_MAP";
        private NegEvento negevent;


        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }


        /**
          * Devuelve un vector DE OBJETOS SesSrvMap(la clase de persistencia) con los servicios que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase SesSrvMap
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesSrvMap (la clase de persistencia) con los servicios que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

        public Vector busquedaMapServ(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{

                          v = SesSrvMap.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar el Servicio mapeado:"+ e) ;
                }

                finally{
                         pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }


        /**
          * Realiza la insercion del Servicio mapeado.
          * @param _sesMapServ Objeto de clase SesSrvMap (clase de persistencia asociada a tabla Ses_Srv_Map)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Srv_Map
          * @throws Exception
         */

        public void insertarMapServ(SesSrvMap _sesMapServ) throws ExceptionSESCAM,Exception{
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                v = busquedaMapServ("SRV_CEN_COD='"+_sesMapServ.getSrvCenCod()+"' AND CENTRO="+_sesMapServ.getCentro(),"");
                if (v.size()>0){
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_MAPEO_EXISTENTE);
                    throw es;
                }else{
                try{
                   _sesMapServ.insert(conexion);
                   try {
                     if (negevent != null) {
                      negevent.addVar(100,_sesMapServ.getSessrvCod());
                      negevent.addVar(101,_sesMapServ.getSrvCenCod());
                      Double centro = new Double(_sesMapServ.getCentro());
                      negevent.addVar(102,centro.toString());

                      negevent.guardar(conexion);
                    }
                   }catch (Exception e) {
                         fichero_log = Log.getInstance();
                         fichero_log.warning(" No se ha insertado el evento de inserci�n del Servicio con C�digo = " + _sesMapServ.getSrvCenCod() + "para el Centro:"+ _sesMapServ.getCentro()+" Error -> " + e.toString());
                   }
                   conexion.commit();
                }catch(SQLException e){
//			System.out.println("Error al Insertar el Usuario:"+ e) ;
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la insercion del Servicio con C�digo = " + _sesMapServ.getSrvCenCod() + "para el Centro:"+ _sesMapServ.getCentro()+" Error -> " + e.toString());
                        conexion.rollback();
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
                   }finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                   }
                }
        }

        /**
          * Realiza la modificacion del Mapeo del Servicio.
          * @param _sesMapServ Objeto de clase SesSrvMap (clase de persistencia asociada a tabla Ses_Srv_Map)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Srv_Map
         */
        public void modificarMapServ(SesSrvMap _sesMapServ) throws SQLException,ExceptionSESCAM{
                Log fichero_log;
                Pool pool=null;
                Connection conexion = null;
                TrataClave tC = new TrataClave();

                pool = Pool.getInstance();
                Vector v = new Vector();
                try{
                    conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                    conexion.setAutoCommit(false);
                    _sesMapServ.update(conexion);
                    try {
                      if (negevent != null) {
                        negevent.addVar(103,_sesMapServ.getSessrvCod());
                        negevent.addVar(104,_sesMapServ.getSrvCenCod());
                        Double centro = new Double(_sesMapServ.getCentro());
                        negevent.addVar(105,centro.toString());
                        negevent.guardar(conexion);
                      }
                    }catch (Exception e) {
                      fichero_log = Log.getInstance();
                      fichero_log.warning(" No se ha insertado el evento de modificacion del Mapeo del Servicio con C�digo = " + _sesMapServ.getSrvCenCod() + "para el Centro:"+_sesMapServ.getCentro()+" Error -> " + e.toString());
                    }
                    conexion.commit();
                  }catch(Exception e){
                    conexion.rollback();
//                        System.out.println("Error al Modificar el mapeo del servicio:"+ e) ;
                    fichero_log = Log.getInstance();
                    fichero_log.error(" No se ha realizado la modificacion del mapeo del servicio con C�digo = " + _sesMapServ.getSrvCenCod() + "para el Centro:"+_sesMapServ.getCentro()+" Error -> " + e.toString());
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                    throw es;
                  }
                  finally{
                    pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                  }
            }

        /**
         * Realiza la eliminacion del mapeo del servicio.
         * @param _sesMapServ Objeto de clase SesSrvMap (clase de persistencia asociada a tabla Ses_Srv_Map)
         * @throws Exception
         */

        public void eliminarMapServ(SesSrvMap _sesMapServ) throws Exception {
          Pool pool=null;
          Connection conexion = null;
          Log fichero_log = null;
          Vector v = new Vector();
          pool = Pool.getInstance();
          conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
          conexion.setAutoCommit(false);
          try{
            _sesMapServ.delete(conexion);
            try{
              if (negevent != null) {
                      negevent.addVar(107,_sesMapServ.getSrvCenCod());
                      Double centro = new Double(_sesMapServ.getCentro());
                      negevent.addVar(108,centro.toString());
                      negevent.guardar(conexion);
              }
            }catch(Exception e){
              fichero_log = Log.getInstance();
              fichero_log.warning(" No se ha insertado el evento de eliminacion del Mapeo de Servicio con C�digo = "+ _sesMapServ.getSrvCenCod() + "para el Centro:"+_sesMapServ.getCentro()+" Error -> " + e.toString());
            }
            conexion.commit();
          }catch(SQLException e){
            conexion.rollback();
            //System.out.println(" Error al borrar Servicio: "+e);
            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
            throw es;
           }
           finally{
             pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
           }
        }

} //fin de la clase

