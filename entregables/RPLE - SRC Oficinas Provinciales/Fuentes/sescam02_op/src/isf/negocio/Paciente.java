/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;

/**
 *
 * <p>Clase: Paciente </p>
 * <p>Descripcion: Clase que guarda la informaci�n de un paciente incluido en Lista de Espera</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 28/10/2002
 * @author Iv�n Enrique Caneri
 */

public class Paciente{

  /**
   * Numero de Orden (campo NOrden) si el paciente esta LESP o Numero de Cita (Campo NCita) del paciente es encontrado en LESPCEX
   */
   public int 		fNorden=0;

   /**
   * Centro al que pertenece el paciente (campo CENTRO si el paciente esta LESP o Campo CEGA si el paciente es encontrado en LESPCEX)
   */
   public int 		fCentro=0;

   /**
   * Centro al que pertenece el paciente (campo CENTRO si el paciente esta LESP o Campo CEGA si el paciente es encontrado en LESPCEX)
   */
   public String 	fDesCentro="SD";

   /**
   * Cip del paciente
   */
   public String 	fCip="SD";

   /**
   * Historia Clinica  del paciente
   */
   public int		fHicl=0;

   /**
   * Primer Apellido del paciente
   */
   public String 	fApe1="SD";

   /**
   * Segundo Apellido del paciente
   */
   public String 	fApe2="SD";

   /**
    * Nombre del paciente
    */
   public String 	fNomb="SD";

   /**
    * Fecha de Nacimiento del paciente
    */
   public String 	fNac ="SD";

   /**
    * Sexo del paciente
    */
   public String 	fSexo="SD";

   /**
    * Domicilio del paciente
    */
   public String 	fDomi="SD";

   /**
    * Poblacion de residencia del paciente
    */
   public String 	fPobl="SD";

   /**
    * Provincia de residencia del paciente
    */
   public String 	fProv="SD";
   
   /**
	 * Telefono del paciente paciente
	 */
   public String 	fTelef="SD";
   

}