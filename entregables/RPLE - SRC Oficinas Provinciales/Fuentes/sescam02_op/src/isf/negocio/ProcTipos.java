/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;
import conf.Constantes;

/**
 * <p>Clase: ProcTipos </p>
 * <p>Descripcion: Clase que maneja la clase de persistencia SesProcTipos </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Compa�ia: Soluziona</p>
 * @since 18/02/2003
 * @author Felix Alberto Bravo Hermoso
 */

public class ProcTipos {

          /**
            * Nombre de la clase de persistencia asociada a la clase de Negocio SesProcTipos
            */
        public static final String ID_TABLA="SES_PROC_TIPOS";
        private NegEvento negevent;


        /**
          * Instancia el objeto de clase NegEvento
          * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
          * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
          */

        public void SetNegEvento (NegEvento evento) {
          negevent = evento;
        }


        /**
          * Devuelve un vector DE OBJETOS SesProcTipos(la clase de persistencia) con los Tipos que cumplan la clausula del where.
          * @param _where Clausula where para la consulta de objetos de clase SesProcTipos
          * @param _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesProcTipos (la clase de persistencia) con los Tipos que cumplan la clausula del where.
          * @autor Felix Alberto Bravo Hermoso
          */

        public Vector busquedaProcTipo(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                try{

                          v = SesProcTipos.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar Tipo de Procedimiento:"+ e) ;
                }

                finally{
                         pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                }
                return v;
        }


        /**
          * Realiza la insercion del Tipo.
          * @param _sesPTipo Objeto de clase SesProcTipos (clase de persistencia asociada a tabla Ses_Proc_Tipos)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Proc_Tipos
          * @throws Exception
         */

        public void insertarProcTipo(SesProcTipos _sesPTipo) throws ExceptionSESCAM,Exception{
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesPTipo.newId(Constantes.FICHERO_CONFIGURACION);
                   _sesPTipo.insert(conexion);
                   try {
                     if (negevent != null) {
                      String valor= new Long(_sesPTipo.getProcTipo()).toString();
                      negevent.addVar(95,valor);
                      negevent.addVar(96,_sesPTipo.getProcTcod());
                      negevent.addVar(96,_sesPTipo.getProcTdesc());
                      negevent.guardar(conexion);
                    }
                   }catch (Exception e) {
                         fichero_log = Log.getInstance();
                         fichero_log.warning(" No se ha insertado el evento de inserci�n del tipo de procedimiento con codigo = " + _sesPTipo.getProcTcod() + " Error -> " + e.toString());
                   }
                   conexion.commit();
                }catch(SQLException e){
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la insercion del tipo de procedimiento con codigo = " + _sesPTipo.getProcTcod()+ " Error -> " + e.toString());
                        conexion.rollback();
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                        throw es;
                   }finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                   }
                }

        /**
          * Realiza la modificacion del Tipo de Procedimiento.
          * @param _sesPTipo Objeto de clase SesProcTipos (clase de persistencia asociada a tabla Ses_Proc_Tipos)
          * @autor Felix Alberto Bravo Hermoso
          * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Proc_Tipos
         */
        public void modificarEsp(SesProcTipos _sesPTipo) throws SQLException,ExceptionSESCAM{
                Log fichero_log;
                Pool pool=null;
                Connection conexion = null;
                pool = Pool.getInstance();
                conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
                conexion.setAutoCommit(false);
                try{
                   _sesPTipo.update(conexion);
                   try {
                    if (negevent != null) {
                      String valor= new Long(_sesPTipo.getProcTipo()).toString();
                      negevent.addVar(95,valor);
                      negevent.addVar(96,_sesPTipo.getProcTcod());
                      negevent.addVar(96,_sesPTipo.getProcTdesc());
                      negevent.guardar(conexion);
                    }
                   }catch (Exception e) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha insertado el evento de modificacion del Tipo de Procedimiento con C�digo = " + _sesPTipo.getProcTcod() + " Error -> " + e.toString());
                   }
                   conexion.commit();
                }catch(Exception e){
                        conexion.rollback();
                        fichero_log = Log.getInstance();
                        fichero_log.error(" No se ha realizado la modificacion del Tipo de Procedimiento con Codigo = " + _sesPTipo.getProcTcod() + " Error -> " + e.toString());
                        ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_MODIFICAR);
                        throw es;
                 }

                 finally{
                        pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
                 }

        }

        /**
         * Realiza la eliminacion del Tipo de Procedimiento.
         * @param _sesPTipo Objeto de clase SesProcTipos (clase de persistencia asociada a tabla Ses_Proc_Tipo)
         * @throws Exception
         */

        public void eliminarProcTipo(SesProcTipos _sesPTipo) throws Exception,ExceptionSESCAM {
          Log log_fichero = null;
          Pool pool=null;
          Connection conexion = null;
          Log fichero_log = null;
          Vector v = new Vector();
          pool = Pool.getInstance();
          conexion = pool.getConnection(Constantes.FICHERO_CONFIGURACION);
          conexion.setAutoCommit(false);
          try{
            _sesPTipo.delete(conexion);
            try{
              if (negevent != null) {
                  String valor= new Long(_sesPTipo.getProcTipo()).toString();
                  negevent.addVar(95,valor);
                  negevent.guardar(conexion);
                    }
            }catch(Exception e){
              fichero_log = Log.getInstance();
              fichero_log.warning(" No se ha insertado el evento de eliminacion del tipo de Procedimiento  con C�digo = " + _sesPTipo.getProcTcod() + " Error -> " + e.toString());
            }
            conexion.commit();
          }catch(SQLException e){
                  String mensaje = new String(e.getMessage());
                  if (mensaje.indexOf("FK_SES_PROC_TIPOS_RELATION__") > 0) {
                   conexion.rollback();
                   fichero_log = Log.getInstance();
                   fichero_log.error(" No se ha realizado la eliminacion del tipo de Procedimiento  con C�digo = " + _sesPTipo.getProcTcod()  + " Error -> " + e.toString());
                   ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.PROC_TIPO_USADO);
                   throw es;
                  }else {
                      conexion.rollback();
                      fichero_log = Log.getInstance();
                      fichero_log.error(" No se ha realizado la eliminacion del tipo de Procedimiento  con C�digo = " + _sesPTipo.getProcTcod() + " Error -> " + e.toString());
                      ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
                      throw es;
                    }
           }

           finally{
             pool.freeConnection(Constantes.FICHERO_CONFIGURACION,conexion);
           }
        }

} //fin de la clase

