/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/

package isf.negocio;
// Imports
import java.sql.*;
import java.util.Vector;
import isf.db.*;
import isf.persistencia.*;
import isf.exceptions.*;
import isf.util.log.Log;
/**
 * <p>Clase: Tramos </p>
 * <p>Descripcion: Clase de negocio de la tabla Tramos </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Compa�ia: Soluziona </p>
 * @since 04/03/2003
 * @author David Fernandez-Vegue Garcia
 */
public class Tramos {

  /**
   * Nombre de la clase de persistencia asociada a la clase de Negocio Tramos
   */
        public static final String ID_TABLA="SES_TRAMOS";
        private int NTramos = 0;
        static public final int LEQ = 1;
        static public final int CEX = 2;
        private static final String DB = "db"; //Nombre de la conexi�n a la base de datos
        static public final String[] STR_CODTRAMOS={"LEQ","CEX"};
        private NegEvento negevent;

        /**
        * Instancia el objeto de clase NegEvento
        * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
        * @autor David Fernandez-Vegue Garcia
        */

        public void SetNegEvento (NegEvento evento) {
         negevent = evento;
         }
        // Devuelve una cadena que describe los tramos de un codigo de tramo
        /**
         * Devuelve una cadena que describe los tramos de un codigo de tramo.
         * @param long codtramo - Codigo de tramo
         * @return Devuelve una cadena que describe los tramos de un codigo de tramo.
         * @autor Jos� Vicente Nieto
         */

         public String CadenaTramo(long codtramo) {
           Vector v = new Vector();
           String cadena = " CODTRAMO = "+codtramo;
           v = this.busquedaTramo(cadena,"");
           String toString = "";
           if (v.size() > 0) {
             this.setNTramos(v.size());
             toString = "[";
             int inicio,fin;

             for (int pos = 0; pos < v.size();pos++) {
               SesTramos sestram = (SesTramos)v.elementAt(pos);
               if (pos > 0) {
                 toString = toString + ",";
               }
               if  (sestram.isNullInicio())  {
                 inicio = 0;
               }else {
                 inicio = new Double(sestram.getInicio()).intValue();
               }

               if  (sestram.isNullFin())  {
                 fin = -1;
               }else {
                 fin = new Double(sestram.getFin()).intValue();
               }

               toString = toString + "(" +sestram.getDescripcion() + "," + inicio + "," +fin + ")";
             }
             toString = toString + "]";
           }
           return toString;
         }

         public Vector DescTramo(long codtramo) {
           Vector v = new Vector();
           Vector v1 = new Vector();
           String cadena = " CODTRAMO = "+codtramo;
           v = this.busquedaTramo(cadena,"");
           String toString = "";
           for (int pos = 0; pos < v.size();pos++) {
             SesTramos sestram = (SesTramos)v.elementAt(pos);
             v1.addElement(sestram.getDescripcion());
           }
           return v1;
         }

         // Devuelve un vector DE OBJETOS SesTramos(la clase de persistencia)
         // con los Tramos que cumplan la clausula del where.
         /**
          * Devuelve un vector DE OBJETOS SesTramos(la clase de persistencia)con los Tramos que cumplan la clausula del where.
          * @param String _where Clausula where para la consulta de objetos de clase SesTramos
          * @param String _sort Tipo de ordenaci�n deseada para los objetos devueltos por la consulta
          * @return Devuelve un vector DE OBJETOS SesTramos(la clase de persistencia) con los Tramos que cumplan la clausula del where.
          * @autor David Fernandez-Vegue Garcia
         */
         public Vector busquedaTramo(String _where,String _sort) {
                Vector v = new Vector();
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);

                  v = SesTramos.search(conexion,_where,_sort);
                }
                catch(Exception e){
                        System.out.println("Error al Buscar Tramo:"+ e) ;
                }

                finally{
                        pool.freeConnection(DB,conexion);
                }
                return v;
        }


	//Realiza la insercion del Tramo.
        /**
         * Realiza la insercion del Tramo
         * @param SesTramos _sesTramo Objeto de clase SesTramos (clase de persistencia asociada a tabla SesTramos)
	 * @param Conexion _connection Objeto de conexion utilizado para hacer el insert
         * @autor Jose Vicente Nieto-Marquez
         * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Tramos
         */

        public void insertarTramo(Connection conexion,SesTramos _sesTramo) throws SQLException,ExceptionSESCAM {
	  Log fichero_log = null;
	  try {
            _sesTramo.insert(conexion);
            try {
                   if (negevent != null) {

                   	Long codTramo = new Long (_sesTramo.getCodtramo());
                   	Long posicion = new Long (_sesTramo.getPosicion());
                   	Double inicio = new Double (_sesTramo.getInicio());
                   	Double fin = new Double (_sesTramo.getFin());
                   	negevent.addVar(145,codTramo.toString());
                   	negevent.addVar(146,posicion.toString());
                   	negevent.addVar(147,inicio.toString());
                   	negevent.addVar(148,fin.toString());
                   	negevent.addVar(149,_sesTramo.getDescripcion());
                   	negevent.guardar(conexion);

                   }
                  }
                  catch (SQLException ex) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha realizado el evento de inserci�n del tramo con descripcion  = " + _sesTramo.getDescripcion() + " Error -> " + ex.toString());
                  }

	  }catch(SQLException e){
 	     fichero_log = Log.getInstance();
             fichero_log.error(" No se ha realizado la inserci�n del tramo " + _sesTramo.getDescripcion() + " Error -> " + e.toString());
             ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
             throw es;
           }catch(Exception ex){
 	     fichero_log = Log.getInstance();
             fichero_log.error(" No se ha realizado la inserci�n del tramo " + _sesTramo.getDescripcion() + " Error -> " + ex.toString());
             ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
             throw es;
           }
	}

	//Realiza la insercion del Tramo.
        /**
         * Realiza la insercion del Tramo
         * @param SesTramos _sesTramo Objeto de clase SesTramos (clase de persistencia asociada a tabla SesTramos)
         * @autor David Fernandez-Vegue Garcia
         * @throws SQLException Exception producida por la operacion INSERT sobre tabla Ses_Tramos
         */

        public void insertarTramo(SesTramos _sesTramo) throws SQLException,ExceptionSESCAM {
                Pool pool=null;
                Connection conexion = null;
                try{
                  pool = Pool.getInstance();
                  conexion = pool.getConnection(DB);
                  this.insertarTramo(conexion,_sesTramo);
                  conexion.commit();
                }catch( Exception e){
                  conexion.rollback();
                  ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                  throw es;
                }finally{
                  pool.freeConnection(DB,conexion);
                }
        }

	/**
	 * Realiza la modificacion del Tramo
         * @param SesTramos _sesTramo Objeto de clase SesTramos (clase de persistencia asociada a tabla SesTramos)
	 * @param Conexion _connection Objeto de conexion utilizado para hacer el update
         * @autor Jose Vicente Nieto-Marquez
         * @throws SQLException Exception producida por la operacion UPDATE sobre tabla Ses_Tramos
         */

        public void modificarTramo(Connection conexion,SesTramos _sesTramo) throws SQLException,ExceptionSESCAM {
	  Log fichero_log = null;
	  try {
            _sesTramo.insert(conexion);
            try {
                   if (negevent != null) {

                   	Long codTramo = new Long (_sesTramo.getCodtramo());
                   	Long posicion = new Long (_sesTramo.getPosicion());
                   	Double inicio = new Double (_sesTramo.getInicio());
                   	Double fin = new Double (_sesTramo.getFin());
                   	negevent.addVar(150,codTramo.toString());
                   	negevent.addVar(151,posicion.toString());
                   	negevent.addVar(152,inicio.toString());
                   	negevent.addVar(153,fin.toString());
                   	negevent.addVar(154,_sesTramo.getDescripcion());
                   	negevent.guardar(conexion);

                   }
                  }
                  catch (SQLException ex) {
                    fichero_log = Log.getInstance();
                    fichero_log.warning(" No se ha realizado el evento de inserci�n del tramo con descripcion  = " + _sesTramo.getDescripcion() + " Error -> " + ex.toString());
                  }

	  }catch(SQLException e){
	     fichero_log = Log.getInstance();
             fichero_log.error(" No se ha realizado la modificaci�n del tramo " + _sesTramo.getDescripcion() + " Error -> " + e.toString());
             ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
             throw es;
           }catch(Exception ex){
	     fichero_log = Log.getInstance();
             fichero_log.error(" No se ha realizado la modificaci�n del tramo " + _sesTramo.getDescripcion() + " Error -> " + ex.toString());
             ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
             throw es;
           }
	}

        //Realiza la modificacion del Tramo
        /**
         * Realiza la modificacion del Tramo
         * @param SesTramos _sesTramo Objeto de clase SesTramos (clase de persistencia asociada a tabla SesTramos)
         * @autor David Fernandez-Vegue Garcia
         * @throws SQLException Exception producida por la operacion UPDATE sobre tabla SesTramos
         */

        public void modificarTramo(SesTramos _sesTramo) throws SQLException,ExceptionSESCAM  {
                Log fichero_log = null;
                Pool pool=null;
                Connection conexion = null;
                try{
                pool = Pool.getInstance();
                conexion = pool.getConnection(DB);
		this.modificarTramo(conexion,_sesTramo);
                conexion.commit();
                }catch(Exception e){
                  conexion.rollback();
		  ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
                  throw es;
                }finally{
                  pool.freeConnection(DB,conexion);
                }
        }

        //Realiza la eliminacion del Tramo.
         /**
         * Realiza la eliminacion del Tramo.
         * @param SesTramos _sesTramo Objeto de clase SesTramos (clase de persistencia asociada a tabla SesTramos)
	 * @param Connection conexion
         * @autor Jos� Vicente Nieto
         * @throws ExceptionSESCAM Excepcion producida porque no se cumpla alguna restriccion de integridad con las tablas asociadas
         */

        public void eliminarTramo(Connection conexion,SesTramos _sesTramo) throws ExceptionSESCAM,Exception {
	  Log fichero_log = null;
          try{
	    _sesTramo.delete(conexion);
            try {
	       if (negevent != null) {
		 Long codTramo = new Long (_sesTramo.getCodtramo());
		 Long posicion = new Long (_sesTramo.getPosicion());
		 negevent.addVar(155,codTramo.toString());
		 negevent.addVar(156,posicion.toString());
		 negevent.guardar(conexion);
               }
            }
            catch (SQLException ex) {
	      fichero_log = Log.getInstance();
              fichero_log.warning(" No se ha realizado el evento de eliminacion de tramo con descripcion= " + _sesTramo.getDescripcion() + " Error -> " + ex.toString());
            }
          }catch(SQLException e){
	    fichero_log = Log.getInstance();
            fichero_log.error(" No se ha realizado la eliminaci�n del tramo " + _sesTramo.getDescripcion() + " Error -> " + e.toString());
            ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
            throw es;
          }
        }

        /**
         * Realiza la eliminacion del Tramo.
         * @param SesTramos _sesTramo Objeto de clase SesTramos (clase de persistencia asociada a tabla SesTramos)
         * @autor David Fernandez-Vegue Garcia
         * @throws ExceptionSESCAM Excepcion producida porque no se cumpla alguna restriccion de integridad con las tablas asociadas
         */

        public void eliminarTramo(SesTramos _sesTramo) throws ExceptionSESCAM,Exception {
                Pool pool=null;
                Connection conexion = null;
                Vector v = new Vector();
                  try{
                     pool = Pool.getInstance();
                     conexion = pool.getConnection(DB);
	 	     this.eliminarTramo(conexion,_sesTramo);
                     conexion.commit();
                  }catch(Exception e){
		    conexion.rollback();
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
                    throw es;
                  }
                  finally{
		    pool.freeConnection(DB,conexion);
                  }
        }

        public void EliminarTodos (Vector Origen) throws ExceptionSESCAM{
	  Tramos tram = new Tramos();
          for (int i = 0; i < Origen.size(); i++) {
	    SesTramos sesTram = (SesTramos) Origen.elementAt(i);
            System.out.println (sesTram.getInicio() + " - " + sesTram.getFin());
            try{
	      this.eliminarTramo(sesTram);
            }catch (Exception e) {
              ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_BORRAR);
              throw es;
	    }
          }
        }

	public void InsertarTodos (Vector Destino) throws ExceptionSESCAM{
	  Tramos tram = new Tramos();
          for (int i = 0; i < Destino.size(); i++) {
	    SesTramos sesTram = (SesTramos) Destino.elementAt(i);
            try{
	      this.insertarTramo(sesTram);
            }catch (Exception e) {
              ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.ERROR_AL_INSERTAR);
              throw es;
	    }
          }
        }

  public int getNTramos() {
    return NTramos;
  }
  public void setNTramos(int NTramos) {
    this.NTramos = NTramos;
  }


} //fin de la clase

