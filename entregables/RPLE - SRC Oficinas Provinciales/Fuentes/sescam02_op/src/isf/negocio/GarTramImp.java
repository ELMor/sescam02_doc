package isf.negocio;
public class GarTramImp{
   public int 		fgar_pk=0;
   public int 		fnorden=0;
   public int 		fcentro=0;
   public int 		fgar_tipo=0;
   public int 		fgar_estado=0;
   public double	fgar_monto=0;
   public String	fgar_motivo_rech="SD";
   public int 		fcega=0;
   public int 		fncita=0;
   public String	fgar_cq="SD";
   public int 		fgar_tram_pk=0;
   public String	fsyslogin="SD";
   public int 		foficina_pk=0;
   public String	fgar_fechor="SD";
   public String	fgar_repleg_apenom="SD";
   public String	fgar_repleg_dni="SD";
   public int 		fgar_operacion=0;
   public String	fpac_nss="SD";
   public String	fofi_prov="SD";
   public String	fofi_coordinador="SD";

   public String getfgar_motivo_rech(){if (fgar_motivo_rech==null) return"";else return fgar_motivo_rech;}
   public String getfgar_cq(){if (fgar_cq==null) return"";else return fgar_cq;}
   public String getfsyslogin(){if (fsyslogin==null) return"";else return fsyslogin;}
   public String getfgar_fechor(){if (fgar_fechor==null) return"";else return fgar_fechor;}
   public String getfgar_repleg_apenom(){if (fgar_repleg_apenom==null) return"";else return fgar_repleg_apenom;}
   public String getfgar_repleg_dni(){if (fgar_repleg_dni==null) return"";else return fgar_repleg_dni;}
   public String getfpac_nss(){if (fpac_nss==null) return"";else return fpac_nss;}
   public String getfofi_prov(){if (fofi_prov==null) return"";else return fofi_prov;}
   public String getfofi_coordinador(){if (fofi_coordinador==null) return"";else return fofi_coordinador;}



}