/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaGaranTram </p>
 * <p>Description: Bean que realiza la consulta de tramos de garantias. Realiza un join entre las tablas SES_GARANTIA</p>
 *             <p> y SES_GARAN_TRAM.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Iv�n Enrique Caneri
 * @version 1.0
 */
public class ConsultaGaranTram {
    private int gar_pk = 0;
    private int gar_operacion = 0;
    private int resultCount;
    public final int MAX_RESULT_SET = 500;

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de tramos de garantias y de su ejecucion.
     * Utiliza objetos de la clase GarTramImp para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos GarTramImp, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * Se utiliza para la impresion de documentos.
     * @return Vector de objetos de la clase GarTramImp.
     * @throws Exception
     */
    public Vector resultado() throws Exception {
        Vector vec_GarTramImp = new Vector();

        // Validaci�n de parametros
        if ((gar_pk == 0) || (gar_operacion == 0)) {
            return (null);
        }

        Connection conn = null;
        Pool ref_pool = null;
        resultCount = 0;

        PreparedStatement st;
        ResultSet rs;
        String sentBusqueda = "select " + "sg.gar_pk gar_pk, " + "norden, " +
            "centro, " + "gar_tipo, " + "gar_estado, " + "gar_monto, " +
            "gar_motivo_rech, " + "cega, " + "ncita, " + "gar_cq, " +
            "gar_tram_pk, " + "sgt.syslogin, " + "sgt.oficina_pk, " +
            "to_char(gar_fechor,'" + "DD/MM/YYYY" + "') gar_fechor ," +
            "gar_repleg_apenom, " + "gar_repleg_dni, " + "gar_operacion ," +
            "pac_nss, " + "provincia ofi_prov, " +
            "su.nombre||' '||su.apellido1||' '||su.apellido2 ofi_coordinador " +
            " from ses_garantia sg,ses_garan_tram sgt,ses_oficina o " +
            "   ,ses_provincias p,ses_localidades l, sys_usu su " +
            " where sg.gar_pk=sgt.gar_pk and " +
            " 	o.oficina_pk=sgt.oficina_pk and " +
            " 	p.provincia_pk=l.provincia_pk and " +
            " 	o.pk_localidad=l.pk_localidad and " +
            "       su.syslogin (+)= o.syslogin  and " +
            "       sg.gar_pk=? and gar_operacion=? ";
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection("db");

        try {
            conn.setAutoCommit(false);
            st = conn.prepareStatement(sentBusqueda);
            st.setInt(1, gar_pk);
            st.setInt(2, gar_operacion);
            rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                GarTramImp gTI = new GarTramImp();
                gTI.fgar_pk = rs.getInt("gar_pk");
                gTI.fnorden = rs.getInt("norden");
                gTI.fcentro = rs.getInt("centro");
                gTI.fgar_tipo = rs.getInt("gar_tipo");
                gTI.fgar_estado = rs.getInt("gar_estado");
                gTI.fgar_monto = rs.getDouble("gar_monto");
                gTI.fgar_motivo_rech = rs.getString("gar_motivo_rech");
                gTI.fcega = rs.getInt("cega");
                gTI.fncita = rs.getInt("ncita");
                gTI.fgar_cq = rs.getString("gar_cq");
                gTI.fgar_tram_pk = rs.getInt("gar_tram_pk");
                gTI.fsyslogin = rs.getString("syslogin");
                gTI.foficina_pk = rs.getInt("oficina_pk");
                gTI.fgar_fechor = rs.getString("gar_fechor");
                gTI.fgar_repleg_apenom = rs.getString("gar_repleg_apenom");
                gTI.fgar_repleg_dni = rs.getString("gar_repleg_dni");
                gTI.fgar_operacion = rs.getInt("gar_operacion");
                gTI.fofi_prov = rs.getString("ofi_prov");
                gTI.fofi_coordinador = rs.getString("ofi_coordinador");
                gTI.fpac_nss = rs.getString("pac_nss");

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_GarTramImp.addElement(gTI);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            vec_GarTramImp.clear();

            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            throw e;
        } finally {
            ref_pool.freeConnection("db", conn);
        }

        return vec_GarTramImp;
    }

    /**
      * Asigna el parametro al atributo gar_pk de la clase.
      * @param pGar_pk : int, pk de la Garantia asociada
      * @since 15/10/2002
      * @author IEC
      */
    public void setGar_pk(int pGar_pk) {
        this.gar_pk = pGar_pk;
    }

    /**
      * Obtiene el valor del atributo gar_pk de la clase.
      * @return int, pk de la Garantia asociada
      * @since 15/10/2002
      * @author IEC
      */
    public int getGar_pk() {
        return this.gar_pk;
    }

    /**
      * Asigna el parametro al atributo gar_operacion de la clase.
      * @param pGar_operacion ,int, gar_operacion (1:Solicitada, 2:Aprobada, 3:Denegada, 4:Renunciada).
      * @since 15/10/2002
      * @author IEC
      */
    public void setGar_operacion(int pGar_operacion) {
        this.gar_operacion = pGar_operacion;
    }

    /**
      * Obtiene el valor del atributo gar_operacion de la clase.
      * @return int, gar_operacion
      * @since 15/10/2002
      * @author IEC
      */
    public int getGar_operacion() {
        return this.gar_operacion;
    }

    /*
      public static void main(String args[]){
              ConsultaGaranTram cpb=new ConsultaGaranTram();
              Vector vp=null;
              try{
              cpb.setGar_operacion(3);
              cpb.setGar_pk(3);
              vp=cpb.resultado();
              }catch(Exception e){e.printStackTrace();}
              if (vp==null){
                      System.out.println("No hay registros con las condiciones: gar_pk/"+cpb.getGar_pk()+" gar_operacion/"+cpb.getGar_operacion());
                      }
              else        {
                        Enumeration e = vp.elements();
                        GarTramImp gTI;
                        while (e.hasMoreElements()) {
                            gTI=(GarTramImp)e.nextElement();
                        }
                      }
              }
    */
}
