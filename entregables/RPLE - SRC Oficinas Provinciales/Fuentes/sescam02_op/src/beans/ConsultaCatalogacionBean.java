/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.exceptions.*;

import isf.negocio.*;

import isf.persistencia.SesTramos;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaCatalogacionBean</p>
 * <p>Descripcion: Bean que realiza la consulta de entradas en LESP � LESPCEX por el tramo de antiguedad</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Empresa: Soluziona </p>
 */
public class ConsultaCatalogacionBean {
    private long centro;
    private String orden;
    private int tramo;
    private String catalogacion;
    private int tipolista;

    // private String gar_cq;
    private int resultCount;
    public final int MAX_RESULT_SET = 60000; //Maximo n� de registros resultantes por consulta.

    /**
     * Metodo que se encarga de la construccion de la consulta, atendiendo a los filtros, y de su ejecucion.
     * Utiliza objetos de la clase PacientesPorCatalogacion para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos PacientesPorCatalogacion, en la que cada uno atender� a un registro resultado
     * de la consulta.
     * Se realizan dos consultas, una para obtener las solicitudes de la tabla LESP (Lista de Espera Quirurgia) y
     * otra para obtener las correspondientes a la tabla LESPCEX (Lista de Espera de CEX).
     * @return Vector de objetos de la clase PacientesPorCatalogacion.
     * @throws SQLException
     * @throws ExceptionSESCAM
     */
    public Vector resultado() throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        resultCount = 0;

        boolean bWhere = false;
        boolean lblesp = true;
        boolean lblespcex = false;
        Vector vec_catalogacion = new Vector();
        Pool ref_pool = null;

        String sentBusqueda =
            " select l.cip,l.ape1,l.ape2,l.nomb,l.norden,l.centro,l.descentro," +
            " l.deli,l.cq,l.proc,l.catalog " + " from CONSULTATRAMOS l ";

        String sentBusqueda1 =
            " select l.cip,l.ape1,l.ape2,l.nomb,l.norden,l.centro,l.descentro, l.deli, l.cq, l.proc,l.catalog " +
            " from CONSULTATRAMOSCEX l ";

        /*
            String sentBusqueda1Cex = " select l.cip,l.ape1,l.ape2,l.nomb,l.norden,l.centro,l.descentro, l.deli, l.cq, l.proc, l.tipo,l.catalog "+
                                      " from CONSULTATRAMOSCEX l,ses_proc_map smp,ses_procedimientos s "+
                                      " where l.proc = smp.proc_cen_cod and l.centro = smp.centro and l.tipo = smp.proc_cen_tipo and smp.proc_pk = s.proc_pk and s.proc_tipo = 2 ";

            String sentBusqueda1Tec = " select l.cip,l.ape1,l.ape2,l.nomb,l.norden,l.centro,l.descentro, l.deli, l.cq, l.proc, l.tipo,l.catalog "+
                                      " from CONSULTATRAMOSCEX l,ses_proc_map smp,ses_procedimientos s "+
                                      " where l.proc = smp.proc_cen_cod and l.centro = smp.centro and l.tipo = smp.proc_cen_tipo and smp.proc_pk = s.proc_pk and s.proc_tipo = 3 ";

            String sentBusqueda2Cex = " union all "+
                                      " select l.cip,l.ape1,l.ape2,l.nomb,l.norden,l.centro,l.descentro, l.deli, l.cq, l.proc, l.tipo,l.catalog "+
                                      " from CONSULTATRAMOSCEX l "+
                                      " where l.proc not in (select smp.proc_cen_cod  from ses_proc_map smp where l.centro = smp.centro and l.tipo = smp.proc_cen_tipo) and l.tipo <> 'T' ";
            String sentBusqueda2Tec = " union all "+
                                      " select l.cip,l.ape1,l.ape2,l.nomb,l.norden,l.centro,l.descentro, l.deli, l.cq, l.proc, l.tipo,l.catalog "+
                                      " from CONSULTATRAMOSCEX l "+
                                      " where l.proc not in (select smp.proc_cen_cod from ses_proc_map smp where l.centro = smp.centro and l.tipo = smp.proc_cen_tipo) and l.tipo = 'T' ";

        */
        if ((tramo != 0) && (tipolista != 0)) {
            Tramos tram = new Tramos();
            Vector v = new Vector();
            v = tram.busquedaTramo(" CODTRAMO = " + tipolista +
                    " AND POSICION = " + tramo, "");

            if (v.size() > 0) {
                SesTramos sestram = (SesTramos) v.elementAt(0);
                int inicio = new Double(sestram.getInicio()).intValue();
                int fin = new Double(sestram.getFin()).intValue();

                if (fin > 0) {
                    sentBusqueda += (" where deli <= " + fin + " and deli > " +
                    inicio);
                    sentBusqueda1 += (" where deli <= " + fin + " and deli > " +
                    inicio);
                } else {
                    sentBusqueda += (" where deli > " + inicio);
                    sentBusqueda1 += (" where deli > " + inicio);
                }

                bWhere = true;
            }
        }

        if (catalogacion.equals("")) {
            return null;
        }

        if (catalogacion.equals("2")) {
            if (bWhere == false) {
                sentBusqueda += " where l.CATALOG IN ('TG','TG_DV','TG_DM','SG_RO','PSG','SG_BPA') ";
                sentBusqueda1 += " where   l.CATALOG IN ('TG','TG_DV','TG_DM','SG_RO','PSG','SG_BPA') ";
            } else {
                sentBusqueda += " and   l.CATALOG IN ('TG','TG_DV','TG_DM','SG_RO','PSG','SG_BPA') ";
                sentBusqueda1 += " and   l.CATALOG IN ('TG','TG_DV','TG_DM','SG_RO','PSG','SG_BPA') ";
            }
        } else {
            if (!catalogacion.equals("1")) {
                if (bWhere == false) {
                    sentBusqueda += (" where l.CATALOG = '" + catalogacion +
                    "'");
                    sentBusqueda1 += (" where l.CATALOG = '" + catalogacion +
                    "'");
                } else {
                    sentBusqueda += (" and l.CATALOG = '" + catalogacion + "'");
                    sentBusqueda1 += (" and l.CATALOG = '" + catalogacion +
                    "'");
                }
            }
        }

        if (centro > 0) {
            if (bWhere == false) {
                sentBusqueda += (" where l.centro = " + centro);
            } else {
                sentBusqueda += (" and l.centro = " + centro);
            }
        }

        if (tipolista != 0) {
            switch (tipolista) {
            case 1:
                lblesp = true;
                lblespcex = false;

                break;

            case 2:
                lblesp = false;
                lblespcex = true;

                break;
            }

            bWhere = true;
        }

        // Si no tiene condiciones de busqueda no se hace la consulta
        if (!bWhere) {
            return null;
        }

        sentBusqueda += (" ORDER BY " + orden);
        sentBusqueda1 += (" ORDER BY " + orden);

        /*  System.out.println(sentBusqueda);
            System.out.println(sentBusqueda1);
        */
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            Statement stm = conn.createStatement();
            ResultSet rs = null;

            if (lblesp == true) {
                rs = stm.executeQuery(sentBusqueda);
            } else {
                rs = stm.executeQuery(sentBusqueda1);
            }

            while (rs.next()) {
                resultCount++;

                PacientesPorCatalogacion catalogacion = new PacientesPorCatalogacion();
                catalogacion.Centro = rs.getString(7);
                catalogacion.Cip = rs.getString(1);
                catalogacion.Ape1 = rs.getString(2);
                catalogacion.Ape2 = rs.getString(3);
                catalogacion.Nombre = rs.getString(4);
                catalogacion.Norden = rs.getLong(5);
                catalogacion.CodCentro = rs.getLong(6);
                catalogacion.antiguedad = rs.getInt(8);
                catalogacion.Situacion = rs.getString(11);

                if (lblesp == true) {
                    catalogacion.Cq = "Q";
                } else {
                    catalogacion.Cq = "C";
                }

                if (resultCount == MAX_RESULT_SET) {
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.MAX_RESULTSET);
                    throw es;
                }

                vec_catalogacion.addElement(catalogacion);
            }

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_catalogacion;
    }

    /**
     * Obtiene el valor del atributo tramo de la clase.
     * @return int, tramo.
     * @since 31/12/2002
     */
    public int getTramo() {
        return tramo;
    }

    /**
     * Asigna el parametro al atributo tramo de la clase.
     * @param tram : int, Tramo para el que se quiere buscar los pacientes.
     * @since 31/12/2002
     */
    public void setTramo(int tram) {
        if (tram != 0) {
            tramo = tram;
        }
    }

    /**
     * Asigna el parametro al atributo catalogacion de la clase.
     * @param cat : String,  Catalogacion
     * @since 31/12/2002
     */
    public void setCatalogacion(String cat) {
        if (!cat.equals("")) {
            this.catalogacion = cat;
        }
    }

    /**
     * Obtiene el valor del atributo catalogacion de la clase.
     * @return String, catalogacion.
     * @since 31/12/2002
     */
    public String getCatalogacion() {
        return catalogacion;
    }

    /**
     * Asigna el parametro al atributo tipolista de la clase.
     * @param tlista : int, Tipo de Lista de Espera para la que se solicita la consulta (LESP/LESPCEX)
     * @since 07/01/2002
     */
    public void setTipoLista(int tlista) {
        if (tlista != 0) {
            this.tipolista = tlista;
        }
    }

    /**
     * Obtiene el valor del atributo tipolista de la clase.
     * @return int, tipo de lista (LESP / LESPCEX) .
     * @since 07/01/2002
     */
    public int getTipoLista() {
        return tipolista;
    }

    /**
     * Asigna el parametro al atributo orden de la clase.
     * @param ordenar :string, ordenacion de la consulta (ASC o DESC)
     * @since 31/12/2002
     */
    public void setOrden(String ordenar) {
        this.orden = ordenar;
    }

    /**
     * Obtiene el valor del atributo result_count de la clase.
     * @return int, numero de filas resultantes de la consulta.
     * @since 31/12/2002
     */
    public int getResultCount() {
        return resultCount;
    }

    /**
     * Asigna el parametro al atributo centro de la clase.
     * @param vcentro, centro con el cual filtrar la consulta.
     * @since 31/12/2002
     */
    public void setCentro(long vcentro) {
        centro = vcentro;
    }

    /**
     * Obtiene el valor del atributo centro de la clase.
     * @return long, centro utilizado como filtro en la consulta.
     * @since 11/04/2003
     */
    public long getCentro() {
        return centro;
    }

    /*
      public static void main(String args[]){
              ConsultaPacienteSolicitud cps=new ConsultaPacienteSolicitud();
              Solilcitud sol;
              Vector vp;
              cps.setNombre("nom");
              cps.setApellido1("");
              cps.setApellido2("");
              cps.setCip("");
              cps.setCentro("");
              cps.setHicl("");
              cps.setOrden("cip");
              try{
                vp=cpb.resultado();
                if (vp==null){
                  System.out.println("No se trajo nada");
                }
                else        {
                  Enumeration e = vp.elements();
                  while (e.hasMoreElements()) {
                    pac1=(Paciente)e.nextElement();
                    System.out.println(pac1);
                  }
                  System.out.println("Cantidad de registros:"+cpb.getResultCount());
                }
              }
              catch(Exception e){
                System.out.println("Error al ejecutar la Select.");
                e.printStackTrace();
              }
              }*/
}
