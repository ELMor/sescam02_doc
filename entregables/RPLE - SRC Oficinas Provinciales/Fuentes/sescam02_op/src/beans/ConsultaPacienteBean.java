/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import isf.db.*;

import isf.negocio.*;

import isf.util.Global;

import isf.util.log.Log;

import java.sql.*;

import java.util.Vector;

import isf.util.Utilidades;


/**
 * <p>Clase: ConsultaPacienteBean </p>
 * <p>Description: Bean que realiza la consulta de busqueda de pacientes existentes en la Lista de Espera (tablas LESP y LESPCEX)</p>
 *             <p> por distintos campos: Nombre, Apellidos, CIP, Centro, N� Historia Clinica. </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Iv�n Enrique Caneri
 * @version 1.0
 */
public class ConsultaPacienteBean {
    private String cip;
    private String apellido1;
    private String apellido2;
    private String nombre;
    private String centro;
    private String hicl;
    private String orden;
    private int resultCount;
    private NegEvento negevent;
    public final int MAX_RESULT_SET = 500;

    /**
              * Instancia el objeto de clase NegEvento
              * @param NegEvento evento Objeto de clase NegEvento ya instanciado con el evento que ser� lanzado
              * @autor Jos� Vicente Nieto-M�rquez Fdez-Medina
    */
    public void SetNegEvento(NegEvento evento) {
        negevent = evento;
    }

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda y de su ejecucion.
     * Utiliza objetos de la clase Paciente para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos Paciente, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * Se realizan dos consultas, una para realizar la busqueda de los pacientes con entradas en LESP (Lista de Espera Quirurgia) y
     * otra para realizar la busqueda de en la tabla LESPCEX (Lista de Espera de CEX).
     * Una vez se tienen formadas las dos consultas se realiza una union de las mismas.
     * @return Vector de objetos de la clase Paciente.
     * @throws SQLException
     */
    public Vector resultado() throws SQLException {
        Connection conn = null;
        resultCount = 0;

        Vector vec_pacientes = new Vector();
        Pool ref_pool = null;

        String sentBusqueda1 =
            "select centro,cip,ape1,ape2,nomb,hicl,to_date(to_char(fnac,'DD/MM/YYYY'),'DD/MM/YYYY') fnac, " +
            "domi,pobl,prov,c.DESCENTRO descentro,0 tipo,tel1 telefono from lesp l,centrosescam c where l.CENTRO=c.CODCENTRO";

        String sentBusqueda2 =
            "select cega,cip,ape1,ape2,nomb,hicl,to_date(to_char(fnac,'DD/MM/YYYY'),'DD/MM/YYYY') fnac, " +
            "domi,pobl,prov,c.DESCENTRO descentro,0 tipo,tel1 telefono from lespcex l,centrosescam c where l.CEGA=c.CODCENTRO";

        String sentBusqueda3 =
            "select nl.CENTRO centro, nl.CIP cip, nl.APELLIDO1 ape1, nl.APELLIDO2 ape2, nl.NOMBRE nomb, nl.NHC hicl, to_date(to_char(nl.FECHA_NAC,'DD/MM/YYYY'),'DD/MM/YYYY') fnac," +
            " nl.DOMICILIO domi,nl.POBLACION pobl,nl.PROVINCIA prov,c.DESCENTRO descentro,1 tipo,telefono from nolesp nl,centrosescam c where nl.CENTRO=c.CODCENTRO";

        String Where = "";
        String Where2 = "";

        if ((cip != null) && (!cip.equals(""))) {
            Where += (" and cip='" + cip + "'");
            Where2 += (" and cip='" + cip + "'");
        }

        if ((apellido1 != null) && (!apellido1.equals(""))) {
            Where += (" and ape1 like '" + apellido1 + "%'");
            Where2 += (" and apellido1 like'" + apellido1 + "%'");
        }

        if ((apellido2 != null) && (!apellido2.equals(""))) {
            Where += (" and ape2 like '" + apellido2 + "%'");
            Where2 += (" and apellido2 like '" + apellido2 + "%'");
        }

        if ((nombre != null) && (!nombre.equals(""))) {
            Where += (" and nomb like '" + nombre + "%'");
            Where2 += (" and nombre like '" + nombre + "%'");
        }

        if ((centro != null) && (!centro.equals("0"))) {
            Where += (" and centro = " + centro);
            Where2 += (" and centro = " + centro);
        }

        if ((hicl != null) && (!hicl.equals(""))) {
            Where += (" and hicl like '" + hicl + "%'");
            Where2 += (" and nhc like '" + hicl + "%'");
        }

        // Si no tiene condiciones de busqueda no se hace la consulta
        if (Where == "") {
            return null;
        }

        //Hacemos un Union con ambas selects
        String sentBusqueda = sentBusqueda1 + Where + " union " +
            sentBusqueda3 + Where2 + " union " + sentBusqueda2 +
            Global.replaceString(Where, " centro ", " cega ");

        if (orden != null) {
            sentBusqueda += (" ORDER BY "+ orden);
        } else {
            sentBusqueda += " ORDER BY tipo";
        }

        //System.out.println(sentBusqueda);
        Log fichero_log = null;
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection("db");

        try {
            conn.setAutoCommit(false);

            Statement stm = conn.createStatement();

            try {
                if (negevent != null) {
                    negevent.addVar(77, apellido1);
                    negevent.addVar(78, apellido2);
                    negevent.addVar(79, nombre);
                    negevent.addVar(80, cip);
                    negevent.addVar(81, hicl);
                    negevent.addVar(82, centro);

                    negevent.guardar(conn);
                }
            } catch (Exception ex) {
                fichero_log = Log.getInstance();
                fichero_log.warning(
                    " No se ha realizado el evento de consulta de LESP  Error -> +" +
                    ex.toString());
            }

            ResultSet rs = stm.executeQuery(sentBusqueda);

            while (rs.next()) {
                resultCount++;

                Paciente pac = new Paciente();
                pac.fCentro = rs.getInt("centro");
                pac.fCip = rs.getString("cip");
                pac.fHicl = rs.getInt("hicl");
                pac.fApe1 = rs.getString("ape1");
                pac.fApe2 = rs.getString("ape2");
                pac.fNomb = rs.getString("nomb");
                pac.fDomi = rs.getString("domi");
                pac.fPobl = rs.getString("pobl");
                pac.fProv = rs.getString("prov");
                if (rs.getDate("fnac")!=null) {
					pac.fNac = Utilidades.dateToString(rs.getDate("fnac"));
                }else pac.fNac = "S/D";
                pac.fDesCentro = rs.getString("descentro");
                pac.fTelef = rs.getString("telefono");

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_pacientes.addElement(pac);
            }

            conn.commit();

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } catch (Exception exc){
        	System.out.println("Error en la conversion de la fecha");
        	
        } finally {
            ref_pool.freeConnection("db", conn);
        }

        return vec_pacientes;
    }

    /**
     * Obtiene el valor del atributo cip de la clase.
     * @return String, CIP.
     * @since 15/10/2002
     * @author IEC
     */
    public String getCip() {
        return cip;
    }

    /**
     * Asigna el parametro al atributo cip de la clase.
     * @param pCip :String, CIP del paciente.
     * @since 15/10/2002
     * @author IEC
     */
    public void setCip(String pCip) {
        if (pCip != null) {
            cip = pCip;
        }

        this.cip = pCip;
    }

    /**
     * Asigna el parametro al atributo apellido1 de la clase.
     * @param pApellido1 :String, 1er apellido del paciente.
     * @since 15/10/2002
     * @author IEC
     */
    public void setApellido1(String pApellido1) {
        if (pApellido1 != null) {
            this.apellido1 = pApellido1;
        }
    }

    /**
     * Obtiene el valor del atributo apellido1 de la clase.
     * @return String, 1er apellido del paciente.
     * @since 15/10/2002
     * @author IEC
     */
    public String getApellido1() {
        return apellido1;
    }

    /**
     * Asigna el parametro al atributo apellido2 de la clase.
     * @param pApellido2 : String, 2� apellido del paciente.
     * @since 15/10/2002
     * @author IEC
     */
    public void setApellido2(String pApellido2) {
        if (pApellido2 != null) {
            this.apellido2 = pApellido2;
        }
    }

    /**
     * Obtiene el valor del atributo apellido2 de la clase.
     * @return String, 2� apellido del paciente.
     * @since 15/10/2002
     * @author IEC
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     * Asigna el parametro al atributo nombre de la clase.
     * @param pNombre : String, Nombre del paciente.
     * @since 15/10/2002
     * @author IEC
     */
    public void setNombre(String pNombre) {
        if (pNombre != null) {
            this.nombre = pNombre;
        }
    }

    /**
      * Obtiene el valor del atributo nombre de la clase.
      * @return String, Nombre del paciente.
      * @since 15/10/2002
      * @author IEC
      */
    public String getNombre() {
        return nombre;
    }

    /**
      * Asigna el parametro al atributo centro de la clase.
      * @param pCentro : String, Centro.
      * @since 15/10/2002
      * @author IEC
      */
    public void setCentro(String pCentro) {
        if (pCentro != null) {
            this.centro = pCentro;
        }
    }

    /**
      * Obtiene el valor del atributo centro de la clase.
      * @return String, Centro.
      * @since 15/10/2002
      * @author IEC
      */
    public String getCentro() {
        return centro;
    }

    /**
      * Asigna el parametro al atributo hicl de la clase.
      * @param pHicl , String, Numero de Historia Clinica
      * @since 15/10/2002
      * @author IEC
      */
    public void setHicl(String pHicl) {
        if (pHicl != null) {
            this.hicl = pHicl;
        }
    }

    /**
      * Obtiene el valor del atributo centro de la clase.
      * @return String, Numero de Historia Clinica
      * @since 15/10/2002
      * @author IEC
      */
    public String getHicl() {
        return hicl;
    }

    /**
      * Asigna el parametro al atributo orden de la clase.
      * @param ordenar :String, cadena de ordenacion para el ORDER BY de la consulta (ASC/DESC campo1,campo2..)
      * @since 15/10/2002
      * @author IEC
      */
    public void setOrden(String ordenar) {
        this.orden = ordenar;
    }

    /**
     * Obtiene el valor del atributo resultcount de la clase.
     * @return int, numero de filas resultantes de la consulta.
     * @since 15/10/2002
     * @author IEC
     */
    public int getResultCount() {
        return resultCount;
    }

    /*
      public static void main(String args[]){
              ConsultaPacienteBean cpb=new ConsultaPacienteBean();
              Paciente pac1;
              Vector vp;
              cpb.setNombre("nom");
              cpb.setApellido1("");
              cpb.setApellido2("");
              cpb.setCip("");
              cpb.setCentro("");
              cpb.setHicl("");
              cpb.setOrden("cip");
              try{
                vp=cpb.resultado();
                if (vp==null){
                  System.out.println("No se trajo nada");
                }
                else        {
                  Enumeration e = vp.elements();
                  while (e.hasMoreElements()) {
                    pac1=(Paciente)e.nextElement();
                  }
      //            System.out.println("Cantidad de registros:"+cpb.getResultCount());
                }
              }
              catch(Exception e){
                System.out.println("Error al ejecutar la Select.");
                e.printStackTrace();
              }
              }
    */
}
