/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaProcMapeados </p>
 * <p>Description: Bean que realiza la consulta de los procedimientos susceptibles de ser mapeados automaticamente.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona </p>
 * @author Jose Vicente Nieto-Marquez Fdez-Medina
 * @version 1.0
 */
public class ConsultaDeteccionProcMap {
    private long centro;
    private int resultCount;
    Pool ref_pool = null;
    public final int MAX_RESULT_SET = 2000;

    /**
     * Metodo que se encarga de la construccion de la consulta de busqueda de procedimientos no mapeados.
     * Utiliza objetos de la clase ProcMapeados para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos ProcMapeados, en el que cada uno atender� a un registro resultado
     * de la consulta.
     * @return Vector de objetos de la clase ProcMapeados.
     * @throws Exception
     */
    public Vector resultado() throws Exception {
        // Validaci�n de parametros
        if (centro <= 0) {
            return (null);
        }

        String sentBusqueda = new String("");
        Connection conn = null;
        resultCount = 0;

        SingleConn sc = new SingleConn();
        Vector vec_Proc = new Vector();
        sentBusqueda = " select codprest codigo,desprest descripcion,identpres tipo from prestacion ";

        String ls_where =
            " where CNORMAL is null and centro=? and trim(codprest) not in " +
            " (select trim(spm.PROC_CEN_COD) from ses_proc_map spm " +
            "  where trim(spm.PROC_CEN_COD)=trim(prestacion.CODPREST) and  " +
            "        spm.PROC_CEN_TIPO=prestacion.IDENTPRES)";
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);
            sentBusqueda = sentBusqueda + ls_where + " order by codprest ";

            PreparedStatement st = conn.prepareStatement(sentBusqueda);
            st.setLong(1, centro);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                resultCount++;

                ProcMapeados proc = new ProcMapeados();
                proc.CodProcMap = rs.getString(1);
                proc.ProcDesc = rs.getString(2);
                proc.CodProcTipo = rs.getString(3);

                if (resultCount == MAX_RESULT_SET) {
                    break;
                }

                vec_Proc.addElement(proc);
            }

            conn.commit();

            try {
                rs.close();
                st.close();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            try {
                conn.rollback();
            } catch (Exception esql) {
            }

            e.printStackTrace();
            throw e;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_Proc;
    }

    /**
     * Asigna el parametro al atributo centro de la clase.
     * @param centro :long, l_centro por el que filtramos la consulta.
     * @since 10/02/2003
     * @author JVN
     */
    public void setCentro(long l_centro) {
        if (l_centro > 0) {
            this.centro = l_centro;
        }
    }

    /**
     * Obtiene el parametro al atributo centro de la clase.
     * @return long, centro.
     * @since 10/02/2003
     * @author JVN
     */
    public long Centro() {
        return this.centro;
    }

    /*

      public static void main(String args[]){
              ConsultaNodosProcedimientos cp=new ConsultaNodosProcedimientos();
              NodoProcedimiento np;
              Vector vp=null;
              cp.setCodigo("12");
              cp.setDesc("");
              cp.setPkPadre(3);
              try{
              vp=cp.resultado();
              }catch(Exception ex){}
              if (vp==null){
                      System.out.println("No hay registros con las condiciones: Centro /"+cp.getCodigo()+"/ - HICL /"+cp.GetPkPadre()+"/");
                      }
              else        {
                        Enumeration e = vp.elements();
                        while (e.hasMoreElements()) {
                            np=(NodoProcedimiento)e.nextElement();
                            System.out.println(np.NodoDesc);
                            }
                      }
              }
    */
}
