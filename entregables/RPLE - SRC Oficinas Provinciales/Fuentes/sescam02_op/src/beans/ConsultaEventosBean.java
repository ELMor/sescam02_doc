/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.exceptions.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;


/**
 * <p>Clase: ConsultaSolicitudesBean</p>
 * <p>Descripcion: Bean que realiza la consulta de solicitudes existentes aplicando determinados fitros:</p>
 *              <p>Oficina,Rango de fechas, Centro, Estado.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Empresa: Soluziona </p>
 * @author Felix Alberto Bravo Hermoso
 */
public class ConsultaEventosBean {
    private String usuario;
    private String orden;
    private long oficina;
    private String fecha_ini;
    private String fecha_fin;
    private long rol;
    private long tipo_evto;
    private String ip;
    private int resultCount;
    public final int MAX_RESULT_SET = 500; //Maximo n� de registros resultantes por consulta.

    /**
     * Metodo que se encarga de la construccion de la consulta, atendiendo a los filtros, y de su ejecucion.
     * Utiliza objetos de la clase VisualizEventos para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos VisualizEventos, en la que cada uno atender� a un evento obtenido
     * en la consulta.
     * @return Vector de objetos de la clase VisualizEventos
     * @throws SQLException
     * @throws ExceptionSESCAM
     * @autor Jose Vicente Nieto-M�rquez Fdez-Medina
     */
    public Vector resultado() throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        resultCount = 0;

        boolean bWhere = false;
        Vector vec_eventos = new Vector();
        Pool ref_pool = null;

        String sentBusqueda = "select o.oficina_nombre,u.syslogin,evtpo.tpoevto_pk,evtpo.tpoevto_desc,to_char(evto_sesion.sess_fecha_inicio,'DD/MM/YYYY') fecha, ev.evto_pk,evto_sesion.sess_pk from Evento_Session evto_sesion,Evento ev,Evento_Tipo evtpo,Sys_Usu u,Ses_Oficina o";
        sentBusqueda += " where evto_sesion.sess_pk = ev.sess_pk and evto_sesion.syslogin = u.syslogin and evto_sesion.oficina_pk = o.oficina_pk and ev.tpoevto_pk = evtpo.tpoevto_pk";

        String sentBusqueda1 = "select  o.oficina_nombre,evto_sesion.syslogin,evtpo.tpoevto_pk,evtpo.tpoevto_desc,to_char(evto_sesion.sess_fecha_inicio,'DD/MM/YYYY') fecha,0,evto_sesion.sess_pk from Evento_Session evto_sesion,Evento_Tipo evtpo,Ses_Oficina o";
        sentBusqueda1 += " where evto_sesion.oficina_pk = o.oficina_pk and evtpo.tpoevto_pk = 19 ";

        if (oficina != 0) {
            sentBusqueda += (" and o.oficina_pk = " + oficina);
            sentBusqueda1 += (" and o.oficina_pk = " + oficina);
            bWhere = true;
        }

        if (rol != 0) {
            sentBusqueda += (" and u.sys_rol_pk=" + rol);
            bWhere = true;
        }

        if ((fecha_ini != null) && (!fecha_ini.equals(""))) {
            sentBusqueda += (" and evto_sesion.sess_fecha_inicio >= to_date('" +
            fecha_ini + "','DD/MM/YYYY')");
            sentBusqueda1 += (" and evto_sesion.sess_fecha_inicio >= to_date('" +
            fecha_ini + "','DD/MM/YYYY')");
            bWhere = true;
        }

        if ((fecha_fin != null) && (!fecha_fin.equals(""))) {
            sentBusqueda += (" and evto_sesion.sess_fecha_inicio <= to_date('" +
            fecha_fin + " 23:59:59','DD/MM/YYYY HH24:MI:SS')");
            sentBusqueda1 += (" and evto_sesion.sess_fecha_inicio <= to_date('" +
            fecha_fin + " 23:59:59','DD/MM/YYYY HH24:MI:SS')");
            bWhere = true;
        }

        if (!usuario.equals("")) {
            sentBusqueda += (" and u.syslogin= '" + usuario + "'");
            sentBusqueda1 += (" and evto_sesion.syslogin= '" + usuario + "'");
            bWhere = true;
        }

        if (!ip.equals("")) {
            sentBusqueda += (" and evto_sesion.sess_srv_rmt = '" + ip + "'");
            sentBusqueda1 += (" and evto_sesion.sess_srv_rmt = '" + ip + "'");
            bWhere = true;
        }

        boolean bEvtoconex = true;

        if (tipo_evto != 0) {
            sentBusqueda += (" and evtpo.tpoevto_pk = " + tipo_evto);
            sentBusqueda1 += (" and evtpo.tpoevto_pk = " + tipo_evto);
            bEvtoconex = false;
            bWhere = true;
        }

        //   System.out.println(sentBusqueda);
        // Si no tiene condiciones de busqueda no se hace la consulta
        if (!bWhere) {
            return null;
        }

        //   System.out.println(sentBusqueda);
        if (bEvtoconex = true) {
            sentBusqueda = sentBusqueda + " UNION ALL " + sentBusqueda1;
        }

        sentBusqueda += (" ORDER BY " + orden);

        //System.out.println(sentBusqueda);
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sentBusqueda);

            while (rs.next()) {
                resultCount++;

                VisualizEventos visevto = new VisualizEventos();
                visevto.evtoOficina = rs.getString(1);
                visevto.evtoLogin = rs.getString(2);
                visevto.evtoCodigo = rs.getLong(3);
                visevto.evtoDesc = rs.getString(4);
                visevto.evtoFecha = rs.getString(5);
                visevto.evtoevto_pk = rs.getLong(6);
                visevto.evtosess_pk = rs.getLong(7);

                if (resultCount == MAX_RESULT_SET) {
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.MAX_RESULTSET);
                    throw es;
                }

                vec_eventos.addElement(visevto);
            }

            conn.commit();

            try {
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_eventos;
    }

    /**
     * Obtiene el valor del atributo oficina de la clase.
     * @return long, Oficina.
     * @since 12/12/2002
     * @author JVN
     */
    public long getOficina() {
        return oficina;
    }

    /**
     * Asigna el parametro al atributo oficina de la clase.
     * @param ofi : long, Oficina_pk de la tabla Oficinas.
     * @since 12/12/2002
     * @author JVN
     */
    public void setOficina(long ofi) {
        long oficina = 0;

        if (ofi != 0) {
            oficina = ofi;
        }

        this.oficina = oficina;
    }

    /**
     * Asigna el parametro al atributo rol de la clase.
     * @param rol : long, Sys_rol_pk de la tabla Sys_roles
     * @since 12/12/2002
     * @author JVN
     */
    public void setRol(long rol_pk) {
        if (rol_pk != 0) {
            this.rol = rol_pk;
        }
    }

    /**
     * Obtiene el valor del atributo ROL de la clase.
     * @return long, Rol.
     * @since 12/12/2002
     * @author JVN
     */
    public long getRol() {
        return rol;
    }

    /**
     * Asigna el parametro al atributo fecha_ini de la clase.
     * @param fini : string, fecha de inicio.
     * @since 12/12/2002
     * @author JVN
     */
    public void setFechaini(String fini) {
        if (fini != null) {
            this.fecha_ini = fini;
        }
    }

    /**
     * Obtiene el valor del atributo fecha_ini de la clase.
     * @return string, Fecha de inicio
     * @since 12/12/2002
     * @author JVN
     */
    public String getFechaini() {
        return fecha_ini;
    }

    /**
     * Asigna el parametro al atributo fecha_fin de la clase.
     * @param ffin : string, fecha fin
     * @since 12/12/2002
     * @author JVN
     */
    public void setFechafin(String ffin) {
        if (ffin != null) {
            this.fecha_fin = ffin;
        }
    }

    /**
     * Obtiene el valor del atributo fecha_fin de la clase.
     * @return string, fecha fin.
     * @since 12/12/2002
     * @author JVN
     */
    public String getFechafin() {
        return fecha_fin;
    }

    /**
     * Obtiene el valor del atributo usuario de la clase.
     * @return String, Login
     * @since 12/12/2002
     * @author JVN
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Asigna el parametro al atributo usuario de la clase.
     * @param login :String, Usuario responsable evento
     * @since 12/12/2002
     * @author JVN
     */
    public void setUsuario(String login) {
        this.usuario = login;
    }

    /**
       * Obtiene el valor del atributo ip de la clase.
       * @return String, Ip
       * @since 12/12/2002
       * @author JVN
       */
    public String getIp() {
        return ip;
    }

    /**
     * Asigna el parametro al atributo ip de la clase.
     * @param login :String, Direccion IP desde la cual se realizo la conexion
     * @since 12/12/2002
     * @author JVN
     */
    public void setIp(String direc) {
        this.ip = direc;
    }

    /**
    * Obtiene el valor del atributo tipo_evto de la clase.
    * @return long, Tipo de Evento.
    * @since 12/12/2002
    * @author JVN
    */
    public long getTipoEvto() {
        return tipo_evto;
    }

    /**
     * Asigna el parametro al atributo tipo_evto de la clase.
     * @param tpoevto : long, tpoevto_pk de la tabla EVENTO_TIPO.
     * @since 12/12/2002
     * @author JVN
     */
    public void setTipoEvto(long tpoevto) {
        if (tpoevto != 0) {
            tipo_evto = tpoevto;
        }
    }

    /**
     * Asigna el parametro al atributo orden de la clase.
     * @param ordenar :string, ordenacion de la consulta (ASC o DESC)
     * @since 12/12/2002
     * @author JVN
     */
    public void setOrden(String ordenar) {
        this.orden = ordenar;
    }

    /**
     * Obtiene el valor del atributo result_count de la clase.
     * @return int, numero de filas resultantes de la consulta.
     * @since 12/12/2002
     * @author JVN
     */
    public int getResultCount() {
        return resultCount;
    }

    /*
      public static void main(String args[]){
              ConsultaPacienteSolicitud cps=new ConsultaPacienteSolicitud();
              Solilcitud sol;
              Vector vp;
              cps.setNombre("nom");
              cps.setApellido1("");
              cps.setApellido2("");
              cps.setCip("");
              cps.setCentro("");
              cps.setHicl("");
              cps.setOrden("cip");
              try{
                vp=cpb.resultado();
                if (vp==null){
                  System.out.println("No se trajo nada");
                }
                else        {
                  Enumeration e = vp.elements();
                  while (e.hasMoreElements()) {
                    pac1=(Paciente)e.nextElement();
                    System.out.println(pac1);
                  }
                  System.out.println("Cantidad de registros:"+cpb.getResultCount());
                }
              }
              catch(Exception e){
                System.out.println("Error al ejecutar la Select.");
                e.printStackTrace();
              }
              }*/
}
