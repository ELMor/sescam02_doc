/*
*   Copyright ( 1999 Union Fenosa International Software Factory,
*   S.A.. All Rights Reserved.
*
*   This software is the confidential and proprietary information of
*   Uni�n Fenosa International Software Factory, S.A.. You shall not
*   disclose such confidential information and shall not accordance
*   with the terms of the license agreement you entered into with
*   I.S.F..
*
*/
package beans;

import conf.Constantes;

import isf.db.*;

import isf.exceptions.*;

import isf.negocio.*;

import java.sql.*;

import java.util.Vector;

import isf.util.Utilidades;


/**
 * <p>Clase: ConsultaSolicitudesBean</p>
 * <p>Descripcion: Bean que realiza la consulta de solicitudes existentes aplicando determinados fitros:</p>
 *              <p>Oficina,Rango de fechas, Centro, Estado.</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Empresa: Soluziona </p>
 * @author Felix Alberto Bravo Hermoso
 */
public class ConsultaSolicitudesBean {
    private long centro;
    private String orden;
    private long oficina;
    private String fecha_ini;
    private String fecha_fin;
    private long estado;

    // private String gar_cq;
    private int resultCount;
    public final int MAX_RESULT_SET = 500; //Maximo n� de registros resultantes por consulta.

    /**
     * Metodo que se encarga de la construccion de la consulta, atendiendo a los filtros, y de su ejecucion.
     * Utiliza objetos de la clase Solicitud para almacenar los campos resultantes, para cada registro obtenido.
     * Por ello, como resultado se obtendra un vector de objetos Solicitud, en la que cada uno atender� a un registro resultado
     * de la consulta.
     * Se realizan dos consultas, una para obtener las solicitudes de la tabla LESP (Lista de Espera Quirurgia) y
     * otra para obtener las correspondientes a la tabla LESPCEX (Lista de Espera de CEX).
     * @return Vector de objetos de la clase Solicitud.
     * @throws SQLException
     * @throws ExceptionSESCAM
     * @autor F�lix Alberto Bravo Hermoso
     */
    public Vector resultado() throws ExceptionSESCAM, SQLException {
        Connection conn = null;
        resultCount = 0;

        boolean bWhere = false;
        Vector vec_solicitudes = new Vector();
        Pool ref_pool = null;
        String sentBusqueda = "";
        String sentWhere = "";
        String sentWhere2 = "";
        String sentWhere3 = "";

        /*    String sentBusqueda="select distinct c.descentro,l.cip,l.ape1,l.ape2,l.nomb,l.hicl,o.oficina_nombre,to_char(gt.gar_fechor,'DD/MM/YYYY') fechor,l.norden,l.centro,gt.gar_operacion,g.gar_cq,g.gar_pk from lesp l,ses_garantia g,ses_garan_tram gt,centrosescam c, ses_oficina o";
            sentBusqueda+=" where l.norden = g.norden and l.centro=g.centro and g.gar_pk = gt.gar_pk  and l.centro=c.codcentro and gt.oficina_pk=o.oficina_pk";
            String sentBusqueda1="select distinct c.descentro,l.cip,l.ape1,l.ape2,l.nomb,l.hicl,o.oficina_nombre,to_char(gt.gar_fechor,'DD/MM/YYYY') fechor,l.ncita,l.cega,gt.gar_operacion,g.gar_cq,g.gar_pk from lespcex l,ses_garantia g,ses_garan_tram gt,centrosescam c, ses_oficina o";
            sentBusqueda1+=" where l.ncita = g.ncita and l.cega=g.cega and g.gar_pk = gt.gar_pk  and l.cega=c.codcentro and gt.oficina_pk=o.oficina_pk";*/
        String sentBusqueda1 =
            "select distinct c.descentro,l.cip,l.ape1,l.ape2,l.nomb,l.hicl,o.oficina_nombre,to_date(to_char(gt.gar_fechor,'DD/MM/YYYY'),'DD/MM/YYYY') fechor,l.norden,l.centro,gt.gar_operacion,g.gar_cq,g.gar_pk from lesp l,ses_garantia g,ses_garan_tram gt,centrosescam c, ses_oficina o " +
            "where l.norden = g.norden and l.centro=g.centro and g.gar_pk = gt.gar_pk  and l.centro=c.codcentro and gt.oficina_pk=o.oficina_pk ";
        String sentBusqueda2 =
            "select distinct c.descentro,l.cip,l.ape1,l.ape2,l.nomb,l.hicl,o.oficina_nombre,to_date(to_char(gt.gar_fechor,'DD/MM/YYYY'),'DD/MM/YYYY') fechor,l.ncita,l.cega,gt.gar_operacion,g.gar_cq,g.gar_pk from lespcex l,ses_garantia g,ses_garan_tram gt,centrosescam c, ses_oficina o " +
            "where l.ncita = g.ncita and l.cega=g.cega and g.gar_pk = gt.gar_pk  and l.cega=c.codcentro and gt.oficina_pk=o.oficina_pk ";
        String sentBusqueda3 =
            "select distinct c.descentro,nl.cip,nl.APELLIDO1 ape1,nl.APELLIDO2 ape2,nl.NOMBRE nomb,nl.NHC hicl,o.OFICINA_NOMBRE,to_date(to_char(nl.GAR_FECHOR,'DD/MM/YYYY'),'DD/MM/YYYY') fechor,0,nl.CENTRO,3,'N',nl.NNOLESP from nolesp nl, centrosescam c, ses_oficina o " +
            "where nl.CENTRO=c.CODCENTRO and nl.GAR_OFICINA=o.OFICINA_PK ";

        if (oficina != 0) {
            sentWhere += (" and gt.oficina_pk = " + oficina);
            sentWhere2 += (" and gt.oficina_pk = " + oficina);
            sentWhere3 += (" and nl.GAR_OFICINA = " + oficina);
            bWhere = true;
        }

        if (centro != 0) {
            sentWhere += (" and l.centro=" + centro);
            sentWhere2 += (" and l.cega=" + centro);
            sentWhere3 += (" and nl.centro=" + centro);
            bWhere = true;
        }

        if ((fecha_ini != null) && (!fecha_ini.equals(""))) {
            sentWhere += (" and gt.gar_fechor >= to_date('" + fecha_ini +
            "','DD/MM/YYYY')");
            sentWhere2 += (" and gt.gar_fechor >= to_date('" + fecha_ini +
            "','DD/MM/YYYY')");
            sentWhere3 += (" and nl.gar_fechor >= to_date('" + fecha_ini +
            "','DD/MM/YYYY')");
            bWhere = true;
        }

        if ((fecha_fin != null) && (!fecha_fin.equals(""))) {
            sentWhere += (" and gt.gar_fechor <= to_date('" + fecha_fin +
            " 23:59:59','DD/MM/YYYY HH24:MI:SS')");
            sentWhere2 += (" and gt.gar_fechor <= to_date('" + fecha_fin +
            " 23:59:59','DD/MM/YYYY HH24:MI:SS')");
            sentWhere3 += (" and nl.gar_fechor <= to_date('" + fecha_fin +
            " 23:59:59','DD/MM/YYYY HH24:MI:SS')");
            bWhere = true;
        }

        if (estado != 0) {
            sentWhere += (" and gt.gar_operacion=" + estado);
            sentWhere2 += (" and gt.gar_operacion=" + estado);
            bWhere = true;
        }

        if ((estado == 0) || (estado == 3)) { //TODOS y Denegacion consulto en NOLESP
            sentBusqueda = sentBusqueda1 + sentWhere + " union " +
                sentBusqueda2 + sentWhere2 + " union " + sentBusqueda3 +
                sentWhere3;
        } else {
            sentBusqueda = sentBusqueda1 + sentWhere + " union " +
                sentBusqueda2 + sentWhere2;
        }

        // Si no tiene condiciones de busqueda no se hace la consulta
        if (!bWhere) {
            return null;
        }

        sentBusqueda += (" ORDER BY " + orden);

        // System.out.println(sentBusqueda);
        ref_pool = Pool.getInstance();
        conn = ref_pool.getConnection(Constantes.FICHERO_CONFIGURACION);

        try {
            conn.setAutoCommit(false);

            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sentBusqueda);

            while (rs.next()) {
                resultCount++;

                Solicitud sol = new Solicitud();
                sol.fCentro = rs.getString(1);
                sol.fCip = rs.getString(2);
                sol.fApe1 = rs.getString(3);
                sol.fApe2 = rs.getString(4);
                sol.fNomb = rs.getString(5);
                sol.fHicl = rs.getInt(6);
                sol.fOficina = rs.getString(7);
                //String fecha = Utilidades.dateToString(rs.getDate(8));
                sol.fFecha = Utilidades.dateToString(rs.getDate(8));
                sol.fNorden = rs.getInt(9);
                sol.fCodCentro = rs.getInt(10);
                sol.fEstado = rs.getInt(11);
                sol.fCq = rs.getString(12);

                if (resultCount == MAX_RESULT_SET) {
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.MAX_RESULTSET);
                    throw es;
                }

                vec_solicitudes.addElement(sol);
            }

            /*      Statement stm1=conn.createStatement();
                    ResultSet rs1= stm.executeQuery(sentBusqueda1);

                    while(rs1.next()){
                        resultCount++;
                        Solicitud sol=new Solicitud();
                        sol.fCentro  =rs1.getString(1);
                        sol.fCip     =rs1.getString(2);
                        sol.fApe1    =rs1.getString(3);
                        sol.fApe2    =rs1.getString(4);
                        sol.fNomb    =rs1.getString(5);
                        sol.fHicl    =rs1.getInt(6);
                        sol.fOficina = rs1.getString(7);
                        sol.fFecha    =rs1.getString(8);
                        sol.fNorden   =rs1.getInt(9);
                        sol.fCodCentro = rs1.getInt(10);
                        sol.fEstado = rs1.getInt(11);
                        sol.fCq = rs.getString(12);
                        if (resultCount==MAX_RESULT_SET) {
                          ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.MAX_RESULTSET);
                          throw es;
                        }
                        vec_solicitudes.addElement(sol);
                   }*/
            conn.commit();

            try {
                //rs1.close();
                //stm1.close();
                rs.close();
                stm.close();
            } catch (Exception ex) {
            }
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException esql) {
            }

            throw ex;
        } catch (Exception exc){
        	System.out.println("Error en la conversion de la fecha.");
        }finally {
            ref_pool.freeConnection(Constantes.FICHERO_CONFIGURACION, conn);
        }

        return vec_solicitudes;
    }

    /**
     * Obtiene el valor del atributo oficina de la clase.
     * @return long, Oficina.
     * @since 15/11/2002
     * @author ABH
     */
    public long getOficina() {
        return oficina;
    }

    /**
     * Asigna el parametro al atributo oficina de la clase.
     * @param ofi : long, Oficina_pk de la tabla Oficinas.
     * @since 15/11/2002
     * @author ABH
     */
    public void setOficina(long ofi) {
        long oficina = 0;

        if (ofi != 0) {
            oficina = ofi;
        }

        this.oficina = oficina;
    }

    /**
     * Asigna el parametro al atributo centro de la clase.
     * @param c : long, CodCentro de la tabla CentroSescam
     * @since 15/11/2002
     * @author ABH
     */
    public void setCentro(long c) {
        if (c != 0) {
            this.centro = c;
        }
    }

    /**
     * Obtiene el valor del atributo centro de la clase.
     * @return long, Centro.
     * @since 15/11/2002
     * @author ABH
     */
    public long getCentro() {
        return centro;
    }

    /**
     * Asigna el parametro al atributo fecha_ini de la clase.
     * @param fini : string, fecha de inicio.
     * @since 15/11/2002
     * @author ABH
     */
    public void setFechaini(String fini) {
        if (fini != null) {
            this.fecha_ini = fini;
        }
    }

    /**
     * Obtiene el valor del atributo fecha_ini de la clase.
     * @return string, Fecha de inicio
     * @since 15/11/2002
     * @author ABH
     */
    public String getFechaini() {
        return fecha_ini;
    }

    /**
     * Asigna el parametro al atributo fecha_fin de la clase.
     * @param ffin : string, fecha fin
     * @since 15/11/2002
     * @author ABH
     */
    public void setFechafin(String ffin) {
        if (ffin != null) {
            this.fecha_fin = ffin;
        }
    }

    /**
     * Obtiene el valor del atributo fecha_fin de la clase.
     * @return string, fecha fin.
     * @since 15/11/2002
     * @author ABH
     */
    public String getFechafin() {
        return fecha_fin;
    }

    /**
     * Obtiene el valor del atributo estado de la clase.
     * @return long, 1: Solicitada, 2:Aprobada, 3:Denegada, 4:Renunciada
     * @since 15/11/2002
     * @author ABH
     */
    public long getEstado() {
        return estado;
    }

    /**
     * Asigna el parametro al atributo estado de la clase.
     * @param est :long, estado de la solicitud
     * @since 15/11/2002
     * @author ABH
     */
    public void setEstado(long est) {
        this.estado = est;
    }

    /**
     * Asigna el parametro al atributo orden de la clase.
     * @param ordenar :string, ordenacion de la consulta (ASC o DESC)
     * @since 15/11/2002
     * @author ABH
     */
    public void setOrden(String ordenar) {
        this.orden = ordenar;
    }

    /**
     * Obtiene el valor del atributo result_count de la clase.
     * @return int, numero de filas resultantes de la consulta.
     * @since 15/11/2002
     * @author ABH
     */
    public int getResultCount() {
        return resultCount;
    }

    /*
      public static void main(String args[]){
              ConsultaPacienteSolicitud cps=new ConsultaPacienteSolicitud();
              Solilcitud sol;
              Vector vp;
              cps.setNombre("nom");
              cps.setApellido1("");
              cps.setApellido2("");
              cps.setCip("");
              cps.setCentro("");
              cps.setHicl("");
              cps.setOrden("cip");
              try{
                vp=cpb.resultado();
                if (vp==null){
                  System.out.println("No se trajo nada");
                }
                else        {
                  Enumeration e = vp.elements();
                  while (e.hasMoreElements()) {
                    pac1=(Paciente)e.nextElement();
                    System.out.println(pac1);
                  }
                  System.out.println("Cantidad de registros:"+cpb.getResultCount());
                }
              }
              catch(Exception e){
                System.out.println("Error al ejecutar la Select.");
                e.printStackTrace();
              }
              }*/
}
