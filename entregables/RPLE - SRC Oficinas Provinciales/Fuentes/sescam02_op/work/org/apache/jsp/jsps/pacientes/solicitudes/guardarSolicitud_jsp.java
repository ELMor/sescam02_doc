package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import java.util.*;

public class guardarSolicitud_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      java.util.HashMap beanParamListaSolic = null;
      synchronized (session) {
        beanParamListaSolic = (java.util.HashMap) pageContext.getAttribute("beanParamListaSolic", PageContext.SESSION_SCOPE);
        if (beanParamListaSolic == null){
          try {
            beanParamListaSolic = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("beanParamListaSolic", beanParamListaSolic, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");

                /* Variables */
                long l_centro,l_norden;
                l_norden = 0;
                String s_garcq, str_pacnss;
                boolean imprimir=false;
                SesGarantia sG = new SesGarantia();

                /* Recojida de parametros */
                s_garcq = new String((String)beanParamListaSolic.get("GAR_CQ").toString());
                l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();
                /* Diferenciacion de registro de LESP o LESPCEX */
                if (s_garcq.equals("Q")) {
                  l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
                } else {
                  l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
                }
                String str_nombre = request.getParameter("nombre");
                String str_dni = request.getParameter("dni");
                String str_letra = request.getParameter("letradni");
                String str_nif = str_dni + str_letra;
                str_pacnss = request.getParameter("nss");
                Garantia gar = new Garantia();
                /* Fin Recojida de Parametros */

                /* Insercion de la solicitud */
                try{
                  if (s_garcq.equals("Q")) {
                    sG.setNorden(l_norden);
                    sG.setCentro(l_centro);
                  }else{
                    sG.setNcita(l_norden);
                    sG.setCega(l_centro);
                  }
                  sG.setGarCq(s_garcq);
                  sG.setGarTipo(1); //Tipo 1: Solicitud
                  sG.setGarEstado(1); //Estado 1: Solicitada
                  sG.setPacNss(str_pacnss);
                  try{
                   gar.SetNegEvento(accesoBeanId.getNegEventoTramInstance(15),61,"DOC01",l_centro,l_norden);
                   gar.insertarGaran(sG,1,accesoBeanId.getUsuario(),str_nombre,str_nif);
                  }catch ( Exception eses ){
                           System.out.println("Excepcion Sescam capturada en guardarSolicitud.jsp");

      out.write("\r\n                          ");
      out.write("<script language=\"javascript\">\r\n                           alert(\"");
      out.print(eses);
      out.write("\");\r\n                          ");
      out.write("</script>\r\n");

                  }
                  imprimir=true;
                }catch(Exception e){
                  System.out.println(" Error " +e+ "al generar la solicitud");
                  imprimir=false;
                }
  
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../../css/estilo.css\">\r\n");
      out.write("<script language=\"javascript\">\r\n    var doc=\"DOC01\";\r\n    function ImprimirDoc(){\r\n        //llamado='../imprime_doc.jsp?centro=");
      out.print(l_centro);
      out.write("&norden=");
      out.print(l_norden);
      out.write("&cq=");
      out.print(s_garcq);
      out.write("&documento='+doc+'&usuario=");
      out.print(accesoBeanId.getUsuario());
      out.write("';\r\n        llamado='../imprime_doc.jsp?centro=");
      out.print(l_centro);
      out.write("&norden=");
      out.print(l_norden);
      out.write("&cq=");
      out.print(s_garcq);
      out.write("&documento='+doc+'&garpk=");
      out.print(sG.getGarPk());
      out.write("&operacion=1&usuario=");
      out.print(accesoBeanId.getUsuario());
      out.write("';\r\n        window.showModalDialog(llamado,\"\",\"dialogWidth:80em;dialogHeight:50em\");\r\n    }\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<body class=\"down\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\">\r\n");

   /* Si la insercion ha sido correcta imprimir documento */
   if (imprimir){

      out.write("\r\n                ");
      out.write("<script language=\"javascript\">\r\n                        ImprimirDoc();\r\n                        parent.centro.location.href = \"lista_solicitudes_main.jsp\";\r\n                        parent.botonera.location.href = \"lista_solicitudes_down.jsp\";\r\n                ");
      out.write("</script>\r\n");

     }

      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
