package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import java.util.*;
import conf.*;
import isf.util.Utilidades;
import isf.util.log.Log;

public class ficha_vis_solicitud_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-15");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");
      out.write("<HTML>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../css/estilo.css\">\r\n\r\n");
      out.write("<script language=\"javascript\">\r\n    function imprimir(doc,garpk,operacion){\r\n         cq = this.document.solicitud.gar_cq.value;\r\n         if (cq == \"Q\") {\r\n           centro=this.document.solicitud.centro.value;\r\n           norden=this.document.solicitud.norden.value;\r\n         } else {\r\n           centro=this.document.solicitud.centro.value;\r\n           norden=this.document.solicitud.ncita.value;\r\n         }\r\n\r\n         llamado='../imprime_doc.jsp?centro='+centro+'&norden='+norden+'&cq='+cq+'&documento='+doc+'&garpk='+garpk+'&operacion='+operacion+'&impresion='+'1'+'&usuario=");
      out.print(accesoBeanId.getUsuario());
      out.write("';\r\n         window.showModalDialog(llamado,\"\",\"dialogWidth:80em;dialogHeight:50em\");\r\n    }\r\n\r\n");
      out.write("</script>\r\n\r\n");
      out.write("</head>\r\n\r\n");

    /* Variables */
    String foco=""; //variable para controlar el foco en la visualizacion
    String nombre_oficina,estado,nif,letra="",dni="",nss;
    long oficina_pk=0;
    Garantia gar = new Garantia();
    SesGarantia sesGar = new SesGarantia();
    GaranTram gT = new GaranTram();
    Vector vg = new Vector();
    Vector vgt = new Vector();

    /* Recogida de parametros */
    long l_pkgar = new Long(request.getParameter("garpk")).longValue();


    /* Busqueda de la Solicitud */
    vg = gar.busquedaGaran("GAR_PK="+l_pkgar,"");
    sesGar = (SesGarantia)vg.elementAt(0);
    Log fichero_log = null;
    if (vg != null) {
     NegEvento negevent = accesoBeanId.getNegEventoInstance(22);
     try {
        if (negevent != null) {
          Long dato = new Long (l_pkgar);
          negevent.addVar(86,dato.toString());
          dato = new Long (sesGar.getGarEstado());
          negevent.addVar(87,dato.toString());

          negevent.guardar();
        }
      }
      catch (Exception ex) {
        fichero_log = Log.getInstance();
        fichero_log.warning(" No se ha realizado el evento de consulta de solicitud con pk " + l_pkgar + "  Error -> +" + ex.toString());
        }
    }
    //Se toma el primer tramo de garantia realizado, que sera siempre la solicitud
    vgt = gT.busquedaGaranTram("GAR_PK="+sesGar.getGarPk(),"GAR_TRAM_PK ASC");
    SesGaranTram sesGT = (SesGaranTram)vgt.elementAt(0);
    if (sesGar.getGarEstado()==1) {
        foco="";
    }else{
        long pos = vgt.size() -  1;
        String marco = "marco"+pos;
        foco="this.document.all."+marco+".focus();";
    }
    oficina_pk = sesGT.getOficinaPk();
    nif=sesGT.getGarReplegDni();
    if ((nif!=null) && (nif!="")){
     letra=nif.substring(nif.length()-1);
     dni=nif.substring(0,nif.length()-1);
    }
    nombre_oficina=OficinaHelper.DameOficina(oficina_pk);
    estado = Constantes.EST_TEXTO[new Long(sesGT.getGarOperacion()).intValue()-1];
    nss=sesGar.getPacNss();

      out.write("\r\n\r\n");
      out.write("<BODY class=\"mainFondoInterno\" onLoad=\"");
      out.print(foco);
      out.write("\" >\r\n");
      out.write("<form name=\"solicitud\" action=\"guardarSolicitud.jsp\" target=\"oculto1\">\r\n ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n  ");
      out.write("<tr>\r\n  ");
      out.write("<td>\r\n   ");
      out.write("<FIELDSET >\r\n   ");
      out.write("<LEGEND>");
      out.write("<a class=\"texto\">");
      out.write("<b>DATOS SOLICITUD");
      out.write("</b>");
      out.write("</a>");
      out.write("</LEGEND>\r\n    ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n     ");
      out.write("<tr>\r\n      ");
      out.write("<td width=\"80%\">\r\n       ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n        ");
      out.write("<tr>\r\n         ");
      out.write("<td class= \"texto\" width=\"20%\">Oficina:");
      out.write("</td>\r\n         ");
      out.write("<td class= \"texto\">");
      out.write("<input type=\"text\" name=\"Oficina\" class=\"cajatexto2\" value=\"");
      out.print(nombre_oficina);
      out.write("\" readonly>");
      out.write("</td>\r\n         ");
      out.write("<td class= \"texto\">Fecha:");
      out.write("</td>\r\n         ");
      out.write("<td class= \"texto\">");
      out.write("<input type=\"text\" name=\"fecha\" class=\"cajatexto2\" value=\"");
      out.print(Utilidades.dateToString(sesGT.getGarFechor()));
      out.write("\" readonly >");
      out.write("</td>\r\n        ");
      out.write("</tr>\r\n        ");
      out.write("<tr>\r\n         ");
      out.write("<td class= \"texto\">Responsable Legal:");
      out.write("</td>\r\n         ");
      out.write("<td class= \"texto\" COLSPAN=4>");
      out.write("<input type=\"text\" name=\"nombre\" class=\"cajatexto3\" value=\"");
      out.print(sesGT.getGarReplegApenom());
      out.write("\" readonly>");
      out.write("</td>\r\n        ");
      out.write("</tr>\r\n        ");
      out.write("<tr>\r\n         ");
      out.write("<td class= \"texto\">N.I.F:");
      out.write("</td>\r\n         ");
      out.write("<td class= \"texto\">");
      out.write("<input maxlength=9 type=\"text\" name=\"dni\"  style=\"width:35%\"class=\"cajatexto2\"  value=\"");
      out.print(dni);
      out.write("\" readonly>&nbsp;\r\n         - ");
      out.write("<input maxlength=1 type=\"text\" name=\"letradni\" style=\"width:8%\" class=\"cajatexto2\" value=\"");
      out.print(letra);
      out.write("\" readonly>");
      out.write("</td>\r\n         ");
      out.write("<td class= \"texto\">Estado:");
      out.write("</td>\r\n         ");
      out.write("<td class= \"texto\">");
      out.write("<input type=\"text\" name=\"estado\" class=\"cajatexto2\" value=\"");
      out.print(estado);
      out.write("\" readonly>");
      out.write("</td>\r\n        ");
      out.write("</tr>\r\n        ");
      out.write("<tr>\r\n         ");
      out.write("<td class= \"texto\">N.S.S.:");
      out.write("</td>\r\n         ");
      out.write("<td class= \"texto\">");
      out.write("<input type=\"text\" name=\"nss\" class=\"cajatexto2\" value=\"");
      out.print(nss);
      out.write("\" readonly>");
      out.write("</td>\r\n        ");
      out.write("</tr>\r\n       ");
      out.write("</table>\r\n      ");
      out.write("</td>\r\n      ");
      out.write("<td align=\"center\">");
      out.write("<a onClick=\"imprimir('DOC01',");
      out.print(sesGT.getGarPk());
      out.write(",");
      out.print(sesGT.getGarOperacion());
      out.write(");\" style=cursor:\"hand\">");
      out.write("<img  alt=\"Documento de Solicitud\" src=\"../../../imagenes/impresora.gif\">");
      out.write("</a>");
      out.write("</td>\r\n     ");
      out.write("</tr>\r\n    ");
      out.write("</table>\r\n   ");
      out.write("</FIELDSET>\r\n  ");
      out.write("</td>\r\n  ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n ");
      out.write("</table>\r\n\r\n ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n\r\n");

  int i_cont;
  for (i_cont=1;i_cont<vgt.size();i_cont++){

  //Se toma ahora el ultimo tramo de garantia generado, que sera la aprobacion o la denegacion.
    SesGaranTram sGT = (SesGaranTram)vgt.elementAt(i_cont);

      out.write("\r\n\r\n");

       if (sGT.getGarOperacion()==2){

      out.write("\r\n       ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n        ");
      out.write("<tr>\r\n         ");
      out.write("<td>\r\n          ");
      out.write("<FIELDSET id=\"marco1\">\r\n          ");
      out.write("<LEGEND>");
      out.write("<a class=\"texto\">");
      out.write("<b>DATOS APROBACION");
      out.write("</b>");
      out.write("</a>");
      out.write("</LEGEND>\r\n");

       }else{
          if (sGT.getGarOperacion()==3){

      out.write("\r\n\r\n      ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n        ");
      out.write("<tr>\r\n         ");
      out.write("<td>\r\n           ");
      out.write("<FIELDSET id=\"marco1\">\r\n           ");
      out.write("<LEGEND>");
      out.write("<a class=\"texto\">");
      out.write("<b>DATOS DENEGACION");
      out.write("</b>");
      out.write("</a>");
      out.write("</LEGEND>\r\n");

         }else{

      out.write("\r\n       ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n        ");
      out.write("<tr>\r\n          ");
      out.write("<td>\r\n             ");
      out.write("<FIELDSET id=\"marco2\">\r\n             ");
      out.write("<LEGEND>");
      out.write("<a class=\"texto\">");
      out.write("<b>DATOS RENUNCIA");
      out.write("</b>");
      out.write("</a>");
      out.write("</LEGEND>\r\n");

         }
       }

      out.write("\r\n       ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n         ");
      out.write("<tr>\r\n          ");
      out.write("<td width=\"80%\">\r\n            ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"c1enter\">\r\n");

            oficina_pk = sGT.getOficinaPk();
            nombre_oficina=OficinaHelper.DameOficina(oficina_pk);
            estado = Constantes.EST_TEXTO[new Long(sGT.getGarOperacion()).intValue()-1];
            nif=sGT.getGarReplegDni();
            if ((nif != null) && (!nif.equals(""))) {
               letra=nif.substring(nif.length()-1);
               dni=nif.substring(0,nif.length()-1);
            }

      out.write("\r\n\r\n            ");
      out.write("<tr>\r\n             ");
      out.write("<td class= \"texto\">Oficina:");
      out.write("</td>\r\n             ");
      out.write("<td class= \"texto\">");
      out.write("<input type=\"text\" name=\"oficina_a\" class=\"cajatexto2\" value=\"");
      out.print(nombre_oficina);
      out.write("\" readonly>");
      out.write("</td>\r\n             ");
      out.write("<td class= \"texto\">Fecha:");
      out.write("</td>\r\n             ");
      out.write("<td class= \"texto\">");
      out.write("<input type=\"text\" name=\"fecha_a\" class=\"cajatexto2\" value=\"");
      out.print(Utilidades.dateToString(sGT.getGarFechor()));
      out.write("\" readonly >");
      out.write("</td>\r\n            ");
      out.write("</tr>\r\n            ");
      out.write("<tr>\r\n             ");
      out.write("<td class= \"texto\">Responsable Legal:");
      out.write("</td>\r\n             ");
      out.write("<td class= \"texto\" COLSPAN=3>");
      out.write("<input type=\"text\" name=\"nombre_a\" class=\"cajatexto3\" value=\"");
      out.print(sGT.getGarReplegApenom());
      out.write("\" readonly>");
      out.write("</td>\r\n            ");
      out.write("</tr>\r\n            ");
      out.write("<tr>\r\n             ");
      out.write("<td class= \"texto\">N.I.F:");
      out.write("</td>\r\n             ");
      out.write("<td class= \"texto\">");
      out.write("<input maxlength=9 type=\"text\" name=\"dni\" style=\"width:35%\" class=\"cajatexto2\"  value=\"");
      out.print(dni);
      out.write("\" readonly>&nbsp;\r\n             - ");
      out.write("<input maxlength=1 type=\"text\" name=\"letradni\" style=\"width:8%\" class=\"cajatexto2\" value=\"");
      out.print(letra);
      out.write("\" readonly>");
      out.write("</td>\r\n             ");
      out.write("<td class= \"texto\">Estado:");
      out.write("</td>\r\n             ");
      out.write("<td class= \"texto\">");
      out.write("<input type=\"text\" name=\"estado_a\" class=\"cajatexto2\" value=\"");
      out.print(estado);
      out.write("\" readonly>");
      out.write("</td>\r\n            ");
      out.write("</tr>\r\n");

            if (sGT.getGarOperacion()==2){

      out.write("\r\n            ");
      out.write("<tr>\r\n             ");
      out.write("<td class= \"texto\">Precio:");
      out.write("</td>\r\n             ");
      out.write("<td class= \"texto\">");
      out.write("<input type=\"text\" name=\"monto_a\" class=\"cajatexto4\" style=\"width:30%\" value=\"");
      out.print(sesGar.getGarMonto());
      out.write("\" readonly>&nbsp;");
      out.write("<img align=\"middle\" src=\"../../../imagenes/euro.gif\">");
      out.write("</td>\r\n            ");
      out.write("</tr>\r\n");

            }else{
               if (sGT.getGarOperacion()==3){

      out.write("\r\n                 ");
      out.write("<tr>\r\n                  ");
      out.write("<td class= \"texto\">Motivo de Rechazo:");
      out.write("</td>\r\n                  ");
      out.write("<td class= \"texto\" COLSPAN=3>");
      out.write("<textarea name=\"rechazo_a\" maxlength=10000 class=\"textarea\" rows=\"2\" readonly>");
      out.print(sesGar.getGarMotivoRech());
      out.write("</textarea>");
      out.write("</td>\r\n                 ");
      out.write("</tr>\r\n");

               }
            }

      out.write("\r\n            ");
      out.write("</table>\r\n           ");
      out.write("</td>\r\n");

           // Dependiendo de la visualizacion de la Aprobacion, Denegacion y/o Renuncia se imprime uno u otro documento.
           if (sGT.getGarOperacion()==2){

      out.write("\r\n             ");
      out.write("<td align=\"center\">");
      out.write("<a onClick=\"imprimir('DOC02',");
      out.print(sGT.getGarPk());
      out.write(",");
      out.print(sGT.getGarOperacion());
      out.write(");\" style=cursor:\"hand\">");
      out.write("<img  alt=\"Documento de Aprobacion\" src=\"../../../imagenes/impresora.gif\">");
      out.write("</a>");
      out.write("</td>\r\n");

          }else{
             if (sGT.getGarOperacion()==3){

      out.write("\r\n                ");
      out.write("<td align=\"center\">");
      out.write("<a onClick=\"imprimir('DOC03',");
      out.print(sGT.getGarPk());
      out.write(",");
      out.print(sGT.getGarOperacion());
      out.write(");\" style=cursor:\"hand\">");
      out.write("<img  alt=\"Documento de Denegacion\" src=\"../../../imagenes/impresora.gif\">");
      out.write("</a>");
      out.write("</td>\r\n");

             }else{

      out.write("\r\n                ");
      out.write("<td align=\"center\">");
      out.write("<a onClick=\"imprimir('DOC04',");
      out.print(sGT.getGarPk());
      out.write(",");
      out.print(sGT.getGarOperacion());
      out.write(");\" style=cursor:\"hand\">");
      out.write("<img  alt=\"Documento de Renuncia\" src=\"../../../imagenes/impresora.gif\">");
      out.write("</a>");
      out.write("</td>\r\n");

             }
          }

      out.write("\r\n         ");
      out.write("</tr>\r\n        ");
      out.write("</table>\r\n       ");
      out.write("</FIELDSET>\r\n      ");
      out.write("</td>\r\n      ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n     ");
      out.write("</tr>\r\n    ");
      out.write("</table>\r\n");

 } //for
 if (sesGar.getGarCq().equals("Q")){

      out.write("\r\n   ");
      out.write("<td>");
      out.write("<input type=\"hidden\" name=\"centro\" value=\"");
      out.print(new Double(sesGar.getCentro()).longValue());
      out.write("\">");
      out.write("</td>\r\n");

 }else{

      out.write("\r\n   ");
      out.write("<td>");
      out.write("<input type=\"hidden\" name=\"centro\" value=\"");
      out.print(new Double(sesGar.getCega()).longValue());
      out.write("\">");
      out.write("</td>\r\n");

 }

      out.write("\r\n   ");
      out.write("<td>");
      out.write("<input type=\"hidden\" name=\"norden\" value=\"");
      out.print(new Double(sesGar.getNorden()).longValue());
      out.write("\">");
      out.write("</td>\r\n   ");
      out.write("<td>");
      out.write("<input type=\"hidden\" name=\"ncita\" value=\"");
      out.print(new Double(sesGar.getNcita()).longValue());
      out.write("\">");
      out.write("</td>\r\n   ");
      out.write("<td>");
      out.write("<input type=\"hidden\" name=\"gar_cq\" value=");
      out.print(sesGar.getGarCq());
      out.write(">");
      out.write("</td>\r\n");
      out.write("</form>\r\n");
      out.write("</BODY>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</HTML>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
