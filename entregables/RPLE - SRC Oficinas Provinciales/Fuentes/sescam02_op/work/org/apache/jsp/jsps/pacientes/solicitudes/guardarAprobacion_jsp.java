package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import java.util.*;
import isf.exceptions.ExceptionSESCAM;

public class guardarAprobacion_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      java.util.HashMap beanParamListaSolic = null;
      synchronized (session) {
        beanParamListaSolic = (java.util.HashMap) pageContext.getAttribute("beanParamListaSolic", PageContext.SESSION_SCOPE);
        if (beanParamListaSolic == null){
          try {
            beanParamListaSolic = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("beanParamListaSolic", beanParamListaSolic, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");

                /***************************/
                /* Definicion de variables */
                /***************************/
                SesGarantia sG = new SesGarantia();
                Vector v=new Vector();
                long l_centro,l_norden=0;
                String s_garcq;
                double d_monto = 0;
                int i_error = 1;
                boolean imprimir=false;

                /**************************/
                /* Recojida de parametros */
                /**************************/
                s_garcq = new String((String)beanParamListaSolic.get("GAR_CQ").toString());
                l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();
                /* Diferenciacion de registro de LESP o LESPCEX */
                if (s_garcq.equals("Q")) {
                  l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
                } else {
                  l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
                }
                String str_nombre = request.getParameter("nombre");
                String str_dni = request.getParameter("dni");
                String str_letra = request.getParameter("letradni");
                String str_nif = str_dni+str_letra;
                try {
                  d_monto = new Double(request.getParameter("monto")).doubleValue();
                } catch (Exception ex) {
                    i_error = -1;
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.FORMATO_NUMERO_INCORRECTO);
                    
      out.write("\r\n                  ");
      out.write("<script language=\"javascript\">\r\n                      alert ('");
      out.print(es.getMensaje());
      out.write("');\r\n                  ");
      out.write("</script>\r\n                    ");

                }

                /******************************/
                /* Insercion de la Aprobacion */
                /******************************/
                if (i_error > 0) {
                  try{
                    /* Busqueda de la Solicitud asociada */
                    long garpk= new Long(request.getParameter("garpk")).longValue();
                    Garantia gar = new Garantia();
                    v = gar.busquedaGaran("GAR_PK="+garpk,"");
                    sG = (SesGarantia)v.elementAt(0);

                    /*Seteo de datos */
                    sG.setGarEstado(2);
                    sG.setGarMonto(d_monto);

                    /*Insercion*/
                    try{
                    gar.SetNegEvento(accesoBeanId.getNegEventoTramInstance(16),65,"DOC02",l_centro,l_norden);
                    gar.modificarGaran(sG,2,accesoBeanId.getUsuario(),str_nombre,str_nif);
                    }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en guardarAprobacion.jsp");

      out.write("\r\n                        ");
      out.write("<script language=\"javascript\">\r\n                        alert(\"");
      out.print(eses);
      out.write("\");\r\n                        ");
      out.write("</script>\r\n");

                    }

                   imprimir = true;
                  }catch (Exception e){
                    System.out.println(" Error " +e+ "al generar la aprobacion");
                    imprimir=false;
                  }
                }

      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../../css/estilo.css\">\r\n");
      out.write("<script language=\"javascript\">\r\n    var doc=\"DOC02\";\r\n    function ImprimirDoc(){\r\n      //llamado='../imprime_doc.jsp?centro=");
      out.print(l_centro);
      out.write("&norden=");
      out.print(l_norden);
      out.write("&cq=");
      out.print(s_garcq);
      out.write("&documento='+doc+'&usuario=");
      out.print(accesoBeanId.getUsuario());
      out.write("';\r\n      llamado='../imprime_doc.jsp?centro=");
      out.print(l_centro);
      out.write("&norden=");
      out.print(l_norden);
      out.write("&cq=");
      out.print(s_garcq);
      out.write("&documento='+doc+'&garpk=");
      out.print(sG.getGarPk());
      out.write("&operacion=2&usuario=");
      out.print(accesoBeanId.getUsuario());
      out.write("';\r\n      window.showModalDialog(llamado,\"\",\"dialogWidth:80em;dialogHeight:50em\");\r\n    }\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<body class=\"down\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\">\r\n");

               /* Si la insercion ha sido correcta imprimir documento */
               if ((i_error > 0) && (imprimir)){

      out.write("\r\n\r\n                   ");
      out.write("<script language=\"javascript\">\r\n                        ImprimirDoc();\r\n                        parent.centro.location.href = \"lista_solicitudes_main.jsp\"\r\n                        parent.botonera.location.href = \"lista_solicitudes_down.jsp\"\r\n                   ");
      out.write("</script>\r\n                ");

                }
                
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
