package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import isf.util.Utilidades;
import conf.*;
import java.util.*;

public class acceso_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"pacientes/error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<title>\r\nAcceso\r\n");
      out.write("</title>\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../css/estilo.css\">\r\n");
      java.util.HashMap beanParamListaSolic = null;
      synchronized (session) {
        beanParamListaSolic = (java.util.HashMap) pageContext.getAttribute("beanParamListaSolic", PageContext.SESSION_SCOPE);
        if (beanParamListaSolic == null){
          try {
            beanParamListaSolic = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("beanParamListaSolic", beanParamListaSolic, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      java.util.HashMap beanClave = null;
      synchronized (session) {
        beanClave = (java.util.HashMap) pageContext.getAttribute("beanClave", PageContext.SESSION_SCOPE);
        if (beanClave == null){
          try {
            beanClave = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("beanClave", beanClave, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");

String strUsuario, strPassword, strDesde;
int i_error;
strUsuario  = request.getParameter("usuario");
strPassword = request.getParameter("password");
strDesde = request.getParameter("desde");
beanParamListaSolic.clear();
if ((strUsuario!=null)&&(!strUsuario.equals(""))&&
    (strPassword!=null)&&(!strPassword.equals(""))){

    beanParamListaSolic.clear();
    accesoBeanId.setUsuario(strUsuario);
    accesoBeanId.setPassword(strPassword);
    accesoBeanId.setStrSess_id(request.getRequestedSessionId());
    accesoBeanId.setStrSrvRmt(request.getRemoteHost());
    i_error=accesoBeanId.initAcceso();
    if (i_error>0){
      if (accesoBeanId.accesoA("acceso_LEQ")||accesoBeanId.accesoA("acceso_Mant")||accesoBeanId.accesoA("acceso_Estad")){
        if (strDesde.equals("a")){

      out.write("\r\n          ");
      out.write("<script language=\"javascript\">\r\n           parent.parent.location='index_validado.htm';\r\n          ");
      out.write("</script>\r\n");

        }else{
          beanClave.put("usuario",strUsuario);
          beanClave.put("clave",strPassword);

      out.write("\r\n          ");
      out.write("<script language=\"javascript\">\r\n           var retorno=false;\r\n           llamado=\"nueva_clave_index.jsp?expirada=no\";\r\n           retorno=window.showModalDialog(llamado,\"\",\"dialogWidth:25em;dialogHeight:15em;status=no\");\r\n           if (retorno){\r\n             parent.parent.location='index_validado.htm';\r\n           }\r\n          ");
      out.write("</script>\r\n");

        }
      }else{

      out.write("\r\n          ");
      out.write("<script language=\"javascript\">\r\n          alert(\"Acceso denegado.\");\r\n          parent.centro.document.all.usuario.value=\"\";\r\n          parent.centro.document.all.password.value=\"\";\r\n          ");
      out.write("</script>\r\n");

     }
    }else{
      switch (i_error){
        case -2:

      out.write("\r\n          ");
      out.write("<script language=\"javascript\">\r\n          alert(\"Acceso denegado, usuario sin permisos definidos.\");\r\n          parent.centro.document.all.usuario.value=\"\";\r\n          parent.centro.document.all.password.value=\"\";\r\n          ");
      out.write("</script>\r\n");

          break;
        case -3:
          beanClave.put("usuario",strUsuario);
          beanClave.put("clave",strPassword);

      out.write("\r\n          ");
      out.write("<script language=\"javascript\">\r\n            llamado=\"nueva_clave_index.jsp?expirada=si\";\r\n            retorno=window.showModalDialog(llamado,\"\",\"dialogWidth:25em;dialogHeight:15em;status=no\");\r\n            if (retorno){\r\n             parent.parent.location='index_validado.htm';\r\n           }\r\n          ");
      out.write("</script>\r\n");

          break;
        case -4:

      out.write("\r\n          ");
      out.write("<script language=\"javascript\">\r\n          alert(\"Acceso denegado usuario inhabilitado.\");\r\n          parent.centro.document.all.usuario.value=\"\";\r\n          parent.centro.document.all.password.value=\"\";\r\n          ");
      out.write("</script>\r\n");

          break;
        case -5:

      out.write("\r\n          ");
      out.write("<script language=\"javascript\">\r\n          alert(\"Acceso denegado, clave invalida.\");\r\n          parent.centro.document.all.usuario.value=\"\";\r\n          parent.centro.document.all.password.value=\"\";\r\n          ");
      out.write("</script>\r\n");

          break;
        case -6:

      out.write("\r\n          ");
      out.write("<script language=\"javascript\">\r\n          alert(\"Acceso denegado temporalmente por reiteracion de clave no valida.\");\r\n          parent.centro.document.all.usuario.value=\"\";\r\n          parent.centro.document.all.password.value=\"\";\r\n          ");
      out.write("</script>\r\n");

          break;
      }
    }
}else{
  if ((strDesde!=null)&&(strDesde.equals("b"))){

      out.write("\r\n          ");
      out.write("<script language=\"javascript\">\r\n          alert(\"Ha de insertar usuario y clave.\");\r\n          parent.centro.document.all.usuario.value=\"\";\r\n          parent.centro.document.all.password.value=\"\";\r\n          ");
      out.write("</script>\r\n");

  }
}


      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n\r\n\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
