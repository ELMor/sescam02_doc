package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import java.util.*;

public class lista_solicitudes_down_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      java.util.HashMap beanParamListaSolic = null;
      synchronized (session) {
        beanParamListaSolic = (java.util.HashMap) pageContext.getAttribute("beanParamListaSolic", PageContext.SESSION_SCOPE);
        if (beanParamListaSolic == null){
          try {
            beanParamListaSolic = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("beanParamListaSolic", beanParamListaSolic, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      beans.ConsultaEntradasLE consultaEntradaLEBeanId = null;
      synchronized (session) {
        consultaEntradaLEBeanId = (beans.ConsultaEntradasLE) pageContext.getAttribute("consultaEntradaLEBeanId", PageContext.SESSION_SCOPE);
        if (consultaEntradaLEBeanId == null){
          try {
            consultaEntradaLEBeanId = (beans.ConsultaEntradasLE) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.ConsultaEntradasLE");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.ConsultaEntradasLE", exc);
          }
          pageContext.setAttribute("consultaEntradaLEBeanId", consultaEntradaLEBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");


long l_centro=0,l_norden=0;
String garcq="",consulta_sol="";
boolean lb_solicitud=true,lb_renuncia=false,lb_outofdate=false,lb_botonera_desactivada=false,lb_ishistory=false;
boolean lb_acceso=false;

if (beanParamListaSolic.size()>1){ // siempre va a haber en el bean al menos un elemento el STRSIT.
 if (((Long)beanParamListaSolic.get("NNOLESP")).longValue()==0){
  if (accesoBeanId.accesoA("emision_SOL")){
    lb_acceso=true;
    //System.out.println("Tengo permiso");
      l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();
      garcq=((String)beanParamListaSolic.get("GAR_CQ")).toString();
      if (garcq.equals("Q")){
         l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
         consulta_sol = "CENTRO="+l_centro+" AND NORDEN="+l_norden+" AND GAR_ESTADO <> 3 AND GAR_ESTADO <> 4 ";
      }else{
         l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
         consulta_sol = "CEGA="+l_centro+" AND NCITA="+l_norden+" AND GAR_ESTADO <> 3 AND GAR_ESTADO <> 4 ";
      }
      EntradaLE eLE=new EntradaLE();
//    if (consultaEntradaLEBeanId.existeEntrada(l_centro,l_norden)!=null){
      if (consultaEntradaLEBeanId.resultado()!=null){
         eLE = consultaEntradaLEBeanId.existeEntrada(l_centro,l_norden,garcq);
         if (eLE.isHistory()){
           lb_ishistory=true;
         }else{
           if (!eLE.isOutOfDate()){
             lb_outofdate=false;
           }else{
             lb_outofdate=true;
           }
           if (((String)beanParamListaSolic.get("STRSIT")).equals("SSG")){
               lb_outofdate=true;    
           }
           lb_solicitud = false;
           Garantia gar = new Garantia();
           Vector vg = new Vector();
           vg = gar.busquedaGaran(consulta_sol,"");
           if (vg.size() == 0){
               lb_solicitud = true;
           }
         }
      }
  }else{
        System.out.println("No Tengo permiso");
  }
 }else{
      lb_botonera_desactivada=true;
 }
}else{
  lb_botonera_desactivada=true;
}


      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Untitled Document");
      out.write("</title>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../../css/estilo.css\">\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/botonera.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\">\r\n    function NuevaSolicitud(){\r\n      if (");
      out.print(lb_acceso);
      out.write("==true){\r\n       if (");
      out.print(lb_ishistory);
      out.write("==false){\r\n         if (");
      out.print(lb_outofdate);
      out.write("==false){\r\n           alert(\"El registro está dentro de fecha o no tiene garantia, no se puede añadir nueva solicitud.\");\r\n         }else{\r\n           if (");
      out.print(lb_solicitud);
      out.write(" == true) {\r\n             parent.centro.location.href=\"ficha_nueva_solicitud.jsp\";\r\n             parent.botonera.location.href= \"ficha_nueva_solicitud_botonera.jsp\";\r\n           }else {\r\n             alert(\"Ya existe una solicitud, no se puede añadir ninguna nueva.\");\r\n           }\r\n         }\r\n        }else{\r\n          alert(\"Registro historico, no se puede añadir nueva solicitud\");\r\n        }\r\n      }else{\r\n        alert(\"No tiene permiso para realizar la accion\");\r\n      }\r\n    }\r\n\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n\r\n");
      out.write("<body class=\"down\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\">\r\n");

  if (lb_botonera_desactivada){

      out.write("\r\n");
      out.write("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" vspace=\"0\" hspace=\"0\" height=\"100%\">\r\n  ");
      out.write("<tr>\r\n    ");
      out.write("<td  width=\"40%\" class=\"botonAccion\">&nbsp;");
      out.write("</td>\r\n    ");
      out.write("<td  width=\"20%\" class=\"botonAccion\" >Nueva Solicitud");
      out.write("</td>\r\n    ");
      out.write("<td  width=\"40%\" class=\"botonAccion\">&nbsp;");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");

  }else{

      out.write("\r\n");
      out.write("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" vspace=\"0\" hspace=\"0\" height=\"100%\">\r\n  ");
      out.write("<tr>\r\n    ");
      out.write("<td  width=\"40%\" class=\"botonAccion\">&nbsp;");
      out.write("</td>\r\n    ");
      out.write("<td  width=\"20%\" class=\"botonAccion\" onMouseOver=\"mOver(this)\" onMouseOut=\"mOut(this)\" onMouseDown=\"mDown(this)\" onMouseUp=\"mOver(this)\" onclick=\"NuevaSolicitud();\">Nueva Solicitud");
      out.write("</td>\r\n    ");
      out.write("<td  width=\"40%\" class=\"botonAccion\">&nbsp;");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");

}

      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
