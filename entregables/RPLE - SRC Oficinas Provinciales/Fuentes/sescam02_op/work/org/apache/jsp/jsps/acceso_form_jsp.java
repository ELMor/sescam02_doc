package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import conf.*;
import isf.negocio.*;

public class acceso_form_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"pacientes/error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");

 int longitud;
 FicheroClave fc = new FicheroClave();
 longitud = fc.DameLongitudClave();

      out.write("\r\n\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<title>\r\nAcceso\r\n");
      out.write("</title>\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../css/estilo.css\">\r\n");
      java.util.HashMap beanClave = null;
      synchronized (session) {
        beanClave = (java.util.HashMap) pageContext.getAttribute("beanClave", PageContext.SESSION_SCOPE);
        if (beanClave == null){
          try {
            beanClave = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("beanClave", beanClave, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../javascript/botonera.js\">");
      out.write("</script>\r\n");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\">\r\n  var txt=\"Registro de Pacientes de Lista de Espera de Castilla-La Mancha.                                         \";\r\n\r\n  function scroll(){\r\n   window.status = txt;\r\n   //txt = txt.substring(1, txt.length) + txt.charAt(0);\r\n   //window.setTimeout(\"scroll()\",150);\r\n  }\r\n\r\n  function hacer_submit() {\r\n    this.document.forms('controlAccesoForm').submit();\r\n  }\r\n\r\n  function control_teclado(){\r\n    if (event.keyCode == 13) hacer_submit();\r\n    if (((event.keyCode > 47) && (event.keyCode ");
      out.write("< 58)) || ((event.keyCode >64) && (event.keyCode ");
      out.write("<91)) || ((event.keyCode >96) && (event.keyCode ");
      out.write("<123)) || (event.keyCode==45) || (event.keyCode==95) || (event.keyCode==209) || (event.keyCode==241) ) event.returnValue=true;\r\n         else event.returnValue=false;\r\n  }\r\n\r\n  function abrir_modal(){\r\n    window.showModalDialog(\"nueva_clave.jsp?expirada='no'\",\"\",\"dialogWidth:25em;dialogHeight:15em\");\r\n  }\r\n\r\n  function cambiar_clave(){\r\n    this.document.all.desde.value=\"b\";\r\n    hacer_submit();\r\n    this.document.all.desde.value=\"a\";\r\n  }\r\n");
      out.write("</script>\r\n");
      out.write("<body class=\"paginaPrincipalCentro\" onLoad=\"scroll();document.controlAccesoForm.usuario.focus();\">\r\n");
      out.write("<form name=\"controlAccesoForm\" method=\"post\" target=\"ctrl_acceso\" action=\"acceso.jsp\" >\r\n");
      out.write("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" vspace=\"0\" hspace=\"0\">\r\n");
      out.write("<tr>\r\n");
      out.write("<td width=\"30%\">");
      out.write("</td>\r\n");
      out.write("<td width=\"40%\">\r\n");
      out.write("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" vspace=\"0\" hspace=\"0\">\r\n  ");
      out.write("<tr class=\"cabeceraSuperior\">\r\n    ");
      out.write("<td>Usuario");
      out.write("</td>\r\n    ");
      out.write("<td>");
      out.write("<input type=\"text\" name=\"usuario\" id=\"usuario\" maxlength=8>");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n  ");
      out.write("<tr class=\"cabeceraSuperior\">\r\n    ");
      out.write("<td>Clave");
      out.write("</td>\r\n    ");
      out.write("<td>");
      out.write("<input type=\"password\" name=\"password\" id=\"password\" onkeypress=\"control_teclado();\" maxlength=");
      out.print(longitud);
      out.write(">");
      out.write("</td>\r\n    ");
      out.write("<td align=\"center\">");
      out.write("<a onClick=\"cambiar_clave();\"style=cursor:\"hand\">");
      out.write("<img src=\"../imagenes/candado.gif\" alt=\"Cambiar clave\">");
      out.write("</a>");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n  ");
      out.write("<tr class=\"cabeceraSuperior\">\r\n    ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n    ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n  ");
      out.write("<tr class=\"cabeceraSuperior\">\r\n    ");
      out.write("<td>");
      out.write("</td>\r\n    ");
      out.write("<td  width=\"25%\" class=\"botonAccion\" onMouseOver=\"mOver(this)\" onMouseOut=\"mOut(this)\" onMouseDown=\"mDown(this)\" onMouseUp=\"mOver(this)\" onclick=\"document.forms('controlAccesoForm').submit();\">Acceder");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("<br>\r\n");
      out.write("<br>\r\n");
      out.write("<br>\r\n");
      out.write("<img src=\"../imagenes/logoSescam.gif\">\r\n");
      out.write("</td>\r\n");
      out.write("<td width=\"30%\">");
      out.write("<input type=\"hidden\" name=\"desde\" value=\"a\">");
      out.write("</td>\r\n");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</form>\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
