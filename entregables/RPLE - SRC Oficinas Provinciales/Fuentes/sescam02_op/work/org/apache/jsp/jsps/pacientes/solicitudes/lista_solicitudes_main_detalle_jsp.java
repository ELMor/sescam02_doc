package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import java.util.*;
import isf.util.Utilidades;
import beans.ConsultaEntradasNoLE;

public class lista_solicitudes_main_detalle_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      java.util.HashMap beanParamListaSolic = null;
      synchronized (session) {
        beanParamListaSolic = (java.util.HashMap) pageContext.getAttribute("beanParamListaSolic", PageContext.SESSION_SCOPE);
        if (beanParamListaSolic == null){
          try {
            beanParamListaSolic = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("beanParamListaSolic", beanParamListaSolic, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      java.util.HashMap EntradaNoLEBean = null;
      synchronized (session) {
        EntradaNoLEBean = (java.util.HashMap) pageContext.getAttribute("EntradaNoLEBean", PageContext.SESSION_SCOPE);
        if (EntradaNoLEBean == null){
          try {
            EntradaNoLEBean = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("EntradaNoLEBean", EntradaNoLEBean, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../css/estilo.css\">\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/menus.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\">\r\nvar FilaSelected=-1;\r\nfunction seleccionaFila(filas, id){\r\n    if (FilaSelected!=-1){\r\n        if((FilaSelected%2)==0)\r\n          filas[FilaSelected].className = \"filaPar\";\r\n        else\r\n          filas[FilaSelected].className = \"filaImpar\";\r\n       }\r\n    filas[id].className = \"filaSelecionada\";\r\n    FilaSelected=id;\r\n}\r\n\r\nfunction selec(t,garpk,esnolesp){\r\n  if ((t==1)&&(esnolesp==0)){ //solicitud\r\n    parent.parent.centro.location='ficha_vis_solicitud.jsp?garpk='+garpk;\r\n    parent.parent.botonera.location.href = 'ficha_vis_solicitud_botonera.jsp?garpk='+garpk;\r\n  }else if (t!=1){\r\n         parent.parent.centro.location='ficha_vis_renuncia.jsp?garpk='+garpk;\r\n         parent.parent.botonera.location.href = 'ficha_vis_renuncia_botonera.jsp?garpk='+garpk;\r\n        }else{\r\n         parent.parent.centro.location='ficha_vis_denegacion_nolesp.jsp?garpk='+garpk;\r\n         parent.parent.botonera.location.href = 'ficha_vis_denegacion_nolesp_botonera.jsp?garpk='+garpk;\r\n        }\r\n}\r\n");
      out.write("\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n\r\n");
      out.write("<body id=\"sel\" class=\"mainFondoInterno\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" onload=\"\">\r\n");

  String tipo,estado,oficina,consulta_gar="CENTRO=-1 ",garcq="";
  boolean sol_con_registro=true;
  long gar_pk,l_centro=0,l_norden=0,nnolesp=0;
  Vector vg=new Vector();
  Vector vgt=new Vector();
  //System.out.println(beanParamListaSolic);
  if (beanParamListaSolic.size()>1){
    l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();
    garcq = ((String)beanParamListaSolic.get("GAR_CQ")).toString();
    if (garcq.equals("Q")){
       l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
       consulta_gar="CENTRO="+l_centro+" AND NORDEN="+l_norden;
    }else{
         l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
         consulta_gar="CEGA="+l_centro+" AND NCITA="+l_norden;
    }
    if (beanParamListaSolic.get("NNOLESP")!=null){
         nnolesp = ((Long)beanParamListaSolic.get("NNOLESP")).longValue();
    }
    if (nnolesp>0) sol_con_registro=false;
  }
  //System.out.println(l_centro+" "+l_norden);
  oficina = OficinaHelper.DameOficina(accesoBeanId.getUsuario());
  //System.out.println(oficina);
 // System.out.println("CENTRO: "+ l_centro);
 // System.out.println("GAR_CQ: "+garcq);
 // System.out.println("NORDEN: "+l_norden);
 // System.out.println("consulta: "+consulta_gar);

      out.write("\r\n\r\n\r\n");
      out.write("<form name=\"seleccion\">\r\n");
      out.write("<layer>");
      out.write("<!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->\r\n");
      out.write("<table height=\"100%\" width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n   ");
      out.write("<tr>\r\n    ");
      out.write("<td class=\"areaCentralInterno\" >\r\n        ");
      out.write("<table class=\"centroTabla\" width=\"100%\" id=\"tablaPac\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n        ");

             boolean bFilaPar=true;
             int idCounter=0;
             long gar_estado,gar_tipo;
             Garantia garan= new Garantia();
             if (sol_con_registro){
                vg = garan.busquedaGaran(consulta_gar,"");
                if (vg.size()>0){
                          //System.out.println("El vector de garantias no es nulo:"+vg.size());
                          for(int int_pos = 0;int_pos < vg.size(); int_pos++){
                                SesGarantia sesGar=(SesGarantia)vg.elementAt(int_pos);
                                gar_estado=sesGar.getGarEstado();
                                gar_pk=sesGar.getGarPk();
                                gar_tipo=sesGar.getGarTipo();
                                //System.out.println("Estado:"+gar_estado);
                                //System.out.println("Gar pk:"+gar_pk);
                                GaranTram gt = new GaranTram();
                                vgt = gt.busquedaGaranTram("GAR_PK="+gar_pk+" AND GAR_OPERACION="+gar_estado,"");
                                SesGaranTram sgt = (SesGaranTram)vgt.elementAt(0);
                                if (bFilaPar){
                                        bFilaPar=false;
        
      out.write("\r\n                                        ");
      out.write("<tr class=\"filaPar\" id=");
      out.print(idCounter);
      out.write(" onclick=\"seleccionaFila(document.all['tablaPac'].rows, this.id);selec(");
      out.print(gar_tipo);
      out.write(",");
      out.print(gar_pk);
      out.write(",0)\" style=\"cursor:hand;\">\r\n        ");
				}else{
                                            bFilaPar=true;
        
      out.write("\r\n                                        ");
      out.write("<tr class=\"filaImpar\" id=");
      out.print(idCounter);
      out.write(" onclick=\"seleccionaFila(document.all['tablaPac'].rows, this.id);selec(");
      out.print(gar_tipo);
      out.write(",");
      out.print(gar_pk);
      out.write(",0)\" style=\"cursor:hand;\">\r\n        ");

                                        }
                                if (sesGar.getGarTipo()==1) {
                                  tipo="Solicitud";
                                }else{
                                  tipo="Renuncia";
                                }
                                //System.out.println(tipo);
        
      out.write("\r\n                                ");
      out.write("<td width=\"20%\" height=\"12px\">");
      out.print(tipo);
      out.write("</td>\r\n        ");

                                 if (tipo!="Renuncia"){
                                    if (sesGar.getGarEstado()==1) {
                                       estado="Solicitada";
                                    }else{
                                       if (sesGar.getGarEstado()==2) {
                                          estado="Aprobada";
                                       }else if (sesGar.getGarEstado()==3) {
                                         estado="Denegada";;
                                       }else {
                                         estado= "Renunciada";
                                       }
                                    }
                                 }else{
                                   estado="";
                                 }
                                 //System.out.println(estado);
        
      out.write("\r\n                                ");
      out.write("<td width=\"20%\" height=\"12px\">");
      out.print(estado);
      out.write("</td>\r\n        ");

                                 if (sesGar.getGarTipo()==1) {
                                  tipo="Solicitud";
                                 }else{
                                  tipo="Renuncia";
                                 }
        
      out.write("\r\n                                ");
      out.write("<td width=\"28%\" height=\"12px\">");
      out.print(oficina);
      out.write("</td>\r\n                                ");
      out.write("<td width=\"14%\" height=\"12px\">");
      out.print(sesGar.getGarMonto());
      out.write("</td>\r\n                                ");
      out.write("<td width=\"18%\" height=\"12px\">");
      out.print(Utilidades.dateToString(sgt.getGarFechor()));
      out.write("</td>\r\n\r\n                           ");
      out.write("</tr>\r\n");

                                  idCounter++;
                                  //System.out.println("Hace el counter++");
                          }
                }
             }else{
                 if (bFilaPar){
                    bFilaPar=false;
        
      out.write("\r\n                    ");
      out.write("<tr class=\"filaPar\" id=");
      out.print(idCounter);
      out.write(" onclick=\"seleccionaFila(document.all['tablaPac'].rows, this.id);selec(1,");
      out.print(nnolesp);
      out.write(",1)\" style=\"cursor:hand;\">\r\n        ");
	}else{
                    bFilaPar=true;
        
      out.write("\r\n                    ");
      out.write("<tr class=\"filaImpar\" id=");
      out.print(idCounter);
      out.write(" onclick=\"seleccionaFila(document.all['tablaPac'].rows, this.id);selec(1,");
      out.print(nnolesp);
      out.write(",1)\" style=\"cursor:hand;\">\r\n        ");

                }
        
      out.write("\r\n                     ");
      out.write("<td width=\"20%\" height=\"12px\">Solicitud");
      out.write("</td>\r\n                     ");
      out.write("<td width=\"20%\" height=\"12px\">Denegacion");
      out.write("</td>\r\n                     ");
      out.write("<td width=\"28%\" height=\"12px\">");
      out.print(EntradaNoLEBean.get("OFICINA"));
      out.write("</td>\r\n                     ");
      out.write("<td width=\"14%\" height=\"12px\">0");
      out.write("</td>\r\n                     ");
      out.write("<td width=\"18%\" height=\"12px\">");
      out.print(EntradaNoLEBean.get("FECHA"));
      out.write("</td>\r\n                    ");
      out.write("</tr>\r\n");

              }

      out.write("\r\n        ");
      out.write("</table>\r\n    ");
      out.write("</td>\r\n   ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</form>\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
