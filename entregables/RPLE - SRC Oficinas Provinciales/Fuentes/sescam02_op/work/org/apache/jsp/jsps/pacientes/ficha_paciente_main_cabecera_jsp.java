package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import java.util.Vector;
import java.util.Enumeration;
import beans.ConsultaPacienteBean;

public class ficha_paciente_main_cabecera_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Untitled Document");
      out.write("</title>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../css/estilo.css\">\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../javascript/menus.js\">");
      out.write("</script>\r\n\r\n");
      out.write("</head>\r\n");
      out.write("<body class=\"mainFondo\" marginwidth=\"0\" marginheight=\"0\" topmargin=\"0\" leftmargin=\"0\">\r\n");
      out.write("<!--\r\n");
      out.write("<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" align=\"center\">\r\n ");
      out.write("<tr>\r\n   ");
      out.write("<td align=\"center\">\r\n-->\r\n\t");
      out.write("<table border=\"0\" width=\"100%\">\r\n");

  Centros cent = new Centros();
  Paciente pacRet;
  Vector vp;
  ConsultaPacienteBean cp=new ConsultaPacienteBean();
  cp.setCip(request.getParameter("cip"));
  vp=cp.resultado();
  if (vp!=null){
      Enumeration e = vp.elements();
      pacRet=(Paciente)e.nextElement();
      }
  else{
    pacRet=new Paciente();
    }

      out.write("\r\n\t   ");
      out.write("<tr style={padding: 0px 0px 0px 0px;}>\r\n\t     ");
      out.write("<td width=\"15%\" class=\"texto\" >Apellidos y Nombre:");
      out.write("</td>\r\n       ");
      out.write("<td width=\"45%\" class=\"normal\" >\r\n         ");
      out.write("<input class=\"cajaTexto2\" style=\"width:90%\" type=\"text\" ID=\"txAyN\" readonly value=\"");
      out.print(pacRet.fApe1+' '+pacRet.fApe2+','+pacRet.fNomb);
      out.write("\">\r\n       ");
      out.write("</td>\r\n     \t ");
      out.write("<td width=\"15%\" class=\"texto\">Fecha de nacimiento:");
      out.write("</td>\r\n\t     ");
      out.write("<td width=\"25%\" class=\"normal\">\r\n\t       ");
      out.write("<input class=\"cajaTexto2\" style=\"width:50%\" type=\"text\" ID=\"txFNac\" readonly value=\"");
      out.print(pacRet.fNac);
      out.write("\">\r\n\t     ");
      out.write("</td>\r\n\t   ");
      out.write("</tr>\r\n\t");
      out.write("</table>\r\n  ");
      out.write("<table border=\"0\" width=\"100%\">\r\n\t   ");
      out.write("<tr style={padding: 0px 0px 0px 0px}>\r\n       ");
      out.write("<td width=\"10%\" class=\"texto\">C.I.P.:");
      out.write("</td>\r\n\t     ");
      out.write("<td width=\"25%\" class=\"normal\">\r\n\t        ");
      out.write("<input class=\"cajaTexto2\"  type=\"text\" ID=\"txCIP\" readonly value=\"");
      out.print(pacRet.fCip);
      out.write("\">\r\n\t     ");
      out.write("</td>\r\n\t     ");
      out.write("<td width=\"10%\" class=\"texto\">Telefono:");
      out.write("</td>\r\n\t     ");
      out.write("<td width=\"55%\" class=\"normal\">\r\n\t\t      ");
      out.write("<input class=\"cajaTexto2\"  type=\"text\" ID=\"txCIP\" readonly value=\"");
      out.print(pacRet.fTelef);
      out.write("\">\r\n\t     ");
      out.write("</td>\r\n     ");
      out.write("</tr>\r\n\t");
      out.write("</table>\r\n  ");
      out.write("<table border=\"0\" width=\"100%\">\r\n\t   ");
      out.write("<tr style={padding: 0px 0px 0px 0px;}>\r\n       ");
      out.write("<td width=\"10%\" class=\"texto\">Domicilio:");
      out.write("</td>\r\n\t     ");
      out.write("<td width=\"90%\" class=\"normal\" >\r\n\t        ");
      out.write("<input class=\"cajaTexto2\" style=\"width:400px\" type=\"text\" ID=\"txCIP\" readonly value=\"");
      out.print(pacRet.fDomi+','+pacRet.fPobl+','+pacRet.fProv);
      out.write("\">\r\n\t     ");
      out.write("</td>\r\n     ");
      out.write("</tr>\r\n  ");
      out.write("</table>\r\n");
      out.write("<!--\r\n    ");
      out.write("</td>\r\n ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n-->\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
