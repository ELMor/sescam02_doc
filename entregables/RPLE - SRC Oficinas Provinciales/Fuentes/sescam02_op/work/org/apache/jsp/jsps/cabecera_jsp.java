package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;

public class cabecera_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>");
      out.write("</title>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../css/estilo.css\">\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../javascript/persiana.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../javascript/FormUtil.js\">");
      out.write("</script>\r\n");
      out.write("<SCRIPT language=\"javascript\">\r\n\t\t\tfunction irAHome(){\r\n                          parent.oculto1.location.href = \"reset.jsp\";\r\n                          parent.location.href = \"index.htm\";\r\n                        }\r\n\t\t\tfunction irAlista()\r\n\t\t\t{\r\n\t\t\t\tif (abiertoIZQI == 0)\r\n\t\t\t\t{\r\n\t\t\t\t\tparent.centro.location.href = \"centro.htm\";\r\n\t\t\t\t\tparent.izquierda.location.href = \"pacientes/leq_menu.htm\"\r\n\t\t\t\t}\r\n\t\t\t\telse\r\n\t\t\t\t\tcorrerPersiana();\r\n\r\n  \t\t\t}\r\n\r\n\t\t\tfunction irAmant()\r\n\t\t\t{\r\n\t\t\t\tif (abiertoIZQI == 0)\r\n\t\t\t\t{\r\n\t\t\t\t\tparent.centro.location.href = \"centro.htm\";\r\n\t\t\t\t\tparent.izquierda.location.href = \"mantenimiento/mant_menu.htm\";\r\n\t\t\t\t}\r\n\t\t\t\telse\r\n\t\t\t\t\tcorrerPersiana();\r\n\r\n  \t\t\t}\r\n                        function irAEstadisticas()\r\n                          {\r\n                                  if (abiertoIZQI == 0)\r\n                                  {\r\n                                          parent.centro.location.href = \"centro.htm\";\r\n                                          parent.izquierda.location.href = \"estadisticas/esta_menu.htm\";\r\n");
      out.write("                                  }\r\n                                  else\r\n                                          correrPersiana();\r\n\r\n                          }\r\n\r\n                        function irAHelp(){\r\n                          window.open(\"ayuda/index_help.htm\",\"\",\"toolbar=no,status=yes,location=no,resizable=yes,width=790,height=550\");\r\n                        }\r\n");
      out.write("</SCRIPT>\r\n");
      out.write("</head>\r\n");
      out.write("<body bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\">\r\n");
      out.write("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"100%\" align=\"center\" vspace=\"0\" hspace=\"0\">\r\n  ");
      out.write("<tr class=\"cabeceraSuperior\">\r\n  ");
      out.write("<td  width=\"115\" height=\"21\">\r\n      ");
      out.write("<div valign=\"bottom\">");
      out.write("<img src=\"../imagenes/logoSescampeque.gif\" >");
      out.write("</div>\r\n    ");
      out.write("</td>\r\n    ");
      out.write("<td  width=\"15\" >\r\n      &nbsp;\r\n    ");
      out.write("</td>\r\n    ");
      out.write("<td  valign=\"bottom\" width=\"701\">R.P.L.E Castilla - La Mancha");
      out.write("</td>\r\n");
      out.write("<!--\r\n    ");
      out.write("<td align=\"right\" style=\"padding-bottom:5px; padding-right:7px;\">");
      out.write("<a href=\"#\">");
      out.write("<img src=\"../imagenes/ico_agenda.gif\" alt=\"Agenda\" border=0>");
      out.write("</a>");
      out.write("</td>\r\n    ");
      out.write("<td align=\"right\" style=\"padding-bottom:5px; padding-right:7px;\" >");
      out.write("<a href=\"#\">");
      out.write("<img src=\"../imagenes/ico_idioma.gif\" alt=\"Selecci&oacute;n de Idioma\" border=0>");
      out.write("</a>");
      out.write("</td>\r\n    ");
      out.write("<td align=\"right\" style=\"padding-bottom:5px; padding-right:7px;\" >");
      out.write("<a href=\"#\">");
      out.write("<img src=\"../imagenes/tablon.gif\" alt=\"Tabl&oacute;n de Anuncios\" border=0>");
      out.write("</a>");
      out.write("</td>\r\n-->\r\n    ");
      out.write("<td align=\"right\" style=\"padding-bottom:5px; padding-right:7px;\" >");
      out.write("<img onclick=\"irAHelp();\" src=\"../imagenes/ico_ayuda_1.gif\" alt=\"Ayuda\" border=0 style=\"cursor:hand\">");
      out.write("</a>");
      out.write("</td>\r\n    ");
      out.write("<td align=\"right\" style=\"padding-bottom:5px; padding-right:7px;\" >");
      out.write("<a href=\"\">");
      out.write("<img onclick=\"irAHome();\" src=\"../imagenes/iconos_11.gif\" alt=\"Inicio\" border=0>");
      out.write("</a>");
      out.write("</td>\r\n");
      out.write("<!--\r\n    ");
      out.write("<td align=\"right\" style=\"padding-bottom:5px; padding-right:7px;\" >");
      out.write("<a href=\"#\">");
      out.write("<img src=\"../imagenes/mapa.gif\" alt=\"Mapa de la Aplicaci&oacute;n\" border=0>");
      out.write("</a>");
      out.write("</td>\r\n-->\r\n    ");
      out.write("<td align=\"right\" style=\"padding-bottom:0px\" >");
      out.write("<img src=\"../imagenes/manostrabajando.gif\">");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n  ");
      out.write("<tr class=\"cabeceraInferior\" >\r\n    ");
      out.write("<td  width=\"115\" height=\"18\" >\r\n      ");
      out.write("<div align=\"right\">");
      out.write("<img src=\"../imagenes/ssclm.jpg\" width=\"102\" border=0>");
      out.write("</div>\r\n    ");
      out.write("</td>\r\n    ");
      out.write("<td  width=\"15\" >\r\n      &nbsp;\r\n    ");
      out.write("</td>\r\n    ");
      out.write("<td  style=\"padding-bottom:5px\" align=\"center\" colspan=8>\r\n");

      boolean primero = false;
      boolean segundo = false;

      if (accesoBeanId.accesoA("acceso_LEQ")||accesoBeanId.accesoA("acceso_LECEX")){
        primero =true;

      out.write("\r\n      ");
      out.write("<nobr>&nbsp;&nbsp; ");
      out.write("<a title=\"Registro de pacientes en Lista de Espera\" href=\"javascript:irAlista()\">R.P.L.E.");
      out.write("</a>\r\n");

       }
      if ((primero)&&(accesoBeanId.accesoA("acceso_Mant"))){

      out.write("\r\n      ");
      out.write("<nobr>&nbsp;&nbsp; ");
      out.write("<a> | ");
      out.write("</a>\r\n");

      }
      if (accesoBeanId.accesoA("acceso_Mant")){
         segundo = true;

      out.write("\r\n      ");
      out.write("<nobr>&nbsp;&nbsp; ");
      out.write("<a title=\"Mantenimiento de la Aplicacion\" href=\"javascript:irAmant()\" >Mantenimiento");
      out.write("</a>\r\n");

       }
      if ((segundo)&&(accesoBeanId.accesoA("acceso_Estad"))){

      out.write("\r\n       ");
      out.write("<nobr>&nbsp;&nbsp; ");
      out.write("<a> | ");
      out.write("</a>\r\n");

     } if (accesoBeanId.accesoA("acceso_Estad")){

      out.write("\r\n      ");
      out.write("<nobr>&nbsp;&nbsp; ");
      out.write("<a title=\"Estadisticas \" href=\"javascript:irAEstadisticas()\" >Estadisticas");
      out.write("</a>\r\n");

     }

      out.write("\r\n    ");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
