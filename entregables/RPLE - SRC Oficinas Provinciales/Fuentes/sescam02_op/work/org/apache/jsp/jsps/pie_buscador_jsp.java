package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;

public class pie_buscador_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Untitled Document");
      out.write("</title>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../css/estilo.css\">\r\n");
      out.write("</head>\r\n");
      out.write("<body class=\"mainFondo\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" onload=\"\">\r\n");

  String estado = " Ingrese condiciones de busqueda. ";
  if (request.getParameter("estado") != null) {
    estado = request.getParameter("estado");
  }
  int nfilas = 0;
  if (request.getParameter("filas") != null) {
    nfilas = new Integer(request.getParameter("filas")).intValue();
  }

      out.write("\r\n");
      out.write("<form name=\"pieEtado\">\r\n  ");
      out.write("<table height=\"100%\" width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n   ");
      out.write("<tr>\r\n    ");
      out.write("<td>\r\n\t  ");
      out.write("<table style=\"width:100%\" align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n\t   ");
      out.write("<tr>\r\n\t    ");
      out.write("<td>\r\n\t\t");
      out.write("<table cellspacing=\"1\" cellpadding=\"1\" border=\"0\">\r\n\t\t   ");
      out.write("<tr class=\"pieInferior\" height=\"18px\">\r\n\t\t\t   ");
      out.write("<td width=\"10%\" style={vertical-align:top;}>Estado:");
      out.write("</td>\r\n\t\t\t   ");
      out.write("<td width=\"40%\" >");
      out.write("<input type=\"text\" class=\"cajaTextoLabel\" ID=\"lEstado\" style={width:200;} value = \"");
      out.print(estado);
      out.write("\" readOnly >");
      out.write("</td>\r\n\t\t\t   ");
      out.write("<td width=\"10%\" style={vertical-align:top;}>Filas:");
      out.write("</td>\r\n\t\t\t   ");
      out.write("<td width=\"40%\" >");
      out.write("<input type=\"text\" class=\"cajaTextoLabel\" ID=\"lFilas\" value = ");
      out.print(nfilas);
      out.write(" readOnly >");
      out.write("</td>\r\n\t\t   ");
      out.write("</tr>\r\n\t\t");
      out.write("</table>\r\n\t    ");
      out.write("</td>\r\n\t   ");
      out.write("</tr>\r\n\t   ");
      out.write("</table>\r\n   ");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</form>\r\n");
      out.write("</body>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
