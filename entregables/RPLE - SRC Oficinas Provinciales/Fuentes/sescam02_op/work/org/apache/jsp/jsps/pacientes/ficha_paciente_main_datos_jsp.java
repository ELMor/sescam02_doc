package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.EntradaLE;
import isf.negocio.EntradaNoLE;
import isf.negocio.Centros;
import java.util.Vector;
import beans.AccesoBean;
import java.util.Enumeration;
import beans.ConsultaEntradasLE;
import beans.ConsultaEntradasNoLE;

public class ficha_paciente_main_datos_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      beans.ConsultaEntradasLE consultaEntradaLEBeanId = null;
      synchronized (session) {
        consultaEntradaLEBeanId = (beans.ConsultaEntradasLE) pageContext.getAttribute("consultaEntradaLEBeanId", PageContext.SESSION_SCOPE);
        if (consultaEntradaLEBeanId == null){
          try {
            consultaEntradaLEBeanId = (beans.ConsultaEntradasLE) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.ConsultaEntradasLE");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.ConsultaEntradasLE", exc);
          }
          pageContext.setAttribute("consultaEntradaLEBeanId", consultaEntradaLEBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      beans.ConsultaEntradasNoLE consultaEntradaNoLEBeanId = null;
      synchronized (session) {
        consultaEntradaNoLEBeanId = (beans.ConsultaEntradasNoLE) pageContext.getAttribute("consultaEntradaNoLEBeanId", PageContext.SESSION_SCOPE);
        if (consultaEntradaNoLEBeanId == null){
          try {
            consultaEntradaNoLEBeanId = (beans.ConsultaEntradasNoLE) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.ConsultaEntradasNoLE");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.ConsultaEntradasNoLE", exc);
          }
          pageContext.setAttribute("consultaEntradaNoLEBeanId", consultaEntradaNoLEBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      java.util.HashMap EntradaNoLEBean = null;
      synchronized (session) {
        EntradaNoLEBean = (java.util.HashMap) pageContext.getAttribute("EntradaNoLEBean", PageContext.SESSION_SCOPE);
        if (EntradaNoLEBean == null){
          try {
            EntradaNoLEBean = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("EntradaNoLEBean", EntradaNoLEBean, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Untitled Document");
      out.write("</title>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../css/estilo.css\">\r\n");
      out.write("<script language=\"javascript\">\r\nvar FilaSelected=-1;\r\nvar SelCentro=\"\",SelNOrden=\"\";\r\n\r\nfunction seleccionaFila(id,pNnolesp,pSelCentro,pSelNOrden,pSelTipoCQ,strSit){\r\n  //alert(pSelTipoCQ);\r\n  var nomCheck=\"\";\r\n  SelCentro=pSelCentro;\r\n  SelNOrden=pSelNOrden;\r\n  if (FilaSelected!=-1){\r\n    nomCheck=\"chequeo\"+FilaSelected;\r\n    document.all(nomCheck).style.visibility =\"hidden\";\r\n  }\r\n  nomCheck=\"chequeo\"+id;\r\n  document.all(nomCheck).style.visibility =\"visible\";\r\n  FilaSelected=id;\r\n  parent.frames(\"datos_tabs\").document.frames(\"detalle_dat\").location='ficha_paciente_main_datos_det.jsp?centro='+pSelCentro+'&norden='+pSelNOrden+'&gar_cq='+pSelTipoCQ;\r\n  parent.frames(\"datos_tabs\").document.frames(\"detalle_tit\").location='ficha_paciente_main_datos_det_tit.jsp?gar_cq='+pSelTipoCQ;\r\n  parent.frames(\"datos_tabs\").document.frames(\"historia_dat\").location='ficha_paciente_main_datos_histo.jsp?centro='+pSelCentro+'&norden='+pSelNOrden+'&gar_cq='+pSelTipoCQ;;\r\n  parent.frames(\"datos_tabs\").document.frames(\"solicitud_index\").location='solicitudes/lista_solicitudes_index.jsp?centro='+pSelCentro+'&norden='+pSelNOrden+'&gar_cq='+pSelTipoCQ+'&nnolesp='+pNnolesp+'&strsit='+strSit;\r\n");
      out.write("}\r\n\r\n");
      out.write("</script>\r\n\r\n");
      out.write("</head>\r\n");
      out.write("<body class=\"mainFondo\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" onload=\"\">\r\n");
      out.write("<!-- DETALLE INI-->\r\n  ");
      out.write("<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n   ");
      out.write("<tr>\r\n    ");
      out.write("<td class=\"areaCentral\" >\r\n        ");
      out.write("<table id=\"entradasLista\" class=\"centroTabla\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n  ");

  EntradaLE entLE;
  EntradaNoLE entnoLE;
  boolean bFilaPar=true;
  boolean hayLESP=false;
  int idCounter=0;
  Vector vp;
  Vector vnl;
  String strCIP=request.getParameter("cip");
  consultaEntradaLEBeanId.setCip(strCIP);
  consultaEntradaNoLEBeanId.setTipo(1); //Consulta por CIP
  consultaEntradaNoLEBeanId.setCip(strCIP);
  int iPCentro=0;
  int iPNOrden=0;
  String strPHistorico="0";
  int iCntHist=0;
  int iCntAct=0;
  int iPID=0;
  long pNnolesp=0;
  boolean bRecarga=true;
  String strPCQ="";
  String PSit="";
  strPHistorico=request.getParameter("historico");
  if ((strPHistorico==null)||(strPHistorico.equals(""))){
    strPHistorico="0";
    bRecarga=true;
  }else{
      bRecarga=false;
  }
  try {
      iPCentro=Integer.parseInt(request.getParameter("centro").trim());
      iPNOrden=Integer.parseInt(request.getParameter("norden").trim());
      strPCQ=request.getParameter("gar_cq").trim();
      iPID=-1;
  }catch (Exception e) {
         iPCentro=0;
         iPNOrden=0;
  }
  if (bRecarga){
    vp=consultaEntradaLEBeanId.resultado();
  }else{
    vp=consultaEntradaLEBeanId.vec_EntradasLE;
  }

  long nolesp=0;
  if (vp!=null){
    Enumeration e = vp.elements();
    if ((iPID==-1)&&(consultaEntradaLEBeanId.existeEntrada(iPCentro,iPNOrden,strPCQ)==null)){
      iPID=0;
      }
    Centros c=new Centros();
    while (e.hasMoreElements()) {
        entLE=(EntradaLE)e.nextElement();
        String descentro = c.dameDescCentro(entLE.fCentro);
        idCounter++;
        String strColor="";
        String strSit="";
        String descsit="";
        strSit=ConsultaEntradasLE.STR_SITUACION[entLE.fSituacion];

        if (strSit.equals("HISTOR")) descsit="Prestacion atendida";
        if (strSit.equals("TG")) descsit="Prestacion y Especialidad garantizadas";
        if (strSit.equals("TG-DV")) descsit="TG con Aplazamiento Voluntario";
        if (strSit.equals("TG-DM")) descsit="TG con Aplazamiento Medico";
        if (strSit.equals("SG-RO")) descsit="Sin garantia por rechazo de oferta";
        if (strSit.equals("SG-BPA")) descsit="Sin garantia por varios aplazamientos";
        if (strSit.equals("PSG")) descsit="Prestacion y/o Especialidad sin garantizar";
        if (strSit.equals("NOCAT")) descsit="Situcion no Catalogada.Avisar a informatica.";
        if (strSit.equals("NOMAP")) descsit="Prestacion y/o Especialidad sin mapear.";
        if (strSit.equals("SSG")) descsit="Sucesiva Sin Garantia.";

        if(!entLE.isHistory()){
                iCntAct++;
                if (vp.size()>0) hayLESP=true;
                if(entLE.isOutOfDate()){ //ROJO
                  strColor="style='background-color:#FEA8A2;'";
                }else{
                 //VERDE tanto los no vencidos como los no garantizados
                  strColor="style='background-color:#99D7C6;'";
                }
        }else{
                iCntHist++;
                if (strPHistorico.equals("0")) continue;
                  //GRIS
                strColor="style='background-color:#687CAB;'";
        }
        if (bFilaPar){
          bFilaPar=false;
 
      out.write("\r\n         ");
      out.write("<tr class=\"filaPar\" id=\"");
      out.print(idCounter);
      out.write("\" onclick=\"seleccionaFila(");
      out.print(idCounter);
      out.write(",");
      out.print(nolesp);
      out.write(",");
      out.print(entLE.fCentro);
      out.write(",");
      out.print(entLE.fNorden);
      out.write(",'");
      out.print(entLE.fTipoCQ);
      out.write("','");
      out.print(strSit);
      out.write("');\" style=\"cursor:hand;\">\r\n ");
	 }else{
         bFilaPar=true;
 
      out.write("\r\n         ");
      out.write("<tr class=\"filaImpar\" id=\"");
      out.print(idCounter);
      out.write("\" onclick=\"seleccionaFila(");
      out.print(idCounter);
      out.write(",");
      out.print(nolesp);
      out.write(",");
      out.print(entLE.fCentro);
      out.write(",");
      out.print(entLE.fNorden);
      out.write(",'");
      out.print(entLE.fTipoCQ);
      out.write("','");
      out.print(strSit);
      out.write("');\" style=\"cursor:hand;\">\r\n ");

         }
 
      out.write("\r\n           ");
      out.write("<td width=\"2%\">");
      out.write("<DIV id=\"chequeo");
      out.print(idCounter);
      out.write("\" style=\"visibility: hidden;\">");
      out.write("<img src=\"../../imagenes/verifica.gif\" border=0>");
      out.write("</DIV>");
      out.write("</td>\r\n           ");
      out.write("<td width=\"8%\" >");
      out.print(entLE.fNorden);
      out.write("</td>\r\n           ");
      out.write("<td width=\"10%\" title=\"");
      out.print(descentro);
      out.write("\">");
      out.print(entLE.fCentro);
      out.write("/");
      out.print(entLE.fHicl);
      out.write("</td>\r\n           ");
      out.write("<td width=\"7%\">");
      out.print(entLE.fCProc);
      out.write("</td>\r\n           ");
      out.write("<td width=\"50%\">");
      out.print(entLE.fDProc);
      out.write("</td>\r\n           ");
      out.write("<td width=\"5%\">");
      out.print(entLE.fTipoCQ);
      out.write("</td>\r\n           ");
      out.write("<td width=\"8%\">");
      out.write("<a title=\"");
      out.print(descsit);
      out.write("\">");
      out.print(strSit);
      out.write("</a>");
      out.write("</td>\r\n           ");
      out.write("<td width=\"10%\" ");
      out.print(strColor);
      out.write(" >");
      out.print(entLE.fAntiguedad);
      out.write("/");
      out.print(entLE.fMaxEspera);
      out.write("</td>\r\n         ");
      out.write("</tr>\r\n");

          if (iPID==0){
            iPID=idCounter;
            iPCentro=entLE.fCentro;
            iPNOrden=entLE.fNorden;
            strPCQ=entLE.fTipoCQ;
            PSit=strSit;
          }else if(iPID==-1){
            if ((iPCentro==entLE.fCentro)&&(iPNOrden==entLE.fNorden)&&(strPCQ.equals(entLE.fTipoCQ))){
              iPID=idCounter;
            }
          }
    }
  }


  int iCntNolesp=0;
  boolean pacNoLesp=false;
  vnl=consultaEntradaNoLEBeanId.resultado();
   if (vnl.size()>0){
    if (strPCQ=="") strPCQ="Q";
    String strSituacion="NOLESP";
    String strColor="style='background-color:#9D9DCE;'";
    Enumeration e = vnl.elements();
    while (e.hasMoreElements()) {
        iCntNolesp++;
        idCounter++;
        if (iPID==0) iPID=idCounter; //iPID controla la seleccion automatica
        entnoLE=(EntradaNoLE)e.nextElement();

        if (bFilaPar){
          bFilaPar=false;
          pNnolesp=entnoLE.getFNnolesp();
          nolesp=pNnolesp;

 
      out.write("\r\n         ");
      out.write("<tr class=\"filaPar\" id=");
      out.print(idCounter);
      out.write(" onclick=\"seleccionaFila(this.id,");
      out.print(pNnolesp);
      out.write(",0,0,'Q')\" style=\"cursor:hand;\">\r\n ");
	 }else{
            bFilaPar=true;
            pNnolesp=entnoLE.getFNnolesp();
 
      out.write("\r\n         ");
      out.write("<tr class=\"filaImpar\" id=");
      out.print(idCounter);
      out.write(" onclick=\"seleccionaFila(this.id,");
      out.print(pNnolesp);
      out.write(",0,0,'Q')\" style=\"cursor:hand;\">\r\n ");

         }
         //Guardamos infomacion en el bean acerca de la solicitud
          EntradaNoLEBean.put("OFICINA",entnoLE.getFOfi_nombre());
          EntradaNoLEBean.put("FECHA",entnoLE.getFFecha());
          EntradaNoLEBean.put("GARPK",new Long(entnoLE.getFNnolesp()));

 
      out.write("\r\n           ");
      out.write("<td width=\"2%\">");
      out.write("<DIV id=\"chequeo");
      out.print(idCounter);
      out.write("\" style=\"visibility: hidden;\">");
      out.write("<img src=\"../../imagenes/verifica.gif\" border=0>");
      out.write("</DIV>");
      out.write("</td>\r\n           ");
      out.write("<td width=\"5%\" >");
      out.print(entnoLE.getFNnolesp());
      out.write("</td>\r\n           ");
      out.write("<td width=\"10%\" title=\"");
      out.print(entnoLE.getFDesCentro());
      out.write("\">");
      out.print(entnoLE.getFCentro());
      out.write("/");
      out.print(entnoLE.getFHicl());
      out.write("</td>\r\n           ");
      out.write("<td width=\"10%\">SD");
      out.write("</td>\r\n           ");
      out.write("<td width=\"48%\">");
      out.print(entnoLE.getFPrestacion());
      out.write("</td>\r\n           ");
      out.write("<td width=\"5%\">N");
      out.write("</td>\r\n           ");
      out.write("<td width=\"8%\">");
      out.write("<a title=\"Fuera de Lista de Espera\">");
      out.print(strSituacion);
      out.write("</a>");
      out.write("</td>\r\n           ");
      out.write("<td width=\"12%\" ");
      out.print(strColor);
      out.write(" >0/0");
      out.write("</td>\r\n         ");
      out.write("</tr>\r\n ");

      }
  }

      out.write("\r\n        ");
      out.write("</table>\r\n    ");
      out.write("</td>\r\n   ");
      out.write("</tr>\r\n   ");
      out.write("<tr class=\"pieInferior\" width=\"100%\">\r\n        ");
      out.write("<td width=\"100%\">\r\n         Activos:");
      out.print(iCntAct);
      out.write("&nbsp;&nbsp;Historicos:");
      out.print(iCntHist);
      out.write("&nbsp;&nbsp;Sin Registrar:");
      out.print(iCntNolesp);
      out.write("\r\n        ");
      out.write("</td>\r\n    ");
      out.write("</tr>\r\n   ");
      out.write("</table>\r\n   ");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n\r\n");
      out.write("<!-- DETALLE FIN-->\r\n");


        if ((iPID!=0)&&(iPID!=-1)){
              if (hayLESP) nolesp=0;

      out.write("        ");
      out.write("<script language=\"javascript\">\r\n              this.seleccionaFila(");
      out.print(iPID);
      out.write(",");
      out.print(nolesp);
      out.write(",");
      out.print(iPCentro);
      out.write(",");
      out.print(iPNOrden);
      out.write(",'");
      out.print(strPCQ);
      out.write("','");
      out.print(PSit);
      out.write("');\r\n          ");
      out.write("</script>\r\n");

          }else{

      out.write("        ");
      out.write("<script language=\"javascript\">\r\n            parent.frames(\"datos_tabs\").document.frames(\"detalle_dat\").location='ficha_paciente_main_datos_det.jsp';\r\n            parent.frames(\"datos_tabs\").document.frames(\"solicitud_index\").location='solicitudes/lista_solicitudes_index.jsp';\r\n            parent.frames(\"datos_tabs\").document.frames(\"historia_dat\").location='ficha_paciente_main_datos_histo.jsp';\r\n          ");
      out.write("</script>\r\n");

          }

      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
