package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import java.util.*;
import isf.util.*;

public class busca_paciente_main_cab_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Untitled Document");
      out.write("</title>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../css/estilo.css\">\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/botonera.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\">\r\n        function hacer_submit() {\r\n          var cadena_texto;\r\n          cadena_texto = this.document.buscapac.apellido1.value.toUpperCase();\r\n          this.document.buscapac.apellido1.value = cadena_texto;\r\n          cadena_texto = this.document.buscapac.apellido2.value.toUpperCase();\r\n          this.document.buscapac.apellido2.value = cadena_texto;\r\n          cadena_texto = this.document.buscapac.nombre.value.toUpperCase();\r\n          this.document.buscapac.nombre.value = cadena_texto;\r\n          if ((this.document.buscapac.cip.value != '') && (this.document.buscapac.cip.value != null)) {\r\n            this.document.buscapac.viene_cip.value = \"1\";\r\n          }\r\n          else {\r\n            this.document.buscapac.viene_cip.value = \"\";\r\n          }\r\n          this.document.buscapac.submit();\r\n          this.document.buscapac.viene_cip.value = \"\";\r\n        }\r\n        function calcular_cadena(){\r\n          var buscamos = this.document.buscapac.cip.value;\r\n          if (buscamos.length > 0) {\r\n");
      out.write("            i = buscamos.indexOf(\"&\");\r\n            if (i != -1) {\r\n              i = i + 1;\r\n              j = buscamos.indexOf('&', i);\r\n              if (j != -1) {\r\n                 buscamos = buscamos.substring(i,j)\r\n                 this.document.buscapac.cip.value = buscamos;\r\n                 cadena_texto = this.document.buscapac.apellido1.value.toUpperCase();\r\n                 this.document.buscapac.apellido1.value = cadena_texto;\r\n                 cadena_texto = this.document.buscapac.apellido2.value.toUpperCase();\r\n                 this.document.buscapac.apellido2.value = cadena_texto;\r\n                 cadena_texto = this.document.buscapac.nombre.value.toUpperCase();\r\n                 this.document.buscapac.nombre.value = cadena_texto;\r\n                 this.document.buscapac.viene_cip.value = \"1\";\r\n                 this.document.buscapac.submit();\r\n                 this.document.buscapac.viene_cip.value = \"\";\r\n              }\r\n            }\r\n          }\r\n        }\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<body class=\"mainFondo\" marginwidth=\"0\" marginheight=\"0\" topmargin=\"0\" leftmargin=\"0\" onLoad=\"document.buscapac.hicl.focus();\">\r\n");
      out.write("<form name=\"buscapac\" action=\"busca_paciente_main_lista_det.jsp\" target=\"consultaPacDetalle\" height=\"10%\" method=\"post\">\r\n        ");
      out.write("<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\">\r\n                ");
      out.write("<tr height=\"12px\">\r\n                  ");
      out.write("<td width=\"5%\">&nbsp;");
      out.write("</td>\r\n                  ");
      out.write("<td width=\"30%\" class=\"texto\">Primer Apellido");
      out.write("</td>\r\n                  ");
      out.write("<td width=\"30%\" class=\"texto\">Segundo Apellido");
      out.write("</td>\r\n                  ");
      out.write("<td width=\"30%\" class=\"texto\">Nombre");
      out.write("</td>\r\n                  ");
      out.write("<td width=\"5%\">&nbsp;");
      out.write("</td>\r\n                ");
      out.write("</tr>\r\n                ");
      out.write("<tr>\r\n                  ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n                        ");
      out.write("<td class=\"normal\">");
      out.write("<input type=\"text\" class=\"cajaTexto2\" name=\"apellido1\" maxlength = 255 value = \"\" onkeypress = \"if (event.keyCode == 13) hacer_submit();\">");
      out.write("</td>\r\n                        ");
      out.write("<td class=\"normal\">");
      out.write("<input type=\"text\" class=\"cajaTexto2\" name=\"apellido2\" maxlength = 255 value = \"\"onkeypress = \"if (event.keyCode == 13) hacer_submit();\">");
      out.write("</td>\r\n                        ");
      out.write("<td class=\"normal\">");
      out.write("<input type=\"text\" class=\"cajaTexto2\" name=\"nombre\" maxlength = 255 value = \"\" onkeypress = \"if (event.keyCode == 13) hacer_submit();\">");
      out.write("</td>\r\n                        ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n                ");
      out.write("</tr>\r\n                ");
      out.write("<tr>\r\n                  ");
      out.write("<td width=\"5%\">&nbsp;");
      out.write("</td>\r\n                  ");
      out.write("<td width=\"30%\" class=\"texto\">Tarjeta sanitaria (CIP)");
      out.write("</td>\r\n                  ");
      out.write("<td width=\"30%\" class=\"texto\">Historia Cl&iacute;nica");
      out.write("</td>\r\n                  ");
      out.write("<td width=\"30%\" class=\"texto\">Centro");
      out.write("</td>\r\n                  ");
      out.write("<td width=\"5%\">&nbsp;");
      out.write("</td>\r\n                ");
      out.write("</tr>\r\n                ");
      out.write("<tr>\r\n                  ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n                        ");
      out.write("<td class=\"normal\">");
      out.write("<input type=\"text\" class=\"cajaTexto2\" name=\"cip\" maxlength = 64 value=\"\" onblur = \"calcular_cadena();\" onkeypress = \"if (event.keyCode == 13) hacer_submit();\"> ");
      out.write("</td>\r\n                        ");
      out.write("<td class=\"normal\">");
      out.write("<input type=\"text\" class=\"cajaTexto2\" name=\"hicl\" value = \"\" onkeypress = \"if (event.keyCode == 13) hacer_submit();\"> ");
      out.write("</td>\r\n                        ");
      out.write("<td class=\"normal\">");
      out.write("<div id=\"centros\">\r\n                        ");
      out.write("<Select name=\"centro\" class=\"cajatexto2\" value=\"\" style=\"width:90%\">\r\n                            ");
      out.write("<option default value='0'>TODOS");
      out.write("</option>\r\n");

                        Centros c = new Centros();
                        Vector v = new Vector();
                        v = c.busquedaCentro("","DESCENTRO ASC");
                        for (int i=0;i<v.size();i++){
                            Centrosescam cs = (Centrosescam)v.elementAt(i);

      out.write("\r\n                            ");
      out.write("<option default value='");
      out.print(cs.getCodcentro());
      out.write("'>");
      out.print(cs.getDescentro());
      out.write("</option>\r\n");

                        }

      out.write("\r\n            ");
      out.write("</div>");
      out.write("</td>\r\n                        ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n                ");
      out.write("</tr>\r\n                ");
      out.write("<tr>\r\n                  ");
      out.write("<td>&nbsp;");
      out.write("</td>\r\n                ");
      out.write("</tr>\r\n        ");
      out.write("</table>\r\n        ");
      out.write("<td>");
      out.write("<input type=\"hidden\" value=\"cip\" name=\"orden\">");
      out.write("</td>\r\n        ");
      out.write("<td>");
      out.write("<input type=\"hidden\" name=\"viene_cip\" value=\"\">");
      out.write("</td>\r\n");
      out.write("</form>\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
