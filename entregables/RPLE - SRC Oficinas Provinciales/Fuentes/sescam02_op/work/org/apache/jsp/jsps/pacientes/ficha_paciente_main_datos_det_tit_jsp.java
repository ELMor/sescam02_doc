package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;

public class ficha_paciente_main_datos_det_tit_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Untitled Document");
      out.write("</title>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../css/estilo.css\">\r\n");
      out.write("</head>\r\n");

  String gar_cq = "";
  boolean lb_cabecera_cex = true;
  if (request.getParameter("gar_cq") != null) {
    gar_cq = request.getParameter("gar_cq");
    if (gar_cq.equals("Q")) {
      lb_cabecera_cex = false;
    }
  }

      out.write("\r\n");
      out.write("<body class=\"mainFondo\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\">\r\n");
      out.write("<!-- CABECERA INICIO-->\r\n");
      out.write("<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n   ");
      out.write("<tr>\r\n    ");
      out.write("<td class=\"areaCentralInterno\">\r\n\t   ");
      out.write("<table style=\"width:100%\" align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n\t    ");
      out.write("<tr>\r\n\t     ");
      out.write("<td class=\"areaCentral2\" >\r\n\t\t    ");
      out.write("<table class=\"cabeceraTabla\"  cellspacing=\"1\" cellpadding=\"1\" border=\"0\">\r\n\t\t     ");
      out.write("<tr class=\"filaCabecera\" height=\"12px\" align=\"center\">\r\n");

if (lb_cabecera_cex == true ) {

      out.write("\r\n           ");
      out.write("<td width=\"5%\">Orden");
      out.write("</td>\r\n           ");
      out.write("<td width=\"13%\">Entrada");
      out.write("</td>\r\n           ");
      out.write("<td width=\"20%\">Fecha Cita");
      out.write("</td>\r\n           ");
      out.write("<td width=\"5%\">Aplazamiento");
      out.write("</td>\r\n           ");
      out.write("<td width=\"15%\">Tiempo Aplazado");
      out.write("</td>\r\n           ");
      out.write("<td width=\"13%\">Salida");
      out.write("</td>\r\n           ");
      out.write("<td width=\"6%\">Motivo");
      out.write("</td>\r\n           ");
      out.write("<td width=\"23%\">Servicio");
      out.write("</td>\r\n");

} else {

      out.write("\r\n           ");
      out.write("<td width=\"6%\">Orden");
      out.write("</td>\r\n           ");
      out.write("<td width=\"20%\">Entrada");
      out.write("</td>\r\n           ");
      out.write("<td width=\"20%\">Aplazamiento");
      out.write("</td>\r\n           ");
      out.write("<td width=\"20%\">Tiempo Aplazado");
      out.write("</td>\r\n           ");
      out.write("<td width=\"20%\">Salida");
      out.write("</td>\r\n           ");
      out.write("<td width=\"12%\">Motivo");
      out.write("</td>\r\n");

}

      out.write("\r\n\t\t     ");
      out.write("</tr>\r\n\t\t    ");
      out.write("</table>\r\n\t     ");
      out.write("</td>\r\n\t    ");
      out.write("</tr>\r\n\t   ");
      out.write("</table>\r\n    ");
      out.write("</td>\r\n   ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("<!-- CABECERA FIN-->\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
