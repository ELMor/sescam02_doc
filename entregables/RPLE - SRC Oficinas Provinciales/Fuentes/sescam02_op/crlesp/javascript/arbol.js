//****************************************************************
// You are free to copy the "Folder-Tree" script as long as you
// keep this copyright notice:
// Script found in: http://www.geocities.com/Paris/LeftBank/2178/
// Author: Marcelino Alves Martins (martins@hks.com) December '97.
// Update: Juli�n D�az del Campo. Permite la generaci�n din�mica
// de los nodos hijos en funci�n de la navegaci�n por los nodos padres.   (19/9/2002)
//****************************************************************

var pathRaiz="";

function eliminaElem (lista, indice) {
//var tmp=new Array();
//tmp=lista;
//tmp.splice(indice,1);
return (lista.slice(0, indice)).concat(lista.slice(indice+1))
//return tmp;
}
function setStateFolder(isOpen)
{
  var subEntries
  var totalHeight
  var fIt = 0
  var i=0

  if (isOpen == this.isOpen)
    return

  if (browserVersion == 2)
  {
    totalHeight = 0
    for (i=0; i < this.nChildren; i++)
      totalHeight = totalHeight + this.children[i].navObj.clip.height
      subEntries = this.subEntries()
    if (this.isOpen)
      totalHeight = 0 - totalHeight
    for (fIt = this.id + subEntries + 1; fIt < nEntries; fIt++)
      indexOfEntries[fIt].navObj.moveBy(0, totalHeight)
  }
  this.isOpen = isOpen
  propagateChangesInState(this)
}

function propagateChangesInState(folder)
{
  var i=0

  if (folder.isOpen)
  {
    if (folder.nodeImg)
      if (folder.isLastNode)
        folder.nodeImg.src =""+pathRaiz+"/imagenes/menu/ftv2mlastnode.gif"
      else
	  folder.nodeImg.src = ""+pathRaiz+"/imagenes/menu/ftv2mnode.gif"
    folder.iconImg.src = ""+pathRaiz+"/imagenes/menu/iOpenFolder.gif"
    for (i=0; i<folder.nChildren; i++)
      folder.children[i].display()
  }
  else
  {
    if (folder.nodeImg)
      if (folder.isLastNode)
        folder.nodeImg.src = ""+pathRaiz+"/imagenes/menu/ftv2plastnode.gif"
      else
  	folder.nodeImg.src = ""+pathRaiz+"/imagenes/menu/ftv2pnode.gif"
    folder.iconImg.src = ""+pathRaiz+"/imagenes/menu/" + folder.folderIconType + "Folder.gif"
    for (i=0; i<folder.nChildren; i++)
      folder.children[i].hide()
  }
}

function hideFolder()
{
  if (browserVersion == 1)
    this.navObj.style.display = "none"
  else
    this.navObj.visibility = "hiden"
}

function addChild(childNode)
{
  this.children[this.nChildren] = childNode
  this.nChildren++
  childNode.padre=this.id;
  return childNode
}

function folderSubEntries()
{
  var i = 0
  var se = this.nChildren

  for (i=0; i < this.nChildren; i++){
    if (this.children[i].children) //is a folder
      se = se + this.children[i].subEntries()
  }

  return se
}


function hideItem()
{
  if (browserVersion == 1)
    this.navObj.style.display = "none"
  else
    this.navObj.visibility = "hiden"
}


// Methods common to both objects (pseudo-inheritance)
// ********************************************************

function display()
{
  if (browserVersion == 1)
    this.navObj.style.display = "block"
  else
    this.navObj.visibility = "show"
}

function createEntryIndex()
{
//  nEntries=indexOfEntries.length;
  this.id = nEntries
  indexOfEntries[nEntries] = this
  nEntries++;
}

// total height of subEntries open
function totalHeight() //used with browserVersion == 2
{
  var h = this.navObj.clip.height
  var i = 0

  if (this.isOpen) //is a folder and _is_ open
    for (i=0 ; i < this.nChildren; i++)
      h = h + this.children[i].totalHeight()

  return h
}


function buildItem(code, description, hreference, leftSide, isLastNode, codPadre)
{
	var linea="";
	var idDiv;
	// creamos un nuevo nodo de tipo item
	var node = gLnk(3/*tipo de target*/, description+"^iJvmAttribute.gif", hreference, code);
	node.initialize() ;

	if (isLastNode) //the last 'brother' in the children array
    {
      	node.leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2blank.gif' width=16 height=22>"
      	leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2lastnode.gif' width=16 height=22>"
    }
    else
    {
	node.leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2vertline.gif' width=16 height=22>"
      	leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2node.gif' width=16 height=22>"
    }

	if (browserVersion == 2)
		linea = linea + "<layer id='div" + node.code + "' top=" + doc.yPos + " visibility=hiden>";

	if (browserVersion == 1)
		linea = linea + " <div id='div"+node.code+"'>";

	linea = linea + " <table ";

	if (browserVersion == 1)
		linea = linea + " id='item" + node.id + "' style='position:block;' ";

	linea = linea + " border=0 cellspacing=0 cellpadding=0>";
	linea = linea + "<tr><td>";
	linea = linea + leftSide;
	//linea = linea + "<a href=" + node.link + ">";
	linea = linea + "<img id='itemIcon"+node.id+"' "
	linea = linea + " src='"+node.iconSrc+"' border=0>";
	//linea = linea + "</a>";
	linea = linea + "</td><td class='celdaArbol' valign=middle nowrap >";

	if (USETEXTLINKS && (node.link != ""))
	{
		linea = linea + "<a href='" + node.link + "'>&nbsp;" + node.desc + "</a>";
		//alert(linea);
	}
	else
		linea = linea + "&nbsp;" + node.desc;

	linea = linea + "</td></table>";

	if (browserVersion == 1)
		linea = linea + "</div>";

	if (browserVersion == 2)
		linea = linea + "</layer>";

	// Insertamos cada nodo hijo en la estructura del arbol
	insDoc(nodeSelected, node)
	// Y posteriormente lo insertamos al final del div correspondiente al padre
	idDiv = "'div" + codPadre + "'";
	document.all[eval(idDiv)].insertAdjacentHTML ('BeforeEnd', linea);

	if (browserVersion == 1)
	{
		node.navObj = doc.all["div"+node.code]
		node.iconImg = doc.all["itemIcon"+node.id]
	}
	else if (browserVersion == 2)
	{
		node.navObj = doc.layers["div"+node.code]
		node.iconImg = node.navObj.document.images["itemIcon"+node.id]
		doc.yPos=doc.yPos+node.navObj.clip.height
	}
	// lo visualizammos
	node.display();
}

function initializeItem()
{
  	this.createIndex()
}

// Definition of class Item (a document or link inside a Folder)
// *************************************************************

function Item(itemDescription, itemLink, _code) // Constructor
{
  // constant data
	descTemp_arr = itemDescription.split("^");
	this.desc=descTemp_arr[0];
	// Assign the default icon if none specified
	if( descTemp_arr[1] == null ) {
		this.iconSrc = ""+pathRaiz+"/imagenes/menu/iGenericAttribute.gif";
	} else {
		this.iconSrc = ""+pathRaiz+"/imagenes/menu/" + descTemp_arr[1];
	}

	//this.desc = itemDescription
	this.link = itemLink
	this.id = -1 //initialized in initalize()
	this.navObj = 0 //initialized in render()
	this.iconImg = 0 //initialized in render()
	this.code = _code;
	this.leftSide = ""
        this.padre=-1;
        this.eliminado=false;

	// alert('desc= '+this.desc);
	this.initialize = initializeItem
	this.createIndex = createEntryIndex
	this.hide = hideItem
	this.display = display
	this.totalHeight = totalHeight
        this.dibujar=dibujar
        this.eliminar=eliminar
}


function buildFolder(code, description, hreference, leftSide, isLastNode, codPadre)
{
	var linea="";
	var idDiv;
	var auxEv;
	// creamos un nuevo nodo de tipo Folder
	var node = gFld(description+"|iGeneric", hreference, code);
	node.initialize() ;

	if (browserVersion == 2)
	{
		if (!doc.yPos)
			doc.yPos=8
		linea = linea + "<layer id='div" + node.code + "' top=" + doc.yPos + " visibility=hiden>";
	}

	if (browserVersion == 1)
		linea = linea + "<div id='div"+node.code+"'>";

	linea = linea + "<table ";

	if (browserVersion == 1)
		linea = linea + " id='folder" + node.id + "' style='position:block;' ";

	linea = linea + " border=0 cellspacing=0 cellpadding=0>";
	linea = linea + "<tr><td>";

	if (browserVersion > 0)
		auxEv = "<a href='javascript:clickOnNode("+node.id+")'  >" ;
	else
		auxEv = "<a>" ;


	if (isLastNode) //the last 'brother' in the children array
	{
		node.leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2blank.gif' width=16 height=22>"
		leftSide = leftSide + auxEv + "<img name='nodeIcon" + node.id + "' src='"+pathRaiz+"/imagenes/menu/ftv2plastnode.gif' width=16 height=22 border=0></a>";
	}
	else
	{
		node.leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2vertline.gif' width=16 height=22>"
		leftSide = leftSide + auxEv + "<img name='nodeIcon" + node.id + "' src='"+pathRaiz+"/imagenes/menu/ftv2pnode.gif' width=16 height=22 border=0></a>";
	}

	node.isLastNode = isLastNode
	linea = linea + leftSide;

	linea = linea + "<a href='javascript:clickOnNode("+node.id+")'  >" ;
	linea = linea + "<img name='folderIcon" + node.id + "' ";
	linea = linea + "src='" + node.iconSrc+"' height=16 width=16 border=0></a>";
	linea = linea + "</td><td valign=middle nowrap >";

	if (USETEXTLINKS)
	{
		linea = linea + "<a href='"+node.link+"'  >" ;
		linea = linea + "&nbsp;" + node.desc + "</a>";
	}
	else
		linea = linea + "&nbsp;" + node.desc;

	linea = linea + "</td>";
	linea = linea + "</table>";

	if (browserVersion == 1)
		linea = linea + "</div>";

	if (browserVersion == 2)
		linea = linea + "</layer>";

	// Insertamos cada nodo hijo en la estructura del arbol, excepto cuando es el raiz (-1)
	if (codPadre != -1)
	{
		insFld(nodeSelected, node)
		// Y posteriormente lo insertamos al final del div correspondiente al padre
		idDiv = "'div" + codPadre + "'";
		doc.all[eval(idDiv)].insertAdjacentHTML ('BeforeEnd', linea);
//		doc.all[eval(idDiv)].insertAdjacentHTML ('BeforeEnd',"<TextArea>"+linea+"</TextArea>");
	}
	else
	{
		doc.all['arbolVisible'].innerHTML= linea;
//		doc.write(linea);
//		doc.write("<textarea>"+linea+"</textarea>");
	}

	if (browserVersion == 1)
	{
		node.navObj = doc.all["div"+node.code]
		node.iconImg = doc.all["folderIcon"+node.id]
		node.nodeImg = doc.all["nodeIcon"+node.id]
	}
	else if (browserVersion == 2)
	{
		node.navObj = doc.layers["div"+node.code]
		node.iconImg = node.navObj.document.images["folderIcon"+node.id]
		node.nodeImg = node.navObj.document.images["nodeIcon"+node.id]
		doc.yPos=doc.yPos+node.navObj.clip.height
	}

	// lo visualizammos
	node.display();
	if (codPadre == -1) // En caso del nodo raiz, la volvemos a abrir
		clickOnNode(node.id);
}

function initializeFolder()
{
  	this.createIndex()
}

function Folder(folderDescription, hreference, _code) //constructor
{
	//constant data
	descTemp_arr = folderDescription.split("|");
	this.desc=descTemp_arr[0];
	// Assign the default icon if none specified
	if( descTemp_arr[1] == null ) {
		this.folderIconType = "iGeneric";
	} else {
		this.folderIconType = descTemp_arr[1];
	}
	//this.desc = folderDescription
	this.hreference = hreference
	this.link = hreference
	this.id = -1
	this.navObj = 0
	this.iconImg = 0
	this.nodeImg = 0
	this.isLastNode = 0
	this.code = _code
	this.expanded = 0
        this.padre=-1;
        this.eliminado=false;

	//dynamic data
	this.isOpen = false
	this.iconSrc = ""+pathRaiz+"/imagenes/menu/" + this.folderIconType + "Folder.gif"
	this.children = new Array
	this.nChildren = 0
	this.leftSide = ""

	//methods
	this.initialize = initializeFolder
	this.setState = setStateFolder
	this.addChild = addChild
	this.createIndex = createEntryIndex
	this.hide = hideFolder
	this.display = display
	this.totalHeight = totalHeight
	this.subEntries = folderSubEntries
        this.dibujar = dibujar
        this.eliminar=eliminar
}

// Auxiliary Functions for Folder-Treee backward compatibility
// *********************************************************

// Funcion para crear un nuevo arbol, recibe code, description, hreference
function newTree(code, description, hreference, nodosHijos,pPathRaiz)
{
        pathRaiz=pPathRaiz;
	if (doc.all)
    	browserVersion = 1 //IE4
  	else
    	if (doc.layers)
      		browserVersion = 2 //NS4
    	else
      		browserVersion = 0 //other

	buildFolder(code, description, hreference, "", 1, -1);
	insertNodes(code, nodosHijos);
}

// Funci�n para insertar los nodos hijos del nodo desplegado
// El array de nodos hijos lo forman otro array con los siguientes atributos ((0)code, (1)type, (2)description, (3)hreference)
function insertNodes(codPadre, nodosHijos)
{
	var nodo;
	var isLastNode=0;
	if (doc.all)
    	browserVersion = 1 //IE4
  	else
    	if (doc.layers)
      		browserVersion = 2 //NS4
    	else
      		browserVersion = 0 //other
	for (var i = 0; i < nodosHijos.length; i++)
	{
		if (i == nodosHijos.length - 1)
			isLastNode = 1;
		nodo = nodosHijos[i];
		if (nodo[1] == 0) // Nodo hoja
			buildItem(nodo[0], nodo[2], nodo[3], nodeSelected.leftSide, isLastNode, codPadre);
		else
			buildFolder(nodo[0], nodo[2], nodo[3], nodeSelected.leftSide, isLastNode, codPadre);
	}
}

function gFld(description, hreference, _code)
{
  folder = new Folder(description, hreference, _code)
  return folder
}

function gLnk(target, description, linkData, _code)
{
  fullLink = ""

  if (target==0)
  {
    fullLink = "'"+linkData+"' target=\"_top\""
  }
  else
  {
    if (target==1)
       	fullLink = "'http://"+linkData+"' target=_blank"
    else if (target==2)
       		fullLink = "\"javascript:abrirDocumento('"+linkData+"');\"";
    else
       		fullLink = linkData;
  }

  linkItem = new Item(description, fullLink, _code)
  return linkItem
}

function insFld(parentFolder, childFolder)
{
  return parentFolder.addChild(childFolder)
}

function insDoc(parentFolder, document)
{
  parentFolder.addChild(document)
}

// Events
// *********************************************************

function clickOnNode(folderId)
{
  var clickedFolder = 0
  var state = 0
  clickedFolder = indexOfEntries[folderId];
  // Mantenemos apuntado el �ltimo nodo se�alado
  nodeSelected = clickedFolder;
  state = clickedFolder.isOpen
  if (folderId !=0 )
  	expandirNodo(nodeSelected);
  clickedFolder.setState(!state); //open<->close
//  alert("Termine de expandir()"+folderId);
//  alert("nodeImg:"+clickedFolder.nodeImg.src+"\n IsOpen:"+clickedFolder.isOpen);
}

function expandirNodo(clickedFolder)
{
	if (!(clickedFolder.expanded))
	{
		formArbol.nodo.value = clickedFolder.code;
		formArbol.submit();
		clickedFolder.expanded = 1;
		//alert("Trayendo datos");
	}
}

function abrirDocumento(documento)
{
  var v = window.open(documento,'newwindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=no,resizable=no');
}

function redibujado(){
  // Proboca el redibujado del �rbol completo.
  indexOfEntries[0].dibujar(-1,'',true);
}

function dibujar(codPadre,leftSide,isLastNode){
  var iSec=0;
  if (this.children) //is a folder
    dibujar_folder(this,codPadre,leftSide,isLastNode);
  else
    dibujar_nodo(this,codPadre,leftSide,isLastNode);

  if ((this.nChildren>0)&&(this.isOpen)) {//is a folder
    for (var iSec = 0; iSec < this.nChildren; iSec++){
      if (this.nChildren-1==iSec)
        pIsLastNode=true;
      else
        pIsLastNode=false;
      this.children[iSec].dibujar(this.code,this.leftSide,pIsLastNode);
    }
  }
}

function dibujar_nodo(node,codPadre,leftSide,isLastNode){
	var linea="";
	var idDiv;

	if (isLastNode) //the last 'brother' in the children array
        {
          node.leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2blank.gif' width=16 height=22>"
          leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2lastnode.gif' width=16 height=22>"
        }
        else
        {
          node.leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2vertline.gif' width=16 height=22>"
          leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2node.gif' width=16 height=22>"
        }
	// creamos un nuevo nodo de tipo item
	if (browserVersion == 2)
		linea = linea + "<layer id='div" + node.code + "' top=" + doc.yPos + " visibility=hiden>";
	if (browserVersion == 1)
		linea = linea + " <div id='div"+node.code+"'>";
	linea = linea + " <table ";
	if (browserVersion == 1)
		linea = linea + " id='item" + node.id + "' style='position:block;' ";
	linea = linea + " border=0 cellspacing=0 cellpadding=0>";
	linea = linea + "<tr><td>";
	linea = linea + leftSide;
	linea = linea + "<img id='itemIcon"+node.id+"' "
	linea = linea + " src='"+node.iconSrc+"' border=0>";
	linea = linea + "</td><td class='celdaArbol' valign=middle nowrap >";
	if (USETEXTLINKS && (node.link != ""))
	{
		linea = linea + "<a href='" + node.link + "'>&nbsp;" + node.desc + "</a>";
	}
	else
		linea = linea + "&nbsp;" + node.desc;

	linea = linea + "</td></table>";

	if (browserVersion == 1)
		linea = linea + "</div>";

	if (browserVersion == 2)
		linea = linea + "</layer>";

	// Y posteriormente lo insertamos al final del div correspondiente al padre
	if (codPadre != -1)
	{
		// Y posteriormente lo insertamos al final del div correspondiente al padre
		idDiv = "'div" + codPadre + "'";
		doc.all[eval(idDiv)].insertAdjacentHTML ('BeforeEnd', linea);
//		doc.all[eval(idDiv)].insertAdjacentHTML ('BeforeEnd',"<TextArea>"+linea+"</TextArea>");
	}
	else
	{
		doc.write(linea);
	}
	if (browserVersion == 1)
	{
		node.navObj = doc.all["div"+node.code]
		node.iconImg = doc.all["itemIcon"+node.id]
	}
	else if (browserVersion == 2)
	{
		node.navObj = doc.layers["div"+node.code]
		node.iconImg = node.navObj.document.images["itemIcon"+node.id]
		doc.yPos=doc.yPos+node.navObj.clip.height
	}
	// lo visualizammos
	node.display();
}

function dibujar_folder(node,codPadre,leftSide,isLastNode){
	var linea="";
	var idDiv;
	var auxEv;
	// creamos un nuevo nodo de tipo Folder
	if (browserVersion == 2)
	{
		if (!doc.yPos)
			doc.yPos=8
		linea = linea + "<layer id='div" + node.code + "' top=" + doc.yPos + " visibility=hiden>";
	}
	if (browserVersion == 1)
		linea = linea + "<div id='div"+node.code+"'>";

	linea = linea + "<table ";

	if (browserVersion == 1)
		linea = linea + " id='folder" + node.id + "' style='position:block;' ";

	linea = linea + " border=0 cellspacing=0 cellpadding=0>";
	linea = linea + "<tr><td>";

	if (browserVersion > 0)
		auxEv = "<a href='javascript:clickOnNode("+node.id+")'  >" ;
	else
		auxEv = "<a>" ;


	if (isLastNode) //the last 'brother' in the children array
	{
		node.leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2blank.gif' width=16 height=22>"
                if (node.isOpen)
                  leftSide = leftSide + auxEv + "<img name='nodeIcon" + node.id + "' src='"+pathRaiz+"/imagenes/menu/ftv2mlastnode.gif' width=16 height=22 border=0></a>";
                else
                  leftSide = leftSide + auxEv + "<img name='nodeIcon" + node.id + "' src='"+pathRaiz+"/imagenes/menu/ftv2plastnode.gif' width=16 height=22 border=0></a>";
	}
	else
	{
		node.leftSide = leftSide + "<img src='"+pathRaiz+"/imagenes/menu/ftv2vertline.gif' width=16 height=22>"
                if(node.isOpen)
                  leftSide = leftSide + auxEv + "<img name='nodeIcon" + node.id + "' src='"+pathRaiz+"/imagenes/menu/ftv2mnode.gif' width=16 height=22 border=0></a>";
                else
                  leftSide = leftSide + auxEv + "<img name='nodeIcon" + node.id + "' src='"+pathRaiz+"/imagenes/menu/ftv2pnode.gif' width=16 height=22 border=0></a>";
	}

	node.isLastNode = isLastNode
	linea = linea + leftSide;

	linea = linea + "<a href='javascript:clickOnNode("+node.id+")'  >" ;
	linea = linea + "<img name='folderIcon" + node.id + "' ";
        if (node.isOpen)
          linea = linea + "src='" + ""+pathRaiz+"/imagenes/menu/iOpenFolder.gif"+"' height=16 width=16 border=0></a>";
        else
          linea = linea + "src='" + node.iconSrc+"' height=16 width=16 border=0></a>";
	linea = linea + "</td><td valign=middle nowrap >";

	if (USETEXTLINKS)
	{
		linea = linea + "<a href='"+node.link+"'  >" ;
		linea = linea + "&nbsp;" + node.desc + "</a>";
	}
	else
		linea = linea + "&nbsp;" + node.desc;

	linea = linea + "</td>";
	linea = linea + "</table>";

	if (browserVersion == 1)
		linea = linea + "</div>";

	if (browserVersion == 2)
		linea = linea + "</layer>";

	// Insertamos cada nodo hijo en la estructura del arbol, excepto cuando es el raiz (-1)
	if (codPadre != -1)
	{
		// Y posteriormente lo insertamos al final del div correspondiente al padre
		idDiv = "'div" + codPadre + "'";
		doc.all[eval(idDiv)].insertAdjacentHTML ('BeforeEnd', linea);
//		doc.all[eval(idDiv)].insertAdjacentHTML ('BeforeEnd',"<TextArea>"+linea+"</TextArea>");
	}
	else
	{
//		doc.write(linea);
		doc.all['arbolVisible'].innerHTML= linea;
	}
	if (browserVersion == 1)
	{
		node.navObj = doc.all["div"+node.code]
		node.iconImg = doc.all["folderIcon"+node.id]
		node.nodeImg = doc.all["nodeIcon"+node.id]
	}
	else if (browserVersion == 2)
	{
		node.navObj = doc.layers["div"+node.code]
		node.iconImg = node.navObj.document.images["folderIcon"+node.id]
		node.nodeImg = node.navObj.document.images["nodeIcon"+node.id]
		doc.yPos=doc.yPos+node.navObj.clip.height
	}
	// lo visualizammos
	node.display();
}

function eliminar(){
  var id_nro_hijo=0;

  if (this.children){ //Es una carpata
    if (this.nChildren>0){//Si tiene hijos
      //Elimino Hijos
      for (var iSec = 0; iSec < this.nChildren; iSec++){
        this.children[iSec].eliminar();
      }
    }
  }
  // Eliminar el propio nodo
  id_nro_hijo=nodo_hijo_pos(this.padre,this.id);
  // Me quito de mi padre
  if (id_nro_hijo!=-1){
    indexOfEntries[this.padre].children=eliminaElem (indexOfEntries[this.padre].children, id_nro_hijo);
    indexOfEntries[this.padre].nChildren--;
  }
  // Me marco como eliminado
  this.eliminado=true;
}

function eliminar_nodo(codigo_del){
  var iSec=0;
  iSec=nodo_pos(codigo_del);
  if (iSec!=-1)
    indexOfEntries[iSec].eliminar();
  redibujado();
}

function nodo_pos(codigo_del){
    for (var iNodoElim = 0; iNodoElim < indexOfEntries.length; iNodoElim++){
      if ((indexOfEntries[iNodoElim].code==codigo_del)&&(indexOfEntries[iNodoElim].eliminado==false) )
        return(iNodoElim)
    }
    return(-1)
}
function nodo_hijo_pos(padre_id,hijo_id){
for (var iNodoElim = 0; iNodoElim < indexOfEntries[padre_id].children.length; iNodoElim++){
      if (indexOfEntries[padre_id].children[iNodoElim].id==hijo_id)
        return(iNodoElim)
    }
    return(-1)
}

// Global variables
// ****************

USETEXTLINKS = 1
indexOfEntries = new Array
nEntries = 0
doc = document;
browserVersion = 0
selectedFolder=0
var nodeSelected;
