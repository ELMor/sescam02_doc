<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="pacientes/error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,isf.util.Utilidades,conf.*,java.util.*" %>

<html>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<title>
Acceso
</title>
<link rel=stylesheet type="text/css" href="../css/estilo.css">
<jsp:useBean id="beanParamListaSolic" class="java.util.HashMap" scope="session"/>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<jsp:useBean id="beanClave" scope="session" class="java.util.HashMap"/>
</head>
<body>
<%
String strUsuario, strPassword, strDesde;
int i_error;
strUsuario  = request.getParameter("usuario");
strPassword = request.getParameter("password");
strDesde = request.getParameter("desde");
beanParamListaSolic.clear();
if ((strUsuario!=null)&&(!strUsuario.equals(""))&&
    (strPassword!=null)&&(!strPassword.equals(""))){

    beanParamListaSolic.clear();
    accesoBeanId.setUsuario(strUsuario);
    accesoBeanId.setPassword(strPassword);
    accesoBeanId.setStrSess_id(request.getRequestedSessionId());
    accesoBeanId.setStrSrvRmt(request.getRemoteHost());
    i_error=accesoBeanId.initAcceso();
    if (i_error>0){
      if (accesoBeanId.accesoA("acceso_LEQ")||accesoBeanId.accesoA("acceso_Mant")||accesoBeanId.accesoA("acceso_Estad")){
        if (strDesde.equals("a")){
%>
          <script language="javascript">
           parent.parent.location='index_validado.htm';
          </script>
<%
        }else{
          beanClave.put("usuario",strUsuario);
          beanClave.put("clave",strPassword);
%>
          <script language="javascript">
           var retorno=false;
           llamado="nueva_clave_index.jsp?expirada=no";
           retorno=window.showModalDialog(llamado,"","dialogWidth:25em;dialogHeight:15em;status=no");
           if (retorno){
             parent.parent.location='index_validado.htm';
           }
          </script>
<%
        }
      }else{
%>
          <script language="javascript">
          alert("Acceso denegado.");
          parent.centro.document.all.usuario.value="";
          parent.centro.document.all.password.value="";
          </script>
<%
     }
    }else{
      switch (i_error){
        case -2:
%>
          <script language="javascript">
          alert("Acceso denegado, usuario sin permisos definidos.");
          parent.centro.document.all.usuario.value="";
          parent.centro.document.all.password.value="";
          </script>
<%
          break;
        case -3:
          beanClave.put("usuario",strUsuario);
          beanClave.put("clave",strPassword);
%>
          <script language="javascript">
            llamado="nueva_clave_index.jsp?expirada=si";
            retorno=window.showModalDialog(llamado,"","dialogWidth:25em;dialogHeight:15em;status=no");
            if (retorno){
             parent.parent.location='index_validado.htm';
           }
          </script>
<%
          break;
        case -4:
%>
          <script language="javascript">
          alert("Acceso denegado usuario inhabilitado.");
          parent.centro.document.all.usuario.value="";
          parent.centro.document.all.password.value="";
          </script>
<%
          break;
        case -5:
%>
          <script language="javascript">
          alert("Acceso denegado, clave invalida.");
          parent.centro.document.all.usuario.value="";
          parent.centro.document.all.password.value="";
          </script>
<%
          break;
        case -6:
%>
          <script language="javascript">
          alert("Acceso denegado temporalmente por reiteracion de clave no valida.");
          parent.centro.document.all.usuario.value="";
          parent.centro.document.all.password.value="";
          </script>
<%
          break;
      }
    }
}else{
  if ((strDesde!=null)&&(strDesde.equals("b"))){
%>
          <script language="javascript">
          alert("Ha de insertar usuario y clave.");
          parent.centro.document.all.usuario.value="";
          parent.centro.document.all.password.value="";
          </script>
<%
  }
}

%>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>


