<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM,isf.util.TrataClave,conf.FicheroClave" %>

<html>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../css/estilo.css">
<jsp:useBean id="beanClave" scope="session" class="java.util.HashMap"/>
</head>
<%
   int salir=0;
   String nueva="",repetida="";
   TrataClave tC = new TrataClave();
   String usuario=new String((String)beanClave.get("usuario").toString());
   String clave = new String((String)beanClave.get("clave").toString());
   FicheroClave fC = new FicheroClave();
   int nmin = fC.DameLongitudClaveMin();
   nmin = nmin / 2;
   if (tC.initClave(usuario)){
      if ((request.getParameter("nueva")!=null)&&(request.getParameter("nueva")!="")){
        nueva=request.getParameter("nueva");
        if ((request.getParameter("repetida")!=null)&&(request.getParameter("repetida")!="")&&(request.getParameter("repetida").equals(nueva))){
          repetida=request.getParameter("repetida");
          if (nueva.equals(clave)){
            salir=3;
          }
        }else{
          salir=2;
        }
      }else{
        salir=1;
      }
   //Controlamos que la nueva clave no se haya repetido anteriormente.
      if (salir==0){
        if (tC.esClaveRepetida(nueva)){
          salir=4;
        }else{ //Controlamos que la nueva clave no tenga caracteres repetidos anteriormente.
          if(tC.CaracteresIguales(nueva)){
            salir=5;
          }else{
            try {
             tC.GuardarClaveNueva(nueva);
             salir=7;
            }catch (ExceptionSESCAM eses){
              salir=0;
              System.out.println("Excepcion Sescam capturada en grabar_cambio_clave.jsp");
%>
    <script language="javascript">
    alert ("<%=eses%>");
    </script>
<%
            }
          }
        }
      }
   }else{
     salir=6; //El usuario no tiene claves asociadas.
   }
%>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
 switch (salir){
   case 1:
   {
%>
     <script language="javascript">
       alert("La clave nueva no puede ser vacia");
     </script>
<%
     break;
   }
   case 2:
   {
%>
     <script language="javascript">
       alert("Por favor, la clave y su repeticion ha de ser la misma.");
     </script>
<%
     break;
   }
   case 3:
   {
%>
     <script language="javascript">
       alert("La clave nueva no puede ser la misma que la antigua.");
     </script>
<%
     break;
   }
   case 4:
   {
%>
     <script language="javascript">
       alert("Clave invalida, ya ha utilizado esa clave anteriormente");
     </script>
<%
     break;
   }
   case 5:
   {
%>
     <script language="javascript">
       alert("La clave no puede contener al menos "+<%=nmin%>+" caracteres utilizados anteriormente");
     </script>
<%
     break;
   }
   case 6:
   {
%>
    <script language="javascript">
       alert("El usuario no tiene claves asociadas.");
    </script>
<%
     break;
   }
   case 7:
   {
%>
    <script language="javascript">
      alert("Cambio de clave realizado con exito.");
      top.returnValue=true;
      top.close();
    </script>
<%
     break;
   }
 case 0:
   {
%>
    <script language="javascript">
      //Error: Excepcion sescam
      top.returnValue=false;
      top.close();
    </script>
<%
break;
   }
 }
%>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>