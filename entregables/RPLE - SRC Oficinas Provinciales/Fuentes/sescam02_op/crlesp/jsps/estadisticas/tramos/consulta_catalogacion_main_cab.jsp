<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.*,beans.ConsultaEntradasLE" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Estad";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</head>
<script language="javascript">

function recargar(){
  this.document.buscapac.action="consulta_catalogacion_main_cab.jsp";
  this.document.buscapac.target="FormConsultaCatalogacion";
  this.document.buscapac.submit();
  this.document.buscapac.action="consulta_catalogacion_main_lista_det.jsp";
  this.document.buscapac.target="consultacatalogacionDetalle";

}
</script>
<%
  int lista;
  if ((request.getParameter("tipolista")!=null)&&(!request.getParameter("tipolista").equals(""))){
      lista=new Integer(request.getParameter("tipolista")).intValue();
  }else{
      lista=1; //valor por defecto de la lista = LEQ.
  }
%>
<body class="mainFondo" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0">
<form name="buscapac" action="consulta_catalogacion_main_lista_det.jsp" target="consultacatalogacionDetalle" height="10%" method="post">
        <table width="100%" align="center" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0">
                <tr height="12px">
                  <td width="30%" class="texto">Centro</td>
                  <td width="20%" class="texto">Tipo de Lista</td>
                  <td width="25%" class="texto">Tramo</td>
                  <td width="25%" class="texto">Tipo de Catalogación</td>
                </tr>
                <tr>
                <tr>
                  <td class= "normal"><div id="divcentro">
                   <Select name="centro" class="cajatexto2" >
                         <option selected value='0'>TODOS</option>
<%
                      Centros c = new Centros();
                      Vector vc = new Vector();
                      vc = c.busquedaCentro("","");
                      for (int int_pos = 0;int_pos < vc.size();int_pos++) {
                          Centrosescam cs = (Centrosescam)vc.elementAt(int_pos);
%>
                         <option value=<%=cs.getCodcentro()%>><%=cs.getDescentro()%></option>
<%
                      }
%>
                   </Select></div>
                  </td>
                  <td class= "normal"><div id="divtipolista">
                   <Select name="tipolista" class="cajatexto2" style="width:80%" onChange="recargar();">
<%
                      int pos = 1;
                      for (int int_pos = 0;int_pos < Tramos.STR_CODTRAMOS.length;int_pos++) {
                       pos = int_pos + 1;
                       if (lista == pos) {
%>
                         <option selected value=<%=pos%>><%=Tramos.STR_CODTRAMOS[int_pos]%></option>
<%
                       }else {
%>
                         <option value=<%=pos%>><%=Tramos.STR_CODTRAMOS[int_pos]%></option>
<%
                        }
                     }
%>
                  </Select></div>
                  </td>
                  <td class= "normal"><div id="divtramo">
                  <Select name="tramo" class="cajatexto2" style="width=90%">
                  <%
                     Tramos tram = new Tramos();
                     Vector v = new Vector();
                     v = tram.DescTramo(lista);

                  %>
                  <option value=0>TODOS</option>
                  <%
                  pos = 1;
                  for (int int_pos = 0;int_pos < v.size(); int_pos ++) {
                    pos = int_pos + 1;
%>
                   <option value=<%=pos%>><%=v.elementAt(int_pos)%></option>
<%
                  }

%>
                  </Select></div>
                  </td>

                  <td class= "normal"><div id="divcatalogacion">
                  <Select name="catalogacion" class="cajatexto2" style="width=90%">
                     <option value='1'>TODOS</option>
                     <option value='HISTOR'>HISTÓRICOS</option>
                     <option value='2'>ACTIVO</option>
                     <option value='TG'>&nbsp;&nbsp;&nbsp;TG</option>
                     <option value='TG_DV'>&nbsp;&nbsp;&nbsp;TG_DV</option>
                     <option value='TG_DM'>&nbsp;&nbsp;&nbsp;TG_DM</option>
                     <option value='SG_RO'>&nbsp;&nbsp;&nbsp;SG_RO</option>
                     <option value='PSG'>&nbsp;&nbsp;&nbsp;PSG</option>
                     <option value='SG_BPA'>&nbsp;&nbsp;&nbsp;SG_BPA</option>
                  </Select></div>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
        </table>
<td><input type="hidden" value="deli" name="orden"></td>
<br>
<br>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

