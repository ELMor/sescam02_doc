<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" %>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../css/estilo.css">
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<script language="javascript" type="text/javascript" src="../javascript/persiana.js"></script>
<script language="javascript" type="text/javascript" src="../javascript/FormUtil.js"></script>
<SCRIPT language="javascript">
			function irAHome(){
                          parent.oculto1.location.href = "reset.jsp";
                          parent.location.href = "index.htm";
                        }
			function irAlista()
			{
				if (abiertoIZQI == 0)
				{
					parent.centro.location.href = "centro.htm";
					parent.izquierda.location.href = "pacientes/leq_menu.htm"
				}
				else
					correrPersiana();

  			}

			function irAmant()
			{
				if (abiertoIZQI == 0)
				{
					parent.centro.location.href = "centro.htm";
					parent.izquierda.location.href = "mantenimiento/mant_menu.htm";
				}
				else
					correrPersiana();

  			}
                        function irAEstadisticas()
                          {
                                  if (abiertoIZQI == 0)
                                  {
                                          parent.centro.location.href = "centro.htm";
                                          parent.izquierda.location.href = "estadisticas/esta_menu.htm";
                                  }
                                  else
                                          correrPersiana();

                          }

                        function irAHelp(){
                          window.open("ayuda/index_help.htm","","toolbar=no,status=yes,location=no,resizable=yes,width=790,height=550");
                        }
</SCRIPT>
</head>
<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%" align="center" vspace="0" hspace="0">
  <tr class="cabeceraSuperior">
  <td  width="115" height="21">
      <div valign="bottom"><img src="../imagenes/logoSescampeque.gif" ></div>
    </td>
    <td  width="15" >
      &nbsp;
    </td>
    <td  valign="bottom" width="701">R.P.L.E Castilla - La Mancha</td>
<!--
    <td align="right" style="padding-bottom:5px; padding-right:7px;"><a href="#"><img src="../imagenes/ico_agenda.gif" alt="Agenda" border=0></a></td>
    <td align="right" style="padding-bottom:5px; padding-right:7px;" ><a href="#"><img src="../imagenes/ico_idioma.gif" alt="Selecci&oacute;n de Idioma" border=0></a></td>
    <td align="right" style="padding-bottom:5px; padding-right:7px;" ><a href="#"><img src="../imagenes/tablon.gif" alt="Tabl&oacute;n de Anuncios" border=0></a></td>
-->
    <td align="right" style="padding-bottom:5px; padding-right:7px;" ><img onclick="irAHelp();" src="../imagenes/ico_ayuda_1.gif" alt="Ayuda" border=0 style="cursor:hand"></a></td>
    <td align="right" style="padding-bottom:5px; padding-right:7px;" ><a href=""><img onclick="irAHome();" src="../imagenes/iconos_11.gif" alt="Inicio" border=0></a></td>
<!--
    <td align="right" style="padding-bottom:5px; padding-right:7px;" ><a href="#"><img src="../imagenes/mapa.gif" alt="Mapa de la Aplicaci&oacute;n" border=0></a></td>
-->
    <td align="right" style="padding-bottom:0px" ><img src="../imagenes/manostrabajando.gif"></td>
  </tr>
  <tr class="cabeceraInferior" >
    <td  width="115" height="18" >
      <div align="right"><img src="../imagenes/ssclm.jpg" width="102" border=0></div>
    </td>
    <td  width="15" >
      &nbsp;
    </td>
    <td  style="padding-bottom:5px" align="center" colspan=8>
<%
      boolean primero = false;
      boolean segundo = false;

      if (accesoBeanId.accesoA("acceso_LEQ")||accesoBeanId.accesoA("acceso_LECEX")){
        primero =true;
%>
      <nobr>&nbsp;&nbsp; <a title="Registro de pacientes en Lista de Espera" href="javascript:irAlista()">R.P.L.E.</a>
<%
       }
      if ((primero)&&(accesoBeanId.accesoA("acceso_Mant"))){
%>
      <nobr>&nbsp;&nbsp; <a> | </a>
<%
      }
      if (accesoBeanId.accesoA("acceso_Mant")){
         segundo = true;
%>
      <nobr>&nbsp;&nbsp; <a title="Mantenimiento de la Aplicacion" href="javascript:irAmant()" >Mantenimiento</a>
<%
       }
      if ((segundo)&&(accesoBeanId.accesoA("acceso_Estad"))){
%>
       <nobr>&nbsp;&nbsp; <a> | </a>
<%
     } if (accesoBeanId.accesoA("acceso_Estad")){
%>
      <nobr>&nbsp;&nbsp; <a title="Estadisticas " href="javascript:irAEstadisticas()" >Estadisticas</a>
<%
     }
%>
    </td>
  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
