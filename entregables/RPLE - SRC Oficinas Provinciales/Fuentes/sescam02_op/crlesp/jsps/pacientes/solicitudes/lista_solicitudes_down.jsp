<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<jsp:useBean id="beanParamListaSolic" class="java.util.HashMap" scope="session"/>
<jsp:useBean id="consultaEntradaLEBeanId" scope="session" class="beans.ConsultaEntradasLE"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<%

long l_centro=0,l_norden=0;
String garcq="",consulta_sol="";
boolean lb_solicitud=true,lb_renuncia=false,lb_outofdate=false,lb_botonera_desactivada=false,lb_ishistory=false;
boolean lb_acceso=false;

if (beanParamListaSolic.size()>1){ // siempre va a haber en el bean al menos un elemento el STRSIT.
 if (((Long)beanParamListaSolic.get("NNOLESP")).longValue()==0){
  if (accesoBeanId.accesoA("emision_SOL")){
    lb_acceso=true;
    //System.out.println("Tengo permiso");
      l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();
      garcq=((String)beanParamListaSolic.get("GAR_CQ")).toString();
      if (garcq.equals("Q")){
         l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
         consulta_sol = "CENTRO="+l_centro+" AND NORDEN="+l_norden+" AND GAR_ESTADO <> 3 AND GAR_ESTADO <> 4 ";
      }else{
         l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
         consulta_sol = "CEGA="+l_centro+" AND NCITA="+l_norden+" AND GAR_ESTADO <> 3 AND GAR_ESTADO <> 4 ";
      }
      EntradaLE eLE=new EntradaLE();
//    if (consultaEntradaLEBeanId.existeEntrada(l_centro,l_norden)!=null){
      if (consultaEntradaLEBeanId.resultado()!=null){
         eLE = consultaEntradaLEBeanId.existeEntrada(l_centro,l_norden,garcq);
         if (eLE.isHistory()){
           lb_ishistory=true;
         }else{
           if (!eLE.isOutOfDate()){
             lb_outofdate=false;
           }else{
             lb_outofdate=true;
           }
           if (((String)beanParamListaSolic.get("STRSIT")).equals("SSG")){
               lb_outofdate=true;    
           }
           lb_solicitud = false;
           Garantia gar = new Garantia();
           Vector vg = new Vector();
           vg = gar.busquedaGaran(consulta_sol,"");
           if (vg.size() == 0){
               lb_solicitud = true;
           }
         }
      }
  }else{
        System.out.println("No Tengo permiso");
  }
 }else{
      lb_botonera_desactivada=true;
 }
}else{
  lb_botonera_desactivada=true;
}

%>
<html>
<head>
<title>Untitled Document</title>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">
    function NuevaSolicitud(){
      if (<%=lb_acceso%>==true){
       if (<%=lb_ishistory%>==false){
         if (<%=lb_outofdate%>==false){
           alert("El registro est� dentro de fecha o no tiene garantia, no se puede a�adir nueva solicitud.");
         }else{
           if (<%=lb_solicitud%> == true) {
             parent.centro.location.href="ficha_nueva_solicitud.jsp";
             parent.botonera.location.href= "ficha_nueva_solicitud_botonera.jsp";
           }else {
             alert("Ya existe una solicitud, no se puede a�adir ninguna nueva.");
           }
         }
        }else{
          alert("Registro historico, no se puede a�adir nueva solicitud");
        }
      }else{
        alert("No tiene permiso para realizar la accion");
      }
    }

</script>
</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
  if (lb_botonera_desactivada){
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="40%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" >Nueva Solicitud</td>
    <td  width="40%" class="botonAccion">&nbsp;</td>
  </tr>
</table>
<%
  }else{
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="40%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="NuevaSolicitud();">Nueva Solicitud</td>
    <td  width="40%" class="botonAccion">&nbsp;</td>
  </tr>
</table>
<%
}
%>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
