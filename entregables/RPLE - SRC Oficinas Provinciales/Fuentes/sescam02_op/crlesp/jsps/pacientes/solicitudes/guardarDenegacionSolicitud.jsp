<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.db.*" %>
<%@ page import="conf.Constantes"%>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<jsp:useBean id="beanParamListaSolic" class="java.util.HashMap" scope="session"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<%

                //System.out.println("Entro en guardarSolicitud");

                /* Variables */
                long l_centro,l_norden=0;
                String s_garcq;
                Vector v = new Vector();
                Garantia gar = new Garantia();
                SesGarantia sG = new SesGarantia();
                boolean imprimir = false;

                /* Recojida de parametros */
                s_garcq = new String((String)beanParamListaSolic.get("GAR_CQ").toString());
                l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();

                /* Diferenciacion de registro de LESP o LESPCEX */
                if (s_garcq.equals("Q")) {
                  l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
                } else {
                  l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
                }

                String str_nombre = request.getParameter("nombre");
                String str_dni = request.getParameter("dni");
                String str_letra = request.getParameter("letradni");
                String str_nif = str_dni+str_letra;
                String str_motivo = request.getParameter("motivo");
                long l_garpk = new Long(request.getParameter("garpk")).longValue();
                /* Fin Recojida de Parametros */

                /* Insercion de la denegacion */
                try{
                   v=gar.busquedaGaran("GAR_PK ="+l_garpk,"");
                   if (v.size() > 0) {
                     sG = (SesGarantia)v.elementAt(0);
                     sG.setGarEstado(3); // Estado 3: Denegada.
                     sG.setGarMotivoRech(str_motivo);

                     /*Modificacion de la garantia*/
                     try{
                      gar.SetNegEvento(accesoBeanId.getNegEventoTramInstance(17),69,"DOC03",l_centro,l_norden);
                      gar.modificarGaran(sG,3,accesoBeanId.getUsuario(),str_nombre,str_nif);
                     }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en guardarDenegacionSolicitud.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                     }
                     imprimir = true;
                   }
                }catch(Exception e){
                  System.out.println(" Error " +e+ "al generar la solicitud");
                  imprimir=false;
                }
%>
<html>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript">
    var doc="DOC03";
    function ImprimirDoc(){
      //llamado='../imprime_doc.jsp?centro=<%=l_centro%>&norden=<%=l_norden%>&cq=<%=s_garcq%>&documento='+doc+'&usuario=<%=accesoBeanId.getUsuario()%>';
      llamado='../imprime_doc.jsp?centro=<%=l_centro%>&norden=<%=l_norden%>&cq=<%=s_garcq%>&documento='+doc+'&garpk=<%=sG.getGarPk()%>&operacion=3&usuario=<%=accesoBeanId.getUsuario()%>';
      window.showModalDialog(llamado,"","dialogWidth:80em;dialogHeight:50em");
    }
</script>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
    /* Si la insercion ha sido correcta imprimir documento */
    if (imprimir){
%>
                <script language="javascript">
                        ImprimirDoc();
                        parent.centro.location.href = "lista_solicitudes_main.jsp"
                        parent.botonera.location.href = "lista_solicitudes_down.jsp"
                </script>
<%
    }
%>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>