<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.Oficina,isf.negocio.Usuarios,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanOficina" class="java.util.HashMap" scope="session"/>

</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */
		String str_nombre = request.getParameter("nombre");
		String str_direccion = request.getParameter("direccion");
                long l_telefono=0;
                long l_codpostal=0;
                if ((request.getParameter("telefono")!=null) && (!request.getParameter("telefono").equals(""))){
                       l_telefono = new Long(request.getParameter("telefono")).longValue();
                }
                if ((request.getParameter("codigopostal")!=null) && (!request.getParameter("codigopostal").equals(""))){
                       l_codpostal = new Long(request.getParameter("codigopostal")).longValue();
                }
                long l_loc = new Long(request.getParameter("localidades")).longValue();
                String s_login = request.getParameter("responsable");
		Oficina ofi = new Oficina();
		SesOficina sO = (SesOficina)beanOficina.get("OFICINA");
		//System.out.print(beanPermisos);

		/*Realizo los set */
		sO.setOficinaNombre(str_nombre);
                sO.setOficinaDireccion(str_direccion);
                sO.setOficinaTelefono(l_telefono);
                sO.setOficinaCodpostal(l_codpostal);
                sO.setPkLocalidad(l_loc);
                sO.setSyslogin(s_login);

		/*modifico el objeto*/
                try{
                ofi.SetNegEvento(accesoBeanId.getNegEventoInstance(12));
		ofi.modificarOfi(sO);
                }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en modificarUsuario.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                  }
                // TENGO QUE GUARDAR LA OFICINA EN LA TABLA USUARIOS SINO
                // SE PRODUCEN INCONSISTENCIAS
                Usuarios usu = new Usuarios();
                Vector v = new Vector();
                v = usu.busquedaUsu("syslogin ='" +s_login +"'","");
                if ((v!=null) && (v.size() == 1)){
                  SysUsu sysusu=(SysUsu)v.elementAt(0);
                  sysusu.setOficinaPk(sO.getOficinaPk());
                  usu.modificarUsu(sysusu,"");
                }

		beanOficina.clear();
		%>

		<script language="javascript">
			parent.centro.location.href = "mant_ofi_main.htm"
			parent.botonera.location.href = "mant_ofi_down.htm"
		</script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>