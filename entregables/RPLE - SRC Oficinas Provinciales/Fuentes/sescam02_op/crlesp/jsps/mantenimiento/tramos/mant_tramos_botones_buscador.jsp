<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">
  function isDigit(num) {
          if ((num==0) || (num==1) ||(num==2) ||(num==3) ||(num==4) ||(num==5) ||(num==6) ||(num==7) ||(num==8) ||(num==9))
                  return true
          return false
  }

  function esEntero(numero) {
          var c = numero.charAt(0)
          if ((!isDigit(c) && (c != "-"))) {
                  return false
          } else {
                  if ((c=="-") && (numero.length==1))
                          return false
                  else {
                          for (var i=1; i<numero.length; i++) {
                                  c = numero.charAt(i)
                                  if (!isDigit(c)) {
                                          return false
                                  }
                          }
                          return true
                  }
          }
  }

  function modificar(){

          var codigo = parent.centro.buscador.document.formulario.codigo.value;
          var maxdias = parent.centro.buscador.document.formulario.maxDias.value;

          if ((codigo == 0) || !esEntero(maxdias) || (parseInt(maxdias) < 0)) {
            alert ("Debe seleccionar un c�digo e introducir una duraci�n v�lida.");
          } else {
            parent.centro.buscador.document.formulario.action = "mant_tramos_modificar_datos_tramos.jsp";
            parent.centro.buscador.document.formulario.target ="centro";
            parent.centro.buscador.document.formulario.submit();
            parent.botonera.location.href = "mant_tramos_botones_modificar.jsp";
          }
  }

</script>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="30%" class="botonAccion">&nbsp;</td>
    <td  width="40%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="modificar();">Modificar</td>
    <td  width="30%" class="botonAccion">&nbsp;</td>
  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
