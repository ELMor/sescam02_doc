<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.FicheroClave" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>

   <link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
   <script language="javascript" type="text/javascript" src="../../../javascript/FormUtil.js"></script>
   <script language="javascript" type="text/javascript" src="../../../javascript/SelecUtil.js"></script>
</head>
<script language="javascript">
function control_teclado(){
    if (((event.keyCode > 47) && (event.keyCode < 58)) || ((event.keyCode >64) && (event.keyCode <91)) || ((event.keyCode >96) && (event.keyCode <123)) || (event.keyCode==45) || (event.keyCode==95) || (event.keyCode==209) || (event.keyCode==241) ) event.returnValue=true;
         else event.returnValue=false;
  }
</script>
<BODY class="mainFondo">
<%
 int longitud;
 FicheroClave fc = new FicheroClave();
 longitud = fc.DameLongitudClave();
%>
<TABLE width="100%" class="textoTop">
  <tr>
  	 <td bgcolor=#5C5CAD> A�adir Usuario</td>
  </tr>
</table>
<form name="formulario" action="guardarUsuario.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
    <td class= "texto" width="15%">Nombre: </td>
    <td class= "normal" width="20%"><input type="text" maxlength=20 name="nombre" class="cajatexto2" value="" ></td>
    <td class= "texto"width="5%">&nbsp;</td>
    <td class= "normal"width="15%">&nbsp;</td>
    <td class= "texto"width="15%">&nbsp;</td>
    <td class= "texto"width="30%">&nbsp;</td>
  </tr>
  <tr>
    <td class= "texto">Apellido1: </td>
    <td class= "normal"><input type="text" maxlength=20 name="apellido1" class="cajatexto2" value="" ></td>
    <td class= "normal">&nbsp;</td>
    <td class= "texto">Apellido2: </td>
    <td class= "normal"><input type="text" maxlength=20 name="apellido2" class="cajatexto2" value="" ></td>
  </tr>
  <tr>
    <td class= "texto">Perfil: </td>
    <td class= "normal"><div id="divPerfiles">
	   <Select name="perfiles" class="cajatexto2">
<%
	   Perfiles per = new Perfiles();
	   Vector v = new Vector();
	   v = per.busquedaPerfil("","SYS_ROL_NOMBRE ASC");
	   for(int int_pos = 0;int_pos < v.size(); int_pos++){
		SysRoles sysRoles=(SysRoles)v.elementAt(int_pos);
%>
		   <option default value='<%= sysRoles.getSysRolPk()%>'><%= sysRoles.getSysRolNombre()%></option>
<%
 	   }
%>
	   </Select></div>
    </td>
     <td class= "normal">&nbsp;</td>
     <td class= "texto">Estado: </td>
     <td class= "texto"> Alta </td>
  </tr>
  <tr>
    <td class= "texto">Oficina: </td>
    <td class= "normal"><div id="divoficina">
           <Select name="oficina" class="cajatexto2">
<%
           Oficina ofic = new Oficina();
           Vector v1 = new Vector();
           v1 = ofic.busquedaOfi("","OFICINA_NOMBRE ASC");
           for(int int_pos = 0;int_pos < v1.size(); int_pos++){
                SesOficina sysofic=(SesOficina)v1.elementAt(int_pos);
%>
                   <option default value='<%= sysofic.getOficinaPk()%>'><%= sysofic.getOficinaNombre()%></option>
<%
            }
%>
           </Select></div>
    </td>
     <td class= "normal">&nbsp;</td>
     <td class= "texto">Cargo: </td>
    <td class= "normal"><div id="divcargo">
           <Select name="cargo" class="cajatexto2">
<%
           Cargos car = new Cargos();
           Vector v2 = new Vector();
           v2 = car.busquedaCargo("","SYS_CARGOS_NOMBRE ASC");
           for(int int_pos = 0;int_pos < v2.size(); int_pos++){
                SysCargos syscar=(SysCargos)v2.elementAt(int_pos);
%>
                   <option default value='<%= syscar.getSysCargosPk()%>'><%= syscar.getSysCargosNombre()%></option>
<%
           }
%>
           </Select></div>
    </td>
  </tr>

</table>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
 <tr>
    <td class= "texto" width="15%">Login: </td>
    <td class= "normal" width="20%"><input type="text" maxlength="<%=longitud%>" name="login" class="cajatexto2" value="" ></td>
    <td class= "normal"width="5%">&nbsp;</td>
    <td class= "texto" width="15%">Clave: </td>
    <td class= "normal" width="15%"><input type="password"  maxlength="<%=longitud%>" name="password" class="cajatexto2" value="" onkeypress="control_teclado();"></td>
    <td class= "texto"width="30%">&nbsp;</td>

 </tr>
 <tr>
    <td class= "texto">&nbsp;</td>
    <td class= "normal">&nbsp;</td>
    <td class= "texto">&nbsp;</td>
    <td class= "texto" >Repetir Clave: </td>
    <td class= "normal"><input type="password"  maxlength="<%=longitud%>" name="repetida" class="cajatexto2" value="" onkeypress="control_teclado();"></td>
    <td class= "normal">&nbsp;</td>
  </tr>
</table>
<input type="hidden" name="accion" value="a">
<br>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
