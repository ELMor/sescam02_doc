<%@ page contentType="text/html;charset=ISO-8859-15"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="beans.ConsultaEventosGarantia,isf.negocio.*,isf.persistencia.*,java.util.*,conf.*,isf.util.Utilidades" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language = "javascript">

function poner_foco(marco){
  if (marco == "marco1") {
    document.all.marco1.focus();
  }else if (marco == "marco2") {
       document.all.marco2.focus();
   }
}
</script>
</head>

<%
    /* Variables */
    String foco=""; //variable para controlar el foco en la visualizacion
    String marco = "";
    String nombre_oficina,estado,letra="",dni="";
    EventosGarantia evtogar;

    /* Recogida de parametros */
    long evto_pk = new Long(request.getParameter("evto_pk")).longValue();


    /* Busqueda de datos de la Solicitud */
    ConsultaEventosGarantia ceg =new ConsultaEventosGarantia();
    long garoperacion;
    ceg.setEvtoPk(evto_pk);

    // Comento, esto se hace para colocar el foco sobre el FIELDSET QUE CORRESPONDA
    // Pero no funciona de momento (Jos� Vicente Nieto)

//    garoperacion =ceg.DameOperacion();

//    if (garoperacion ==1) {
//       foco="";
//    }else{
//       marco = "";
//       if ((garoperacion == 2) || (garoperacion == 3)) {
//          marco = "marco1";
//       } else {
//         marco = "marco2";
//       }
//       foco="this.document.all."+marco+".focus();";
//    }
    Vector vp = new Vector();
    vp=ceg.resultado();
    if (vp!=null){

      Enumeration e = vp.elements();
      evtogar = new EventosGarantia();
%>
      <BODY class="mainFondoInterno" onLoad="<%=foco%>" >
      <form name="solicitud">

<%
      while(e.hasMoreElements()) {
        evtogar=(EventosGarantia)e.nextElement();
      //  if (evtogar.evtoTipoEvto > 14) {
          if ((evtogar.evtodni!=null) && (evtogar.evtodni!="")){
            letra=evtogar.evtodni.substring(evtogar.evtodni.length()-1);
            dni=evtogar.evtodni.substring(0,evtogar.evtodni.length()-1);
          }
          estado = Constantes.EST_TEXTO[new Long(evtogar.evtoGarOperacion).intValue() -1];
          nombre_oficina=OficinaHelper.DameOficina(evtogar.evtoOficinaPk);

          if (evtogar.evtoGarOperacion == 1) {
%>

         <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
         <tr>
          <td>
          <FIELDSET >
            <LEGEND><a class="texto">DATOS SOLICITUD</a></LEGEND>

<%
       }else if (evtogar.evtoGarOperacion ==2){
%>
       <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
        <tr>
         <td>
          <FIELDSET id="marco1">
          <LEGEND><a class="texto">DATOS APROBACION</a></LEGEND>
<%
       }else{
          if (evtogar.evtoGarOperacion==3){
%>

       <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
        <tr>
         <td>
           <FIELDSET id="marco1">
           <LEGEND><a class="texto">DATOS DENEGACION</a></LEGEND>
<%
         }else if (evtogar.evtoGarOperacion==4){

%>
       <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
        <tr>
          <td>
             <FIELDSET id="marco2">
             <LEGEND><a class="texto">DATOS RENUNCIA</a></LEGEND>
<%
         }
       }
%>
       <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
         <tr>
          <td width="80%">
            <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="c1enter">
            <tr>
             <td class= "texto">Oficina:</td>
             <td class= "texto"><input type="text" name="oficina_a" class="cajatexto2" value="<%=nombre_oficina%>" readonly></td>
             <td class= "texto">Fecha:</td>
             <td class= "texto"><input type="text" name="fecha_a" class="cajatexto2" value="<%=evtogar.evtoFecha%>" readonly ></td>
            </tr>
            <tr>
             <td class= "texto">Responsable Legal:</td>
             <td class= "texto" COLSPAN=3><input type="text" name="nombre_a" class="cajatexto3" value="<%=evtogar.evtoapenom%>" readonly></td>
            </tr>
            <tr>
             <td class= "texto">N.I.F:</td>
             <td class= "texto"><input maxlength=9 type="text" name="dni" style="width:35%" class="cajatexto2"  value="<%=dni%>">&nbsp;
             - <input maxlength=1 type="text" name="letradni" style="width:8%" class="cajatexto2" value="<%=letra%>" readonly></td>
             <td class= "texto">Estado:</td>
             <td class= "texto"><input type="text" name="estado_a" class="cajatexto2" value="<%=estado%>" readonly></td>
            </tr>
  <%
      if (evtogar.evtoGarOperacion ==1){

  %>
        <tr>
         <td class= "texto">N.S.S.:</td>
         <td class= "texto"><input type="text" name="nss" class="cajatexto2" value="<%=evtogar.evtonss%>" readonly></td>
        </tr>
<%
      } else if (evtogar.evtoGarOperacion ==2){
%>
            <tr>
             <td class= "texto">Precio:</td>
             <td class= "texto"><input type="text" name="monto_a" class="cajatexto4" style="width:30%" value="<%=evtogar.evtoMonto%>" readonly>&nbsp;<img align="middle" src="../../../imagenes/euro.gif"></td>
            </tr>
<%
            }else{
               if (evtogar.evtoGarOperacion == 3){
%>
                 <tr>
                  <td class= "texto">Motivo de Rechazo:</td>
                  <td class= "texto" COLSPAN=3><textarea name="rechazo_a" maxlength=10000 class="textarea" rows="2" ><%=evtogar.evtoMotivo%></textarea></td>
                 </tr>
<%
               }
            }
  %>
            </table>
           </td>
         </tr>
        </table>
       </FIELDSET>
      </td>
      <td>&nbsp;</td>
     </tr>
    </table>
<%
//   }
  } //for
%>
</form>
<script language = "javascript">
//  poner_foco('<%=marco%>')
</script>
</BODY>

<%
}
%>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
