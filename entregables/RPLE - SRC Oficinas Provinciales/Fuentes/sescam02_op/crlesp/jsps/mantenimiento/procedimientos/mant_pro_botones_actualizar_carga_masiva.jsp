<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanBusquedaProcedimiento" class="java.util.HashMap" scope="session"/>

<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">
        function cancelar(){
            alert("No se actualizaran los procedimientos");
            parent.location.href = "mant_pro_index_carga_masiva.htm"
        }

        function actualizar(){
          if (parent.centro.datos.document.seleccion.nreg.value>0){
            resp = confirm("Esta seguro que desea actualizar los procedimientos seleccionados");
            if (resp == true ) {
              if ((parent.centro.carga_masiva.document.actualizar.act_monto.checked == true) || (parent.centro.carga_masiva.document.actualizar.act_garantia.checked == true) || (parent.centro.carga_masiva.document.actualizar.act_estado.checked == true)) {
                parent.centro.carga_masiva.document.actualizar.action = "ActualizarProcedimientos.jsp";
                parent.centro.carga_masiva.document.actualizar.submit();
              }else {
                alert("No se ha activado ninguna opcion de actualizacion.");
              }
            }
          }else{
            alert("No existen registros a actualizar.Repita la busqueda.");
          }
        }

        function buscar(){
            parent.centro.carga_masiva.document.actualizar.no_buscar.value = "1";
            parent.centro.carga_masiva.document.actualizar.submit();
        }

</script>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">


<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="actualizar();">Actualizar </td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="buscar();">Buscar </td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="20%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="cancelar();">Cancelar </td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
