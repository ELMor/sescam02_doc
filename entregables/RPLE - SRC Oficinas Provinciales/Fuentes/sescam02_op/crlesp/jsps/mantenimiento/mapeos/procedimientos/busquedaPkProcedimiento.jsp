<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.TrataClave,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../../css/estilo.css">
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
      <%
      /* Recojo los parametros */
      long proc_pk = 0;
      try {
        proc_pk = new Long(request.getParameter("procpk")).longValue();
      }catch (Exception e) {
      }
      if (proc_pk  > 0) {
        String cod_proc = "S/D";
        String desc_proc = "S/D";
        String tipo = "SD";
        Procedimientos proc = new Procedimientos();
        Vector v = new Vector();
        v = proc.busquedaProced("PROC_PK="+proc_pk,"");
        if (v.size()>0){
          SesProcedimientos sP = (SesProcedimientos)v.elementAt(0);
          cod_proc = sP.getProcCod();
          desc_proc = sP.getProcDesc();
          ProcTipos pT= new ProcTipos();
          Vector vT = new Vector();
          vT = pT.busquedaProcTipo("PROC_TIPO="+sP.getProcTipo(),"");
          if (vT.size()>0){
            SesProcTipos sPT=(SesProcTipos)vT.elementAt(0);
            tipo = sPT.getProcTcod();
          }
        }
%>

        <script language = "javascript">
          parent.centro.document.formulario.descprocses.value = "<%=desc_proc%>";
          parent.centro.document.formulario.codprocses.value = "<%=tipo%> / <%=cod_proc%>";

        </script>
      <%
      }
      %>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</>
