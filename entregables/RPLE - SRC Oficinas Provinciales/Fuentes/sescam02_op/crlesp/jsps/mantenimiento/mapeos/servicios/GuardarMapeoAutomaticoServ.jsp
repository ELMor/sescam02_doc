<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

   <%
   int i,i_filas,i_filas_modificadas,i_modificadas = 0;
   String campo_codigo;
   String campo_centro;
   String campo_sescam;
   String codserv = "";
   String codsescam = "";
   double centro=0;
   long l_activo = 0;
   i_filas = new Integer(request.getParameter("filas")).intValue();
   i_filas_modificadas = new Integer(request.getParameter("filas_modificadas")).intValue();

   Vector v = new Vector();
   for (i=0;i<i_filas;i++) {
       if (i_modificadas < i_filas_modificadas) {
         campo_codigo = "codigo_"+i;
         campo_centro = "centro_"+i;
         campo_sescam = "sescam_"+i;
         if (request.getParameter(campo_codigo) != null) {
           codserv = request.getParameter(campo_codigo).trim();
           codsescam = request.getParameter(campo_sescam).trim();
           centro = new Double(request.getParameter(campo_centro)).doubleValue();
           i_modificadas = i_modificadas + 1;
           MapeoServicio mapserv = new MapeoServicio();
           SesSrvMap sSM = new SesSrvMap();
           /* realizo los sets */
           sSM.setCentro(centro);
           sSM.setSessrvCod(codsescam);
           sSM.setSrvCenCod(codserv);
           /*inserto el objeto */
           try{
             mapserv.SetNegEvento(accesoBeanId.getNegEventoInstance(29));
             mapserv.insertarMapServ(sSM);
           }catch(ExceptionSESCAM eses){
             System.out.println("Excepcion Sescam capturada en guardarMapeoAutomaticoServ.jsp");
%>
              <script language="javascript">
               alert("<%=eses%>");
              </script>
<%
           }
         }
       }else{
         break;
       }
  }
%>
  <script language="">
      parent.parent.document.location.href = "mant_map_index.htm";
  </script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>