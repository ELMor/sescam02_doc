<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Procedimiento Duplicado</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../../javascript/menus.js"></script>
</head>
<script language="javascript">

  function save_pk(procpk){
        var retorno=procpk
        returnValue=retorno;
        top.close();
  }

  var FilaSelected=-1;
  function seleccionaFila(filas, id){
      if (FilaSelected!=-1){
          if((FilaSelected%2)==0)
            filas[FilaSelected].className = "filaPar";
          else
            filas[FilaSelected].className = "filaImpar";
         }
      filas[id].className = "filaSelecionada";
      FilaSelected=id;
  }

</script>
</script>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<form name="formulario">
<table width ="100%" class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="0">
       <tr>
          <td width="100%" height="12px">Seleccione un procedimiento.</td>
       </tr>
</table>
<table width ="100%" class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="0">
       <tr class="filaCabecera">
            <td width="10%" height="12px" class="menuOpcion1">
                <a href="#" class="menuOpcion1">Tipo</a>
            </td>
            <td width="20%" height="12px" class="menuOpcion1">
                <a href="#" class="menuOpcion1">Codigo</a>
            </td>
            <td width="70%" height="12px" class="menuOpcion1">
                <a href="#" class="menuOpcion1">Descripcion </a>
            </td>
        </tr>

</table>
<%
   boolean bFilaPar=true;
   int idCounter=0;
   String codigo_procedimiento="";
   Procedimientos proc = new Procedimientos();
   Vector v = new Vector ();
   SesProcedimientos sP;
   if (request.getParameter("codproc")!=null){
     codigo_procedimiento=request.getParameter("codproc");
   }
%>
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
        <table class="centroTabla" width="100%" id="tablaProc" cellspacing="0" cellpadding="0" border="0">
        <%
          v = proc.busquedaProced("trim(PROC_COD) like '"+codigo_procedimiento+"'","");
          if (v.size()>0){
            for(int int_pos = 0;int_pos < v.size(); int_pos++){
              sP=(SesProcedimientos)v.elementAt(int_pos);
              long procpk = sP.getProcPk();
              if (bFilaPar){
                bFilaPar=false;
        %>
                <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaProc'].rows, this.id);save_pk(<%=procpk%>);" style="cursor:hand;">
        <%
              }else{
                bFilaPar=true;
        %>
                <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaProc'].rows, this.id);save_pk(<%=procpk%>);" style="cursor:hand;">
        <%
              }
              Vector vt = new Vector();
              ProcTipos pT = new ProcTipos();
              vt = pT.busquedaProcTipo("PROC_TIPO="+sP.getProcTipo(),"");
              SesProcTipos sPT = (SesProcTipos) vt.elementAt(0);
              String codtipo = sPT.getProcTcod();
        %>
              <td width="10%" height="12px"><%=codtipo%></td>
              <td width="20%" height="12px"><%=sP.getProcCod()%></td>
              <td width="70%" height="12px"><%=sP.getProcDescCorta()%></td>

              </tr>
<%
              idCounter++;
           }
         }else {
             %>
             <Script language="javaScript">
              alert("No existen Procedimientos Sescam con codigo="+<%=codigo_procedimiento%>+".Consulte al adminitrador.");
             </Script>
             <%
         }
%>
     </table>
    </td>
   </tr>
   </table>
  </form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>