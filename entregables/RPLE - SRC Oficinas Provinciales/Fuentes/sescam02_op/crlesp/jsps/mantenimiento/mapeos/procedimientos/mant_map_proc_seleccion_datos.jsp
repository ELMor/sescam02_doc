<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="beans.ConsultaMapeosProc,isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(codproc,descproc,centro,tipo){
  parent.parent.centro.location='mant_map_proc_modificar_datos.jsp?codproc='+codproc+'&descproc='+descproc+'&centro='+centro+'&tipo='+tipo;
  parent.parent.botonera.location.href = "mant_map_proc_botones_modificar.jsp"

}

</script>

</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
  <%
  boolean bFilaPar=true;
  int idCounter=0;
  long param_centro = new Double(request.getParameter("valor_centro")).longValue();
  if (param_centro >= 0) {
    Vector v = new Vector();
    String cod_sescam="";
    String cod_mapeo = "";
    String descrip = "";
    String descripmapeo = "";
    String tipo_sescam = "";
    String tipo_mapeo="";
    String orden = "";
    cod_sescam = request.getParameter("codigosescam");
    cod_mapeo = request.getParameter("codigo");
    descrip = request.getParameter("descrip");
    descripmapeo = request.getParameter("descripmapeo");
    tipo_sescam =request.getParameter("valor_tipo").toUpperCase();
    tipo_mapeo = request.getParameter("tipomap").toUpperCase();
    orden = request.getParameter("orden");
    ConsultaMapeosProc consulta = new ConsultaMapeosProc();
    consulta.setCentro(param_centro);
    consulta.setCodigoMapeo(cod_mapeo);
    consulta.setDescripmapeo(descripmapeo);
    consulta.setCodigoSescam(cod_sescam);
    consulta.setDescripSescam(descrip);
    consulta.setCodtipoSescam(tipo_sescam);
    consulta.setCodtipoMapeo(tipo_mapeo);
    consulta.setOrden(orden);
    v = consulta.MapeosProcedimientos();
    String desc_centro="";
    if (v!=null){
          for(int int_pos = 0;int_pos < v.size(); int_pos++){
                  ProcedimientosMapeados proc=(ProcedimientosMapeados)v.elementAt(int_pos);
                  Centros cen = new Centros();
                  Vector vc = new Vector();
                  vc = cen.busquedaCentro("CODCENTRO="+proc.centro,"");
                  if (vc.size()>0){
                    Centrosescam cs = (Centrosescam) vc.elementAt(0);
                    desc_centro = cs.getDescentro();
                  }
                  if (bFilaPar){
                                   bFilaPar=false;
%>
                          <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec('<%=proc.CodProcMapeado%>','<%=proc.ProcDescMapeado%>',<%=proc.centro%>,'<%=proc.CodTipoMapeado%>')" style="cursor:hand;">
<%
                  }else{
                                  bFilaPar=true;
%>
                                  <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec('<%=proc.CodProcMapeado%>','<%=proc.ProcDescMapeado%>',<%=proc.centro%>,'<%=proc.CodTipoMapeado%>')" style="cursor:hand;">
<%
                  }
%>
                  <td width="10%" height="12px"><%=proc.CodTipoSescam%>/<%=proc.CodProcSESCAM%></td>
                  <td width="38%" height="12px"><%=proc.ProcDesc%></td>
                  <td width="12%" height="12px"><a title="<%=desc_centro%>"><%=proc.centro%></a></td>
                  <td width="10%" height="12px"><%=proc.CodTipoMapeado%>/<%=proc.CodProcMapeado%></td>
                  <td width="30%" height="12px"><%=proc.ProcDescMapeado%></td>

                     </tr>
<%
                    idCounter++;
            }
  }
}
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>