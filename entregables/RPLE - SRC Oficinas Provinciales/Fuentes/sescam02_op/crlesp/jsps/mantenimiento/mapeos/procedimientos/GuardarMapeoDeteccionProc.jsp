<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanProcNoSescam" class="java.util.HashMap" scope="session"/>
</head>
<script language="javascript">
function ventana_seleccion(centro,tipo,codproc,codsescam){
  var retorno=0;
  llamado="seleccion_dupliccodproc.jsp?codproc="+codsescam;
  retorno=window.showModalDialog(llamado,"","dialogWidth:20em;dialogHeight:10em;status=no");
  document.formulario.procpk.value=retorno;
  document.formulario.centro.value=centro;
  document.formulario.codproc.value=codproc;
  document.formulario.tipo.value=tipo;
  document.formulario.submit();

  }
</script>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

<form name="formulario" action="GuardarMapeoDeteccionProc2.jsp" target="oculto1">
<input type="hidden" name="procpk" id="procpk" value="">
<input type="hidden" name="centro" id="centro" value="">
<input type="hidden" name="codproc" id="codproc" value="">
<input type="hidden" name="tipo" id="tipo" value="">
</form>

<form name="deteccion">
<%
   int i,i_filas,i_filas_modificadas,i_modificadas = 0;
   String campo_codigo;
   String campo_codigo_sescam;
   String campo_centro;
   String campo_pk;
   String campo_tipo;
   String codproc = "";
   String codigo_sescam ="";
   String tipo = "";
   long centro=0;
   long procpk = 0;
   long modificado = 0;
   i_filas = new Integer(request.getParameter("filas")).intValue();
   i_filas_modificadas = new Integer(request.getParameter("filas_modificadas")).intValue();

   beanProcNoSescam.clear();
   Vector v = new Vector();
   String[] codigos=null;
   int cont=0;
   boolean condicion=false;
   for (i=0;i<i_filas;i++) {
       if (i_modificadas < i_filas_modificadas) {
         campo_codigo = "codigo_"+i;
         campo_pk = "pk_"+i;
         campo_tipo = "tipo_"+i;
         campo_centro = "centro";
         campo_codigo_sescam = "codprocses_"+i;
         if (request.getParameter(campo_codigo) != null) {
           codproc = request.getParameter(campo_codigo).trim();
           procpk =  new Long(request.getParameter(campo_pk)).longValue();
           centro = new Double(request.getParameter(campo_centro)).longValue();
           if ((request.getParameter(campo_codigo_sescam)==null)||(request.getParameter(campo_codigo_sescam).equals(""))){
%>
           <script languaje="javascript">
             alert("No se pueden guardar codigos en blanco.");
           </script>
<%
           }
           codigo_sescam =request.getParameter(campo_codigo_sescam).trim();
           tipo = request.getParameter(campo_tipo);
           i_modificadas = i_modificadas + 1;
           MapeoProc mapproc = new MapeoProc();
           SesProcMap spm = new SesProcMap();
           if (procpk==0){
           /*Comprobacion de codigos sescam validos */
             Procedimientos p = new Procedimientos();
             Vector vs = new Vector();
             vs = p.busquedaProced("trim(PROC_COD)='"+codigo_sescam+"'","");
             if (vs.size()>0){
               if (vs.size()>1){
%>
                 <script languaje="javascript">
                  ventana_seleccion(<%=centro%>,"<%=tipo%>","<%=codproc%>","<%=codigo_sescam%>");
                 </script>
<%
                 condicion=true;
               }else{
                 SesProcedimientos sP = (SesProcedimientos) vs.elementAt(0);
                 procpk = sP.getProcPk();
               }
             }else{
%>
             <script language="javascript">
                alert("Se ha detectado codigo:"+"<%=codigo_sescam%>"+" que no corresponden a ningun procedimiento SESCAM. No se guardara.");
             </script>
<%
              if (condicion){
              String objeto = new String(codigo_sescam);
              String campo = "CODPROC"+cont;
              cont=cont+1;
              beanProcNoSescam.put(campo,objeto);
              }
             }
           }
           if (procpk>0){
           /*INSERCION EN SES_PROC_MAP */
           /* realizo los sets */
             spm.setProcPk(procpk);
             spm.setCentro(centro);
             spm.setProcCenCod(codproc);
             spm.setProcCenTipo(tipo);
           /*inserto el objeto */
             try{
               mapproc.SetNegEvento(accesoBeanId.getNegEventoInstance(38));
               mapproc.insertarProcMap(spm);
             }catch(ExceptionSESCAM eses){
               System.out.println("Excepcion Sescam capturada en guardarMapeoAutomaticoProc.jsp al guardar mapeo.");
%>
               <script language="javascript">
                 alert("<%=eses%>");
               </script>
<%
             }
           }
         }
       }else{
         if (i_filas_modificadas==0){
%>
               <script language="javascript">
                 alert("No hay filas seleccionadas para el mapeo.");
               </script>
<%
         }
         break;
       }
  }
  %>
  <script language="javascript">
    parent.parent.document.location = "mant_map_proc_index.htm";
  </script>
<%

%>

</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>