<%@ page language="java" import="java.io.*,isf.negocio.NodoArbol"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<%@ page contentType="text/html;charset=ISO-8859-1"%>
<jsp:useBean id="beanNodoArbol" scope="session" class="isf.negocio.NodoArbol"/>
<html>
<head>
<script language="JavaScript" src="../../../javascript/menusEmergentes.js"></script>
<script language="JavaScript">

function llamadoAInsertarClasificacion() {
  var NodosAniadidos = new Array();
  nodeSelected=indexOfEntries[0];
  if (!nodeSelected.isOpen)
    clickOnNode(nodeSelected.id);
  llamado="ficha_insercion_clasificacion_index.jsp";
  NodosAniadidos=window.showModalDialog(llamado,"","dialogWidth:20em;dialogHeight:10em;status=no");
  if (NodosAniadidos!=null){
    insertNodes(nodeSelected.id, NodosAniadidos);
    redibujado();
  }
}

function llamadoANuevoProcedmimientos(obj_menu) {
  var NodoPadre=obj_menu;
   var NodosAniadidos = "";
   var id=0;
   id=nodo_pos(NodoPadre);
   nodeSelected=indexOfEntries[id];
   if (!nodeSelected.isOpen)
     clickOnNode(nodeSelected.id);

   NodosAniadidos = window.showModalDialog("Insertar_Procedimiento_index.jsp?pkpadre="+obj_menu," ","dialogWidth:35em;dialogHeight:15em;status=no");
   if (NodosAniadidos!=null){
      insertNodes(NodoPadre, NodosAniadidos);
      redibujado();
   }
}

function llamadoAAsociarProcedmimientos(obj_menu) {
  var NodoPadre=obj_menu;
  var NodosAniadidos = "";
  var id=0;
  id=nodo_pos(NodoPadre);
  nodeSelected=indexOfEntries[id];
  if (!nodeSelected.isOpen)
    clickOnNode(nodeSelected.id);

  NodosAniadidos = window.showModalDialog("buscador_proced_index.jsp?pkpadre="+obj_menu," ","dialogWidth:40em;dialogHeight:35em;status=yes");
  if (NodosAniadidos!=null){
     insertNodes(NodoPadre, NodosAniadidos);
     redibujado();
  }
}

function llamadoAEliminarClasificacion(obj_menu) {
 resp = confirm("Esta seguro que desea eliminar la clasificacion, se eliminaran sus hijos tambien");
 if (resp == true ) {
  parent.eliminar.location.href = "eliminarFichaClasificacion.jsp?sesclaspk="+obj_menu;
 }
}

function llamadoAInsertarSubClasificacion(obj_menu) {
 var NodoPadre=obj_menu;
 var NodosAniadidos = new Array();
 var id=0;
 id=nodo_pos(NodoPadre);
 nodeSelected=indexOfEntries[id];
 if (!nodeSelected.isOpen)
    clickOnNode(nodeSelected.id);
 llamado="ficha_insercion_sub-clasificacion_index.jsp?sesclaspk="+obj_menu;
 NodosAniadidos=window.showModalDialog(llamado,"","dialogWidth:20em;dialogHeight:10em;status=no;titlebar=no;help=no");
 if (NodosAniadidos!=null){
   insertNodes(NodoPadre, NodosAniadidos);
   redibujado();
 }
}

function llamadoAFichaProcedimiento(obj_menu) {
//alert("Llamar a ficha de procedimiento..."+obj_menu);
var NodosAniadidos = window.showModalDialog("ficha_procedimiento_index.jsp?pkpadre="+obj_menu," ","dialogWidth:35em;dialogHeight:25em;status=yes");

}

function llamadoAEliminarAsociacion(obj_menu) {
  resp = confirm("Esta seguro que desea eliminar la asociación ");
  if (resp == true ) {
   parent.eliminar.location.href = "eliminarFichaClasificacion.jsp?sesclaspk="+obj_menu;
  }
}

function BorrarNodo(obj_menu){
  eliminar_nodo(obj_menu);
}
function loadMenus() {
    /* Clasificaciones*/
    window.menuClasificaciones = new Menu("Clasificaciones");
    menuClasificaciones.addMenuItem("Insertar clasificación","javascript:llamadoAInsertarClasificacion(menuClasificaciones.id_clas);");
    menuClasificaciones.fontSize = 12;
    menuClasificaciones.fontWeight = "plain";
    menuClasificaciones.fontFamily = "Arial";
    menuClasificaciones.fontColor = "#003366	";	// color normal
    menuClasificaciones.menuItemHeight = 16;
    menuClasificaciones.menuItemIndent = 10;
    menuClasificaciones.menuItemBgColor = "#dddecc";	// color del fondo cuando esta sin elegir
    menuClasificaciones.menuHiliteBgColor = "#003366";	// color del fondo cuando esta elegido
    menuClasificaciones.fontColorHilite = "#dddecc";
    menuClasificaciones.prototypeStyles = menuClasificaciones;

    /* Sub-Clasificaciones con procedimientos asociados*/
    window.menuClasificacionProc = new Menu("ClasificacionProc");
    menuClasificacionProc.addMenuItem("Nuevo procedimientos","javascript:llamadoANuevoProcedmimientos(menuClasificacionProc.id_clas);");
    menuClasificacionProc.addMenuItem("Asociar procedimientos","javascript:llamadoAAsociarProcedmimientos(menuClasificacionProc.id_clas);");
    menuClasificacionProc.addMenuItem("Eliminar clasificación","javascript:llamadoAEliminarClasificacion(menuClasificacionProc.id_clas);");
    menuClasificacionProc.fontSize = 12;
    menuClasificacionProc.fontWeight = "plain";
    menuClasificacionProc.fontFamily = "Arial";
    menuClasificacionProc.fontColor = "#003366	";	// color normal
    menuClasificacionProc.menuItemHeight = 16;
    menuClasificacionProc.menuItemIndent = 10;
    menuClasificacionProc.menuItemBgColor = "#dddecc";	// color del fondo cuando esta sin elegir
    menuClasificacionProc.menuHiliteBgColor = "#003366";	// color del fondo cuando esta elegido
    menuClasificacionProc.fontColorHilite = "#dddecc";
    menuClasificacionProc.prototypeStyles = menuClasificacionProc;

    /* Sub-Clasificaciones pura sin procedimientos asociados*/
    window.menuClasificacion = new Menu("Clasificacion");
    menuClasificacion.addMenuItem("Insertar sub-clasificación","javascript:llamadoAInsertarSubClasificacion(menuClasificacion.id_clas);");
    menuClasificacion.addMenuItem("Eliminar clasificación","javascript:llamadoAEliminarClasificacion(menuClasificacion.id_clas);");
    menuClasificacion.fontSize = 12;
    menuClasificacion.fontWeight = "plain";
    menuClasificacion.fontFamily = "Arial";
    menuClasificacion.fontColor = "#003366	";	// color normal
    menuClasificacion.menuItemHeight = 16;
    menuClasificacion.menuItemIndent = 10;
    menuClasificacion.menuItemBgColor = "#dddecc";	// color del fondo cuando esta sin elegir
    menuClasificacion.menuHiliteBgColor = "#003366";	// color del fondo cuando esta elegido
    menuClasificacion.fontColorHilite = "#dddecc";
    menuClasificacion.prototypeStyles = menuClasificacion;

    /* Asociación de Procedimientos*/
    window.menuProcedimientos = new Menu("Procedimientos");
    menuProcedimientos.addMenuItem("Ficha procedimiento","javascript:llamadoAFichaProcedimiento(menuProcedimientos.id_clas);");
    menuProcedimientos.addMenuItem("Eliminar asociación","javascript:llamadoAEliminarAsociacion(menuProcedimientos.id_clas);");
    menuProcedimientos.fontSize = 12;
    menuProcedimientos.fontWeight = "plain";
    menuProcedimientos.fontFamily = "Arial";
    menuProcedimientos.fontColor = "#003366	";	// color normal
    menuProcedimientos.menuItemHeight = 16;
    menuProcedimientos.menuItemIndent = 10;
    menuProcedimientos.menuItemBgColor = "#dddecc";	// color del fondo cuando esta sin elegir
    menuProcedimientos.menuHiliteBgColor = "#003366";	// color del fondo cuando esta elegido
    menuProcedimientos.fontColorHilite = "#dddecc";
    menuProcedimientos.prototypeStyles = menuProcedimientos;
    menuClasificaciones.writeMenus();
}
</script>

<title>Explorando arbol: </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/arbol.js"></script>
</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<div id="arbolVisible">
</div>

<script language="javascript">
window.loadMenus();
<%beanNodoArbol.clearChilds();%>
var nodosHijos = <%=beanNodoArbol.childsToString(-1)%>;
newTree(0, '<B>Clasificaciones de procedimientos</B>', "javascript:window.showMenu(window.menuClasificaciones,100);", nodosHijos,'/crlesp');
</script>

<form name="formArbol" action="./recuperarHijos.jsp" target="arbolOculto">
	<input type="hidden" name="nodo" value="">
</form>
<iframe name="arbolOculto" align="center" frameborder="yes" border="1" height="0" width="0"></iframe>
</body>
</html>
