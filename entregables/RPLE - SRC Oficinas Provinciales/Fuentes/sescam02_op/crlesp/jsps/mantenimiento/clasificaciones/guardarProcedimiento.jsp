<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */


                String str_codigo = request.getParameter("codigo");
                String str_descrip_corta = request.getParameter("descrip");
                String str_descrip = request.getParameter("descrip_larga");
                String Nodo = new String("");
                long l_activo = 0;
                long l_tipo = 0;
                long l_pkpadre = 0;
                long l_garantia = 0;
                double d_monto = 0;
                int i_error;
                try {
                   l_activo = new Long(request.getParameter("activo")).longValue();
                   l_garantia = new Long(request.getParameter("garantia")).longValue();
                   d_monto = new Double (request.getParameter("monto")).doubleValue();
                   l_tipo = new Long(request.getParameter("valor_tipo")).longValue();
                   l_pkpadre = new Long(request.getParameter("pkpadre")).longValue();
                   i_error = 1;
                } catch (Exception ex) {
                    i_error = -1;
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.FORMATO_NUMERO_INCORRECTO);
                    %>
                  <script language="javascript">
                      alert ('<%=es.getMensaje()%>');
                  </script>
                    <%
                }
                /*Realizo los set */
                if (i_error > 0) {
                 NodoArbol np = new NodoArbol();
                 np.setId(l_pkpadre);
                 Procedimientos proc = new Procedimientos();
                 SesProcedimientos sO = new SesProcedimientos();

                 sO.setProcCod(str_codigo);
                 sO.setProcDescCorta(str_descrip_corta);
                 sO.setProcDesc(str_descrip);
                 sO.setProcActivo(l_activo);
                 sO.setProcGarantia(l_garantia);
                 sO.setProcMontoMaxEst(d_monto);
                 sO.setProcTipo(l_tipo);

                 try{
                   proc.SetNegEvento(accesoBeanId.getNegEventoInstance(1));
                   proc.insertarProced(sO);
                   ProcTipos proctipos = new ProcTipos();
                   SesProcTipos tipo= new SesProcTipos();
                   Vector v = new Vector();
                   v = proctipos.busquedaProcTipo("PROC_TIPO ="+sO.getProcTipo(),"");
                   tipo = (SesProcTipos) v.elementAt(0);
                   String codtipo = tipo.getProcTcod();
                   Clasificaciones clasif = new Clasificaciones();
                   clasif.SetNegEvento(accesoBeanId.getNegEventoInstance(35));
                   NodoArbol hijo = new NodoArbol();
                   hijo.setIEstado(1);
                   hijo.setNType(0);
                   hijo.setId(-1);
                   hijo.setDbType(3);
                   hijo.setDesc(codtipo+" "+sO.getProcCod()+" "+sO.getProcDescCorta());
                   hijo.setProc_pk(sO.getProcPk());
                   np.addChild(hijo);

                 }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en guardarProcedimiento.jsp");
                         i_error = -1;
                         Nodo = null;

%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                  }
                  if (i_error > 0) {

                   np.saveChilds();
                   Nodo = np.childsToString();
                  }
     %>
                  <script language = "javascript">
                    parent.parent.DevolverParametro(<%=Nodo%>);
                  </script>
     <%
       }
     %>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>