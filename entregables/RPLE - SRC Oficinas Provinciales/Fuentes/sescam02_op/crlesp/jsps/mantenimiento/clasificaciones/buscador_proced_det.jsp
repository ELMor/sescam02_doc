<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.NodoProcedimiento,beans.ConsultaNodosProcedimientos,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanPadre" class="java.util.HashMap" scope="session"/>

<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">
  function seleccionadas(contador) {
    var numero_filas = 0;
    parseInt(contador);
    if (this.document.all("marca_"+contador).checked == true) {
      numero_filas = this.document.seleccion.filas_modificadas.value;
      this.document.seleccion.filas_modificadas.value = parseInt(numero_filas)+1;
    }else{
      numero_filas = this.document.seleccion.filas_modificadas.value;
      this.document.seleccion.filas_modificadas.value = parseInt(numero_filas)-1;
    }
  }
</script>
</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
  <%
    String s_cod_proc = "";
    String s_desc_proc = "";
    int idCounter=0;
    boolean b_nobuscar = false;

    if (request.getParameter("codigo") != null) {
      s_cod_proc = new String(request.getParameter("codigo").toUpperCase());
    }
    if (request.getParameter("descrip") != null) {
      s_desc_proc = new String(request.getParameter("descrip").toUpperCase());
    }

    if ((s_cod_proc.equals("")) && (s_desc_proc.equals(""))) {
      b_nobuscar = true;
    }
    String marcar = "";

    if (request.getParameter("marcar_todas") != null) {
        marcar = request.getParameter("marcar_todas");
    }
  %>
<form name="seleccion2" action="buscador_proced_det.jsp" target="_self" method = "post">
   <input type="hidden" name="marcar_todas" value = "">
   <input type="hidden" name="codigo" value = "<%=s_cod_proc%>">
   <input type="hidden" name="descrip" value = "<%=s_desc_proc%>">
</form>
<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaProc" cellspacing="0" cellpadding="0" border="0">
        <%
        if (b_nobuscar == false) {
         %>
            <script language="javascript">
              parent.pie.document.pieEtado.lEstado.value="Generando resultado...";
            </script>
          <%

          boolean bFilaPar=true;

          String s_where = "";
          ConsultaNodosProcedimientos consulta = new ConsultaNodosProcedimientos();
          Vector v = new Vector();
          boolean b_pasa = false;
          consulta.setCodigo(s_cod_proc);
          consulta.setDesc(s_desc_proc);
          int pk_padre = 0;
          if (beanPadre.size() > 0) {
              try {
                Long padre = (Long) beanPadre.get("PADRE");
                pk_padre = padre.intValue();
              }catch(Exception e) {
              }
          }
          consulta.setPkPadre(pk_padre);
          v = consulta.resultado();
          if (v!=null){

            for(int int_pos = 0;int_pos < v.size(); int_pos++){
              NodoProcedimiento nodo=(NodoProcedimiento)v.elementAt(int_pos);
              long procpk = nodo.NodoProcPk;
              if (bFilaPar){
                bFilaPar=false;
	%>
                <tr class="filaPar" id=<%=idCounter%> style="cursor:hand;">
	<%
              }else{
                bFilaPar=true;

	%>
                <tr class="filaImpar" id=<%=idCounter%> style="cursor:hand;">
	<%
              }
	%>
              <td width="5%" height="12px"><input type="checkbox" id="marca_<%=idCounter%>" name="marca_<%=idCounter%>" <%=marcar%> onClick="seleccionadas(<%=idCounter%>);"></td>
              <td width="20%" height="12px"><%=nodo.NodoCodigo%></td>
              <td width="42%" height="12px"><%=nodo.NodoDesc%></td>
              <td width="13%" height="12px"><%=nodo.NodoMonto%></td>
              <td width="12%" height="12px"><%=nodo.NodoGarantia%></td>
              </tr>
              <td><input type="hidden" id="proc_<%=idCounter%>" name="proc_<%=idCounter%>"  value = <%=nodo.NodoProcPk%>></td>
              <td><input type="hidden" id="desc_<%=idCounter%>" name="desc_<%=idCounter%>"  value = "<%=nodo.NodoDesc%>"></td>
              <td><input type="hidden" id="tipo_<%=idCounter%>" name="tipo_<%=idCounter%>"  value = "<%=nodo.NodoProcTipo%>"></td>
              <td><input type="hidden" id="cod_<%=idCounter%>" name="cod_<%=idCounter%>"  value = "<%=nodo.NodoCodigo%>"></td>

<%
              idCounter++;
           }
%>
            <script language="javascript">
              parent.pie.document.pieEtado.lEstado.value="Listo";
              parent.pie.document.pieEtado.lFilas.value=<%=v.size()%>;
           </script>
           <%
         }else {
             %>
             <Script language="javaScript">
               parent.pie.document.pieEtado.lEstado.value="Ingrese condiciones de busqueda";
               parent.pie.document.pieEtado.lFilas.value="0";
             </Script>
             <%
           }
     }
%>
     </table>
    </td>
   </tr>
   </table>
   <td><input type="hidden" name="filas" value = <%=idCounter%>></td>
   <td><input type="hidden" name="marcar_todas" <%=marcar%> value = "<%=marcar%>"></td>
   <td><input type="hidden" name="filas_modificadas" value ="0"></td>

  </form>
  <form name="guardar" action="GuardarClasificacion.jsp" target="oculto1" method="post">
  <div id="datos">
  </div>
  </form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

