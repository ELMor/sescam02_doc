 -- Inicializa CLAVES DEL SISTEMA

delete from evento_tipo_var;
delete from evento_tipo;

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (1,'A�adir nuevo procedimiento',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (1,1,'Pk del nuevo procedimiento a�adido',1);
insert into evento_tipo_var (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (2,1,'C�digo del nuevo procedimiento a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (3,1,'Descripci�n corta del nuevo procedimiento a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (4,1,'Marca de activo/inactivo del nuevo procedimiento ',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (5,1,'Dias de garantia del nuevo procedimiento a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (6,1,'Monto del nuevo procedimiento a�adido',1);


insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (2,'Modificar procedimiento existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (7,2,'Pk del procedimiento modificado',1);
insert into evento_tipo_var (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (8,2,'Nuevo C�digo del procedimiento modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (9,2,'Desc. corta del procedimiento modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (10,2,'Marca de activo/inactivo del procedimiento modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (11,2,'Nuevos dias de garantia del procedimiento modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (12,2,'Nuevo Monto del procedimiento modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (3,'A�adir nuevo usuario',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (13,3,'Login del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (14,3,'Apellido Paterno del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (15,3,'Apellido Materno del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (16,3,'Nombre del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (17,3,'Oficina del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (18,3,'Cargo del nuevo usuario a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (19,3,'Perfil � Rol del nuevo usuario a�adido',1);


insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (4,'Modificar usuario existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (20,4,'Login del usuario modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (21,4,'Apellido Paterno del usuario modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (22,4,'Apellido Materno del usuario modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (23,4,'Marca de alta/baja para el usuario modificado',1);

insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (24,4,'Oficina del usuario modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (25,4,'Cargo del usuario modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (26,4,'Perfil � Rol del usuario modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (5,'A�adir Nuevo Rol',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (27,5,'Pk del nuevo rol a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (28,5,'Nombre del nuevo rol a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (29,5,'Descripci�n del nuevo rol a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (30,5,'Pk de permisos que se le asocian',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (6,'Modificar Rol existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (31,6,'Pk del rol modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (32,6,'Nombre del rol modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (33,6,'Descripci�n del rol modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (34,6,'Pk antiguos permisos que tenia asociados',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (35,6,'Pk nuevos permisos que se le asocian',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (7,'Borrar Rol existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (36,7,'Pk del rol que se va ha borrar',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (8,'A�adir nuevo Cargo',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (37,8,'Pk del nuevo cargo a�adido',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (38,8,'Nombre del nuevo cargo a�adido',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (9,'Modificar Cargo existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (39,9,'Pk del cargo modificado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (40,9,'Nombre del cargo modificado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (10,'Borrar Cargo existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (41,10,'Pk del cargo eliminado',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (11,'A�adir nueva Oficina',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (42,11,'Pk de la nueva oficina a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (43,11,'Nombre de la nueva oficina a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (44,11,'Direcci�n de la nueva oficina a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (45,11,'Telefono de la nueva oficina a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (46,11,'Codigo Postal de la nueva oficina a�adida',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (47,11,'Localidad a la que pertenece la nueva oficina',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (48,11,'Usuario responsable de la nueva oficina',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (12,'Modificar una Oficina existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (49,12,'Pk de la oficina modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (50,12,'Nuevo nombre de la oficina modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (51,12,'Nueva direcci�n de la oficina modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (52,12,'Nuevo telefono de la oficina modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (53,12,'Nuevo Codigo Postal de la oficina modificada',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (54,12,'Nueva Localidad a la que pertenece la oficina',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (55,12,'Nuevo Usuario responsable de la oficina',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (13,'Borrar Oficina existente',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (56,13,'Pk de la oficina eliminada',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (14,'Impresion de Documentos',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (57,14,'Documento a Imprimir',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (58,14,'Usuario responsable de la impresi�n',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (59,14,'Centro � Cega del registro de Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (60,14,'NOrden � NCita del registro de Lista de Espera',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (15,'Emision de Solicitudes de Garantia',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (61,15,'Documento asociado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (62,15,'Usuario responsable de la operacion',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (63,15,'Centro � Cega del registro de Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (64,15,'NOrden � NCita del registro de Lista de Espera',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (16,'Aprobaci�n de una solicitud',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (65,16,'Documento asociado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (66,16,'Usuario responsable de la operacion',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (67,16,'Centro � Cega del registro de Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (68,16,'NOrden � NCita del registro de Lista de Espera',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (17,'Denegacion de una solicitud',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (69,17,'Documento asociado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (70,17,'Usuario responsable de la operacion',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (71,17,'Centro � Cega del registro de Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (72,17,'NOrden � NCita del registro de Lista de Espera',1);

insert into evento_tipo (tpoevto_pk,tpoevto_desc,tpoevto_act) 
values (18,'Renuncia a una Solicitud',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (73,18,'Documento asociado',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (74,18,'Usuario responsable de la operacion',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (75,18,'Centro � Cega del registro de Lista de Espera',1);
insert into evento_tipo_var  (tpoevto_var_pk,tpoevto_pk,tpoevto_var_desc,tpoevto_var_act) 
values (76,18,'NOrden � NCita del registro de Lista de Espera',1);

commit work;
