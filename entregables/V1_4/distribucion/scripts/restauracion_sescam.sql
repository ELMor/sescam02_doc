-- SESCAM: Carga Historico
-- Script: load_historico_consultas.sql
-- Restauracion de las tablas lespcex y lesp
	INSERT INTO LESPCEX
		(cip,cega,ncita,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov,tel1,tel2 
		,slab,fina,gara,area,area_des,servproc,servproc_des,medipeti,medipeti_des 
		,servreal,servreal_des,agen,agen_des,sala,sala_des,cmedreal,medireal,finclusi 
		,prestaci,prestaci_des,priorida,fechacit,fechader,centrder,especder 
		,fsoldem1,fsoldem2,fsoldem3,fsoldem4,fsoldem5,tdemsol1,tdemsol2,tdemsol3,tdemsol4,tdemsol5 
		,fsalida,motisali,indinoga,observac,	finclusihos,	identpres,	tvisita,catalog )
	 SELECT cip,cega,ncita,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov,tel1,tel2 
		,slab,fina,gara,area,area_des,servproc,servproc_des,medipeti,medipeti_des 
		,servreal,servreal_des,agen,agen_des,sala,sala_des,cmedreal,medireal,finclusi 
		,prestaci,prestaci_des,priorida,fechacit,fechader,centrder,especder 
		,fsoldem1,fsoldem2,fsoldem3,fsoldem4,fsoldem5,tdemsol1,tdemsol2,tdemsol3,tdemsol4,tdemsol5 
		,fsalida,motisali,indinoga,observac,	finclusihos,	identpres,	tvisita,catalog
	 FROM LESPCEX_HISTO
	 
	 commit;
	 
	 INSERT INTO LESP 
		(norden,centro,cip,cega,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov 
		,tel1,tel2,tel3,slab,fina,gara,serv,secc,acli,cmed,apno_med,finc,cdi1,ddi1 
		,cdi2,ddi2,cpr1,dpr1,cpr2,dpr2,tcir,tane,prio,cinc,preo,frep,fcap 
		,spac,fder,dder,fsde,tdes,fqui,fsal,moti,obse,finchos,motrechazo,fecrechazo,catalog,link) 
	 SELECT norden,centro,cip,cega,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov 
		,tel1,tel2,tel3,slab,fina,gara,serv,secc,acli,cmed,apno_med,finc,cdi1,ddi1 
		,cdi2,ddi2,cpr1,dpr1,cpr2,dpr2,tcir,tane,prio,cinc,preo,frep,fcap 
		,spac,fder,dder,fsde,tdes,fqui,fsal,moti,obse,finchos,motrechazo,fecrechazo,'HISTOR',link 
	 FROM LESP_HISTO
	 
	 commit;
		