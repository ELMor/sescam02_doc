create  procedure logtolog
  (
     tipope            smallint
    ,fecope            datetime year to second
    ,numerohc          integer
    ,servreal          char(4)
    ,cod_medico        integer
    ,fecha_en_lista    date
    ,codidiag_ingreso  char(6)
    ,codiproc_ingreso  char(6)
    ,prioridad         char(1)
    ,servpeti          char(4)
    ,medipeti          integer
    ,aviso_urg         char(1)
    ,preingresado      char(1)
    ,fecha_ingreso     date
    ,observaciones     char(70)
    ,tipfinancia       smallint
    ,financia          char(6)
    ,ambito            char(1)
    ,descproc          char(60)
    ,descdiag          char(60)
    ,tipoanest         char(1)
    ,norden            integer
    ,tipolis           smallint
    ,procedede         smallint
    ,fechaqui          date
    ,numproce          integer
    ,hemoterapia       char(1)
    ,complicacion      char(1)
    ,inclusion         smallint
    ,codidiag2         char(6)
    ,descdiag2         char(60)
    ,ingresado         char(1)
    ,bajalabor         char(1)
    ,numpreop          integer
    ,fecrechazo        date
    ,tipoacti          smallint
    ,fec_prevista      date
    ,hora_prevista     char(5)
    ,codiproc2         char(6)
    ,descproc2         char(60)
    ,derivable         char(1)
    ,situacion         smallint
    ,est_preop         char(1)
    ,aptociru          char(1)
    ,fecapto           date
    ,feccaduca         date
    ,asa               char(3)
    ,localizado        char(1)
    ,fecdemora         date
    ,tpodemora         smallint
    ,fecderiva         date
    ,hospderiva        char(4)
    ,fecconfirma       date
    ,grabauser         char(20)
    ,grabadia          date
    ,grabahora         char(5)
    ,motrechazo        smallint
    ,f_baja            date
  )

insert into lesplogger
  (
     tipope
    ,fecope
    ,procesada
    ,numerohc
    ,servreal
    ,cod_medico
    ,fecha_en_lista
    ,codidiag_ingreso
    ,codiproc_ingreso
    ,prioridad
    ,servpeti
    ,medipeti
    ,aviso_urg
    ,preingresado
    ,fecha_ingreso
    ,observaciones
    ,tipfinancia
    ,financia
    ,ambito
    ,descproc
    ,descdiag
    ,tipoanest
    ,norden
    ,tipolis
    ,procedede
    ,fechaqui
    ,numproce
    ,hemoterapia
    ,complicacion
    ,inclusion
    ,codidiag2
    ,descdiag2
    ,ingresado
    ,bajalabor
    ,numpreop
    ,fecrechazo
    ,tipoacti
    ,fec_prevista
    ,hora_prevista
    ,codiproc2
    ,descproc2
    ,derivable
    ,situacion
    ,est_preop
    ,aptociru
    ,fecapto
    ,feccaduca
    ,asa
    ,localizado
    ,fecdemora
    ,tpodemora
    ,fecderiva
    ,hospderiva
    ,fecconfirma
    ,grabauser
    ,grabadia
    ,grabahora
    ,motrechazo
    ,fecha_baja
  )
 values
  (
     tipope
    ,fecope
    ,0
    ,numerohc
    ,servreal
    ,cod_medico
    ,fecha_en_lista
    ,codidiag_ingreso
    ,codiproc_ingreso
    ,prioridad
    ,servpeti
    ,medipeti
    ,aviso_urg
    ,preingresado
    ,fecha_ingreso
    ,observaciones
    ,tipfinancia
    ,financia
    ,ambito
    ,descproc
    ,descdiag
    ,tipoanest
    ,norden
    ,tipolis
    ,procedede
    ,fechaqui
    ,numproce
    ,hemoterapia
    ,complicacion
    ,inclusion
    ,codidiag2
    ,descdiag2
    ,ingresado
    ,bajalabor
    ,numpreop
    ,fecrechazo
    ,tipoacti
    ,fec_prevista
    ,hora_prevista
    ,codiproc2
    ,descproc2
    ,derivable
    ,situacion
    ,est_preop
    ,aptociru
    ,fecapto
    ,feccaduca
    ,asa
    ,localizado
    ,fecdemora
    ,tpodemora
    ,fecderiva
    ,hospderiva
    ,fecconfirma
    ,grabauser
    ,grabadia
    ,grabahora
    ,motrechazo
    ,f_baja
  );

end procedure;
