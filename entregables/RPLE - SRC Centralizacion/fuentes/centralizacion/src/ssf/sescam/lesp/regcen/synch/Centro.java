package ssf.sescam.lesp.regcen.synch;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Properties;

import ssf.sescam.lesp.regcen.db.SQLFileRunner;
import ssf.sescam.lesp.regcen.monitor.StatusManager;
import java.util.Date;
import java.text.SimpleDateFormat;
import ssf.sescam.lesp.regcen.db.patcher.Patcher;
import java.util.Hashtable;
import org.apache.log4j.Logger;
import ssf.sescam.lesp.regcen.synch.generic.Tabla;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Sistema de recopilacion de Informix HP-HIS de listas de espera</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class Centro implements Runnable {

	private int codInterno;
	private int CEGA;
	private Properties prop = null;
	boolean available = false;
	private Connection toCentro = null;
	private Connection toSescam = null;
	private StatusManager statMan = null;
	private Logger log = Logger.getLogger(getClass());

	//Consulta de Anestesia
	PreparedStatement psAnestesia;
	//Consulta de Circunstancias de Inclusion
	PreparedStatement psCinclusion;
	//Consulta de Hospital deriva
	PreparedStatement psHospderiva;

	Statement stSes = null;
	Statement stCen = null;

	//Variables usadas para mayor rapidez de consulta desde Properties.
	Date lecexpFechaInicio = null;
	String lecexpGrupocexRegcen = null;
	/**
	 * Constructor
	 * @param p Archivo de definiciones
	 * @param n Numero de centro que se quiere crear
	 * @param _statMan StatusManager para notificacion de cambios de estado
	 * @throws Exception
	 */
	public Centro(Properties p, int n, StatusManager _statMan)
		throws Exception {
		prop = p;
		codInterno = n;
		statMan = _statMan;
		//Precargamos algunos valores de Properties que se consultan mucho:
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		lecexpFechaInicio = sdf.parse(gP("lecexp.fecha.inicio"));
		lecexpGrupocexRegcen = gP("lecexp.grupocex.regcen");
	}

	/**
	 * Ejecuci�n de la hebra principal. Obtiene el codigo de centro,
	 * copia el log al SESCAM y lo procesa despu�s.
	 */
	public void run() {
		boolean purgeLoggers = true;
		if (!run0000_Available()) {
			return;
		}
		try {
			//Procesamos LEQ
			run0200_LEQ();
			//A continuaci�n se centraliza la lista de espera CEX y Pruebas
			run0300_LECEXP();
			//Finalmente las tablas que se hallan definido en internal.properties
			run0400_Tablas();
			// Actualizaci�n / depuraci�n de cips repetidos
			run0500_ActualizarCIP();
	
		} catch (Exception e) {
			//Algo fue mal
			statMan.setStatus(codInterno, -statMan.getStatus(codInterno));
			log.error(" ...Abortando!!!", e);
			String s = e.getMessage();
			if (s.indexOf("Broken pipe") >= 0) {
				statMan.setStatus(codInterno, 0x7f);
				//Codigo especial, BBDD caida
				log.error(
					"BBDD no disponible, refresco detenido hasta proximo schedule");
				purgeLoggers = false;
			}
		} finally {
			//Finalmente se borran los registros transmitidos correctamente
			if (purgeLoggers) {
				runFF00_PurgeLoggers();
			}
		}

	}

	public void run0500_ActualizarCIP() throws Exception {

		PreparedStatement pstAfectados = null;
		ResultSet rsAfectados = null;
		ResultSet rsCipActual = null;
		PreparedStatement pstUpdLESPCEX = null;
		PreparedStatement pstUpdLESP = null;
		PreparedStatement pstSltCipActual = null;
		try {
			log.info("Comenzando a actualizar CIPS incorrectos");
			//1. Detectar los pacientes afectados tanto en lesp como en lespcex

			String sql_afectados =
				" select hicl from  "
					+ " (select hicl,cip from lespcex where cega = ? "
					+ " union all select hicl,cip from lesp  where centro = ? ) "
					+ "	group by hicl having count(distinct cip) > 1 ";

			pstAfectados = sesPS(sql_afectados);
			pstAfectados.setInt(1, CEGA);
			pstAfectados.setInt(2, CEGA);
			rsAfectados = pstAfectados.executeQuery();

			pstSltCipActual =
				cenPS("select numtis from pacientes where numerohc = ?");

			pstUpdLESPCEX =
				sesPS("update lespcex set cip = ? where cega = ? and hicl = ? and cip <> ?");
			pstUpdLESP =
				sesPS("update lesp set cip = ? where centro = ? and hicl = ? and cip <> ?");

			//2. Para cada uno de los pacientes afectados.
			long n = 0; // cuenta de registros afectados
			long err = 0; // cuenta de errores

			while (rsAfectados.next()) {
				long HICL = rsAfectados.getInt(1);

				//2.2  Obtener cip actual
				pstSltCipActual.setLong(1, HICL);
				rsCipActual = pstSltCipActual.executeQuery();
				String cip =
					(rsCipActual.next()) ? rsCipActual.getString(1) : "";

				if (cip != null && !cip.equals("")) {
					cip = cip.trim();
					// realizar el trim porque de HP-HIS puede venir como sea
					//2.3 Actualizar la BD para dejar consistente
					// actualizacion de LESPCEX
					pstUpdLESPCEX.setString(1, cip);
					pstUpdLESPCEX.setLong(2, CEGA);
					pstUpdLESPCEX.setLong(3, HICL);
					pstUpdLESPCEX.setString(4, cip);
					n += pstUpdLESPCEX.executeUpdate();
					// actualizacion de LESP
					pstUpdLESP.setString(1, cip);
					pstUpdLESP.setLong(2, CEGA);
					pstUpdLESP.setLong(3, HICL);
					pstUpdLESP.setString(4, cip);
					n += pstUpdLESP.executeUpdate();
				} else
					err++;
			}
			// cerrar objetos de BD
			rsAfectados.close();
			pstAfectados.close();
			pstUpdLESPCEX.close();
			pstUpdLESP.close();

			log.info(
				"Finalizada la actualizaci�n de CIPS incorrectos. "
					+ n
					+ " registros actualizados "
					+ err
					+ " errores");
		} catch (Exception e) {
			log.error("Ha fallado la actualizacion de CIP en SESCAM", e);
		}

	}

	public void run0200_LEQ() throws Exception {
		statMan.setStatus(codInterno, 200);
		//Copiamos las tablas logger en el Sescam de Quirurgica
		int numReg = run0201_LEQCopiaLogger();
		//Ahora se refresca la LEQ del SESCAM
		run0202_LEQProcesaLogger(numReg);
		// actualizar los registros afectados por cambios en leactihis
		run0203_LEQCambiosLeactihis();

	}

	public void run0203_LEQCambiosLeactihis() throws Exception {

		log.info("Actualizar registros con cambios en leactihis");
		// Consultar registros cuyo leq a cambiado a N
		String sql = gP("sql.select.getCambiosLeactihis");
		ResultSet rs = stCen.executeQuery(sql);

		String sqlRemove = gP("sql.deleteCambioleq.lesp.sescam");
		PreparedStatement pstRemove = sesPS(sqlRemove);

		String sqlUpdateLoggerCentro = gP("sql.update.logger.centro");
		PreparedStatement pstProc = cenPS(sqlUpdateLoggerCentro);

		int n = 0,nproc = 0;
		while (rs.next()) {
			// borrar en LESP
			pstRemove.setInt(1, CEGA);
			pstRemove.setInt(2, rs.getInt("norden"));
			nproc += pstRemove.executeUpdate();
			n++;
			// poner registro como procesado
			pstProc.setInt(1, 2);
			pstProc.setInt(2, rs.getInt("norden"));
			pstProc.setInt(3, rs.getInt("tipope"));
			pstProc.setTimestamp(4, rs.getTimestamp("fecope"));
			pstProc.execute();

		}
		pstRemove.close();
		pstProc.close();
		// purgar masterclaves
		if (n > 0) {
			String sqlPurgeMasterClave = gP("sql.delete.masterclave.sescam");
			PreparedStatement pstPurge = sesPS(sqlPurgeMasterClave);
			pstPurge.setInt(1, CEGA);
			pstPurge.execute();
			pstPurge.close();
		}

		log.info(
			"Fin de deteccion de cambios en leactihis ("
				+ n
				+ " registros procesados,"+ nproc + " eliminados)");

	}

	/**
	 * Conexion con ambas bases de datos.
	 */

	private void iniCon() {
		available = true;
		String urlCentro =
			getURL() + ";user=" + getUser() + ";password=" + getPassword();
		try {
			toCentro = DriverManager.getConnection(urlCentro);
		} catch (Exception e) {
			available = false;
			log.error("url=" + urlCentro, e);
		}
		try {
			toSescam =
				DriverManager.getConnection(
					getSURL(),
					getSUser(),
					getSPassword());
		} catch (Exception e) {
			available = false;
			log.error("Excepcion:", e);
			try {
				toCentro.close();
			} catch (Exception ee) {
				;
			}
		}
	}

	/**
	 * Devuelve la definicion del centro
	 * @return Nombre de centro y URL de conexion
	 */
	public String toString() {
		return "Centro " + getNombre() + " url=" + getURL();
	}

	/**
	 * Comprueba si est� disponible la conexion y creado el esquema de BBDD
	 * @return true si est� disponible
	 */
	public boolean run0000_Available() {
		statMan.setStatus(codInterno, 1);
		//Comprobar si est� disponible la conexi�n
		if (!available) {
			iniCon();
			if (!available) {
				log.error(
					"Centro "
						+ getNombre()
						+ " no esta disponible - Abortando!!!");
				return false; // Esto finaliza la ejecuci�n de la hebra
			}
		}
		try {
			stSes = toSescam.createStatement();
			stCen = toCentro.createStatement();
		} catch (Exception e) {
			return false;
		}
		//Comprobar si es la primera vez
		if (!checkSchema()) {
			log.error(
				"En el centro "
					+ getNombre()
					+ " no existe el esquema - Abortando!!!");
			return false;
		}
		Hashtable patcherParams = new Hashtable();
		patcherParams.put("rootDir", "ssf/sescam/lesp/regcen/db/Centros/");
		patcherParams.put("toCentro", toCentro);
		patcherParams.put("toSescam", toSescam);
		try {
			//Obtener el centro de gasto o codigo de centro
			run0001_getCEGA();
		} catch (SQLException ex) {
			return false;
		}
		patcherParams.put("CEGA", new Integer(CEGA));
		Patcher patch = new Patcher(patcherParams);
		return patch.upgradeLevel();
	}

	private void initPSs() throws SQLException {
		//Consulta de Anestesia
		psAnestesia = cenPS(gP("sql.select.anestesia.centro"));
		//Consulta de Circunstancias de Inclusion
		psCinclusion = cenPS(gP("sql.select.cinclusion.centro"));
		//Consulta de Hospital deriva
		psHospderiva = cenPS(gP("sql.select.hospderiva.centro"));
	}

	private void endPSs() throws SQLException {
		//Consulta de Anestesia
		psAnestesia.close();
		//Consulta de Circunstancias de Inclusion
		psCinclusion.close();
		//Consulta de Hospital deriva
		psHospderiva.close();
	}

	/**
	 * Obtiene el codigo del centro y lo guarda en sescamcentro
	 * @throws SQLException
	 */
	public void run0001_getCEGA() throws SQLException {
		//Obtenemos el CEGA
		statMan.setStatus(codInterno, 100);
		ResultSet rs = stCen.executeQuery(gP("sql.select.getCEGA.center"));
		CEGA = (rs.next()) ? rs.getInt("valclave") : codInterno;
		rs.close();
		PreparedStatement pst = null;
		pst = sesPS(gP("sql.select.getCEGA.sescam"));
		pst.setInt(1, CEGA);
		rs = pst.executeQuery();
		rs.next();
		int cierto = rs.getInt(1);
		rs.close();
		pst.close();
		if (cierto == 0) {
			pst = sesPS(gP("sql.insert.CEGA.sescam"));
			pst.setInt(1, CEGA);
			pst.setString(2, gP("centro." + codInterno + ".nombre"));
			pst.execute();
			pst.close();
		}
	}

	public void run0202_LEQProcesaLogger(int numReg) throws Exception {
		statMan.setStatus(codInterno, 202);
		//Inicializacion de los prepared statements
		try {
			initPSs();
		} catch (Exception e1) {
			log.error("LLamando a initPS() en LEQProcesaLogger", e1);
			throw e1;
		}
		int ret;
		String exceptionError = "Inicio";
		//Obtener el numero de registros a procesar

		try {
			{
				exceptionError = "Contando registros en LEQProcesaLogger";
				PreparedStatement stCount =
					sesPS(gP("sql.select.regTransferidos.count"));
				stCount.setInt(1, CEGA);
				ResultSet rsCount = stCount.executeQuery();
				rsCount.next();
				numReg = rsCount.getInt(1);
				rsCount.close();
				stCount.close();
			}
			//Inicio de proceso
			log.info("Iniciando procesamiento de " + numReg + " registros LEQ");
			//Obtener los registros a procesar
			String sqlRegTransferidos = gP("sql.select.regTransferidos");
			PreparedStatement pstSregTransfer = sesPS(sqlRegTransferidos);
			pstSregTransfer.setInt(1, CEGA);
			//rs Contiene los registros de lesplogger que se han de procesar
			ResultSet rsRegTransfer = pstSregTransfer.executeQuery();

			//Obtencion de los nombres de columna y demas
			ResultSetMetaData rsmd = rsRegTransfer.getMetaData();
			int numColumns = rsmd.getColumnCount();
			PreparedStatement pstULogSes =
				sesPS(gP("sql.update.logger.sescam"));
			PreparedStatement pstULogCen =
				cenPS(gP("sql.update.logger.centro"));
			PreparedStatement pstSVLespCen = cenPS(gP("sql.select.vlesp"));
			PreparedStatement pstILespSes = sesPS(gP("sql.insert.lesp.sescam"));
			PreparedStatement pstDLespSes = sesPS(gP("sql.delete.lesp.sescam"));
			int actualReg = 0;
			exceptionError = "Inicio del recorrido de registros";
			while (rsRegTransfer.next()) {
				actualReg++;
				exceptionError = "LEQProcesaLogger Reg#" + actualReg;
				if (((float) actualReg) % (float) 1000 == (float) 0) {
					log.debug(
						"Procesados " + actualReg + " registros de " + numReg);
				}
				ret = 0;
				switch (rsRegTransfer.getInt("tipope")) {
					case 0 :
					case 1 :
						ret =
							insertLesp(
								pstILespSes,
								pstSVLespCen,
								rsRegTransfer);
						break;
					case 5 :
						ret =
							updateLesp(
								pstILespSes,
								pstSVLespCen,
								rsRegTransfer);
						break;
					case 10 :
						ret = deleteLesp(pstDLespSes, rsRegTransfer);
						break;
				}
				//Set de 'procesada' tanto en sescam como en centro
				int statusProc = (ret == 0) ? 2 : -2;
				int NOrden = rsRegTransfer.getInt("norden");
				Timestamp FecOpe = rsRegTransfer.getTimestamp("fecope");

				exceptionError += ", marcando en sescam";
				//Marcamos lesplogger en Sescam
				pstULogSes.setInt(1, statusProc); //procesada=2 ok, -2 Error
				pstULogSes.setInt(2, NOrden);
				pstULogSes.setInt(3, CEGA);
				pstULogSes.setTimestamp(4, FecOpe);
				pstULogSes.execute();

				exceptionError += ", marcando en centro";
				//Marcamos lesplogger en el Centro
				pstULogCen.setInt(1, (ret == 0) ? 2 : -2);
				//procesada=2 ok, -2 Error);
				pstULogCen.setInt(2, NOrden);
				pstULogCen.setInt(3, rsRegTransfer.getInt("tipope"));
				pstULogCen.setTimestamp(4, FecOpe);
				pstULogCen.execute();
			}
			rsRegTransfer.close();
			pstSregTransfer.close();
			pstULogSes.close();
			pstSVLespCen.close();
			pstILespSes.close();
			pstDLespSes.close();
			endPSs();
		} catch (Exception ex) {
			log.error("Error", ex);
			throw ex;
		}
		log.info("Terminado el refresco de " + getNombre() + " (LEQ)");
	}

	private int insertLesp(
		PreparedStatement pstILespSes,
		PreparedStatement pstSVLespCen,
		ResultSet rsRegTransfer)
		throws Exception {
		int ret = 0;
		//Argumentos de selectLesp
		int NOrden = rsRegTransfer.getInt("norden");
		pstSVLespCen.setInt(1, NOrden);
		pstSVLespCen.setInt(3, NOrden);
		Timestamp fechaFull = rsRegTransfer.getTimestamp("fecope");
		pstSVLespCen.setTimestamp(2, fechaFull);
		pstSVLespCen.setTimestamp(4, fechaFull);

		ResultSet rsVLespCen = pstSVLespCen.executeQuery();
		ResultSetMetaData rsVLespCenMD = rsVLespCen.getMetaData();
		int numCol = rsVLespCenMD.getColumnCount();
		/*Suponemos que vlesp en el centro y lesp en el sescam tienen EXACTAMENTE
		 *las mismas columnas excepto la segunda; que en vlesp es fecope y en el
		 *sescam es centro.
		 */
		if (rsVLespCen.next()) {
			for (int k = 1; k <= numCol; k++) {
				//Inicio de procesamiento. la variable cont contiene el valor del campo
				//de forma que se puede modificar segune xcepciones
				Object cont = rsVLespCen.getObject(k);
				if (cont != null
					&& cont.getClass().getName().equals("java.lang.String")) {
					cont = ((String) cont).trim();
				}
				String colName = rsVLespCenMD.getColumnName(k);
				//Short falla, no se porqu�. Transformar a Integer.
				if (rsVLespCenMD
					.getColumnClassName(k)
					.equals("java.lang.Short")) {
					cont = new Integer(rsVLespCen.getShort(k));
					/*Excepcion 1 : Si k=2, se obtiene el nombre del centro
					 */
				}
				if (k == 2) {
					//System.out.println("<centro>="+CEGA);
					pstILespSes.setInt(2, CEGA);
				} else {
					/*Excepcion 2: Hay que especificar el tipo de NULL �?�? o sea varchar
					 *O bien si es el campo fsal, en cuyo caso se inicializa a null
					 */
					if (cont == null) {
						//System.out.println("<"+colName+">=<null>");
						pstILespSes.setNull(k, Types.VARCHAR);
					} else {

						/*Excepcion 3: tcir (tipo de cirugia) es el campo lambul de leactis
						     *si vale "S" entonces el valor debe ser A(mbulatorio), si no I(ngresado)
						 */
						if (colName.equalsIgnoreCase("tcir")) {
							cont = (((String) cont).equals("S")) ? "A" : "I";
						}

						/*Excepcion 4: tane, si es '' (cadena de longitud nula) o null
						     *Se coloca 'Sin Anestesia', caso contrario consultar la tabla Anestesia
						 */
						if (colName.equalsIgnoreCase("tane")) {
							String val = (String) cont;
							if (val.length() == 0) {
								cont = "Sin Anestesia";
							} else {
								psAnestesia.setString(1, val);
								ResultSet rsAnestesia =
									psAnestesia.executeQuery();
								cont =
									(rsAnestesia.next())
										? rsAnestesia.getString(1)
										: "Sin Anestesia";
								rsAnestesia.close();
							}
						}

						//Excepcion 5: prio Si vale 1, entonces P; si vale 2 entonces N
						// por Preferente y Normal respectivamente
						if (colName.equalsIgnoreCase("prio")) {
							cont = (((String) cont).equals("1")) ? "P" : "N";
						} //Fin Excepcion prio

						/*Excepcion 6 : cinc Circunstancias inclusion
						 * Si vale 1 y ademas lista_espe.procedede=2 y ademas
						 * lista_espe.servpeti=hospitales.codhospi y ademas
						 * hospitales.tipo in ('A','I') entonces cinc='D'
						 * Si vale 1 y no se cumple alguna de las anteriores condiciones
						 * vale P
						 * Si vale 2 cinc=R
						 */
						if (colName.equalsIgnoreCase("cinc")) {
							Integer val = (Integer) cont;
							String sval = "P";
							switch (val.intValue()) {
								case 1 :
									sval = "P";
									psCinclusion.setInt(
										1,
										rsVLespCen.getInt("norden"));
									ResultSet rsCinclusion =
										psCinclusion.executeQuery();
									if (rsCinclusion.next()) {
										if (rsCinclusion.getInt(1) == 2
											&& (rsCinclusion
												.getString(2)
												.equalsIgnoreCase("I")
												|| rsCinclusion.getString(
													2).equalsIgnoreCase(
													"A"))) {
											sval = "D";
										}
									}
									rsCinclusion.close();
									break;
								case 2 :
									sval = "R";
									break;
							}
							cont = sval;
						}
						/*Excepcion 7 : Situacion de paciente spac. Por defecto S,
						 *si kSit=2 y lista_espe.fechaqui<=sysdate en spac<-'Q'
						 *si kSit=3 y lista_espe.fecderiv<=sysdate en spac<-'D'
						 *si kSit=4-5 y fecdemora<=sysdate en spac<-'E'
						 */
						/*ELM se trata esta logica en la aplicaicion OOPP
						                   if (colName.equalsIgnoreCase("spac")) {
						   int kSit = ( (Integer) cont).intValue();
						   String scont = "S";
						   switch (kSit) {
						      case 4:
						      case 5:
						         if (Global.isBeforeNow(rsVLespCen, "fsde"))
						            scont = "E";
						         break;
						      case 3:
						         if (Global.isBeforeNow(rsVLespCen, "fder"))
						            scont = "D";
						         break;
						      case 2:
						         if (Global.isBeforeNow(rsVLespCen, "fqui"))
						            scont = "Q";
						         break;
						   }
						   cont = scont;
						                   }
						 */
						/*Excepcion 8: hospderiva
						 *Si no es nulo, coger la descripcion
						 */
						if (colName.equalsIgnoreCase("dder")) {
							psHospderiva.setString(1, (String) cont);
							ResultSet rsHospderiva =
								psHospderiva.executeQuery();
							if (rsHospderiva.next()) {
								cont = rsHospderiva.getString("nomhospital");
							}
							rsHospderiva.close();
						}
						/*Excepcion 9: cega
						 *se obtiene de la variable CEGA
						 */
						if (colName.equalsIgnoreCase("cega")) {
							cont = new Integer(CEGA);
							//No hay mas excepciones, colocar el dato en SESCAM
							//System.out.println("<"+colName+">="+cont);
						}
						pstILespSes.setObject(k, cont);
					}
				}
			}
			try {
				int n = pstILespSes.executeUpdate();
			//	if (n != 1) log.error("Registro no insertado (" + NOrden + ")");

			} catch (SQLException sqlexception) {
				if (sqlexception.toString().indexOf("ORA-00001") != -1) {
					//Se ha producido una anulacion del historico, o lo que es lo mismo
					//una anulacion de una anulacion de una salida de lesp.
					//Hay que forzar a reintroducirla
					String sqlRemove = gP("sql.deleteForUpdate.sescam.lesp");
					PreparedStatement st = sesPS(sqlRemove);
					st.setInt(1, CEGA);
					st.setInt(2, rsRegTransfer.getInt("norden"));
					st.execute();
					st.close();
					pstILespSes.execute();
				} else log.error("Excepcion SQL no recuperable (" + NOrden + ")" +  sqlexception);
			} catch (Exception excp) {
				log.error("Error al insertar Registro " + NOrden + ":" +  excp);
			}
		} else { //No hay datos del medico o del paciente
			int link = rsRegTransfer.getInt("link");
			if (rsRegTransfer.wasNull()) {
				//Es un registro que est� en lista de espera viva (no historica)
				ret = -1;
				log.warn(
					"Atencion:\n En el centro "
						+ getNombre()
						+ " norden="
						+ rsRegTransfer.getInt("norden")
						+ ":\n"
						+ "\tNo tiene definido el paciente hc="
						+ rsRegTransfer.getInt("numerohc")
						+ " (o su domicilio/provincia) o bien el doctor cod_medico="
						+ rsRegTransfer.getInt("cod_medico"));
			} else {
				ret = 0;
				//En principio no hay log de esto, se borra de lesplogger como si
				//hubiese sido procesada correctamente.
			}
		}
		rsVLespCen.close();
		return ret;
	}

	private int updateLesp(
		PreparedStatement pstILespSes,
		PreparedStatement pstSVLespCen,
		ResultSet rsRegTransfer)
		throws Exception {
		String sqlRemove = gP("sql.deleteForUpdate.sescam.lesp");
		PreparedStatement st = sesPS(sqlRemove);
		st.setInt(1, CEGA);
		st.setInt(2, rsRegTransfer.getInt("norden"));
		st.execute();
		st.close();
		return insertLesp(pstILespSes, pstSVLespCen, rsRegTransfer);
	}

	private int deleteLesp(
		PreparedStatement pstDLespSes,
		ResultSet rsRegTransfer)
		throws Exception {
		String sequal = gP("sql.delete.lesp.sescam.fsal");
		PreparedStatement pstSHLesp = cenPS(sequal);
		pstSHLesp.setInt(1, rsRegTransfer.getInt("norden"));
		ResultSet rsHLesp = pstSHLesp.executeQuery();
		if (rsHLesp.next()) {
			pstDLespSes.setTimestamp(1, rsHLesp.getTimestamp("f_baja"));
			pstDLespSes.setString(2, rsHLesp.getString("codiofi"));
			pstDLespSes.setInt(3, rsHLesp.getInt("nfila_lie"));
			pstDLespSes.setInt(4, rsRegTransfer.getInt("norden"));
			pstDLespSes.setInt(5, CEGA);
			pstDLespSes.execute();
			rsHLesp.close();
			pstSHLesp.close();
			return 0;
		} else {
			rsHLesp.close();
			pstSHLesp.close();
			log.warn(
				"Atencion:\n En el centro "
					+ getNombre()
					+ " norden="
					+ rsRegTransfer.getInt("norden")
					+ ",\n"
					+ "No tiene definida fecha de baja en lista de espera");
			return -1;
		}
	}

	private int getNumRegs() throws Exception {
		String sqlCount = gP("sql.select.regNoProcesados.count");
		ResultSet rsCount = stCen.executeQuery(sqlCount);
		rsCount.next();
		int numReg = rsCount.getInt(1);
		rsCount.close();
		return numReg;
	}

	public int run0201_LEQCopiaLogger() throws Exception {

		//Notificar estado
		statMan.setStatus(codInterno, 201);
		String sqlUpdateLoggerCentro = gP("sql.update.logger.centro");
		String sqlInsertLogSescam = gP("sql.insert.log.sescam");
		String sqlSelectRegNoProc = gP("sql.select.regNoProcesados");

		int numReg = 0;
		String exceptionError = "Inicio";
		try {
			PreparedStatement pst = cenPS(sqlUpdateLoggerCentro);
			PreparedStatement ist = sesPS(sqlInsertLogSescam);
			numReg = getNumRegs();
			log.info("Iniciando copia de " + numReg + " registros LEQ");
			int actualReg = 0;
			ResultSet rs = stCen.executeQuery(sqlSelectRegNoProc);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumnsOfLogger = rsmd.getColumnCount();
			exceptionError = "Comenzando a recorrer sql.select.regNoProcesados";
			while (rs.next()) {
				actualReg++;
				exceptionError = "LEQCopiaLogger Reg#" + actualReg;
				if (((float) actualReg) % (float) 1000 == (float) 0) {
					log.debug(
						"Copiados "
							+ actualReg
							+ " registros de "
							+ numReg
							+ " (LEQ)");
					//Insertarla en el sescam.
				}
				for (int i = 1; i <= numColumnsOfLogger; i++) {
					int ipo = rsmd.getColumnType(i);
					switch (ipo) {
						case Types.DATE :
						case Types.TIMESTAMP :
							ist.setTimestamp(i, rs.getTimestamp(i));
							break;
						default :
							ist.setString(i, rs.getString(i));
							break;
					}
				}
				//Ultimo campo es el codigo del centro.
				ist.setInt(numColumnsOfLogger + 1, CEGA);
				int status = 1;
				try {
					ist.execute();
				} catch (Exception e) {
					status = -1;
					log.error("Norden=" + rs.getInt("norden"), e);
				}
				//marcarla como procesada=1 en el centro
				exceptionError += ", Marcando como procesada";
				pst.setInt(1, status); //procesada=1 OK, -1 Error
				pst.setInt(2, rs.getInt("norden"));
				pst.setInt(3, rs.getInt("tipope"));
				pst.setTimestamp(4, rs.getTimestamp("fecope"));
				pst.execute();
			}
			log.info("Terminada la copia a SESCAM ");
		} catch (Exception ex) {
			log.error("Error:", ex);
			throw ex;
		}
		return numReg;
	}

	/**
	 * Chequea la disponibilidad del esquema de forma remota.
	 * Comprueba la existencia de la tabla LESPLOGGER
	 * @return true si existe la tabla lesplogger
	 */
	public boolean checkSchema() {
		boolean ret = false;
		try {
			ResultSet rs =
				stCen.executeQuery(gP("sql.select.check.schema.center"));
			if (rs.next()) {
				if (rs.getInt(1) == 1) {
					ret = true;
				}
			}
			rs.close();
		} catch (Exception e) {
			ret = false;
		}
		return ret;
	}

	public void runFF00_PurgeLoggers() {
		Hashtable ht = new Hashtable();
		ht.put("{CENTRO}", (new Integer(CEGA)).toString());
		try {
			SQLFileRunner.multipleRun(
				true,
				toCentro,
				prop,
				"centros.sql.purge",
				getNombre(),
				ht);
		} catch (Exception e) {
		}
		try {
			SQLFileRunner.multipleRun(
				true,
				toSescam,
				prop,
				"sescam.sql.purge",
				"Sescam",
				ht);
		} catch (Exception e) {
		}
	}
	/**
	* Insertamos en citas log las citas que tienen prestaciones modificadas
	* @throws Exception
	*/

	public void run0301_LECEXPPrestaciones() throws Exception {

		log.info(
			"Preparar la actualizaci�n de citas con prestaciones modificadas");

		// Consultar Citas afectadas por el cambio de prestaciones.
		String sql = gP("sql.select.centros.getCitasPrest");
		ResultSet rs = stCen.executeQuery(sql);

		PreparedStatement pstCitasLog =
			cenPS(gP("sql.insert.centros.setCitasLogPrest"));

		// Insertar citas afectadas en citaslog
		int n = 0;
		while (rs.next()) {
			int ncita = rs.getInt(1);
			pstCitasLog.setInt(1, ncita);
			pstCitasLog.execute();
			n++;
		}
		rs.close();
		log.info(
			"Fin de actualizacion de citas/prestaciones ("
				+ n
				+ " registros procesados)");

	}

	/**
	 * Procesamos la lista de espera de citas y de pruebas.
	 * @throws Exception
	 */
	public void run0300_LECEXP() throws Exception {

		// Insertamos en citaslog las citas con prestaciones modificadas
		run0301_LECEXPPrestaciones();
		//Notificacion de estado
		statMan.setStatus(codInterno, 300);
		//Para cambiar el estado de los registros procesados
		PreparedStatement pstStatus =
			cenPS(gP("sql.insert.centro.setStatusCitasLog"));

		//Obtener el registro de la cita/actividad del centro
		PreparedStatement pstGetCita = cenPS(gP("sql.select.getCita.centro"));

		//Este ResultSet contiene todos los registros a procesar
		//Ojo con los segmentos de rollback, 'snapshot too old' (instantanea demasiado antigua)
		//Debido a que puede ser grande
		java.sql.Date lecexpSQLFechaInicio =
			new java.sql.Date(lecexpFechaInicio.getTime());

		//Obtenemos cuantos son y cuales son
		int numRegs = getNumeroRegistrosCEX(lecexpSQLFechaInicio);

		//Algo de info
		log.info(
			"Iniciando Copia y Procesamiento de "
				+ numRegs
				+ " registros CEX/P");

		ResultSet masterLoop = getRegistrosCEX();

		float actualReg = 0;
		Cita cita = new Cita(toCentro, prop);
		//Iniciamos el procesamiento

		while (masterLoop.next()) {
			int statusProc = 2; //Suponemos que todo va a ir bien,
			//Borramos el registro cita que hay en memoria
			cita.reset();
			ResultSet getCita = null;
			int ncita = masterLoop.getInt("ncita");

			//Cargamos el numero de cita en el PreparedStatement y nos lo traemos
			pstGetCita.setInt(1, ncita);
			pstGetCita.setInt(2, ncita);
			pstGetCita.setInt(3, ncita);
			getCita = pstGetCita.executeQuery();
			//Primero vemos si hay registro en la vista
			if (getCita.next()) {
				//Comprobamos que cumple las condiciones
				if (cita
					.CheckCond(
						getCita,
						lecexpFechaInicio,
						lecexpGrupocexRegcen)) {
					//Cargamos el registro en el hashtable de Cita
					cita.loadFromRS(getCita);
					//Este campo hay que setearlo desde aqui (el comando anterior lo pone null)
					cita.set("cega", new Integer(CEGA));
					switch (masterLoop.getInt("accion")) {
						case 0 : //Inicializacion
						case 1 : //Insercion posterior a inicializacion
							statusProc = cita.TriggerInsert(stSes);
							break;
						case 2 : //Update
							statusProc = cita.TriggerUpdate(stSes);
							break;
						case 3 : //Delete
							statusProc = cita.TriggerDelete(stSes);
							break;
					}
				} else {
					statusProc = 2;
					//No cumple Condicion de usuario, pero no es un error
				}
			} else {
				cita.set("cega", new Integer(CEGA));
				cita.set("ncita", masterLoop.getString("ncita"));
				cita.RegisterDelete(stSes);
				//No existe registro equivalente a esta ncita
				statusProc = -3;
			}
			//Cerrar, Marcar, Limpiar
			if (getCita != null) {
				getCita.close();
				//Hacemos el set del status
			}
			pstStatus.setInt(1, statusProc);
			pstStatus.setTimestamp(2, masterLoop.getTimestamp("fecha"));
			pstStatus.setInt(3, masterLoop.getInt("ncita"));
			pstStatus.setInt(4, masterLoop.getInt("accion"));
			pstStatus.execute();
			actualReg++;
			if (actualReg % 1000 == 0) {
				log.debug(
					"Procesados "
						+ (int) actualReg
						+ " registros de "
						+ numRegs
						+ "(LEC)");
			}
		}
		masterLoop.close();
		log.info("Fin CEX/P");
	}

	private int getNumeroRegistrosCEX(java.sql.Date f) throws Exception {
		/*
		String upd= gP("sql.update.centros.citas.registrosOutOfDate.1");
		PreparedStatement pst=toCentro.prepareStatement(upd);
		pst.setDate(1,f);
		pst.execute();
		upd= gP("sql.update.centros.citas.registrosOutOfDate.2");
		pst=toCentro.prepareStatement(upd);
		pst.setDate(1,f);
		pst.execute();
		upd= gP("sql.update.centros.citas.registrosOutOfDate.3");
		pst=toCentro.prepareStatement(upd);
		pst.setDate(1,f);
		pst.execute();
		pst.close();
		*/
		String sql = gP("sql.select.centros.getNumeroRegistrosCEX");
		ResultSet rs = stCen.executeQuery(sql);
		rs.next();
		int ret = rs.getInt(1);
		rs.close();
		return ret;
	}

	private ResultSet getRegistrosCEX() throws Exception {
		String sql = gP("sql.select.centros.getRegistrosCEX");
		ResultSet rs = stCen.executeQuery(sql);
		return rs;
	}

	private String getNombre() {
		return gP("centro." + codInterno + ".nombre");
	}

	private String getURL() {
		return gP("centro." + codInterno + ".url");
	}

	private String getUser() {
		return gP("centro." + codInterno + ".usuario");
	}

	private String getPassword() {
		return gP("centro." + codInterno + ".password");
	}

	private String getSURL() {
		return gP("sescam.url");
	}

	private String getSUser() {
		return gP("sescam.usuario");
	}

	private String getSPassword() {
		return gP("sescam.password");
	}

	private String gP(String key) {
		return prop.getProperty(key);
	}

	private String gP(String key, String def) {
		return prop.getProperty(key, def);
	}

	private PreparedStatement sesPS(String sql) throws SQLException {
		return toSescam.prepareStatement(sql);
	}

	private PreparedStatement cenPS(String sql) throws SQLException {
		return toCentro.prepareStatement(sql);
	}

	public void run0400_Tablas() {
		try {
			int numTablas = Integer.parseInt(gP("tabla.autom.size"));
			Tabla tablaAutomatica = null;
			for (int i = 1; i <= numTablas; i++) {
				String nombre = gP("tabla.autom." + i + ".nombre");
				String pkSize = gP("tabla.autom." + i + ".pk.size");
				String where =  gP("tabla.autom." + i + ".where","");
				if (pkSize == null) {
					log.info("Sincronizando tabla automatica " + nombre);
					tablaAutomatica = new Tabla(toCentro, toSescam, nombre,where);
				} else {
					int tamPk = Integer.parseInt(pkSize);
					String pks[] = new String[tamPk];
					for (int j = 1; j <= tamPk; j++)
						pks[j - 1] = gP("tabla.autom." + i + ".pk." + j);
					log.info(
						"Sincronizando tabla automatica " + nombre + "(pkdef)");
					tablaAutomatica =
						new Tabla(toCentro, toSescam, nombre, where, pks);
				}
				tablaAutomatica.synchronize();
			}
		} catch (Exception e) {
			log.error("Fallo la replicacion de las tablas automaticas.", e);
		}
	}
}