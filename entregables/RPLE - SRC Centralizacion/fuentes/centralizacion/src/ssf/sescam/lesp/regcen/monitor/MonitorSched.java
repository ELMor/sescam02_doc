package ssf.sescam.lesp.regcen.monitor;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;


import ssf.sescam.lesp.regcen.synch.Centro;
import org.apache.log4j.Logger;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Sistema de recopilacion de Informix HP-HIS de listas de espera</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */
public class MonitorSched implements Runnable, StatusManager{

  private Properties p=null;
  private Logger logger=Logger.getLogger(getClass());

  Thread allThreads[]=null;
  int allThreadStatus[]=null;
  String allThreadNames[]=null;

  int numCentros=0;

  /**
   * Constructor inicializado con properties
   * @param prop Propiedades cargadas desde config.properties
   * @param ls   Logger para realizar salidas de avance
   */
  public MonitorSched(Properties prop) {
    p=prop;
    numCentros=Integer.parseInt(p.getProperty("centro.size"));
    allThreads=new Thread[numCentros];
    allThreadStatus=new int[numCentros];
    allThreadNames=new String[numCentros];
    for(int i=1;i<=numCentros;i++){
      allThreads[i-1]=null;
      allThreadStatus[i-1]=0;
      allThreadNames[i-1]="C "+p.getProperty("centro."+i+".nombre");
    }
  }

 /**
  * Ejecucion de la hebra: cada minuto comprueba el tick del reloj para ver si
  * coincide con algun schedule, en cuyo caso llama a startCentros
  */
  public void run(){
    boolean salir=false;
    if(p.getProperty("schedule.atinit","false").equals("true")){
      logger.info("Scheduler: Iniciando centros (shedule.atinit=true)");
      startCentros();
    }
    float halfMins=0;
    while(!salir){
      if(halfMins%30==0)
        logger.debug("Tick cada 15 minutos");
      checkSchedules();
      checkThreadStatus();
      try{
        Thread.sleep(1000*30); //Cada 30 segundos
        halfMins++;
      }catch(InterruptedException e){
        logger.warn("Interrupcion",e);
        salir=true;
      }
    }
  }

  /**
   * Transforma a int un parametro recogido de properties
   * @param p Propiedades
   * @param key clave a recoger
   * @return valor de la clave de tipo int
   */
  private int getInt(Properties p,String key){
    int ret=-1;
    try {
      ret=Integer.parseInt(p.getProperty(key));
    }catch(Exception e){
      ;
    }
    return ret;
  }

  /**
   * Ejecuci�n de un tick de reloj. Comprueba la hora con los schedules
   * que se hayan definido
   */
  void checkSchedules(){
    //Obtenemos la hora actual segun el equipo local
    Calendar cal=GregorianCalendar.getInstance();
    Date now=cal.getTime();
    int horaAhora=cal.get(Calendar.HOUR_OF_DAY);
    int minuAhora=cal.get(Calendar.MINUTE);

    //Recorremos los schedules para ver si coinciden con la hora de ahora
    int numScheds=getInt(p,"schedule.size");
    for(int i=1;i<=numScheds;i++){
      // Obtenemos la hora del schedule (cita)
      String sched=p.getProperty("schedule."+i+".inicio");
      int horaSched=Integer.parseInt(sched.substring(0,2));
      int minuSched=Integer.parseInt(sched.substring(3));
      //Comprobar si es ahora la cita
      if(horaAhora==horaSched && minuAhora==minuSched) {
        logger.info("Scheduler: Coincidencia ahora con schedule#"+i);
        startCentros();
      }
    }
  }

  /**
   * Arranca las hebras de los centros definidos comprobando que no lo est�n ya.
   */
  private void startCentros() {
    //Revisar las hebras que se estan ejecutando
    Thread thrRunning[]=new Thread[1024];
    int numThreads=Thread.enumerate(thrRunning);
    //Arrancar las hebras de recolecci�n de los centros,
    //Siempre y cuando no est�n ya arrancadas
    for(int j=1;j<=numCentros;j++){
      //�Esta este centro ejecutandose en este instante?
      boolean runNewThread=true;
      for(int k=0;runNewThread&&k<numThreads;k++){
        if(thrRunning[k].getName().equals(allThreadNames[j-1])){
          logger.info("Hebra "+allThreadNames[j-1]+" aun en ejecucion");
          runNewThread=false;
        }
      }
      if(runNewThread){
        execCentro(j);
      }
    }
  }

  /**
   * Crea y ejecuta una hebra
   * @param j Codigo interno (de config.properties) del centro que se va a arrancar
   */
  private void execCentro(int j) {
    logger.info("Scheduler: Arrancando hebra "+allThreadNames[j-1]);
    try {
      allThreads[j-1]=new Thread(new Centro(p,j,this),allThreadNames[j-1]);
      allThreads[j-1].start();
    }catch(Exception e){
      logger.error("No puedo arrancar la hebra",e);
    }
  }

  /**
   * Desde las hebras se usan estos metodos para notificacion de estado
   * @param centro Codigo del centro (interno)
   * @param status estado que se quiere colocar
   */
  public void setStatus(int centro, int status){
    if(allThreadStatus!=null)
      allThreadStatus[centro-1]=status;
  }

  /**
   * Desde las hebras se usa para conseguir estado
   * @param codigo Codigo del centro (codigo Interno, el que hay en config.properties)
   * @return estado
   */
  public int getStatus(int codigo) {
    if(allThreadStatus!=null)
      return allThreadStatus[codigo-1];
    return -1;
  }

  /**
   * Comprueba si alguna hebra termino debido a un error.
   * Si es as�, la rearranca.
   */
  public void checkThreadStatus(){
    for(int i=1;i<=numCentros;i++){
      if(allThreads[i-1]!=null){
        if(  allThreads[i-1].isAlive()==false
             &&allThreadStatus[i-1]<0
             &&allThreadStatus[i-1]!=0x7f ){ //Termino la hebra de mala manera
          logger.warn("Hebra "+allThreadNames[i-1]+
                     " termino debido a error("+allThreadStatus[i-1]+")"+
                     "rearrancando...");
          execCentro(i);
        }
      }
    }
  }
}

