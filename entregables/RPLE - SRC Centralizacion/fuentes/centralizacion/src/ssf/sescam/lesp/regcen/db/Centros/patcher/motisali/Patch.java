package ssf.sescam.lesp.regcen.db.Centros.patcher.motisali;

import ssf.sescam.lesp.regcen.db.patcher.*;
import java.util.*;
import java.sql.Connection;
import org.apache.log4j.Logger;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import ssf.utils.Global;
import java.sql.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author not attributable
 * @version 1.0
 */

public class Patch
    extends ClassPatcher {
  public Patch() {
  }

  public boolean isDoubleConnectionRequired() {
    return true;
  }

  public boolean patchDoubleConnection(Hashtable params) {
    boolean ret = true;
    int CEGA = 0;
    Connection toSescam = null;
    Connection toCentro = null;

    CEGA = ( (Integer) params.get("CEGA")).intValue();
    toSescam = (Connection) params.get("toSescam");
    toCentro = (Connection) params.get("toCentro");
    Logger log = Logger.getLogger(getClass());

    try {
      String sqlSescam = "update lespcex set motisali=?,fsalida=? where cega=" +
          CEGA + " and ncita=?";
      PreparedStatement pst = toSescam.prepareStatement(sqlSescam);

      Statement st = toCentro.createStatement();

      try {
        st.execute("Drop view rcdcexp");
      }
      catch (SQLException ex) {
      }
      try {
        st.execute("Drop view rcdcexph");
      }
      catch (SQLException ex1) {
      }
      try {
        st.execute("Drop view rcdcexpa");
      }
      catch (SQLException ex2) {
      }

      String cView;
      cView = Global.getContentOfFile(
          "ssf/sescam/lesp/regcen/db/Centros/do/17.sql");
      st.execute(cView);
      cView = Global.getContentOfFile(
          "ssf/sescam/lesp/regcen/db/Centros/do/16.sql");
      st.execute(cView);
      cView = Global.getContentOfFile(
          "ssf/sescam/lesp/regcen/db/Centros/do/18.sql");
      st.execute(cView);

      String sql =
          "Select ncita,motisali,indinoga,fsalida " +
          "from rcdcexp where fechacit>='2003-01-01' " +
          "union all " +
          "Select ncita,motisali,indinoga,fsalida " +
          "from rcdcexph where fechacit>='2003-01-01' " +
          "union all " +
          "Select ncita,motisali,indinoga,fsalida " +
          "from rcdcexpa where fechacit>='2003-01-01' ";
      ResultSet rs = st.executeQuery(sql);

      float numReg = 0;
      while (rs.next()) {
        numReg++;
        if ( (numReg % 1000) == 0) {
          log.info("Refrescados motisali,fsalida de " + numReg + " registros");
        }
        String indinoga = rs.getString("indinoga");
        String motisali = rs.getString("motisali"); ;
        if (indinoga != null) {
          motisali += indinoga.trim();
        }
        pst.setString(1, motisali);
        pst.setDate(2, rs.getDate(4));
        pst.setLong(3, rs.getLong(1));
        pst.execute();
      }
      rs.close();

    }
    catch (Exception e) {
      log.error("Ha fallado la actualizacion de motisali,fsalida", e);
      ret = false;
    }
    return ret;
  }

}