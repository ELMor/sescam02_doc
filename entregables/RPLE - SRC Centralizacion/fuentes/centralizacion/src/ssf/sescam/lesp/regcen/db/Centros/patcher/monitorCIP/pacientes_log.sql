create table pacientes_log (
	numerohc    	integer
	,fecope         datetime year to second
	,nombre       	nchar(20)
	,apellid1     	char(20)
	,apellid2     	char(20)
	,nomb_fonet   	char(20)
	,ape1_fonet   	char(20)
	,ape2_fonet   	char(20)
	,numtis_antiguo 		char(30)
	,numtis_nuevo   	char(30)
	,primary key (numerohc,fecope) constraint pk_pacienteslog
	
);
/
