
Create view vhlesp(
    norden     ,fecope
   ,cip        ,cega       ,hicl       ,cias
   ,ape1       ,ape2       ,nomb
   ,fnac       ,sexo
   ,domi       ,pobl       ,cpos       ,prov       ,tel1       ,tel2       ,tel3
   ,slab       ,fina       ,gara       ,serv       ,secc       ,acli       ,cmed
   ,apno_med   ,finc       ,cdi1       ,ddi1       ,cdi2       ,ddi2       ,cpr1
   ,dpr1       ,cpr2       ,dpr2       ,tcir       ,tane       ,prio       ,cinc
   ,preo       ,frep       ,fcap       ,spac       ,fder       ,dder       ,fsde
   ,tdes       ,fqui       ,fsal       ,moti       ,obse	   ,motrechazo ,fecrechazo	
   ,link
      ) as
  Select
      l.nfila                   ,
      ll.fecope                 ,
      p.numtis                  ,
      kk.n                      ,
      l.numhc                   ,
      p.cias                    ,
      p.apellid1                ,
      p.apellid2                ,
      p.nombre                  ,
      p.fechanac                ,
      dsx.descsexo              ,
      p.domiresi                ,
      pobl.nompobla             ,
      p.codipost                ,
      prov.descprov             ,
      p.telefono                ,
      p.telecont                ,
      p.telefono2               ,
      l.bajalabor               ,
      gc.desccas                ,
      l.garante                 ,
      s.maes_serv               ,
      l.servreal                ,
      -1                        ,
      l.codmedic                ,
      trim(pc.apellido1)||' '||trim(pc.apellido2)||' '||trim(pc.nomb),
      l.f_inclu                 ,
      l.codidiag                ,
      l.descdiag                ,
      l.codidiag2               ,
      l.descdiag2               ,
      l.codiproc                ,
      l.descproc                ,
      l.codiproc2               ,
      l.descproc2               ,
      lh.lambul                 ,
      l.tipoanest               ,
      l.prioridad               ,
      l.inclusion               ,
      l.numpreop                ,
      l.fecapto                 ,
      l.feccaduca               ,
      l.situacion               ,
      l.fecderiva               ,
      l.hospderiva              ,
      l.fecdemora               ,
      l.tpodemora               ,
      kk.d                      ,
      l.f_baja                  ,
      dm.codiofi                ,
      ' '                       ,
      l.motrechazo				,
      l.fecrechazo				,
      l.nfila_lie
From
      pacientes   p,
      h_lespadm   l,
      lesplogger  ll,
      persclin    pc,
      servicios   s,
      provincias  prov,
      poblacion   pobl,
      des_sexo    dsx,
      grupocas    gc,
      leactihis   lh,
      des_blista  dm,
      kk
Where
      p.numerohc=l.numhc
  and ll.procesada=1
  and ll.norden=l.nfila
  and pc.codpers=l.codmedic
  and l.servreal=s.codserv
  and p.provresi=prov.codprov
  and p.provresi=pobl.codiprov
  and p.poblares=pobl.codpobla
  and p.sexo=dsx.codsexo
  and l.tipfinan=gc.codcas
  and l.tipoacti=lh.tipoacti
  and dm.codigo=l.motivo
