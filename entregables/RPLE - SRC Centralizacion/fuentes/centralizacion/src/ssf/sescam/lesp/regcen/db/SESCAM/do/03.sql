create or replace view linklesp as
select
	centro,
	norden,
	cip,
	hicl,
	ape1,
	ape2,
	nomb,
	cpr1,
    trunc( sum(decode(moti,17,finc,18,finc,19,finc,9,fsde,decode(fsde,null,nvl(fsal,sysdate),fsde))-finc)
          -sum(decode(moti,9,nvl(tdes,0),0)))
          as deli
from   lesp
start with link is null or norden=-link
connect by prior norden=link
group by centro,norden,cip,hicl,ape1,ape2,nomb,cpr1
