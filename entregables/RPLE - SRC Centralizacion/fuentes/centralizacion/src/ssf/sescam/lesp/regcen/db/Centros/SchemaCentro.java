package ssf.sescam.lesp.regcen.db.Centros;

import ssf.utils.Global;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Sistema de recopilacion de Informix HP-HIS de listas de espera</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class SchemaCentro {

  public SchemaCentro(String[] args) {
    System.out.println("Modificando esquema del centro...");
    try {
      if(args.length>0){
        if(args[0].equalsIgnoreCase("create")){
          System.out.println("Creando");
          create(args);
          return;
        }else if( args[0].equalsIgnoreCase("drop") ){
          System.out.println("Borrando");
          drop(args);
          return;
        }else if( args[0].equalsIgnoreCase("reinstall") ){
          System.out.println("Reinstalando");
          drop(args);
          create(args);
          return;
        }
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    System.out.println("Error en la linea de comandos");
    SchemaBase.Instrucciones();
  }

  public static void main(String[] args) {
    try {
      Global.initLog4J("ssf/sescam/lesp/regcen/logging.properties");
    }
    catch (Exception ex) {
    }
    SchemaCentro schemaCentro1 = new SchemaCentro(args);
    System.exit(0);
  }

  private void create(String[] args) throws Exception{
    CreateSchema cs=new CreateSchema(args);
  }

  private void drop(String[] args) throws Exception{
    DropSchema ds=new DropSchema(args);
  }
}