package ssf.sescam.lesp.regcen.synch.generic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.io.InputStream;
import org.apache.log4j.PropertyConfigurator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author not attributable
 * @version 1.0
 */

public class testTabla {
  public testTabla() throws Exception{
    //Inicializamos el sistema de log
    String log4jFileName="ssf/sescam/lesp/regcen/logging.properties";
    Properties log4jProp=new Properties();
    InputStream log4jIS=ClassLoader.getSystemResourceAsStream(log4jFileName);
    log4jProp.load(log4jIS);
    PropertyConfigurator.configure(log4jProp);

    Class.forName("oracle.jdbc.driver.OracleDriver");
    Class.forName("com.informix.jdbc.IfxDriver");
    Connection from=
        DriverManager.getConnection(
        "jdbc:informix-sqli://10.111.96.158:9701/dbs/clidem:informixserver=ifxse",
        "informix",
        "informix");
    Connection to=
        DriverManager.getConnection(
        "jdbc:oracle:thin:@sfpc1014.cr.uf-isf.es:1521:lx01",
        "legate",
        "legate");
    Tabla t1=new Tabla(from,to,"cexincid");
    t1.synchronize();
  }
  public static void main(String[] args) {
    try {
      testTabla testTabla1 = new testTabla();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

}