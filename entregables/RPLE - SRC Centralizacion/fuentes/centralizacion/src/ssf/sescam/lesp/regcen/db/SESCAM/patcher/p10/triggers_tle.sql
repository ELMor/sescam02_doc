CREATE OR REPLACE TRIGGER cexapldem01_tle
   BEFORE UPDATE OR DELETE OR INSERT
   ON CEX_APLDEM
   FOR EACH ROW
DECLARE
   exist   NUMBER;
BEGIN
   IF UPDATING OR INSERTING THEN
      SELECT COUNT (*)
        INTO EXIST
        FROM LESPCEX
       WHERE CEGA = :NEW.centro AND ncita = :NEW.ncita;

      IF exist > 0 THEN
         legase.SYNCMASTERCLAVE (:NEW.centro, 2, :NEW.ncita);
      END IF;
   ELSE
      SELECT COUNT (*)
        INTO EXIST
        FROM LESPCEX
       WHERE CEGA = :OLD.centro AND ncita = :OLD.ncita;

      IF exist > 0 THEN
         legase.SYNCMASTERCLAVE (:OLD.centro, 2, :OLD.ncita);
      END IF;
   END IF;
END;
/

CREATE OR REPLACE TRIGGER reprograma01_tle
   BEFORE UPDATE OR DELETE OR INSERT
   ON REPROGRAMA
   FOR EACH ROW
DECLARE
   exist   NUMBER;
BEGIN
   IF UPDATING OR INSERTING THEN
      SELECT COUNT (*)
        INTO EXIST
        FROM LESPCEX
       WHERE CEGA = :NEW.centro AND ncita = :NEW.ncita;

      IF exist > 0 THEN
         legase.SYNCMASTERCLAVE (:NEW.centro, 2, :NEW.ncita);
      END IF;
   ELSE
      SELECT COUNT (*)
        INTO EXIST
        FROM LESPCEX
       WHERE CEGA = :OLD.centro AND ncita = :OLD.ncita;

      IF exist > 0 THEN
         legase.SYNCMASTERCLAVE (:OLD.centro, 2, :OLD.ncita);
      END IF;
   END IF;
END;
/
