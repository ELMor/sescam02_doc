create trigger ci02
  insert on citas
  referencing new as newmov
  for each row
  (
   insert into citaslog(fecha,ncita,accion,estado)
    values(current year to second,newmov.ncita,1,0)
  );

