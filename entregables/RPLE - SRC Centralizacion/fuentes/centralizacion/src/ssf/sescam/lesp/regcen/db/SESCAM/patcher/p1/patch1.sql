Alter table LESP add FINCHOS date
/

Create or Replace Trigger LESP01_FINC
        Before Insert or Update on LESP
        For each row
Declare
  FIE Date :=to_date('2003-02-01','YYYY-MM-DD');
Begin
  If :new.FINCHOS is null then
    :new.FINCHOS:=:new.FINC;
  End If;
  If :new.FINC<FIE Then
    :new.FINC:=FIE;
  End if;
End;
/

Update LESP set finc=finc
/
commit
/

Alter table LESPCEX add FINCLUSIHOS date
/

Create or Replace Trigger LESPCEX01_FINCLUSI
        Before Insert or Update on LESPCEX
        For each row
Declare
  FIE Date :=to_date('2003-02-01','YYYY-MM-DD');
Begin
  If :new.FINCLUSIHOS is null then
    :new.FINCLUSIHOS:=:new.FINCLUSI;
  End If;
  If :new.FINCLUSI<FIE Then
    :new.FINCLUSI:=FIE;
  End if;
End;
/

Update LESPCEX set finclusi=finclusi
/

commit
/
