create trigger ci01
  update on citas
  referencing old as oldmov new as newmov
  for each row
  (
   insert into citaslog(fecha,ncita,accion,estado)
    values(current year to second,newmov.ncita,2,0)
  );

