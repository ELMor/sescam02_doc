create trigger pa01
  update on pacientes
  referencing old as oldmov new as newmov
  for each row
  (execute procedure monitorPacientes(newmov.numerohc));