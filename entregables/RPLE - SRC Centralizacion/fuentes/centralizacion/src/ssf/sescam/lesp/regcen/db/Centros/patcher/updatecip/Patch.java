package ssf.sescam.lesp.regcen.db.Centros.patcher.updatecip;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;
import java.sql.Date;

import org.apache.log4j.Logger;
import ssf.sescam.lesp.regcen.db.patcher.ClassPatcher;

/**
 * <p>T�tulo: Patch</p>
 * <p>Descripci�n: Parche para base de datos del sescam para suprimir duplicidades entre la relaci�n CEGA:HICL -> CIP</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author FRM
 * @version 1.0
 */

public class Patch
    extends ClassPatcher {

  public boolean isDoubleConnectionRequired() {
    return true;
  }

	public int verificarLESP(Hashtable params) throws Exception {
		int CEGA = ( (Integer) params.get("CEGA")).intValue();
		Connection toSescam = (Connection) params.get("toSescam");
    Connection toCentro = (Connection) params.get("toCentro");

		ResultSet rsCipActual = null;
		PreparedStatement	pstSltCipActual = toCentro.prepareStatement("select numtis from pacientes where numerohc = ?");
		PreparedStatement pstUpdLESP = toSescam.prepareStatement("update lesp set cip = ? where norden = ?");
		PreparedStatement pstLESP = toSescam.prepareStatement("select norden,cip,hicl from lesp where centro = ?");
		pstLESP.setInt(1,CEGA);
		ResultSet rsLESP = pstLESP.executeQuery();

		int n = 0;
		while (rsLESP.next()) {
			long norden = rsLESP.getLong(1);
			String cip = rsLESP.getString(2);
			long HICL = rsLESP.getLong(3);

			pstSltCipActual.setLong(1,HICL);
			rsCipActual = pstSltCipActual.executeQuery();
			pstSltCipActual.executeQuery();
			String cipActual = (rsCipActual.next())? rsCipActual.getString(1): "";
			cipActual = cipActual.trim();
			if (cipActual != null && cip != null
					&& !cipActual.equals("") && !cipActual.equals(cip)){
				pstUpdLESP.setString(1,cipActual);
				pstUpdLESP.setLong(2,norden);
				pstUpdLESP.execute();
				n++;
			}


		}


		pstLESP.close();
		rsLESP.close();
		pstSltCipActual.close();
		rsCipActual.close();
		pstUpdLESP.close();

		return n;

	}

	public int verificarLESPCEX(Hashtable params) throws Exception {
		int CEGA = ( (Integer) params.get("CEGA")).intValue();
		Connection toSescam = (Connection) params.get("toSescam");
    Connection toCentro = (Connection) params.get("toCentro");

		ResultSet rsCipActual = null;
		PreparedStatement	pstSltCipActual = toCentro.prepareStatement("select trim(numtis) from pacientes where numerohc = ?");
		PreparedStatement pstUpdLESPCEX = toSescam.prepareStatement("update lespcex set cip = ? where ncita = ?");

		PreparedStatement pstLESPCEX = toSescam.prepareStatement("select ncita,cip,hicl from lespcex where cega = ?");
		pstLESPCEX.setInt(1,CEGA);
		ResultSet rsLESPCEX = pstLESPCEX.executeQuery();

		int n = 0;
		while (rsLESPCEX.next()) {
			long ncita = rsLESPCEX.getLong(1);
			String cip = rsLESPCEX.getString(2);
			long HICL = rsLESPCEX.getLong(3);

			pstSltCipActual.setLong(1,HICL);
			rsCipActual = pstSltCipActual.executeQuery();
			pstSltCipActual.executeQuery();
			String cipActual = (rsCipActual.next())? rsCipActual.getString(1): "";
			cipActual = cipActual.trim();
			if (cipActual != null && cip != null
					&& !cipActual.equals("") && !cipActual.equals(cip)){
				pstUpdLESPCEX.setString(1,cipActual);
				pstUpdLESPCEX.setLong(2,ncita);
				pstUpdLESPCEX.execute();
				n++;
			}


		}

		pstLESPCEX.close();
		rsLESPCEX.close();
		pstSltCipActual.close();
		rsCipActual.close();
		pstUpdLESPCEX.close();

		return n;

	}

  public boolean patchDoubleConnection(Hashtable params) {
    boolean ret = true;
    Logger log = Logger.getLogger(getClass());
/*
		try {
			log.info("Comenzando a actualizar CIPS incorrectos");
			int n = 0;
			n += verificarLESP(params);
			n += verificarLESPCEX(params);
      log.info("Finalizada la actualizaci�n de CIPS incorrectos. " + n + " registros actualizados");
		}  catch (Exception e) {
      log.error("Ha fallado la actualizacion de CIP en SESCAM", e);
      ret = false;
    }
*/

		int CEGA = ( (Integer) params.get("CEGA")).intValue();
    long n = 0; // cuenta de registros afectados
    Connection toSescam = (Connection) params.get("toSescam");
    Connection toCentro = (Connection) params.get("toCentro");


    PreparedStatement pstAfectados = null;
    ResultSet rsAfectados = null;
		ResultSet rsCipActual = null;
    PreparedStatement pstUpdLESPCEX = null;
    PreparedStatement pstUpdLESP = null;
    PreparedStatement pstSltCipActual = null;
    try {
      log.info("Comenzando a actualizar CIPS incorrectos");
      //1. Detectar los pacientes afectados tanto en lesp como en lespcex

			String sql_afectados =
					" select hicl from  " +
					" (select hicl,cip from lespcex where cega = ? " +
					" union all select hicl,cip from lesp  where centro = ? ) " +
				  "	group by hicl having count(distinct cip) > 1 ";

			pstAfectados = toSescam.prepareStatement(sql_afectados);
			pstAfectados.setInt(1,CEGA);
			pstAfectados.setInt(2,CEGA);
      rsAfectados = pstAfectados.executeQuery();

			pstSltCipActual = toCentro.prepareStatement("select numtis from pacientes where numerohc = ?");


      pstUpdLESPCEX = toSescam.prepareStatement(
          "update lespcex set cip = ? where cega = ? and hicl = ? and cip <> ?");
      pstUpdLESP = toSescam.prepareStatement(
          "update lesp set cip = ? where centro = ? and hicl = ? and cip <> ?");

      //2. Para cada uno de los pacientes afectados.
      while (rsAfectados.next()) {
	      long HICL = rsAfectados.getInt(1);

        //2.2  Obtener cip actual
        pstSltCipActual.setLong(1,HICL);
        rsCipActual = pstSltCipActual.executeQuery();
        String cip = (rsCipActual.next())? rsCipActual.getString(1): "";

        if (cip != null && !cip.equals("")) {
					cip = cip.trim(); // realizar el trim porque de HP-HIS puede venir como sea
          log.info("actualizacion CEGA: " + CEGA + " HICL: " + HICL +
                   " Nuevo CIP:" + cip);
          //2.3 Actualizar la BD para dejar consistente
          // actualizacion de LESPCEX
					pstUpdLESPCEX.setString(1,cip);
					pstUpdLESPCEX.setLong(2, CEGA);
					pstUpdLESPCEX.setLong(3, HICL);
					pstUpdLESPCEX.setString(4,cip);
					n += pstUpdLESPCEX.executeUpdate();
          // actualizacion de LESP
					pstUpdLESP.setString(1,cip);
					pstUpdLESP.setLong(2, CEGA);
					pstUpdLESP.setLong(3, HICL);
					pstUpdLESP.setString(4,cip);
					n += pstUpdLESP.executeUpdate();
        } else log.info("Error al obtener el cip m�s reciente  CEGA: " + CEGA + " HICL: " + HICL);
      }
      // cerrar objetos de BD
      rsAfectados.close();
      pstAfectados.close();
      pstUpdLESPCEX.close();
      pstUpdLESP.close();


     log.info("Finalizada la actualizaci�n de CIPS incorrectos. " + n + " registros actualizados");
    }
    catch (Exception e) {
      log.error("Ha fallado la actualizacion de CIP en SESCAM", e);
      ret = false;
    }

    return ret;
  }

}