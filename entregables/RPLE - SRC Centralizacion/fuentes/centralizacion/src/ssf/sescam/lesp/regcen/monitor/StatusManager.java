package ssf.sescam.lesp.regcen.monitor;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author unascribed
 * @version 1.0
 */

public interface StatusManager {
  public void setStatus(int codigo, int status);
  public int  getStatus(int codigo);
}