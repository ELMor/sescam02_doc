create trigger ci03
  delete on citas
  referencing old as oldmov
  for each row
  (
   insert into citaslog(fecha,ncita,accion,estado)
    values(current year to second,oldmov.ncita,3,0)
  );

