create trigger le03
  update on lista_espe
  referencing old as oldmov new as newmov
  for each row
  (execute procedure
    logtolog(
       5
      ,current year to second
      ,newmov.numerohc
      ,newmov.servreal
      ,newmov.cod_medico
      ,newmov.fecha_en_lista
      ,newmov.codidiag_ingreso
      ,newmov.codiproc_ingreso
      ,newmov.prioridad
      ,newmov.servpeti
      ,newmov.medipeti
      ,newmov.aviso_urg
      ,newmov.preingresado
      ,newmov.fecha_ingreso
      ,newmov.observaciones
      ,newmov.tipfinancia
      ,newmov.financia
      ,newmov.ambito
      ,newmov.descproc
      ,newmov.descdiag
      ,newmov.tipoanest
      ,newmov.norden
      ,newmov.tipolis
      ,newmov.procedede
      ,newmov.fechaqui
      ,newmov.numproce
      ,newmov.hemoterapia
      ,newmov.complicacion
      ,newmov.inclusion
      ,newmov.codidiag2
      ,newmov.descdiag2
      ,newmov.ingresado
      ,newmov.bajalabor
      ,newmov.numpreop
      ,newmov.fecrechazo
      ,newmov.tipoacti
      ,newmov.fec_prevista
      ,newmov.hora_prevista
      ,newmov.codiproc2
      ,newmov.descproc2
      ,newmov.derivable
      ,newmov.situacion
      ,newmov.est_preop
      ,newmov.aptociru
      ,newmov.fecapto
      ,newmov.feccaduca
      ,newmov.asa
      ,newmov.localizado
      ,newmov.fecdemora
      ,newmov.tpodemora
      ,newmov.fecderiva
      ,newmov.hospderiva
      ,newmov.fecconfirma
      ,newmov.grabauser
      ,newmov.grabadia
      ,newmov.grabahora
      ,newmov.motrechazo
      ,null
    )
  );

