package ssf.sescam.lesp.regcen.db.Centros.patcher.tvisita;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import ssf.sescam.lesp.regcen.db.patcher.ClassPatcher;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author not attributable
 * @version 1.0
 */

public class Patch
    extends ClassPatcher {
  public boolean isDoubleConnectionRequired() {
    return true;
  }

  public boolean patchDoubleConnection(Hashtable params) {
    boolean ret = true;
    int CEGA = ( (Integer) params.get("CEGA")).intValue();

    Connection toSescam = (Connection) params.get("toSescam");
    Connection toCentro = (Connection) params.get("toCentro");
    Logger log = Logger.getLogger(getClass());

    try {
      String sqlLespcexSescam = "update lespcex set tvisita=? where cega=" +
          CEGA + " and ncita=?";
      PreparedStatement pstSescam = toSescam.prepareStatement(sqlLespcexSescam);

      String sqlCitas = "select ncita,tvisita " +
          "from citas where fechaprog>='2003-01-01' " +
          "and tvisita is not null " +
					"union all " +
					"select ncita,tvisita " +
          "from anulacion where fechaprog>='2003-01-01' " +
          "and tvisita is not null " +
					"union all " +
					"select ncita,tvisita " +
          "from actividad where fechaprog>='2003-01-01' " +
          "and tvisita is not null";


      Statement stCitas = toCentro.createStatement();
      ResultSet rsCitas = stCitas.executeQuery(sqlCitas);
      while (rsCitas.next()) {
        pstSescam.setString(1, rsCitas.getString(2));
        pstSescam.setInt(2, rsCitas.getInt(1));
        pstSescam.executeUpdate();
      }
			pstSescam.close();
      rsCitas.close();
			stCitas.close();
    }
    catch (Exception e) {
			log.error("Ha fallado la actualizacion de TVISITA de LESPCEX", e);
      ret = false;
    }
    return ret;
  }

}
