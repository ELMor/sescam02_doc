package ssf.sescam.lesp.regcen.db.Centros.patcher.identpres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import ssf.sescam.lesp.regcen.db.patcher.ClassPatcher;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author not attributable
 * @version 1.0
 */

public class Patch
    extends ClassPatcher {
  public boolean isDoubleConnectionRequired() {
    return true;
  }

  public boolean patchDoubleConnection(Hashtable params) {
    boolean ret = true;
    int CEGA = 0;
    Connection toSescam = null;
    Connection toCentro = null;

    CEGA = ( (Integer) params.get("CEGA")).intValue();
    toSescam = (Connection) params.get("toSescam");
    toCentro = (Connection) params.get("toCentro");
    Logger log = Logger.getLogger(getClass());
    Statement stCentro = null;
    PreparedStatement pstSescam = null;
    ResultSet rsCentro = null;
    try {
      String sql1="Select codprest,identpres from prestacion";
      stCentro=toCentro.createStatement();
      rsCentro=stCentro.executeQuery(sql1);
      String sql2="Update lespcex set identpres=? where cega="+CEGA+" and prestaci=?";
      pstSescam=toSescam.prepareStatement(sql2);
      while(rsCentro.next()){
        pstSescam.setString(1,rsCentro.getString(2).trim());
        pstSescam.setString(2,rsCentro.getString(1).trim());
        pstSescam.execute();
      }
    }
    catch (Exception e) {
      log.error("Ha fallado la actualizacion de identpres",e);
      ret = false;
    }
    return ret;
  }

}