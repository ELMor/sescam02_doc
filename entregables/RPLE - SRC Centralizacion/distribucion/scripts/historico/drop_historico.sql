-- SESCAM: Carga Historico
-- Script: drop_historico.sql
-- Eliminación del esquema del histórico.
DROP TABLE LESP_HISTO;
DROP TABLE LESPCEX_HISTO;
DROP TABLE LEHPDEMO_HISTO;
DROP TABLE CEXAPL_HISTO;
DROP PROCEDURE InithistoricoConsultas;
DROP PROCEDURE InithistoricoQuirurgica;