CREATE OR REPLACE Package legase Is
   Procedure Init_TLE;

   Procedure Refresh_TLE (vcentro Number);

   Procedure Simple (vcentro In Number, vtipo In Number, vclave In Number);

   Procedure Simple_CIP (vcip In Varchar2);

   Procedure simple_solape (
      vcentro    In   Number
     ,vtipo      In   Number
     ,claveini   In   Number
   );

   Procedure simple_continuo (
      vcentro     In   Number
     ,vtipo       In   Number
     ,vclaveini   In   Number
   );

   Function Simple_catalog (
      vcentro   In   Number
     ,vtipo     In   Number
     ,vclave    In   Number
   )
      Return Varchar2;

   Procedure SyncMasterclave (
      vcentro   In   Number
     ,vtipo     In   Number
     ,vclave    In   Number
   );

   Function Clave_Inicial (
      vcentro   In   Number
     ,vtipo     In   Number
     ,vclave    In   Number
   )
      Return Number;
	  
	  
   Function Clave_Inicial_Histo (
      vcentro   In   Number
     ,vtipo     In   Number
     ,vclave    In   Number
   )
      Return Number;

   Type tend_demor Is Table Of Varchar2 (6)
      Index By Binary_integer;
End;
/
