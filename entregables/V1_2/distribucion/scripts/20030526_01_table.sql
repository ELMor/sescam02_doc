drop table NOLESP;


CREATE TABLE NOLESP
(
  NNOLESP            NUMBER                     NOT NULL,
  NOMBRE             VARCHAR2(256),
  APELLIDO1          VARCHAR2(256),
  APELLIDO2          VARCHAR2(256),
  CIP                VARCHAR2(64),
  DOMICILIO          VARCHAR2(256),
  FECHA_NAC          DATE,
  POBLACION          VARCHAR2(128),
  PROVINCIA          VARCHAR2(128),
  NHC                NUMBER,
  CENTRO             NUMBER,
  PRESTACION         VARCHAR2(256),
  TELEFONO           VARCHAR2(32),
  NSS                VARCHAR2(30),
  GAR_FECHOR         DATE                       DEFAULT sysdate,
  GAR_REPLEG_DNI     VARCHAR2(15),
  GAR_REPLEG_APENOM  VARCHAR2(256),
  GAR_OFICINA        NUMBER,
  SYS_USU            VARCHAR2(8),
  GAR_MOT_RECHAZO    VARCHAR2(4000)
)
TABLESPACE USERS
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          66K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE UNIQUE INDEX PK_NOLESP ON NOLESP
(NNOLESP)
LOGGING
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          66K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE NOLESP ADD (
  CONSTRAINT PK_NOLESP PRIMARY KEY (NNOLESP)
    USING INDEX 
    TABLESPACE USERS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          66K
                MINEXTENTS       1
                MAXEXTENTS       2147483645
                PCTINCREASE      0
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE NOLESP ADD (
  UNIQUE (NNOLESP));

commit;
