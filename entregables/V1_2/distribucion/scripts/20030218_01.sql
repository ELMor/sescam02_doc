-- ============================================================
--   Database name:  sescam                                    
--   DBMS name:      ORACLE Version 8                          
--   Created on:     18/02/2003  11:53                         
-- ============================================================

alter table ses_proc_map
    drop constraint FK_SES_PROC_RELATION__SES_PROC
/

create table ses_proc_tipos
(
    proc_tipo            INTEGER                not null,
    proc_tcod            VARCHAR2(10)           null    ,
    proc_desc            CHAR(10)               null    ,
    constraint PK_SES_PROC_TIPOS primary key (proc_tipo)
)
/

alter table ses_srv_map
    drop primary key
/

alter table ses_srv_map
    add constraint PK_SES_SRV_MAP primary key (centro, srv_cen_cod)
/

alter table ses_proc_map
    drop primary key
/

alter table ses_proc_map
    add     proc_tipo            INTEGER                null    
/

alter table ses_proc_map
    add     proc_cen_tipo        VARCHAR2(4)            null    
/

alter table ses_proc_map
    add constraint PK_SES_PROC_MAP primary key (centro, proc_cen_cod)
/

alter table ses_diag_map
    drop primary key
/

alter table ses_diag_map
    add constraint PK_SES_DIAG_MAP primary key (centro, diag_cen_cod)
/

create index Relation_4263_FK on ses_procedimientos (proc_tipo asc)
/

create index Relation_4264_FK on ses_proc_map (proc_tipo asc)
/

alter table ses_procedimientos
    add constraint FK_SES_P__SES_P foreign key  (proc_tipo)
       references ses_proc_tipos (proc_tipo)
/

alter table ses_proc_map
    add constraint FK_SES_PROC_SES_PROC foreign key  (proc_pk)
       references ses_procedimientos (proc_pk)
/

alter table ses_proc_map
    add constraint FK_SES_PROC_RELATION__SES_PROC foreign key  (proc_tipo)
       references ses_proc_tipos (proc_tipo)
/

commit
/

