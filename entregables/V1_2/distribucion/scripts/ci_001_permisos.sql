--Inicialización de la base de permisos

--Inicializa OFICINA CENTRAL
insert into ses_oficina (oficina_pk,oficina_nombre) values (1,'SESCAM Central')
/

--Inicializa ROL ADMINISTRADOR
insert into sys_roles (sys_rol_pk,sys_rol_nombre,sys_rol_desc)
values (1,'Administrador','Administradores del sistema')
/

--Inicializa CARGO RESPONSABLE
insert into sys_cargos (sys_cargos_pk,sys_cargos_nombre) values (1,'Responsable')
/

--Inicializa USUARIO ADMINISTRADOR
insert into sys_usu (syslogin,sys_rol_pk,apellido1,apellido2,nombre,activo_sn,sys_cargos_pk,oficina_pk)
values ('admin',1,'Administrador','Administrador','Administrador',1,1,1)
/
insert into sys_usu_claves
(syslogin,sysclv_pk,sysclv_clave)
values('admin',1, 'D*L+I E.B(K&')
/



-- Oficina al usuario
update ses_oficina set syslogin='admin' where oficina_pk=1
/

--Inicializa PERMISOS DEL SISTEMA
insert into sys_permisos (sys_perm_pk,sys_perm_nombre,sys_perm_desc)
values (1,'acceso_LEQ','Acceso a la aplicación de lista de espera Quirurgica')
/
insert into sys_permisos (sys_perm_pk,sys_perm_nombre,sys_perm_desc)
values (2,'acceso_LECEX','Acceso a la aplicación de lista de espera CEX')
/
insert into sys_permisos (sys_perm_pk,sys_perm_nombre,sys_perm_desc)
values (3,'acceso_Mant','Acceso al modulo de mantenimiento')
/
insert into sys_permisos (sys_perm_pk,sys_perm_nombre,sys_perm_desc)
values (4,'emision_SOL','Emision de solicitudes')
/
insert into sys_permisos (sys_perm_pk,sys_perm_nombre,sys_perm_desc)
values (5,'aprob_deneg_SOL','Aprobacion y Denegacion de solicitudes')
/
insert into sys_permisos (sys_perm_pk,sys_perm_nombre,sys_perm_desc)
values (6,'acceso_Estad','Acceso al modulo de estadisticas')
/

-- Inicializa PERMISOS DE ADMINISTRADOR
insert into sys_perm_rol (sys_rol_pk,sys_perm_pk) select 1,sys_perm_pk from sys_permisos
/

--Inicializa PROVINCIAS
insert into ses_provincias(provincia_pk,provincia) values(1,'Albacete')
/
insert into ses_provincias(provincia_pk,provincia) values(2,'Ciudad Real')
/
insert into ses_provincias(provincia_pk,provincia) values(3,'Cuenca')
/
insert into ses_provincias(provincia_pk,provincia) values(4,'Guadalajara')
/
insert into ses_provincias(provincia_pk,provincia) values(5,'Toledo')
/


commit work
/
