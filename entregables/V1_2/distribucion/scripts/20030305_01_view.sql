CREATE OR REPLACE VIEW MAPEO_PROCS ( COD_SESCAM, 
DESCRIP, COD_MAPEO, CODTIPO, TIPO, CODTIPOMAPEO, CENTRO, PKTIPOMAPEO )
AS SELECT distinct sp.PROC_COD cod_sescam, sp.PROC_DESC_CORTA descrip, smp.PROC_CEN_COD cod_mapeo,sptipo.PROC_TCOD codtipo, sp.proc_tipo tipo,smp.PROC_CEN_TIPO,smp.CENTRO centro,sptipo2.PROC_TIPO pktipomapeo 
FROM ses_proc_map smp,ses_procedimientos sp, ses_proc_tipos sptipo, ses_proc_tipos sptipo2 
WHERE smp.PROC_PK = sp.PROC_PK and sp.PROC_TIPO = sptipo.PROC_TIPO and smp.PROC_CEN_TIPO=sptipo2.PROC_TCOD