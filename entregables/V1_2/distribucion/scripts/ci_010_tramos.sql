
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (1, 1, NULL, 90, 'De 0 a 90 d�as');
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (1, 2, 91, 180, 'Entre 91 y 180');
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (1, 3, 181, NULL,'Mayor a 180');

INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (2, 1, NULL, 15, 'De 0 a 15 d�as');
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (2, 2, 16, 25, 'Entre 16 y 25');
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (2, 3, 26, 40,'Entre 26 y 40');
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (2, 4, 41, 60,'Entre 41 y 60');
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (2, 5, 61, NULL,'Mayor a 60');

INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (3, 1, NULL, 7, 'De 0 a 7 d�as');
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (3, 2, 8, 15, 'Entre 8 y 15');
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (3, 3, 16, 20,'Entre 16 y 20');
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (3, 4, 21, 30,'Entre 21 y 30');
INSERT INTO ses_tramos ( CODTRAMO, POSICION, INICIO, FIN, DESCRIPCION ) VALUES (3, 5, 31, NULL,'Mayor a 30');


COMMIT;
