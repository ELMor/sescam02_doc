DELETE FROM CENTROSESCAM;

ALTER TABLE CENTROSESCAM
  modify (descentro VARCHAR2(50));
  
ALTER TABLE CENTROSESCAM
  add (descentrolarga VARCHAR2(100));

insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (1313,'C.H. LA MANCHA CENTRO','Complejo Hospitalario La Mancha Centro');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (1901,'H.U. DE GUADALAJARA','Hospital General Universitario de Guadalajara');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (130071,'X.PUERTOLLANO','Hospital de Santa B�rbara');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (4503,'H.N. SE�ORA DEL PRADO','Hospital Nuestra Se�ora del Prado');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (4501,'C.H. DE TOLEDO','Complejo Hospitalario de Toledo');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (1601,'H. G. VIRGEN DE LA LUZ','Hospital Virgen de la Luz');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (1301,'C. H. DE CIUDAD REAL','Complejo Hospitalario Ciudad Real');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (201,'C. H. G. DE ALBACETE','Complejo Hospitalario General de Albacete');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (207,'H. DE HELL�N','Hospital de Hell�n');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (1303,'H. SANTA B�RBARA','Hospital Santa B�rbara');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (1307,'H. GUTI�RREZ ORTEGA','Hospital Gutierrez Ortega');
insert into CENTROSESCAM (codcentro,descentro,descentrolarga) values (4509,'H. N. DE PARAPL�JICOS','Hospital Nacional de Parapl�jicos');

commit;
