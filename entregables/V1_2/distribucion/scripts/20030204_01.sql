-- ============================================================
--   Database name:  sescam                                    
--   DBMS name:      ORACLE Version 8                          
--   Created on:     04/02/2003  09:22                         
-- ============================================================

create table ses_tramos
(
    codtramo             INTEGER                not null,
    posicion             INTEGER                not null,
    inicio               NUMBER                 null    ,
    fin                  NUMBER                 null    ,
    descripcion          VARCHAR2(100)          null    ,
    constraint PK_SES_TRAMOS primary key (codtramo, posicion)
)
/

alter table ses_clasificaciones
    add     sesclas_tipo         INTEGER                null    
/

commit
/

